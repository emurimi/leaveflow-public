﻿Imports Microsoft.VisualBasic
Imports lvManLib.DataFunctions
Imports lvManLib.Config
Imports System.Data.SqlClient
Imports lvManLib.General

Public Class Logic
    Public Shared Function LeaveAllowance(ByVal intusrId As Integer, ByVal intltyId As Integer, ByVal dblDays As Double, ByVal datStartDate As Date) As String
        Dim strSql As String = ""
        Dim objDR As SqlDataReader
        Dim dblAllowance As Double
        Dim strEmailAdd As String = ""
        Dim strCurrency As String = ""
        Dim intcurId As Integer
        Dim strAllowance As String = ""
        Dim bolHasAllowance As Boolean = False
        Dim strMailMessage As String
        Dim strMailTitle As String
        Dim intcomId As Integer
        Dim strFullName As String = ""
        Dim parameters As New List(Of SqlParameter)()
        Dim usrId As New SqlParameter("@usrId", intusrId)
        parameters.Add(usrId)
        Dim LeaveDays As New SqlParameter("@LeaveDays", dblDays)
        parameters.Add(LeaveDays)
        Dim ltyId As New SqlParameter("@ltyId", intltyId)
        parameters.Add(ltyId)

        'Get User Details
        objDR = GetSpDataReader("stpGetUserLeaveAllowance", parameters.ToArray())

        If objDR.Read Then
            dblAllowance = objDR("lasAmount")
            strCurrency = objDR("curName") & ""
            intcurId = objDR("lascurId")
            strAllowance = objDR("latName") & ""
            intcomId = objDR("comId")
            strFullName = objDR("usrFullName")
            strEmailAdd = objDR("lasAlertEmailAdd") & ""
            bolHasAllowance = True
        End If

        objDR.Close()
        objDR = Nothing

        If Not bolHasAllowance Then
            Return "NONE"
        End If

        parameters.Clear()
        Dim lasusrId As New SqlParameter("@lasusrId", intusrId)
        parameters.Add(lasusrId)
        Dim lasDescription As New SqlParameter("@lasDescription", strAllowance)
        parameters.Add(lasDescription)
        Dim lasAmount As New SqlParameter("@lasAmount", dblAllowance)
        parameters.Add(lasAmount)
        Dim lascurId As New SqlParameter("@lascurId", intcurId)
        parameters.Add(lascurId)
        Dim lasLeaveStartDate As New SqlParameter("@lasLeaveStartDate", datStartDate)
        parameters.Add(lasLeaveStartDate)
        Dim laspasId As New SqlParameter("@laspasId", 1)
        parameters.Add(laspasId)

        ExecuteStoredProc("stpLeaveAllowanceSchedule_Add", parameters.ToArray())

        If Len(strEmailAdd) > 2 Then
            strMailMessage = GetMailTemplate("LANA", intcomId, "[NAME]|[STARTDATE]|[ALLOWANCE]", strFullName & "|" & FormatDateTime(datStartDate, DateFormat.LongDate) & "|" & strAllowance & " of " & FormatNumber(dblAllowance, 2) & " " & strCurrency)
            strMailTitle = Mid(strMailMessage, 1, InStr(strMailMessage, "|}*") - 1)
            strMailMessage = Mid(strMailMessage, InStr(strMailMessage, "|}*") + 3)
            SendMail(strMailTitle, strMailMessage, strEmailAdd)
        End If

        Return strAllowance & " of " & FormatNumber(dblAllowance, 2) & " " & strCurrency
    End Function

    Public Shared Function CheckHRApprover(ByVal intLRQ As Integer, ByVal strApprovalType As String) As Boolean
        Dim strSql As String = ""
        Dim strNextApprover As String
        Dim parameters As New List(Of SqlParameter)()
        Dim lrqId As New SqlParameter("@lrqId", intLRQ)
        parameters.Add(lrqId)

        Select Case strApprovalType
            Case "leave"
                Dim Category As New SqlParameter("@Category", "LR")
                parameters.Add(Category)

            Case "plan"
                Dim Category As New SqlParameter("@Category", "LP")
                parameters.Add(Category)

            Case "compensation"
                Dim Category As New SqlParameter("@Category", "LC")
                parameters.Add(Category)

        End Select

        strNextApprover = GetSpDataSingleValue("stpCheckHRApprover", parameters.ToArray())

        If strNextApprover = System.Web.HttpContext.Current.Session("usrId") Then
            Return False
        Else
            Return True
        End If
    End Function

    Shared Sub CreateAlert(ByVal strTitle As String, ByVal strText As String, ByVal intDuration As Integer, ByVal intShowDays As Integer, ByVal intusrId As Integer, Optional ByVal intLRQId As Integer = 0)
        Dim datDate As Date = DateSerial(Year(Now()), Month(Now()), Day(Now()))
        Dim datNextDate As Date = DateAdd(DateInterval.Day, intDuration, datDate)
        Dim parameters As New List(Of SqlParameter)()
        Dim aleTitle As New SqlParameter("@aleTitle", strTitle)
        parameters.Add(aleTitle)
        Dim aleText As New SqlParameter("@aleText", strText)
        parameters.Add(aleText)
        Dim aleDate As New SqlParameter("@aleDate", datDate)
        parameters.Add(aleDate)
        Dim aleusrId As New SqlParameter("@aleusrId", intusrId)
        parameters.Add(aleusrId)

        ExecuteStoredProc("stpAlerts_Add", parameters.ToArray())
    End Sub

    Shared Sub CreateLatestUpdate(ByVal strTitle As String, ByVal strText As String, ByVal intDuration As Integer, ByVal intcomId As Integer, Optional ByVal intbraId As Integer = 0, Optional ByVal intdptId As Integer = 0, Optional ByVal intsecId As Integer = 0)
        Dim datDate As Date = DateSerial(Year(Now()), Month(Now()), Day(Now()))
        Dim datNextDate As Date = DateAdd(DateInterval.Day, intDuration, datDate)
        Dim parameters As New List(Of SqlParameter)()

        Dim lupTitle As New SqlParameter("@lupTitle", strTitle)
        parameters.Add(lupTitle)
        Dim lupText As New SqlParameter("@lupText", strText)
        parameters.Add(lupText)
        Dim lupStartDate As New SqlParameter("@lupStartDate", datDate)
        parameters.Add(lupStartDate)
        Dim lupEndDate As New SqlParameter("@lupEndDate", datNextDate)
        parameters.Add(lupEndDate)
        Dim lupsecId As New SqlParameter("@lupsecId", intsecId)
        parameters.Add(lupsecId)
        Dim lupdptId As New SqlParameter("@lupdptId", intdptId)
        parameters.Add(lupdptId)
        Dim lupcomId As New SqlParameter("@lupcomId", intcomId)
        parameters.Add(lupcomId)
        Dim lupbraId As New SqlParameter("@lupbraId", intbraId)
        parameters.Add(lupbraId)
        Dim lupActive As New SqlParameter("@lupActive", True)
        parameters.Add(lupActive)

        ExecuteStoredProc("stpLatestUpdates_Add", parameters.ToArray())
    End Sub

    Public Shared Function GlobalLeaveDaysManifest() As Boolean
        'Loop through users and call updateleavedaysmanifest
        Dim objDR As SqlDataReader

        objDR = GetSpDataReader("stpEmployees")

        While objDR.Read()
            UpdateLeaveDaysManifest(objDR(0))
        End While

        objDR.Close()
        objDR = Nothing

        Return True
    End Function


    Public Shared Function UpdateLeaveDaysManifest(ByVal intusrId As Integer, Optional ByVal strFrequency As String = "") As String
        Dim objDR As SqlDataReader
        Dim datLastAccrualDate As Date
        Dim datNextAccrualDate As Date
        Dim dblCurrentUnits As Double
        Dim dblAddUnits As Double
        Dim dblEntitlement As Double
        Dim intAccrualUnits As Integer
        Dim strStatus As String
        Dim parameters As New List(Of SqlParameter)()

        'Check user status
        Dim usrId As New SqlParameter("@usrId", intusrId)
        parameters.Add(usrId)
        strStatus = GetSpDataSingleValue("stpUsers_GetStatus", parameters.ToArray())
        If strStatus <> "A" Then Return "NOTACTV"

        'If frequency value blank, check rules for frequency of updating days
        If Len(strFrequency) = 0 Then
            strFrequency = GetAccrualFrequecy(GetUserCompany(intusrId))
            If Len(strFrequency) = 0 Then Return "NOFREQ"
        End If

        'Check if due for an update. if not, exit plus current allocation and last accrual date (in loop if not individual)
        parameters.Clear()
        Dim usrId2 As New SqlParameter("@usrId", intusrId)
        parameters.Add(usrId2)

        objDR = GetSpDataReader("stpUser_LastAccrualUnits", parameters.ToArray())

        If Not objDR.HasRows Then
            objDR.Close()
            objDR = Nothing
            Return "NOLDM"
        End If

        objDR.Read()
        datLastAccrualDate = objDR("ldmLastAccrualDate")
        dblCurrentUnits = objDR("ldmUnits")
        objDR.Close()

        datNextAccrualDate = GetNextAccrualDate(datLastAccrualDate, strFrequency)

        If datNextAccrualDate > datLastAccrualDate Then
            Return "NOTYET"
        End If

        'check for valid grade entitlement (tbl) per accrual period(tblcompanies)
        dblEntitlement = GetGradeEntitlement(intusrId)

        'if valid entitlement found, continue. if not, put error in log & exit
        If dblEntitlement = 0 Then Return "NOENT"

        'check accrual periods that have passed since last update
        intAccrualUnits = GetAccrualUnits(datLastAccrualDate, strFrequency)

        'update valid accrual units and update log and manifest
        dblAddUnits = intAccrualUnits * dblEntitlement

        EffectLeaveDayChanges(intusrId, dblAddUnits, "A", "Leave Accrual", dblCurrentUnits)

        Return "UPD"

    End Function

    Public Shared Function EffectLeaveDayChanges(ByVal intusrId As Integer, ByVal dblUnits As Double, ByVal strAction As String, ByVal strDescription As String, Optional ByVal strCurrentUnits As String = "CHK") As Boolean
        Dim dblCurrentUnits As Double
        Dim dblNewUnits As Double
        Dim strActionBy As String
        Dim parameters As New List(Of SqlParameter)()

        If strCurrentUnits = "CHK" Then
            Dim usrId As New SqlParameter("@usrId", intusrId)
            parameters.Add(usrId)

            strCurrentUnits = GetSpDataSingleValue("stpUser_LastAccrualUnits", parameters.ToArray())
            If Len(strCurrentUnits) = 0 Then
                strCurrentUnits = "0"
                Dim ldmusrId As New SqlParameter("@ldmusrId", intusrId)
                parameters.Add(ldmusrId)
                Dim ldmUnits As New SqlParameter("@ldmUnits", 0)
                parameters.Add(ldmUnits)

                ExecuteStoredProc("stpLeaveDaysManifest_Add", parameters.ToArray())
            End If

        End If

        dblCurrentUnits = CType(strCurrentUnits, Double)
        strActionBy = System.Web.HttpContext.Current.Session("usrFullName")
        If strDescription = "Leave Accrual" Then strActionBy = "System"

        Select Case strAction
            Case "A" 'Add
                dblNewUnits = dblCurrentUnits + dblUnits
            Case "D", "S" 'Deduct
                dblNewUnits = dblCurrentUnits - dblUnits
        End Select

        parameters.Clear()
        Dim ldmusrId2 As New SqlParameter("@ldmusrId", intusrId)
        parameters.Add(ldmusrId2)
        Dim ldmUnits2 As New SqlParameter("@ldmUnits", dblNewUnits)
        parameters.Add(ldmUnits2)
        ExecuteStoredProc("stpLeaveDaysManifest_Update", parameters.ToArray())

        parameters.Clear()
        Dim ldlusrId As New SqlParameter("@ldlusrId", intusrId)
        parameters.Add(ldlusrId)
        Dim ldlDescription As New SqlParameter("@ldlDescription", strDescription)
        parameters.Add(ldlDescription)
        Dim ldlUnits As New SqlParameter("@ldlUnits", dblUnits)
        parameters.Add(ldlUnits)
        Dim ldlBalance As New SqlParameter("@ldlBalance", dblNewUnits)
        parameters.Add(ldlBalance)
        Dim ldlActionBy As New SqlParameter("@ldlActionBy", strActionBy)
        parameters.Add(ldlActionBy)
        ExecuteStoredProc("stpLeaveDaysLog_Add", parameters.ToArray())

        Return True
    End Function

    Public Shared Function GetGradeEntitlement(ByVal intusrId As Integer) As Double
        Dim strTemp As String
        Dim dblEntitlement As Double
        Dim intgraId As Integer
        Dim datEmploymentDate As Date
        Dim dblEmploymentMonths As Double
        Dim objDR As SqlDataReader
        Dim parameters As New List(Of SqlParameter)()

        Dim usrId As New SqlParameter("@usrId", intusrId)
        parameters.Add(usrId)

        objDR = GetSpDataReader("stpUserDetails", parameters.ToArray())

        objDR.Read()
        intgraId = objDR("usrgraId")
        datEmploymentDate = FormatDateTime(objDR("usrDateEmployed"), DateFormat.ShortDate)
        dblEmploymentMonths = DateDiff(DateInterval.Month, datEmploymentDate, Now())
        objDR.Close()

        parameters.Clear()
        Dim graId As New SqlParameter("@graId", intgraId)
        parameters.Add(graId)
        Dim graMinEmploymentMonths As New SqlParameter("@graMinEmploymentMonths", dblEmploymentMonths)
        parameters.Add(graMinEmploymentMonths)

        strTemp = GetSpDataSingleValue("stpGetGradeEntitlements", parameters.ToArray())
        If Len(strTemp) = 0 Then strTemp = "0"

        dblEntitlement = CType(strTemp, Double)
        Return dblEntitlement

    End Function

    Public Shared Function GetAccrualUnits(ByVal datDate As Date, ByVal strFrequency As String) As Integer
        Dim intUnits As Integer

        Select Case strFrequency
            Case "D" 'Daily
                intUnits = DateDiff(DateInterval.Day, datDate, Now())

            Case "W" 'Weekly
                intUnits = DateDiff(DateInterval.WeekOfYear, datDate, Now())

            Case "M" 'Monthly
                intUnits = DateDiff(DateInterval.Month, datDate, Now())

            Case "Y" 'Yearly
                intUnits = DateDiff(DateInterval.Year, datDate, Now())

        End Select

        Return intUnits
    End Function

    Public Shared Function GetNextAccrualDate(ByVal datDate As Date, ByVal strFrequency As String) As Date
        Select Case strFrequency
            Case "D" 'Daily
                datDate = DateAdd(DateInterval.Day, 1, datDate)
            Case "W" 'Weekly
                datDate = DateAdd(DateInterval.WeekOfYear, 1, datDate)
            Case "M" 'Monthly
                datDate = DateAdd(DateInterval.Month, 1, datDate)
            Case "Y" 'Yearly
                datDate = DateAdd(DateInterval.Year, 1, datDate)
        End Select
        Return datDate
    End Function

    Public Shared Function GetAccrualFrequecy(ByVal intCompanyID As Integer) As String
        Dim strAccrualPeriod As String
        Dim parameters As New List(Of SqlParameter)()
        Dim comId As New SqlParameter("@comId", intCompanyID)
        parameters.Add(comId)

        strAccrualPeriod = GetSpDataSingleValue("stpGetAccrualFrequency", parameters.ToArray())

        Return strAccrualPeriod
    End Function

    Public Shared Function GetUserCompany(ByVal intusrId As Integer) As Integer
        Dim strTemp As String
        Dim parameters As New List(Of SqlParameter)()
        Dim usrId As New SqlParameter("@usrId", intusrId)
        parameters.Add(usrId)

        strTemp = GetSpDataSingleValue("stpGetUserCompany", parameters.ToArray())

        If Len(strTemp) = 0 Then strTemp = "0"

        Return CType(strTemp, Integer)
    End Function

End Class
