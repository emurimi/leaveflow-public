﻿Imports Microsoft.VisualBasic
Imports lvManLib.DataFunctions
Imports System.Data.SqlClient
Imports System.Web
Imports System.Configuration

Public Class Config
    Public Shared Function GetConfig(ByVal strConfigItem As String, Optional ByVal bolForceRefresh As Boolean = False) As String
        Dim strValue As String

        strValue = System.Web.HttpContext.Current.Application(strConfigItem) & ""

        If Len(strValue) = 0 Or bolForceRefresh Then
            strValue = System.Configuration.ConfigurationManager.AppSettings(strConfigItem)
            HttpContext.Current.Application(strConfigItem) = strValue
        End If

        Return strValue
    End Function
End Class
