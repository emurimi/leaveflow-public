﻿Imports Microsoft.VisualBasic
Imports System.Security.Cryptography
Imports System.Data.SqlClient
Imports lvManLib.DataFunctions
Imports lvManLib.Menus
Imports lvManLib.Config

Public Class Security
    Public Shared Function EncryptPassword(ByVal strPassword As String, ByVal strUserName As String) As String
        Dim dblNumbers As Double
        Dim strLetterX As String
        Dim intLetterx As Integer
        Dim intCountX As Integer

        strPassword = StrReverse(strPassword)
        dblNumbers = 237

        For intCountX = 1 To Len(strPassword)
            strLetterX = Mid(strPassword, intCountX, 1)
            intLetterx = Asc(strLetterX)
            dblNumbers = dblNumbers + intLetterx + (3 * Len(strUserName))
            dblNumbers = dblNumbers / 0.8339
            dblNumbers = dblNumbers - (Len(strUserName) + Asc(Mid(strUserName, 1, 1)))
        Next

        dblNumbers = dblNumbers * 2.337
        dblNumbers += Asc(Mid(strUserName, 1, 1))

        Return CType(dblNumbers, String)

    End Function

    Public Shared Function ValidateUser(ByVal strUserName As String, ByVal strPassword As String, Optional ByVal bolAbandon As Boolean = True, Optional ByVal strLoginType As String = "U") As Boolean
        Dim objDR As SqlDataReader
        Dim bolValid As Boolean
        Dim strDBPassword As String
        Dim strPassword_nonenc As String
        Dim objCookie As System.Web.HttpCookie
        Dim datExpiry As Date
        Dim strEmailAdd As String
        Dim strusrId As String = ""
        Dim strTemp As String
        Dim strSecId As String = ""
        Dim bolChangePassword As Boolean
        Dim strMenu_SysAdmin As String = "N"
        Dim strMenu_HRAdmin As String = "N"
        Dim strMenu_Reports As String = "N"
        Dim strMenu_Approvals As String = "N"
        Dim intPasswordExpiryDays As String = 0
        Dim datPasswordDate As Date = Now()
        Dim parameters As New List(Of SqlParameter)()
        Dim usrName As New SqlParameter("@usrName", strUserName)
        parameters.Add(usrName)

        datExpiry = DateAdd(DateInterval.Minute, 30, Now())
        strUserName = LCase(Replace(strUserName, "'", ""))

        objDR = GetSpDataReader("stpUserLogin", parameters.ToArray())

        bolValid = False
        If objDR.Read() Then
            strDBPassword = objDR("usrPassword") & ""
            strUserName = objDR("usrName") & ""
            strEmailAdd = objDR("usrEmailAdd") & ""

            strPassword_nonenc = strPassword
            strPassword = EncryptPassword(strPassword, LCase(strUserName))

            If strPassword = strDBPassword Or strPassword_nonenc = strDBPassword Then
                bolValid = True
                bolChangePassword = objDR("usrChangePassword")
                strTemp = GetConfig("PasswordExpiry")
                If Len(strTemp) = 0 Then strTemp = "0"
                If Not IsNumeric(strTemp) Then strTemp = "0"
                intPasswordExpiryDays = CType(strTemp, Integer)

                strusrId = objDR("usrId")
                strTemp = objDR("usrPasswordDate") & ""
                If Len(strTemp) = 0 Then strTemp = objDR("usrDateAdded") & ""
                If Len(strTemp) > 0 Then datPasswordDate = CType(strTemp, Date)

                System.Web.HttpContext.Current.Session("usrName") = strUserName
                System.Web.HttpContext.Current.Session("usrFullName") = objDR("usrFullName")
                System.Web.HttpContext.Current.Session("usrId") = strusrId
                System.Web.HttpContext.Current.Session("usrSex") = objDR("usrSex")
                System.Web.HttpContext.Current.Session("usrEmailAdd") = objDR("usrEmailAdd")
                System.Web.HttpContext.Current.Session("dptId") = objDR("usrdptId")
                System.Web.HttpContext.Current.Session("comId") = objDR("comId")
                System.Web.HttpContext.Current.Session("ctyId") = objDR("ctyId")
                strSecId = objDR("usrsecId") & ""
                If Len(strSecId) = 0 Then strSecId = "0"
                System.Web.HttpContext.Current.Session("secId") = strSecId
                System.Web.HttpContext.Current.Session("comLeaveUnit") = objDR("comLeaveUnit")
                System.Web.HttpContext.Current.Session("usrIsSystemAccount") = objDR("usrIsSystemAccount")

                'Check Sys Admin Menu Rights
                parameters.Clear()
                Dim usrId1 As New SqlParameter("@usrId", strusrId)
                Dim MenuGroup1 As New SqlParameter("@MenuGroup", "SYS")
                parameters.Add(usrId1)
                parameters.Add(MenuGroup1)

                strTemp = GetSpDataSingleValue("stpUserKeyMenuRights", parameters.ToArray())
                If Len(strTemp) > 0 Then
                    If CType(strTemp, Integer) > 0 Then strMenu_SysAdmin = "Y"
                End If

                'Check HR Admin Menu Rights
                parameters.Clear()
                Dim usrId2 As New SqlParameter("@usrId", strusrId)
                Dim MenuGroup2 As New SqlParameter("@MenuGroup", "HR")
                parameters.Add(usrId2)
                parameters.Add(MenuGroup2)

                strTemp = GetSpDataSingleValue("stpUserKeyMenuRights", parameters.ToArray())
                If Len(strTemp) > 0 Then
                    If CType(strTemp, Integer) > 0 Then strMenu_HRAdmin = "Y"
                End If

                'Check Reports Menu Rights
                parameters.Clear()
                Dim usrId3 As New SqlParameter("@usrId", strusrId)
                Dim MenuGroup3 As New SqlParameter("@MenuGroup", "RPT")
                parameters.Add(usrId3)
                parameters.Add(MenuGroup3)

                strTemp = GetSpDataSingleValue("stpUserKeyMenuRights", parameters.ToArray())
                If Len(strTemp) > 0 Then
                    If CType(strTemp, Integer) > 0 Then strMenu_Reports = "Y"
                End If

                'Check Approvals Menu Rights
                parameters.Clear()
                Dim usrId4 As New SqlParameter("@usrId", strusrId)
                Dim MenuGroup4 As New SqlParameter("@MenuGroup", "APR")
                parameters.Add(usrId4)
                parameters.Add(MenuGroup4)

                strTemp = GetSpDataSingleValue("stpUserKeyMenuRights", parameters.ToArray())
                If Len(strTemp) > 0 Then
                    If CType(strTemp, Integer) > 0 Then strMenu_Approvals = "Y"
                End If

                If strMenu_Reports = "N" Then
                    'Check if HR User
                    parameters.Clear()
                    Dim usrId5 As New SqlParameter("@usrId", strusrId)
                    Dim MenuGroup5 As New SqlParameter("@MenuGroup", "HRU")
                    parameters.Add(usrId5)
                    parameters.Add(MenuGroup5)

                    strTemp = GetSpDataSingleValue("stpUserKeyMenuRights", parameters.ToArray())
                    If Len(strTemp) > 0 Then
                        If CType(strTemp, Integer) > 0 Then strMenu_HRAdmin = "Y"
                    End If
                End If

                System.Web.HttpContext.Current.Session("Menu_SysAdmin") = strMenu_SysAdmin
                System.Web.HttpContext.Current.Session("Menu_HRAdmin") = strMenu_HRAdmin
                System.Web.HttpContext.Current.Session("Menu_Reports") = strMenu_Reports
                System.Web.HttpContext.Current.Session("Menu_Approvals") = strMenu_Approvals

                objCookie = New System.Web.HttpCookie("smartLv")
                objCookie.Expires = datExpiry

                objCookie.Values.Add("Menu_SysAdmin", System.Web.HttpContext.Current.Session("Menu_SysAdmin"))
                objCookie.Values.Add("Menu_HRAdmin", System.Web.HttpContext.Current.Session("Menu_HRAdmin"))
                objCookie.Values.Add("Menu_Reports", System.Web.HttpContext.Current.Session("Menu_Reports"))
                objCookie.Values.Add("Menu_Approvals", System.Web.HttpContext.Current.Session("Menu_Approvals"))
                objCookie.Values.Add("usrId", System.Web.HttpContext.Current.Session("usrId"))
                objCookie.Values.Add("usrSex", System.Web.HttpContext.Current.Session("usrSex"))
                objCookie.Values.Add("usrName", System.Web.HttpContext.Current.Session("usrName"))
                objCookie.Values.Add("usrFullName", System.Web.HttpContext.Current.Session("usrFullName"))
                objCookie.Values.Add("dptId", System.Web.HttpContext.Current.Session("dptId"))
                objCookie.Values.Add("comId", System.Web.HttpContext.Current.Session("comId"))
                objCookie.Values.Add("ctyId", System.Web.HttpContext.Current.Session("ctyId"))
                objCookie.Values.Add("secId", System.Web.HttpContext.Current.Session("secId"))
                objCookie.Values.Add("usrIsSystemAccount", System.Web.HttpContext.Current.Session("usrIsSystemAccount"))
                objCookie.Values.Add("usrEmailAdd", System.Web.HttpContext.Current.Session("usrEmailAdd"))
                objCookie.Values.Add("comLeaveUnit", System.Web.HttpContext.Current.Session("comLeaveUnit"))
                System.Web.HttpContext.Current.Response.Cookies.Add(objCookie)
            End If
        End If

        objDR.Close()
        objDR = Nothing

        If Not bolValid And bolAbandon Then System.Web.HttpContext.Current.Session.Abandon() 'Means that it's a login attempt

        If bolValid And Not bolChangePassword And intPasswordExpiryDays > 0 Then
            If DateDiff(DateInterval.Day, datPasswordDate, Now()) >= intPasswordExpiryDays Then bolChangePassword = True
        End If

        parameters.Clear()
        Dim usrId6 As New SqlParameter("@usrId", strusrId)
        parameters.Add(usrId6)

        'Check if leave days manifest has been initialized for this user
        ExecuteStoredProc("stpCheckLeaveDaysManifest", parameters.ToArray())

        If bolValid And bolChangePassword Then System.Web.HttpContext.Current.Response.Redirect("changepassword.aspx?erm=chng")

        Return bolValid
    End Function

    Public Shared Function GetMenuPermissions(ByVal intMenuId As Integer, Optional ByVal strMenuName As String = "") As Boolean
        Dim strusrId As String
        Dim strTemp As String
        Dim parameters As New List(Of SqlParameter)()

        strusrId = System.Web.HttpContext.Current.Session("usrId") & ""

        Dim usrId As New SqlParameter("@intUserId", strusrId)
        parameters.Add(usrId)

        If Len(strusrId) = 0 Then Return False

        If Len(strMenuName) > 0 Then
            Dim MenuName As New SqlParameter("@strMenuName", strMenuName)
            Dim ByName As New SqlParameter("@ByName", "YES")
            parameters.Add(MenuName)
            parameters.Add(ByName)

            strTemp = GetSpDataSingleValue("stpGetMenuPermissions", parameters.ToArray())
        Else
            Dim MenuID As New SqlParameter("@intMenuId", intMenuId)
            parameters.Add(MenuID)
        End If

        strTemp = GetSpDataSingleValue("stpGetMenuPermissions", parameters.ToArray())

        If Len(strTemp) = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Shared Function SecureCurrentPage() As String
        Dim strMenuPermissions As String
        Dim strMenuPermissionsDesc As String
        Dim strusrId As String
        Dim intMenuId As Integer

        If Not UserLoggedIn() Then System.Web.HttpContext.Current.Response.Redirect("/main.aspx?erm=admnu")

        strusrId = System.Web.HttpContext.Current.Session("usrId") & ""
        Dim parameters As New List(Of SqlParameter)()

        strusrId = System.Web.HttpContext.Current.Session("usrId") & ""
        intMenuId = GetCurrentPageId()
        Dim usrId As New SqlParameter("@usrId", strusrId)
        parameters.Add(usrId)
        Dim mnuId As New SqlParameter("@mnuId", intMenuId)
        parameters.Add(mnuId)

        strMenuPermissions = GetSpDataSingleValue("stpGetDetailedMenuPermissions", parameters.ToArray())

        strMenuPermissionsDesc = ""
        If Len(strMenuPermissions) > 0 Then strMenuPermissionsDesc &= "V"
        If Mid(strMenuPermissions, 1, 1) = "1" Then strMenuPermissionsDesc &= "A"
        If Mid(strMenuPermissions, 2, 1) = "1" Then strMenuPermissionsDesc &= "E"
        If Mid(strMenuPermissions, 3, 1) = "1" Then strMenuPermissionsDesc &= "D"

        If Len(strMenuPermissionsDesc & "") = 0 Then System.Web.HttpContext.Current.Response.Redirect("/main.aspx?erm=admnu")

        Return strMenuPermissionsDesc
    End Function

    Public Shared Function UserLoggedIn() As Boolean
        If Len(System.Web.HttpContext.Current.Session("usrId") & "") > 0 Then
            Return True
        Else
            Dim objCookie As System.Web.HttpCookie
            Dim strsusId As String
            objCookie = System.Web.HttpContext.Current.Request.Cookies("smartLv")

            Try
                strsusId = objCookie.Item("usrId").ToString & ""

                System.Web.HttpContext.Current.Session("usrId") = objCookie.Item("usrId").ToString & ""
                System.Web.HttpContext.Current.Session("usrName") = objCookie.Item("usrName").ToString & ""
                System.Web.HttpContext.Current.Session("usrFullName") = objCookie.Item("usrFullName").ToString & ""
                System.Web.HttpContext.Current.Session("usrSex") = objCookie.Item("usrSex").ToString & ""
                System.Web.HttpContext.Current.Session("dptId") = objCookie.Item("dptId").ToString & ""
                System.Web.HttpContext.Current.Session("comId") = objCookie.Item("comId").ToString & ""
                System.Web.HttpContext.Current.Session("ctyId") = objCookie.Item("ctyId").ToString & ""
                System.Web.HttpContext.Current.Session("secId") = objCookie.Item("secId").ToString & ""
                System.Web.HttpContext.Current.Session("usrEmailAdd") = objCookie.Item("usrEmailAdd").ToString & ""
                System.Web.HttpContext.Current.Session("comLeaveUnit") = objCookie.Item("comLeaveUnit").ToString & ""
                System.Web.HttpContext.Current.Session("Menu_SysAdmin") = objCookie.Item("Menu_SysAdmin").ToString & ""
                System.Web.HttpContext.Current.Session("Menu_HRAdmin") = objCookie.Item("Menu_HRAdmin").ToString & ""
                System.Web.HttpContext.Current.Session("Menu_Reports") = objCookie.Item("Menu_Reports").ToString & ""
                System.Web.HttpContext.Current.Session("Menu_Approvals") = objCookie.Item("Menu_Approvals").ToString & ""


                objCookie.Expires = DateAdd(DateInterval.Minute, 30, Now())
            Catch ex As Exception
                strsusId = ""
            End Try

            If IsNumeric(strsusId & "") Then
                Return True
            Else
                Return False
            End If
        End If
    End Function

    Public Shared Function IsValidLicense() As Boolean
        Return True
    End Function
End Class
