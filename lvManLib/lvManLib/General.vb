﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports lvManLib.DataFunctions
Imports lvManLib.Config
Imports System.IO
Imports System.Web
Imports System.Text
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports iTextSharp.text.html
Imports iTextSharp.text.html.simpleparser
Imports System.Web.UI
Imports System.Web.UI.WebControls


Public Class General
    Shared Sub LoadTime(ByVal lstDropDown As DropDownList, Optional ByVal intCompany As Integer = 0)
        Dim intCount As Integer
        Dim strTemp As String
        Dim strStartTime As String
        Dim strEndTime As String
        Dim objDR As SqlDataReader
        Dim intStartHour As Integer = 0
        Dim intEndHour As Integer = 23
        Dim intStartMinute As Integer = 0
        Dim intEndMinute As Integer = 0

        If intCompany > 0 Then
            Dim parameters As New List(Of SqlParameter)()
            Dim comId As New SqlParameter("@comId", intCompany)
            parameters.Add(comId)
            objDR = GetSpDataReader("stpCompanyDetails", parameters.ToArray())

            If objDR.Read() Then
                strStartTime = objDR("comStartTime")
                strEndTime = objDR("comEndTime")
            Else
                strStartTime = "00:00"
                strEndTime = "00:00"
            End If

            objDR.Close()
            objDR = Nothing

            strTemp = Mid(strStartTime, 1, 2)
            intStartHour = CType(strTemp, Integer)
            strTemp = Mid(strEndTime, 1, 2)
            intEndHour = CType(strTemp, Integer)
            strTemp = Mid(strStartTime, 4)
            intStartMinute = CType(strTemp, Integer)
            strTemp = Mid(strEndTime, 4)
            intEndMinute = CType(strTemp, Integer)
        End If

        lstDropDown.Items.Clear()

        For intCount = intStartHour To intEndHour
            strTemp = intCount
            If intCount < 10 Then strTemp = "0" & strTemp

            Select Case intCount
                Case intStartHour
                    Select Case intStartMinute
                        Case 0
                            lstDropDown.Items.Add(strTemp & ":00")
                            lstDropDown.Items.Add(strTemp & ":15")
                            lstDropDown.Items.Add(strTemp & ":30")
                            lstDropDown.Items.Add(strTemp & ":45")

                        Case 15
                            lstDropDown.Items.Add(strTemp & ":15")
                            lstDropDown.Items.Add(strTemp & ":30")
                            lstDropDown.Items.Add(strTemp & ":45")

                        Case 30
                            lstDropDown.Items.Add(strTemp & ":30")
                            lstDropDown.Items.Add(strTemp & ":45")

                        Case 45
                            lstDropDown.Items.Add(strTemp & ":45")

                    End Select

                Case intEndHour
                    Select Case intEndMinute
                        Case 0
                            lstDropDown.Items.Add(strTemp & ":00")
                        Case 15
                            lstDropDown.Items.Add(strTemp & ":00")
                            lstDropDown.Items.Add(strTemp & ":15")
                        Case 30
                            lstDropDown.Items.Add(strTemp & ":00")
                            lstDropDown.Items.Add(strTemp & ":15")
                            lstDropDown.Items.Add(strTemp & ":30")
                        Case 45
                            lstDropDown.Items.Add(strTemp & ":00")
                            lstDropDown.Items.Add(strTemp & ":15")
                            lstDropDown.Items.Add(strTemp & ":30")
                            lstDropDown.Items.Add(strTemp & ":45")
                    End Select

                Case Else
                    lstDropDown.Items.Add(strTemp & ":00")
                    lstDropDown.Items.Add(strTemp & ":15")
                    lstDropDown.Items.Add(strTemp & ":30")
                    lstDropDown.Items.Add(strTemp & ":45")

            End Select
        Next
    End Sub

    Public Shared Function ProperCase(ByVal strText As String, Optional ByVal bolFirstWordOnly As Boolean = False) As String
        Dim astrText As String()
        Dim intCount As Integer

        If bolFirstWordOnly Then
            Return UCase(Mid(strText, 1, 1)) & LCase(Mid(strText, 2))
        Else
            astrText = Split(strText, " ")
            strText = ""
            For intCount = 0 To UBound(astrText)
                If intCount > 0 Then strText &= " "
                strText &= UCase(Mid(astrText(intCount), 1, 1)) & LCase(Mid(astrText(intCount), 2))
            Next
            Return strText
        End If
    End Function


    Public Shared Function SendMail(ByVal strSubject As String, ByVal strMail As String, ByVal strMailTo As String, Optional ByVal strAttachment As String = "", Optional ByVal bolDeleteAttachment As Boolean = False) As Boolean
        Dim objMail As New System.Net.Mail.SmtpClient
        Dim objMessage As New System.Net.Mail.MailMessage
        Dim objAddress As System.Net.Mail.MailAddress
        Dim bolMessageSent As Boolean
        Dim strMailServer As String
        Dim strMailServerUserName As String
        Dim strMailServerPassword As String
        Dim strMailServerSenderName As String
        Dim strMailServerSenderEmail As String
        Dim strSendMail As String

        strSendMail = GetConfig("SendMail")
        If strSendMail = "N" Then 'Send Mail switched off
            Return False
        End If

        strMailServer = GetConfig("MailServer")
        strMailServerUserName = GetConfig("MailServerUserName")
        strMailServerPassword = GetConfig("MailServerPassword")
        strMailServerSenderName = GetConfig("MailServerSenderName")
        strMailServerSenderEmail = GetConfig("MailServerSenderEmail")

        If InStr(strMail, "|}*") > 0 Then
            strSubject = Mid(strMail, 1, InStr(strMail, "|}*") - 1)
            strMail = Mid(strMail, InStr(strMail, "|}*") + 3)
        End If

        If strMailServerUserName <> "NA" Then
            Dim objAutho As New System.Net.NetworkCredential(strMailServerUserName, strMailServerPassword)
            objMail.UseDefaultCredentials = False
            objMail.Credentials = objAutho
        Else
            objMail.UseDefaultCredentials = True
        End If

        objAddress = New System.Net.Mail.MailAddress(strMailServerSenderEmail, strMailServerSenderName)
        objMail.Host = strMailServer
        objMessage.From = objAddress
        objMessage.To.Add(strMailTo)
        objMessage.Subject = strSubject
        objMessage.IsBodyHtml = True
        objMessage.Body = strMail
        bolMessageSent = True

        If Len(strAttachment) > 0 Then
            objMessage.Attachments.Add(New System.Net.Mail.Attachment(strAttachment))
        End If

        Try
            objMail.Send(objMessage)
        Catch e As Exception
            bolMessageSent = False
            System.Web.HttpContext.Current.Response.Write("<!--" & e.Message & "|" & strMailTo & "-->")
        End Try

        If Len(strAttachment) > 0 And bolDeleteAttachment Then
            Try
                File.Delete(strAttachment)
            Catch ex As Exception
                'do nada
            End Try
        End If

        objMessage = Nothing
        objMail = Nothing

        Return bolMessageSent
    End Function


    Public Shared Function RoundUp(ByVal dblNumber As Double, Optional ByVal decPlace As Integer = 0) As Double
        Try
            'Instantiate a variable to hold the multiple of ten to modify the number sent in
            Dim intCont As Integer = 1
            'Loop through and multiply the control property by ten for the number of places to round too
            ' this will eventually be divided out again at the end so it won't really have an effect on the process
            While decPlace > 0
                'For each time through we effectively add a "0" to the control
                intCont = intCont * 10 : decPlace = decPlace - 1
            End While
            'Multiply the number by the control so the integer portion is what will be returned
            dblNumber = dblNumber * intCont
            'First get the decimal portion
            'The IIf statement checks to see if there is a decimal portion and if there is
            ' then we add 1 to force it to round up.
            'Chop off the current decimal portion with the truncate call
            'Divide the number by the control to set it back to the correct number of decimal places
            ' return the double which should be the number rounded up to the correct decimal places
            Return (Math.Truncate(CDbl(IIf(dblNumber - Math.Truncate(dblNumber) > 0, dblNumber + 1, dblNumber))) / intCont)
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Public Shared Function GetRawMailTemplate(ByVal strTemplate As String, ByVal intCompany As Integer) As String
        Dim objDR As SqlDataReader
        Dim strReturn As String = ""
        Dim strLogo As String
        Dim parameters As New List(Of SqlParameter)()
        Dim comId As New SqlParameter("@comId", intCompany)
        parameters.Add(comId)
        Dim tedShortName As New SqlParameter("@tedShortName", strTemplate)
        parameters.Add(tedShortName)

        strLogo = GetConfig("MailLogo")
        objDR = GetSpDataReader("stpGetMailTemplate", parameters.ToArray())

        If objDR.Read() Then
            strReturn = objDR("tedTitle") & "|}*" & objDR("tedText")

        End If

        objDR.Close()
        objDR = Nothing

        strReturn = Replace(strReturn, "[LOGO]", "<img src='" & strLogo & "' alt='Leave Manager' />")

        Return strReturn
    End Function

    Public Shared Function GetMailTemplate(ByVal strTemplate As String, ByVal intCompany As Integer, ByVal strVariables As String, ByVal strVariableValues As String) As String
        Dim strMailMessage As String
        Dim astrVariables As String()
        Dim astrVariableValues As String()
        Dim intCount As Integer

        strMailMessage = GetRawMailTemplate(strTemplate, intCompany)
        astrVariables = Split(strVariables, "|")
        astrVariableValues = Split(strVariableValues, "|")

        For intCount = 0 To UBound(astrVariables)
            strMailMessage = Replace(strMailMessage, astrVariables(intCount), astrVariableValues(intCount))
        Next

        Return strMailMessage
    End Function

    Shared Sub ExportCSV(ByVal strTitle As String, ByVal grvTarget As GridView, ByVal pgTarget As Page, Optional ByVal bolBind As Boolean = True)
        If bolBind Then
            grvTarget.AllowPaging = False
            grvTarget.AllowSorting = False
            grvTarget.DataBind()
        End If
        strTitle = Replace(strTitle, "'", "")
        strTitle &= Day(Now()) & " " & MonthName(Month(Now()), True) & " " & Year(Now())
        System.Web.HttpContext.Current.Response.Clear()
        System.Web.HttpContext.Current.Response.Buffer = True
        System.Web.HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" & strTitle & ".csv")
        System.Web.HttpContext.Current.Response.Charset = "utf-8"
        System.Web.HttpContext.Current.Response.ContentType = "application/text"

        Dim sb As New StringBuilder()
        For k As Integer = 0 To grvTarget.Columns.Count - 1
            sb.Append(grvTarget.Columns(k).HeaderText + ","c)
        Next

        'append new line

        sb.Append(vbCr & vbLf)

        For i As Integer = 0 To grvTarget.Rows.Count - 1

            For k As Integer = 0 To grvTarget.Columns.Count - 1
                'add separator
                sb.Append(grvTarget.Rows(i).Cells(k).Text + ","c)
            Next
            'append new line
            sb.Append(vbCr & vbLf)
        Next

        System.Web.HttpContext.Current.Response.Output.Write(sb.ToString())
        System.Web.HttpContext.Current.Response.Flush()
        System.Web.HttpContext.Current.Response.End()

        If bolBind Then
            grvTarget.AllowSorting = True
            grvTarget.AllowPaging = True
            grvTarget.DataBind()
        End If
    End Sub

    Shared Sub ExportPDF(ByVal strTitle As String, ByVal grvTarget As GridView, ByVal pgTarget As Page, Optional ByVal bolBind As Boolean = True, Optional ByVal bolLargePage As Boolean = False)
        If bolBind Then
            grvTarget.AllowPaging = False
            grvTarget.AllowSorting = False
            grvTarget.DataBind()
        End If
        strTitle = Replace(strTitle, "'", "")
        strTitle &= Day(Now()) & " " & MonthName(Month(Now()), True) & " " & Year(Now())
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        Dim frm As HtmlControls.HtmlForm = New HtmlControls.HtmlForm()
        Dim pgSize As iTextSharp.text.Rectangle

        pgTarget.EnableViewState = False
        pgTarget.Controls.Add(frm)
        frm.Controls.Add(grvTarget)
        frm.RenderControl(hw)
        If bolLargePage Then
            pgSize = PageSize.A0
        Else
            pgSize = PageSize.A4
        End If

        Dim sr As New StringReader(sw.ToString())
        Dim pdfDoc As New Document(pgSize, 10.0F, 10.0F, 10.0F, 0.0F)
        Dim htmlparser As New HTMLWorker(pdfDoc)

        System.Web.HttpContext.Current.Response.ContentType = "application/pdf"
        System.Web.HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" & strTitle & ".pdf")
        System.Web.HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache)

        PdfWriter.GetInstance(pdfDoc, System.Web.HttpContext.Current.Response.OutputStream)
        pdfDoc.Open()
        htmlparser.Parse(sr)
        pdfDoc.Close()
        System.Web.HttpContext.Current.Response.Write(pdfDoc)
        System.Web.HttpContext.Current.Response.End()

        If bolBind Then
            grvTarget.AllowSorting = True
            grvTarget.AllowPaging = True
            grvTarget.DataBind()
        End If
    End Sub

    Shared Sub ExportExcel(ByVal strTitle As String, ByVal grvTarget As GridView, ByVal pgTarget As Page, Optional ByVal bolBind As Boolean = True)
        If bolBind Then
            grvTarget.AllowPaging = False
            grvTarget.AllowSorting = False
            grvTarget.DataBind()
        End If
        strTitle = Replace(strTitle, "'", "")
        strTitle &= Day(Now()) & " " & MonthName(Month(Now()), True) & " " & Year(Now())
        Dim tw As New StringWriter()
        Dim hw As New System.Web.UI.HtmlTextWriter(tw)
        Dim frm As HtmlControls.HtmlForm = New HtmlControls.HtmlForm()
        System.Web.HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"
        System.Web.HttpContext.Current.Response.Charset = "utf-8"

        System.Web.HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" & strTitle & ".xls")

        pgTarget.EnableViewState = False
        pgTarget.Controls.Add(frm)
        frm.Controls.Add(grvTarget)
        frm.RenderControl(hw)
        System.Web.HttpContext.Current.Response.Write(tw.ToString())
        System.Web.HttpContext.Current.Response.End()

        If bolBind Then
            grvTarget.AllowSorting = True
            grvTarget.AllowPaging = True
            grvTarget.DataBind()
        End If
    End Sub

    Shared Sub ExportWord(ByVal strTitle As String, ByVal grvTarget As GridView, ByVal pgTarget As Page, Optional ByVal bolBind As Boolean = True)
        If bolBind Then
            grvTarget.AllowPaging = False
            grvTarget.AllowSorting = False
            grvTarget.DataBind()
        End If
        strTitle = Replace(strTitle, "'", "")
        strTitle &= Day(Now()) & " " & MonthName(Month(Now()), True) & " " & Year(Now())
        Dim tw As New StringWriter()
        Dim hw As New System.Web.UI.HtmlTextWriter(tw)
        Dim frm As HtmlControls.HtmlForm = New HtmlControls.HtmlForm()
        System.Web.HttpContext.Current.Response.ContentType = "application/vnd.ms-word"
        System.Web.HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" & strTitle & ".doc")
        System.Web.HttpContext.Current.Response.Charset = "utf-8"
        pgTarget.EnableViewState = False
        pgTarget.Controls.Add(frm)
        frm.Controls.Add(grvTarget)
        frm.RenderControl(hw)
        System.Web.HttpContext.Current.Response.Write(tw.ToString())
        System.Web.HttpContext.Current.Response.End()

        If bolBind Then
            grvTarget.AllowSorting = True
            grvTarget.AllowPaging = True
            grvTarget.DataBind()
        End If
    End Sub
End Class
