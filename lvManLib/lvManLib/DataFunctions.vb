﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Web
Imports System.Web.UI.WebControls

Public Class DataFunctions
    Public Shared Function GetConnectionString() As String
        Return ConfigurationManager.ConnectionStrings.Item("smartlvConnectionString").ConnectionString
    End Function

    Public Shared Function GetTransactionID() As String
        Dim strTransactionID As String

        strTransactionID = "TRN" & (Year(Now()) - 2000) & Month(Now()) & Day(Now()) & Hour(Now()) & Second(Now()) & System.Web.HttpContext.Current.Session("usrId") & Minute(Now())

        Return strTransactionID
    End Function

    Public Shared Function ExecuteQuery(ByVal strSql As String, Optional ByVal bolFormatSQL As Boolean = True) As String
        Dim objConnection As SqlConnection
        Dim strMessage As String
        Dim strDSN As String

        objConnection = New SqlConnection
        strDSN = GetConnectionString()
        objConnection.ConnectionString = strDSN
        Dim objCommand As SqlCommand
        strSql = Replace(strSql, "--", "")
        If bolFormatSQL Then strSql = FormatDbString(strSql)

        objCommand = New SqlCommand(strSql, objConnection)
        objCommand.Connection.Open()

        strMessage = "SUCCESS!"

        Try
            objCommand.ExecuteNonQuery()

        Catch Exp As SqlException
            If Exp.ErrorCode = -2147467259 Then
                strMessage = "ERROR: A record already exists with the same identity."
            Else
                strMessage = "ERROR: Could not update record, please ensure the fields are correctly filled out.<br>ERR DESC: " & strSql & "<br>" & Exp.Message
            End If
        End Try

        objCommand.Connection.Close()
        objConnection.Dispose()
        objCommand = Nothing
        objConnection = Nothing
        'System.Web.HttpContext.Current.Response.Write("<!--" & strMessage & "-->")
        Return strMessage
    End Function

    Shared Function GetSpDataReader(ByVal strProcName As String, Optional ByVal dbParameters() As SqlParameter = Nothing, Optional ByVal strParameters As String = "") As SqlDataReader
        Dim objDR As SqlDataReader
        Dim objCommand As SqlCommand
        Dim objConnection As SqlConnection

        objConnection = New SqlConnection(GetConnectionString())
        objCommand = New SqlCommand
        objCommand.Connection = objConnection
        objCommand.CommandText = strProcName
        objCommand.CommandType = CommandType.StoredProcedure

        If Not IsNothing(dbParameters) Then objCommand.Parameters.AddRange(dbParameters)

        If Len(strParameters) > 0 Then
            Dim astrParameters() As String
            Dim astrLine() As String
            Dim intCount As Integer

            astrParameters = Split(strParameters, "*")
            For intCount = 0 To UBound(astrParameters)
                If InStr(astrParameters(intCount), "=") > 0 Then
                    astrLine = Split(astrParameters(intCount), "=")
                    objCommand.Parameters.AddWithValue(astrLine(0), astrLine(1))
                End If
            Next
        End If

        objCommand.Connection.Open()
        objDR = objCommand.ExecuteReader(CommandBehavior.CloseConnection)

        Return objDR

        objDR.Close()
        objCommand.Connection.Close()
        objConnection.Dispose()
        objCommand.Dispose()
        objConnection = Nothing
        objCommand = Nothing
        objDR = Nothing
    End Function

    Shared Function GetSpDataSingleValue(ByVal strProcName As String, Optional ByVal dbParameters() As SqlParameter = Nothing, Optional ByVal strParameters As String = "") As String
        Dim objDR As SqlDataReader
        Dim objCommand As SqlCommand
        Dim objConnection As SqlConnection
        Dim strValue As String = ""

        objConnection = New SqlConnection(GetConnectionString())
        objCommand = New SqlCommand
        objCommand.Connection = objConnection
        objCommand.CommandText = strProcName
        objCommand.CommandType = CommandType.StoredProcedure

        If Not IsNothing(dbParameters) Then objCommand.Parameters.AddRange(dbParameters)

        If Len(strParameters) > 0 Then
            Dim astrParameters() As String
            Dim astrLine() As String
            Dim intCount As Integer

            astrParameters = Split(strParameters, "*")
            For intCount = 0 To UBound(astrParameters)
                If InStr(astrParameters(intCount), "=") > 0 Then
                    astrLine = Split(astrParameters(intCount), "=")
                    objCommand.Parameters.AddWithValue(astrLine(0), astrLine(1))
                End If
            Next
        End If

        objCommand.Connection.Open()
        objDR = objCommand.ExecuteReader(CommandBehavior.CloseConnection)

        If objDR.Read() Then
            strValue = objDR(0) & ""
        End If

        objDR.Close()
        objCommand.Connection.Close()
        objConnection.Dispose()
        objCommand.Dispose()
        objConnection = Nothing
        objCommand = Nothing
        objDR = Nothing

        Return strValue
    End Function

    Shared Function ExecuteStoredProc(ByVal strProcName As String, Optional ByVal dbParameters() As SqlParameter = Nothing, Optional ByVal strParameters As String = "") As String
        Dim objCommand As SqlCommand
        Dim objConnection As SqlConnection
        Dim strReturn As String = "OK"

        objConnection = New SqlConnection(GetConnectionString())
        objCommand = New SqlCommand
        objCommand.Connection = objConnection
        objCommand.CommandText = strProcName
        objCommand.CommandType = CommandType.StoredProcedure

        If Not IsNothing(dbParameters) Then objCommand.Parameters.AddRange(dbParameters)

        If Len(strParameters) > 0 Then
            Dim astrParameters() As String
            Dim astrLine() As String
            Dim intCount As Integer

            astrParameters = Split(strParameters, "*")
            For intCount = 0 To UBound(astrParameters)
                If InStr(astrParameters(intCount), "=") > 0 Then
                    astrLine = Split(astrParameters(intCount), "=")

                    'Hardcoding this here because of issues converting the '1' sent to a bit value
                    If astrLine(0) = "appFinalApprover" Then
                        If astrLine(1) = "1" Then
                            objCommand.Parameters.AddWithValue(astrLine(0), True)
                        Else
                            objCommand.Parameters.AddWithValue(astrLine(0), False)
                        End If
                    Else
                        objCommand.Parameters.AddWithValue(astrLine(0), astrLine(1))
                    End If

                End If
            Next
        End If

        objCommand.Connection.Open()

        Try
            objCommand.ExecuteNonQuery()
        Catch ex As Exception
            Return "FAIL|" & ex.Message
        End Try


        objCommand.Connection.Close()
        objConnection.Dispose()
        objCommand.Dispose()
        objConnection = Nothing
        objCommand = Nothing

        Return strReturn
    End Function


    Public Shared Function ReturnSingleDbValue(ByVal strSql As String) As String
        Dim strValue As String
        Dim objDR As SqlDataReader
        Dim objCommand As SqlCommand
        Dim objConnection As SqlConnection
        Dim strDSN As String
        strDSN = GetConnectionString()

        objConnection = New SqlConnection(strDSN)
        objCommand = New SqlCommand(strSql, objConnection)
        objCommand.Connection.Open()

        strValue = ""

        Try
            objDR = objCommand.ExecuteReader()

            strValue = ""
            If objDR.Read() Then
                strValue = objDR(0) & ""
            End If

            objDR.Close()
        Catch
            'System.Web.HttpContext.Current.Response.Write("Error occured." & strSql)
        End Try

        objCommand.Connection.Close()
        objConnection.Dispose()
        objConnection = Nothing
        objCommand = Nothing
        objDR = Nothing

        Return strValue
    End Function

    Public Shared Function FormatDbString(ByVal strText As String) As String
        'Replace , with ,
        strText = Replace(strText, ", ", ",", 1)
        strText = Replace(strText, " ,", ",", 1)
        'Replace ' with ''
        strText = Replace(strText, "'", "''", 1)
        'Replace('' with ('
        strText = Replace(strText, "(''", "('", 1)
        'Replace,'' with ,'
        strText = Replace(strText, ",''", ",'", 1)
        'Replace '', with ',
        strText = Replace(strText, "'',", "',", 1)
        'Replace'') with')
        strText = Replace(strText, "'')", "')", 1)
        'Replace''; with';
        strText = Replace(strText, "'';", "';", 1)
        'Replace = '' with = '
        strText = Replace(strText, "= ''", "= '", 1)
        'Replace ='' with ='
        strText = Replace(strText, "=''", "='", 1)
        'Replace '' WHERE with ' WHERE
        strText = Replace(strText, "'' WHERE", "' WHERE", 1)
        'Replace '' AND with ' AND
        strText = Replace(strText, "'' AND", "' AND", 1)
        Return strText
    End Function

    Public Shared Function GetDataReader(ByVal strSql As String) As SqlDataReader
        Dim objDR As SqlDataReader
        Dim objCommand As SqlCommand
        Dim objConnection As SqlConnection
        Dim strDSN As String

        strDSN = GetConnectionString()

        strSql = Replace(strSql, "--", "")
        strSql = Replace(strSql, ";", "")

        objConnection = New SqlConnection(strDSN)
        objCommand = New SqlCommand(strSql, objConnection)
        objCommand.Connection.Open()

        objDR = objCommand.ExecuteReader(CommandBehavior.CloseConnection)

        Return objDR

        objDR.Close()
        objCommand.Connection.Close()
        objConnection.Dispose()
        objConnection = Nothing
        objCommand = Nothing
        objDR = Nothing
    End Function

    Shared Sub BindDropDown(ByVal strSql As String, ByVal lstTarget As DropDownList)
        Dim objConn As New SqlConnection(GetConnectionString())
        objConn.Open()
        Dim objCmd As New SqlCommand(strSql, objConn)
        Dim objDR As SqlDataReader
        objDR = objCmd.ExecuteReader()
        lstTarget.DataSource = objDR
        lstTarget.DataBind()
        objDR.Close()
        objCmd.Connection.Close()
        objConn.Dispose()
        objCmd = Nothing
        objConn = Nothing
        objDR = Nothing
    End Sub

    Shared Sub BindListBox(ByVal strSql As String, ByVal lstTarget As ListBox)
        Dim objConn As New SqlConnection(GetConnectionString())
        objConn.Open()
        Dim objCmd As New SqlCommand(strSql, objConn)
        Dim objDR As SqlDataReader
        objDR = objCmd.ExecuteReader()
        lstTarget.DataSource = objDR
        lstTarget.DataBind()
        objDR.Close()
        objCmd.Connection.Close()
        objConn.Dispose()
        objCmd = Nothing
        objConn = Nothing
        objDR = Nothing
    End Sub

End Class