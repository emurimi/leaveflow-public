﻿Imports Microsoft.VisualBasic
Imports lvManLib.DataFunctions
Imports lvManLib.Config
Imports System.Data.SqlClient

Public Class Menus
    Public Shared Function GetWebsiteRoot() As String
        Return GetConfig("SystemURL")
    End Function

    Public Shared Function GetCurrentPageName() As String
        Dim strURL As String
        Dim strDST As String 'Specific for Generic Admin Pages
        Dim strType As String 'Specific for pages with subtypes
        Dim strReport As String
        Dim strViewMode As String

        strDST = System.Web.HttpContext.Current.Request.QueryString("dst") & ""
        strType = System.Web.HttpContext.Current.Request.QueryString("typ") & ""
        strViewMode = System.Web.HttpContext.Current.Request.QueryString("mvw") & ""
        strReport = System.Web.HttpContext.Current.Request.QueryString("rep") & ""
        strURL = System.Web.HttpContext.Current.Request.ServerVariables("SCRIPT_NAME")
        strURL = LCase(strURL)
        strURL = Replace(strURL, GetWebsiteRoot, "")
        If Len(strDST) > 0 Then strURL &= "?dst=" & strDST
        If Len(strType) > 0 Then strURL &= "?typ=" & strType
        If Len(strReport) > 0 Then strURL &= "?rep=" & strReport
        If Len(strViewMode) > 0 And strViewMode <> "prn" Then strURL &= "?mvw=" & strViewMode

        Return strURL
    End Function

    Public Shared Function GetCurrentPageId() As Integer
        Dim strId As String
        Dim strCurrentPage As String = GetCurrentPageName()
        Dim parameters As New List(Of SqlParameter)()

        Dim mnuLink1 As New SqlParameter("@mnuLink1", strCurrentPage)
        parameters.Add(mnuLink1)
        Dim mnuLink2 As New SqlParameter("@mnuLink2", GetCurrentPageName())
        parameters.Add(mnuLink2)

        strId = GetSpDataSingleValue("stpMenus_GetID", parameters.ToArray())

        If Len(strId) > 0 Then
            Return CType(strId, Integer)
        Else
            Return 0
        End If
    End Function

    Public Shared Function GetPageName() As String
        Dim strSql As String
        Dim strTemp As String
        Dim strPage As String = LCase(System.Web.HttpContext.Current.Request.ServerVariables("SCRIPT_NAME") & "")


        Select Case strPage
            Case "/main.aspx"
                strTemp = "welcome " & LCase(System.Web.HttpContext.Current.Session("usrFullName"))
            Case "/leaverequest.aspx"
                If Len(System.Web.HttpContext.Current.Request.QueryString("lrq") & "") > 0 Then
                    strTemp = "my leave request"
                Else
                    strTemp = "new leave request"
                End If

            Case "/leavemain.aspx"
                strTemp = "leave requests in process"
            Case "/leavecompensation.aspx"
                strTemp = "leave days compensation"
            Case "/leaverecords.aspx"
                strTemp = "my leave history"
            Case "/leaveplans/leaveplan.aspx"
                strTemp = "make leave plan"
            Case "/leaveplans/default.aspx"
                strTemp = "my leave plans"
            Case "/approvals/default.aspx"
                strTemp = "process leave requests"
            Case "/approvals/plans.aspx"
                strTemp = "process leave plans"
            Case "/approvals/compensation.aspx"
                strTemp = "process leave days compensation requests"
            Case "/approvals/allowances.aspx"
                strTemp = "process leave allowance requests"
            Case "/hradmin/approvalsetup.aspx"
                strTemp = "approvals setup"
            Case "/hradmin/leavetypes.aspx"
                strTemp = "leave types setup"
            Case "/hradmin/publicholidays.aspx"
                strTemp = "holidays setup"
            Case "/hradmin/roles.aspx"
                strTemp = "setup roles"
            Case "/hradmin/leaveallowance.aspx"
                strTemp = "leave allowance setup"
            Case "/hradmin/adjustmentreasons.aspx"
                strTemp = "leave days manual adjustment reasons"
            Case "/hradmin/gradesetup.aspx"
                strTemp = "leave entitlement setup"
            Case "/hradmin/grades.aspx"
                strTemp = "grades setup"
            Case "/hradmin/calendar.aspx"
                strTemp = "setup corporate events"
            Case "/hradmin/manualadjustments.aspx"
                strTemp = "leave days manual adjustments"
            Case "/hradmin/preloadedleave.aspx"
                strTemp = "setup preloaded leave"
            Case "/myprofile.aspx"
                strTemp = "my profile"
            Case "/changepassword.aspx"
                strTemp = "change my password"
            Case Else
                strSql = "SELECT mnuName FROM tblMenus WHERE mnuId = " & GetCurrentPageId()
                strTemp = ReturnSingleDbValue(strSql)

                If Len(strTemp) = 0 Then strTemp = LCase(strPage)

        End Select


        Return strTemp
    End Function

    Public Shared Function GetCurrentParentId() As Integer
        Dim strId As String
        Dim parameters As New List(Of SqlParameter)()
        Dim mnuLink As New SqlParameter("@mnuLink", GetCurrentPageName())
        parameters.Add(mnuLink)

        strId = GetSpDataSingleValue("stpMenus_GetParentID", parameters.ToArray())

        If strId = "0" Then strId = GetCurrentPageId()

        If Len(strId) > 0 Then
            Return CType(strId, Integer)
        Else
            Return 0
        End If
    End Function

    Public Shared Function GetPageNameFromId(ByVal intMenuId As Integer) As String
        Dim strName As String
        Dim parameters As New List(Of SqlParameter)()
        Dim mnuId As New SqlParameter("@mnuId", intMenuId)
        parameters.Add(mnuId)

        strName = GetSpDataSingleValue("stpMenus_GetName", parameters.ToArray())

        If Len(strName) > 0 Then
            Return strName
        Else
            Return ""
        End If
    End Function

    Public Shared Function GetRedirectFromId(ByVal intMenuId As Integer) As String
        Dim strName As String
        Dim parameters As New List(Of SqlParameter)()
        Dim mnuId As New SqlParameter("@mnuId", intMenuId)
        parameters.Add(mnuId)
        Dim ReturnLink As New SqlParameter("@ReturnLink", "YES")
        parameters.Add(ReturnLink)

        strName = GetSpDataSingleValue("stpMenus_GetName", parameters.ToArray())

        If Len(strName) > 0 Then
            Return strName
        Else
            Return ""
        End If
    End Function

    Public Shared Function GetMenuString(ByVal intParentId As Integer) As String
        Dim objDR As SqlDataReader
        Dim strMenu As String = ""
        Dim parameters As New List(Of SqlParameter)()
        Dim mnuParent As New SqlParameter("@mnuParent", intParentId)
        parameters.Add(mnuParent)

        objDR = GetSpDataReader("stpGetParentMenus", parameters.ToArray())

        While objDR.Read
            strMenu &= objDR(0) & "," & objDR(1) & ";"
        End While

        objDR.Close()
        objDR = Nothing

        If Len(strMenu) > 3 Then strMenu = Mid(strMenu, 1, Len(strMenu) - 1)
        Return strMenu
    End Function

    Public Shared Function GetCurrentMenuString() As String
        Dim intCurrentPageId As Integer
        intCurrentPageId = GetCurrentParentId()

        If intCurrentPageId = 0 And InStr(System.Web.HttpContext.Current.Request.ServerVariables("SCRIPT_NAME"), "/reports/") > 0 Then intCurrentPageId = 10
        Return GetMenuString(intCurrentPageId)
    End Function
End Class
