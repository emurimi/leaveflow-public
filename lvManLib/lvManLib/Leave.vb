﻿Imports Microsoft.VisualBasic
Imports lvManLib.DataFunctions
Imports System.Data.SqlClient
Imports lvManLib.General
Imports lvManLib.Config

Public Class Leave
    Public Shared Function GetHoursToDays(ByVal dblHours As Double, ByVal intCompany As Integer) As String
        Dim dblDays As Double
        dblDays = GetHoursToDayNumber(dblHours, intCompany)
        Return " (" & dblDays & " days)"
    End Function

    Public Shared Function GetHoursToDayNumber(ByVal dblHours As Double, ByVal intCompany As Integer) As Double
        Dim intHoursPerDay As Integer
        Dim dblDays As Double
        Dim strTemp As String
        Dim parameters As New List(Of SqlParameter)()
        Dim comId As New SqlParameter("@comId", intCompany)
        parameters.Add(comId)

        strTemp = GetSpDataSingleValue("stpCompany_GetHoursPerDay", parameters.ToArray())

        If Len(strTemp) = 0 Then strTemp = "8"
        intHoursPerDay = CType(strTemp, Integer)
        dblDays = dblHours / intHoursPerDay

        Return dblDays
    End Function

    Public Shared Function GetLeaveDaysValueForWeekday(ByVal intWeekDay As Integer, Optional ByVal bolInvert As Boolean = False) As Double
        Dim objDR As SqlDataReader
        Dim dblCountDays As Double = 1
        Dim parameters As New List(Of SqlParameter)()
        Dim dadId As New SqlParameter("@dadId", intWeekDay)
        parameters.Add(dadId)

        objDR = GetSpDataReader("stpDayDefinitions_Details", parameters.ToArray())

        While objDR.Read()
            Select Case objDR("dadType")
                Case "N" 'No working day
                    If bolInvert Then
                        dblCountDays = 1
                    Else
                        dblCountDays = 0
                    End If

                Case "W" 'Working day
                    If bolInvert Then
                        dblCountDays = 0
                    Else
                        dblCountDays = 1
                    End If

                Case "H" 'Half day
                    dblCountDays = 0.5

            End Select

        End While
        objDR.Close()

        Return dblCountDays
    End Function

    Public Shared Function CountLeaveDays(ByVal datStartDate As Date, ByVal datEndDate As Date, ByVal strUnits As String, ByVal intUserID As Integer, ByVal strStartTime As String, ByVal strEndTime As String, ByVal intLeaveType As Integer) As Double
        Dim intDays As Double
        Dim datDate As Date
        Dim datDate2 As Date
        Dim objDR As SqlDataReader
        Dim intCountryID As Integer
        Dim intCompanyID As Integer
        Dim strTemp As String
        Dim dblHoursPerDay As Double
        Dim strCOMStartTime As String
        Dim strCOMEndTime As String
        Dim strCOMLBStartTime As String 'Lunch break start time
        Dim strCOMLBEndTime As String
        Dim dblHoursUsed As Double
        Dim intStartHour As Integer
        Dim intEndHour As Integer
        Dim intStartMinute As Integer
        Dim intEndMinute As Integer
        Dim intStartUnits As Integer
        Dim intEndUnits As Integer
        Dim intUsedUnits As Integer
        Dim intFridayHolidayCompensation As Integer
        Dim intSaturdayHolidayCompensation As Integer
        Dim intSundayHolidayCompensation As Integer
        Dim intMondayHolidayCompensation As Integer
        Dim bolCountNonWorkDays As Boolean = False
        Dim bolCompensated As Boolean
        Dim strLastDatesDeducted As String = ""
        Dim parameters As New List(Of SqlParameter)()


        Dim ltyId As New SqlParameter("@ltyId", intLeaveType)
        parameters.Add(ltyId)
        strTemp = GetSpDataSingleValue("stpLeaveTypes_CountNonWorkDays", parameters.ToArray())
        If LCase(strTemp) = "true" Then bolCountNonWorkDays = True
        If datStartDate > datEndDate Then Return 0

        parameters.Clear()
        Dim usrId As New SqlParameter("@usrId", intUserID)
        parameters.Add(usrId)
        strTemp = GetSpDataSingleValue("stpGetUserCompany", parameters.ToArray())
        If Len(strTemp) = 0 Then strTemp = "0"
        intCompanyID = CType(strTemp, Integer)

        parameters.Clear()
        Dim comId As New SqlParameter("@comId", intCompanyID)
        parameters.Add(comId)
        objDR = GetSpDataReader("stpCompanyDetails", parameters.ToArray())

        objDR.Read()
        intCountryID = objDR("comctyId")
        dblHoursPerDay = objDR("comHoursPerDay")
        strCOMStartTime = objDR("comStartTime")
        strCOMEndTime = objDR("comEndTime")
        strCOMLBStartTime = objDR("comLBSTartTime")
        strCOMLBEndTime = objDR("comLBEndTime")
        intFridayHolidayCompensation = objDR("comFridayHolidayCompensation")
        intSaturdayHolidayCompensation = objDR("comSaturdayHolidayCompensation")
        intSundayHolidayCompensation = objDR("comSundayHolidayCompensation")
        intMondayHolidayCompensation = objDR("comMondayHolidayCompensation")
        objDR.Close()


        intDays = DateDiff(DateInterval.Day, datStartDate, datEndDate)
        intDays += 1
        'intDays += GetLeaveDaysValueForWeekday(Weekday(datStartDate))

        If Not bolCountNonWorkDays Then
            datDate = datStartDate
            Do While datDate <= datEndDate
                intDays -= GetLeaveDaysValueForWeekday(Weekday(datDate), True)

                datDate = DateAdd(DateInterval.Day, 1, datDate)
            Loop
        End If

        If Not bolCountNonWorkDays Then
            parameters.Clear()
            Dim StartYear As New SqlParameter("@StartYear", Year(datStartDate))
            parameters.Add(StartYear)
            Dim EndYear As New SqlParameter("@EndYear", Year(datEndDate))
            parameters.Add(EndYear)
            Dim ctyId As New SqlParameter("@ctyId", intCountryID)
            parameters.Add(ctyId)

            objDR = GetSpDataReader("stpGetPublicHolidays", parameters.ToArray())

            While objDR.Read()
                bolCompensated = False
                datDate = DateSerial(Year(datStartDate), objDR("pbhMonth"), objDR("pbhDay"))
                datDate2 = DateSerial(Year(datEndDate), objDR("pbhMonth"), objDR("pbhDay"))

                If intFridayHolidayCompensation > 0 Then
                    If LCase(WeekdayName(Weekday(datDate))) = "friday" Then
                        datDate = DateAdd(DateInterval.Day, intFridayHolidayCompensation, datDate)
                        bolCompensated = True
                    End If

                    If LCase(WeekdayName(Weekday(datDate2))) = "friday" Then
                        datDate2 = DateAdd(DateInterval.Day, intFridayHolidayCompensation, datDate2)
                        bolCompensated = True
                    End If

                End If

                If intSaturdayHolidayCompensation > 0 And Not bolCompensated Then
                    If LCase(WeekdayName(Weekday(datDate))) = "saturday" Then
                        datDate = DateAdd(DateInterval.Day, intSaturdayHolidayCompensation, datDate)
                        bolCompensated = True
                    End If

                    If LCase(WeekdayName(Weekday(datDate2))) = "saturday" Then
                        datDate2 = DateAdd(DateInterval.Day, intSaturdayHolidayCompensation, datDate2)
                        bolCompensated = True
                    End If
                End If

                If intSundayHolidayCompensation > 0 And Not bolCompensated Then
                    If LCase(WeekdayName(Weekday(datDate))) = "sunday" Then
                        datDate = DateAdd(DateInterval.Day, intSundayHolidayCompensation, datDate)
                        bolCompensated = True
                    End If
                    If LCase(WeekdayName(Weekday(datDate2))) = "sunday" Then
                        datDate2 = DateAdd(DateInterval.Day, intSundayHolidayCompensation, datDate2)
                        bolCompensated = True
                    End If
                End If

                If intMondayHolidayCompensation > 0 And Not bolCompensated Then
                    If LCase(WeekdayName(Weekday(datDate))) = "monday" Then
                        datDate = DateAdd(DateInterval.Day, intMondayHolidayCompensation, datDate)
                        bolCompensated = True
                    End If
                    If LCase(WeekdayName(Weekday(datDate2))) = "monday" Then
                        datDate2 = DateAdd(DateInterval.Day, intMondayHolidayCompensation, datDate2)
                        bolCompensated = True
                    End If
                End If

                If (datDate >= datStartDate And datDate <= datEndDate) Or (datDate2 >= datStartDate And datDate2 <= datEndDate) Then
                    If InStr(strLastDatesDeducted, "|" & CType(datDate, String) & "|") = 0 Then
                        intDays -= GetLeaveDaysValueForWeekday(Weekday(datDate))
                        strLastDatesDeducted &= "|" & CType(datDate, String) & "|"
                    End If
                End If
            End While

            objDR.Close()
            objDR = Nothing
        End If

        If UCase(Mid(strUnits, 1, 1)) = "H" Then intDays = intDays * dblHoursPerDay

        If Len(strStartTime) > 0 And Len(strEndTime) > 0 Then
            If strStartTime <> strCOMStartTime Or strEndTime <> strCOMEndTime Then
                intStartHour = Mid(strStartTime, 1, 2)
                intEndHour = Mid(strEndTime, 1, 2)
                intStartMinute = Mid(strStartTime, 4)
                intEndMinute = Mid(strEndTime, 4)

                intStartUnits = (intStartHour * 60) + intStartMinute
                intEndUnits = (intEndHour * 60) + intEndMinute
                intUsedUnits = intEndUnits - intStartUnits

                dblHoursUsed = intUsedUnits / 60

                If UCase(Mid(strUnits, 1, 1)) = "H" Then
                    intDays -= dblHoursPerDay
                    intDays += dblHoursUsed
                Else
                    intDays -= 1
                    intDays += FormatNumber(dblHoursUsed / dblHoursPerDay, 1)
                End If
            End If
        End If
        '1730-1130=600/ 1700-1430=270
        'dblHoursUsed = CType(strEndTime, Double) - CType(strStartTime, Double)

        'strCOMStartTime = objDR("comStartTime")
        'strCOMEndTime = objDR("comEndTime")
        'strCOMLBStartTime = objDR("comLBSTartTime")
        'strCOMLBEndTime = objDR("comLBEndTime")

        Return intDays
    End Function

    Public Shared Function CalculateAvailableDays(ByVal intusrId As Integer, ByVal intltyId As Integer) As Double
        Dim dblDays As Double
        Dim strTemp As String
        Dim objDR As SqlDataReader
        Dim bolCount As Boolean
        Dim bolCountAfterMax As Boolean
        Dim dblMax As Double
        Dim dblDaysUsed As Double
        Dim parameters As New List(Of SqlParameter)()

        Dim usrId As New SqlParameter("@usrId", intusrId)
        parameters.Add(usrId)
        strTemp = GetSpDataSingleValue("stpGetUserAvailableDays", parameters.ToArray())
        If Len(strTemp) = 0 Then strTemp = "0"

        dblDays = CType(strTemp, Double)

        parameters.Clear()
        Dim ltyId As New SqlParameter("@ltyId", intltyId)
        parameters.Add(ltyId)

        objDR = GetSpDataReader("stpLeaveType_Details", parameters.ToArray())

        If objDR.Read() Then
            bolCount = objDR("ltyCount")
            bolCountAfterMax = objDR("ltyCountAfterMax")
            dblMax = objDR("ltyMax")
        Else
            bolCount = True
            bolCountAfterMax = True
            dblMax = 0
        End If
        objDR.Close()

        'get how many days have been spent in the past
        If dblMax > 0 Then
            dblDaysUsed = LeaveTypeDaysUsed(intltyId, intusrId, Year(Now()))
        Else
            dblDaysUsed = 0
        End If

        If dblMax = 0 Then
            'do nothing
        Else
            dblDays = dblMax
        End If

        dblDays = dblDays - dblDaysUsed

        objDR = Nothing

        Return dblDays
    End Function

    Public Shared Function LeaveTypeDaysUsed(ByVal intltyId As Integer, ByVal intusrId As Integer, ByVal intYear As Integer) As Double
        Dim dblDays As Double
        Dim strTemp As String
        Dim parameters As New List(Of SqlParameter)()

        Dim usrId As New SqlParameter("@usrId", intusrId)
        parameters.Add(usrId)
        Dim ltyId As New SqlParameter("@ltyId", intltyId)
        parameters.Add(ltyId)
        Dim intYear1 As New SqlParameter("@intYear", intYear)
        parameters.Add(intYear1)

        strTemp = GetSpDataSingleValue("stpGetLeaveTypeDaysUsed", parameters.ToArray())

        If Len(strTemp) = 0 Then strTemp = "0"

        dblDays = CType(strTemp, Double)

        Return dblDays
    End Function

    Public Shared Function AlertApprovers(ByVal intlrqId As Integer, Optional ByVal strCategory As String = "LR") As Boolean
        Dim strSql As String = ""
        Dim objDR As SqlDataReader
        Dim intusrId As Integer 'User
        Dim strFullName As String
        Dim strStaffID As String
        Dim intposId As Integer 'Position
        Dim intdptId As Integer 'Department
        Dim intsecId As Integer 'Section
        Dim intcomId As Integer 'Company
        Dim intbraId As Integer 'Branch
        Dim strStatus As String
        Dim strMailTitle As String
        Dim strMailMessage As String
        Dim strTemp As String
        Dim strMailTemplate As String = ""
        Dim strPrevApprover As String = ""
        Dim strUserSql As String = ""
        Dim strURL As String = GetConfig("SystemURL")
        Dim strApproversTable As String = "tblApprovalPath"
        Dim parameters As New List(Of SqlParameter)()

        Dim intID As New SqlParameter("@intID", intlrqId)
        parameters.Add(intID)
        Dim Category As New SqlParameter("@Category", strCategory)
        parameters.Add(Category)

        '**** Check the status of the leave request
        strStatus = GetSpDataSingleValue("stpGetRequestStatus", parameters.ToArray())

        If strStatus <> "1" Then 'The leave application is not pending, exit
            Return False
        End If

        parameters.Clear()
        Dim intID2 As New SqlParameter("@intID", intlrqId)
        parameters.Add(intID2)
        Dim Category2 As New SqlParameter("@Category", strCategory)
        parameters.Add(Category2)

        intusrId = GetSpDataSingleValue("stpGetRequestOwner", parameters.ToArray())

        'Get details of the leave applicant
        parameters.Clear()
        Dim usrId As New SqlParameter("@usrId", intusrId)
        parameters.Add(usrId)
        objDR = GetSpDataReader("stpUserDetails", parameters.ToArray())
        If objDR.Read() Then
            strFullName = objDR("usrFullName") & ""
            strStaffID = objDR("usrStaffID") & ""
            intposId = objDR("usrposId")
            intdptId = objDR("usrdptId")
            intcomId = objDR("comId")
            intbraId = objDR("usrbraId")
            If Len(objDR("usrsecId") & "") > 0 Then
                intsecId = objDR("usrsecId")
            Else
                intsecId = 0
            End If
        Else
            objDR.Close()
            objDR = Nothing
            Return False
        End If

        objDR.Close()

        '*********** Check whether approval has been initiated*********
        parameters.Clear()
        Dim Category3 As New SqlParameter("@Category", strCategory)
        parameters.Add(Category3)
        Dim intID3 As New SqlParameter("@intID", intlrqId)
        parameters.Add(intID3)

        strTemp = GetSpDataSingleValue("stpCheckIfApprovalInitiated", parameters.ToArray())

        Select Case strCategory
            Case "LC" 'Leave Compensation
                strMailTemplate = "ALCAA" 'Next Approver

            Case "LP" 'Leave Plan
                strMailTemplate = "ALPAA" 'Next Approver

            Case "LR" 'Leave Request
                strMailTemplate = "ALAA" 'Next Approver

        End Select

        If strTemp = "0" Then
            LoadApprovers(intcomId, intbraId, intdptId, intsecId, intposId, intusrId, intlrqId, strCategory)

            Select Case strCategory
                Case "LC" 'Leave Compensation
                    strMailTemplate = "ALCNA" 'First approver

                Case "LP" 'Leave Plan
                    strMailTemplate = "ALPNA" 'First approver

                Case "LR" 'Leave Request
                    strMailTemplate = "ALNA" 'First approver

            End Select
        End If

        'send alerts to next approver
        parameters.Clear()
        Dim Category4 As New SqlParameter("@Category", strCategory)
        parameters.Add(Category4)
        Dim intID4 As New SqlParameter("@intID", intlrqId)
        parameters.Add(intID4)

        strTemp = GetSpDataSingleValue("stpGetNextApprover", parameters.ToArray())

        If Len(strTemp) > 0 Then
            parameters.Clear()
            Dim usrId1 As New SqlParameter("@usrId", strTemp)
            parameters.Add(usrId1)

            objDR = GetSpDataReader("stpUserDetails", parameters.ToArray())

            If Not objDR.HasRows Then
                objDR.Close()
                objDR = Nothing
                'send to HR
                Return True
            End If

            objDR.Read()

            If strMailTemplate = "ALAA" Or strMailTemplate = "ALPAA" Or strMailTemplate = "ALCAA" Then
                strSql = "SELECT TOP 1 (SELECT usrFullName FROM tblUsers WHERE usrId = appApproverUsrId) As usrFullName FROM " & strApproversTable & " WHERE applrqId = " & intlrqId & " AND appstaId = 1 ORDER BY appOrder DESC"
                strPrevApprover = ReturnSingleDbValue(strSql)

                strMailMessage = GetMailTemplate(strMailTemplate, intcomId, "[NAME]|[APPROVER]|[URL]|[COMMENTS]|[PREAPPROVER]|[APPROVERCOMMENTS]", strFullName & "|" & objDR("usrFullName") & "|" & strURL & "||" & strPrevApprover & "|")
            Else
                strMailMessage = GetMailTemplate(strMailTemplate, intcomId, "[NAME]|[APPROVER]|[URL]|[COMMENTS]", strFullName & "|" & objDR("usrFullName") & "|" & strURL & "|")
            End If


            If Len(strMailMessage) > 10 Then
                strMailTitle = Mid(strMailMessage, 1, InStr(strMailMessage, "|}*") - 1)
                strMailMessage = Mid(strMailMessage, InStr(strMailMessage, "|}*") + 3)
                strMailMessage = Replace(strMailMessage, Chr(13), "<br />")
                strMailMessage = Replace(strMailMessage, vbCrLf, "<br />")
                SendMail(strMailTitle, strMailMessage, objDR("usrEmailAdd"))
            End If
            objDR.Close()

            parameters.Clear()
            Dim Category5 As New SqlParameter("@Category", strCategory)
            parameters.Add(Category5)
            Dim intID5 As New SqlParameter("@intID", intlrqId)
            parameters.Add(intID5)
            Dim appApproverUsrId As New SqlParameter("@appApproverUsrId", strTemp)
            parameters.Add(appApproverUsrId)

            ExecuteStoredProc("stpUpdateApproverAlerted", parameters.ToArray())
        End If

        objDR = Nothing
        Return True
    End Function

    Public Shared Function GetApproversQuery(ByVal intcomId As Integer, ByVal intbraId As Integer, ByVal intdptId As Integer, ByVal intsecId As Integer, ByVal intposId As Integer, ByVal intusrId As Integer, ByVal strApprovalPath As String) As String
        Dim strSql As String = ""
        Dim strQuery As String
        Dim strSpecificQuery As String = ""
        Dim bolSpecificDone As Boolean = False
        Dim strTemp As String
        Dim parameters As New List(Of SqlParameter)()
        Dim strEnableSections As String = GetConfig("EnableSections")
        Dim strEnableBranches As String = GetConfig("EnableBranches")
        Dim usrId As New SqlParameter("@usrId", intusrId)
        parameters.Add(usrId)

        'Get Approvers Propagating Down
        'strSql = "SELECT apsApproverUsrId, apsFinalApprover, apsApprovalOrder FROM tblApprovalSetup WHERE apsdptId = " & intdptId & " AND apssecId = " & intsecId & " AND apsposId = " & intposId & " AND (apsusrId = 0 OR apsusrId = " & intusrId & ") ORDER BY apsApprovalOrder"

        'GET SPECIFIC

        'Get User Specific Approvers

        strSql = "SELECT apsApproverUsrId, apsFinalApprover, apsApprovalOrder FROM tblApprovalSetup WHERE apsusrId = " & intusrId
        strTemp = GetSpDataSingleValue("stpGetApprovers_User", parameters.ToArray())
        If Len(strTemp) > 0 Then
            strSpecificQuery = strSql
            bolSpecificDone = True
        End If

        'Get Section Specific Approvers (with or without branch/with or without position)
        If Not bolSpecificDone And strEnableSections = "Y" Then
            parameters.Clear()
            Dim secId As New SqlParameter("@secId", intsecId)
            parameters.Add(secId)
            Dim posId As New SqlParameter("@posId", intposId)
            parameters.Add(posId)
            Dim braId As New SqlParameter("@braId", intbraId)
            parameters.Add(braId)

            strSql = "SELECT apsApproverUsrId, apsFinalApprover, apsApprovalOrder FROM tblApprovalSetup WHERE apssecId = " & intsecId & " AND (apsposId = " & intposId & " OR apsposId = 0) AND apsusrId = 0 AND (apsbraId = " & intbraId & " OR apsbraId = 0)"
            strTemp = GetSpDataSingleValue("stpGetApprovers_Section", parameters.ToArray())
            If Len(strTemp) > 0 Then
                strSpecificQuery = strSql
                bolSpecificDone = True
            End If
        End If

        'Get Non Global Company/ Department Specific Approvers (with or without branch)
        If Not bolSpecificDone Then
            parameters.Clear()
            Dim dptId As New SqlParameter("@dptId", intdptId)
            parameters.Add(dptId)
            Dim posId2 As New SqlParameter("@posId", intposId)
            parameters.Add(posId2)
            Dim braId2 As New SqlParameter("@braId", intbraId)
            parameters.Add(braId2)
            Dim Sections As New SqlParameter("@Sections", "N")
            parameters.Add(Sections)
            Dim Branches As New SqlParameter("@Branches", "N")
            parameters.Add(Branches)

            strSql = "SELECT apsApproverUsrId, apsFinalApprover, apsApprovalOrder FROM tblApprovalSetup"
            strSql &= " WHERE apsdptId = " & intdptId
            strSql &= " AND (apssecId = 0 OR '" & strEnableSections & "' = 'N')"
            strSql &= " AND (apsposId = " & intposId & " OR apsposId = 0)"
            strSql &= " AND apsusrId = 0"
            strSql &= " AND (apsbraId = " & intbraId & " OR apsbraId = 0 OR '" & strEnableBranches & "' = 'N')"

            strTemp = GetSpDataSingleValue("stpGetApprovers_Department", parameters.ToArray())
            If Len(strTemp) > 0 Then
                strSpecificQuery = strSql
                bolSpecificDone = True
            End If
        End If


        If Len(strSpecificQuery) > 0 Then
            'strQuery = strSpecificQuery & " UNION "
            strQuery = strSpecificQuery
        Else
            strQuery = ""
        End If


        'Disabling Global Approvers for now since it seems to be a confusing concept

        'Append Department Global Approvers
        'strQuery &= " SELECT apsApproverUsrId, apsFinalApprover, apsApprovalOrder FROM tblApprovalSetup WHERE apsdptId = " & intdptId & " AND (apsbraId = " & intbraId & " OR apsbraId = 0) AND apsPropagate = 1 AND apssecId= 0 AND (apsposId = " & intposId & " OR apsposId = 0) AND apsusrId = 0"

        'Append Company Global Approvers
        'strQuery &= " UNION SELECT apsApproverUsrId, apsFinalApprover, apsApprovalOrder FROM tblApprovalSetup WHERE apscomId = " & intcomId & " AND apsdptId = 0 AND (apsbraId = " & intbraId & " OR apsbraId = 0) AND apsPropagate = 1 AND apssecId= 0 AND (apsposId = " & intposId & " OR apsposId = 0) AND apsusrId = 0"

        'Append Branch Global Approvers
        'If strEnableBranches = "Y" Then
        'strQuery &= " UNION SELECT apsApproverUsrId, apsFinalApprover, apsApprovalOrder FROM tblApprovalSetup WHERE apsbraId = " & intbraId & " AND apsPropagate = 1 AND apsdptId = 0 AND apssecId= 0 AND (apsposId = " & intposId & " OR apsposId = 0) AND apsusrId = 0"
        'End If

        Return strQuery
    End Function

    Shared Sub LoadApprovers(ByVal intcomId As Integer, ByVal intbraId As Integer, ByVal intdptId As Integer, ByVal intsecId As Integer, ByVal intposId As Integer, ByVal intusrId As Integer, ByVal intlrqId As Integer, Optional ByVal strCategory As String = "LR")
        Dim strSql As String = ""
        Dim objDR As SqlDataReader
        Dim bolHasApprovers As Boolean = False
        Dim strFinalApprover As String = ""
        Dim strApprovalTable As String = ""
        Dim strApprovalPath As String = "AA" 'All Approvers
        Dim intCount As Integer
        Dim intLastAppId As Integer
        Dim intApprover As Integer
        Dim parameters As New List(Of SqlParameter)()
        Dim bolFinalApprover As Boolean
        
        Select Case strCategory
            Case "LC" 'Leave Compensation
                strApprovalTable = "tblApprovalPathCompensation"
                strApprovalPath = GetConfig("LeaveCompensationApproval")
            Case "LP" 'Leave Plan
                strApprovalTable = "tblApprovalPathPlans"
                strApprovalPath = GetConfig("LeavePlanApproval")
            Case "LR" 'Leave Request
                strApprovalTable = "tblApprovalPath"
        End Select

        '******* Check whether there is a setup in the dept, section, position, and/or user that applied
        Select Case strApprovalPath
            Case "AA", "ALL" 'Load All Approvers
                'strSql = "SELECT apsApproverUsrId, apsFinalApprover, apsApprovalOrder FROM tblApprovalSetup WHERE apsdptId = " & intdptId & " AND apssecId = " & intsecId & " AND apsposId = " & intposId & " AND (apsusrId = 0 OR apsusrId = " & intusrId & ") ORDER BY apsApprovalOrder"
                strSql = GetApproversQuery(intcomId, intbraId, intdptId, intsecId, intposId, intusrId, "AA")

            Case "FA" 'Load First Approver
                'strSql = "SELECT TOP 1 apsApproverUsrId, apsFinalApprover, apsApprovalOrder FROM tblApprovalSetup WHERE apsdptId = " & intdptId & " AND apssecId = " & intsecId & " AND apsposId = " & intposId & " AND (apsusrId = 0 OR apsusrId = " & intusrId & ") ORDER BY apsApprovalOrder"
                strSql = GetApproversQuery(intcomId, intbraId, intdptId, intsecId, intposId, intusrId, "FA")

            Case "HR" 'HR Issue
                Dim Category As New SqlParameter("@Category", strCategory)
                parameters.Add(Category)
                Dim intID As New SqlParameter("@intID", intlrqId)
                parameters.Add(intID)

                ExecuteStoredProc("stpCreateHRApproval", parameters.ToArray())
                Exit Sub
        End Select

        'System.Web.HttpContext.Current.Response.Write("HAPA:" & strApprovalPath & "|" & strCategory & "<br />")
        'System.Web.HttpContext.Current.Response.Write("HAPA 1: " & strApprovalPath & ":" & strSql & "<br />")

        objDR = GetDataReader(strSql)
        If objDR.HasRows Then
            bolHasApprovers = True
            intCount = 0

            While objDR.Read()

                bolFinalApprover = objDR("apsFinalApprover")
                intApprover = objDR("apsApproverUsrId")
                strFinalApprover = "0"
                If bolFinalApprover Then strFinalApprover = "1"
                If strApprovalPath = "FA" Then strFinalApprover = "1" 'First Approver is final approver
                intCount += 1

                If intApprover <> intusrId Then
                    'stpCreateApproval?
                    'System.Web.HttpContext.Current.Response.Write(bolFinalApprover & " + Category=" & strCategory & "*applrqId=" & intlrqId & "*appOrder=" & objDR("apsApprovalOrder") & "*appApproverUsrId=" & intApprover & "*appFinalApprover=" & strFinalApprover)
                    ExecuteStoredProc("stpCreateApproval", , "Category=" & strCategory & "*applrqId=" & intlrqId & "*appOrder=" & objDR("apsApprovalOrder") & "*appApproverUsrId=" & intApprover & "*appFinalApprover=" & strFinalApprover)
                End If

                If strApprovalPath = "FA" And intCount = 1 Then Exit Sub
            End While
        End If

        objDR.Close()

        parameters.Clear()
        Dim Category2 As New SqlParameter("@Category", strCategory)
        parameters.Add(Category2)
        Dim applrqId As New SqlParameter("@applrqId", intlrqId)
        parameters.Add(applrqId)

        objDR = GetSpDataReader("stpGetApprovers", parameters.ToArray())

        intCount = 1
        intLastAppId = 0
        While objDR.Read
            intLastAppId = objDR("appId") 'Last Approver
            'stpUpdateApproversOrder?
            strSql = "UPDATE " & strApprovalTable & " SET appOrder = " & intCount & " WHERE appId = " & intLastAppId
            ExecuteQuery(strSql)
            intCount += 1
        End While

        If intLastAppId > 0 Then 'Last Approver
            parameters.Clear()
            Dim Category3 As New SqlParameter("@Category", strCategory)
            parameters.Add(Category3)
            Dim applrqId1 As New SqlParameter("@applrqId", intlrqId)
            parameters.Add(applrqId1)

            ExecuteStoredProc("stpUpdateFinalApprovers", parameters.ToArray())

            parameters.Clear()
            Dim Category4 As New SqlParameter("@Category", strCategory)
            parameters.Add(Category4)
            Dim appld As New SqlParameter("@appld", intLastAppId)
            parameters.Add(appld)
            'Stored Proc not working for whatever reason!
            'ExecuteStoredProc("stpUpdateFinalApprovers", parameters.ToArray())

            strSql = "UPDATE " & strApprovalTable & " SET appFinalApprover = 1 WHERE appId = " & intLastAppId
            ExecuteQuery(strSql)


        End If

        objDR.Close()
        objDR = Nothing
    End Sub

    Shared Sub LoadClashingDates(ByVal lblCalendarEvents As Web.UI.WebControls.Label, ByVal intUser As Integer, ByVal datSelectedStartDate As Date, ByVal datSelectedEndDate As Date, Optional ByVal bolShowPlans As Boolean = False)
        Dim objDR As SqlDataReader
        Dim intYear As Integer
        Dim intMonth As Integer
        Dim intDay As Integer
        Dim bolRecurring As Boolean
        Dim strName As String
        Dim datDate As Date
        Dim bolShow As Boolean
        Dim datStartDate As Date
        Dim datEndDate As Date
        Dim strTemp As String
        Dim strSection As String
        Dim strBranch As String
        Dim parameters As New List(Of SqlParameter)()

        lblCalendarEvents.Text = "<table width='100%' cellpadding='3' cellspacing='0' border='1' bordercolor='#cccccc' style='border-collapse:collapse'>" & Chr(13)

        'Company Events
        lblCalendarEvents.Text &= " <tr>" & Chr(13)
        lblCalendarEvents.Text &= "     <td bgcolor='#eeeeee' align='center' colspan='3'><font color='#333333'><b>COMPANY EVENTS</b></font></td>" & Chr(13)
        lblCalendarEvents.Text &= " </tr>" & Chr(13)

        Dim usrId As New SqlParameter("@usrId", intUser)
        parameters.Add(usrId)
        Dim calStartDate As New SqlParameter("@calStartDate", datSelectedStartDate)
        parameters.Add(calStartDate)

        objDR = GetSpDataReader("stpGetUserCalendarEvents", parameters.ToArray())
        strTemp = ""
        While objDR.Read()
            bolShow = False
            datStartDate = objDR("calStartDate")
            datEndDate = objDR("calEndDate")

            If datStartDate >= datSelectedStartDate And datStartDate <= datSelectedEndDate Then bolShow = True
            If datEndDate >= datSelectedStartDate And datEndDate <= datSelectedEndDate Then bolShow = True

            If bolShow Then
                strName = "<b>" & objDR("calTitle") & "</b><br />" & objDR("calText")
                strTemp &= " <tr>" & Chr(13)
                strTemp &= "     <td bgcolor='#ffffff' align='left' width='40%'>" & strName & "</td>" & Chr(13)
                strTemp &= "     <td bgcolor='#ffffff' align='left' width='30%'>" & FormatDateTime(datStartDate, DateFormat.LongDate) & " <font color='blue'><i>" & objDR("calStartTime") & "</i></font></td>" & Chr(13)
                strTemp &= "     <td bgcolor='#ffffff' align='left' width='30%'>" & FormatDateTime(datEndDate, DateFormat.LongDate) & " <font color='blue'><i>" & objDR("calEndTime") & "</i></font></td>" & Chr(13)
                strTemp &= " </tr>" & Chr(13)
            End If
        End While
        objDR.Close()

        If Len(strTemp) > 0 Then
            lblCalendarEvents.Text &= " <tr>" & Chr(13)
            lblCalendarEvents.Text &= "     <td bgcolor='#cccccc' align='left' width='40%'><b>Event</b></td>" & Chr(13)
            lblCalendarEvents.Text &= "     <td bgcolor='#cccccc' align='left' width='30%'><b>Start Date</b></td>" & Chr(13)
            lblCalendarEvents.Text &= "     <td bgcolor='#cccccc' align='left' width='30%'><b>End Date</b></td>" & Chr(13)
            lblCalendarEvents.Text &= " </tr>" & Chr(13) & strTemp & Chr(13)
        Else
            lblCalendarEvents.Text &= " <tr>" & Chr(13)
            lblCalendarEvents.Text &= "     <td bgcolor='#ffffff' align='center' colspan='3'>There are no corporate events listed during the selected period</td>" & Chr(13)
            lblCalendarEvents.Text &= " </tr>" & Chr(13)
        End If

        'Other Department/ Section people on leave
        lblCalendarEvents.Text &= " <tr>" & Chr(13)
        lblCalendarEvents.Text &= "     <td bgcolor='#eeeeee' align='center' colspan='3'><font color='#333333'><b>OTHER PEOPLE ON LEAVE IN THE DEPARTMENT</b></font></td>" & Chr(13)
        lblCalendarEvents.Text &= " </tr>" & Chr(13)

        parameters.Clear()
        Dim usrId1 As New SqlParameter("@usrId", intUser)
        parameters.Add(usrId1)
        Dim lrcStartDate As New SqlParameter("@lrcStartDate", datSelectedStartDate)
        parameters.Add(lrcStartDate)
        objDR = GetSpDataReader("stpLeaveCalendar_UserDepartment", parameters.ToArray())

        strTemp = ""

        While objDR.Read()
            bolShow = False
            datStartDate = objDR("lrcStartDate")
            datEndDate = objDR("lrcEndDate")

            If datStartDate >= datSelectedStartDate And datStartDate <= datSelectedEndDate Then bolShow = True
            If datEndDate >= datSelectedStartDate And datEndDate <= datSelectedEndDate Then bolShow = True

            If bolShow Then
                strName = "<b>" & objDR("usrFullName") & "</b><br />" & objDR("ltyName")
                strSection = objDR("secName") & ""
                strBranch = objDR("braName") & ""
                If Len(strSection) > 0 Then strName &= " | Section: " & strSection
                If Len(strBranch) > 0 Then strName &= " | Branch: " & strBranch

                strTemp &= " <tr>" & Chr(13)
                strTemp &= "     <td bgcolor='#ffffff' align='left' width='40%'>" & strName & "</td>" & Chr(13)
                strTemp &= "     <td bgcolor='#ffffff' align='left' width='30%'>" & FormatDateTime(datStartDate, DateFormat.LongDate) & "</td>" & Chr(13)
                strTemp &= "     <td bgcolor='#ffffff' align='left' width='30%'>" & FormatDateTime(datEndDate, DateFormat.LongDate) & "</td>" & Chr(13)
                strTemp &= " </tr>" & Chr(13)
            End If
        End While
        objDR.Close()


        If Len(strTemp) > 0 Then
            lblCalendarEvents.Text &= " <tr>" & Chr(13)
            lblCalendarEvents.Text &= "     <td bgcolor='#cccccc' align='left' width='40%'><b>Name</b></td>" & Chr(13)
            lblCalendarEvents.Text &= "     <td bgcolor='#cccccc' align='left' width='30%'><b>Start Date</b></td>" & Chr(13)
            lblCalendarEvents.Text &= "     <td bgcolor='#cccccc' align='left' width='30%'><b>End Date</b></td>" & Chr(13)
            lblCalendarEvents.Text &= " </tr>" & Chr(13) & strTemp & Chr(13)
        Else
            lblCalendarEvents.Text &= " <tr>" & Chr(13)
            lblCalendarEvents.Text &= "     <td bgcolor='#ffffff' align='center' colspan='3'>There are no department/section workmates on leave during the selected period</td>" & Chr(13)
            lblCalendarEvents.Text &= " </tr>" & Chr(13)
        End If

        If bolShowPlans Then
            'Other Department/ Section people leave plans
            lblCalendarEvents.Text &= " <tr>" & Chr(13)
            lblCalendarEvents.Text &= "     <td bgcolor='#eeeeee' align='center' colspan='3'><font color='#333333'><b>OTHER PEOPLE WITH APPROVED LEAVE PLANS OVER THE SAME DATES IN THE DEPARTMENT</b></font></td>" & Chr(13)
            lblCalendarEvents.Text &= " </tr>" & Chr(13)

            parameters.Clear()
            Dim usrId2 As New SqlParameter("@usrId", intUser)
            parameters.Add(usrId2)
            Dim lepStartDate As New SqlParameter("@lrcStartDate", datSelectedStartDate)
            parameters.Add(lepStartDate)
            Dim CheckPlans As New SqlParameter("@CheckPlans", "YES")
            parameters.Add(CheckPlans)

            objDR = GetSpDataReader("stpLeaveCalendar_UserDepartment", parameters.ToArray())
            strTemp = ""

            While objDR.Read()
                bolShow = False
                datStartDate = objDR("lepStartDate")
                datEndDate = objDR("lepEndDate")

                If datStartDate >= datSelectedStartDate And datStartDate <= datSelectedEndDate Then bolShow = True
                If datEndDate >= datSelectedStartDate And datEndDate <= datSelectedEndDate Then bolShow = True

                If bolShow Then
                    strName = "<b>" & objDR("usrFullName") & "</b><br />" & objDR("ltyName")
                    strSection = objDR("secName") & ""
                    strBranch = objDR("braName") & ""
                    If Len(strSection) > 0 Then strName &= " | Section: " & strSection
                    If Len(strBranch) > 0 Then strName &= " | Branch: " & strBranch

                    strTemp &= " <tr>" & Chr(13)
                    strTemp &= "     <td bgcolor='#ffffff' align='left' width='40%'>" & strName & "</td>" & Chr(13)
                    strTemp &= "     <td bgcolor='#ffffff' align='left' width='30%'>" & FormatDateTime(datStartDate, DateFormat.LongDate) & "</td>" & Chr(13)
                    strTemp &= "     <td bgcolor='#ffffff' align='left' width='30%'>" & FormatDateTime(datEndDate, DateFormat.LongDate) & "</td>" & Chr(13)
                    strTemp &= " </tr>" & Chr(13)
                End If
            End While
            objDR.Close()


            If Len(strTemp) > 0 Then
                lblCalendarEvents.Text &= " <tr>" & Chr(13)
                lblCalendarEvents.Text &= "     <td bgcolor='#cccccc' align='left' width='40%'><b>Name</b></td>" & Chr(13)
                lblCalendarEvents.Text &= "     <td bgcolor='#cccccc' align='left' width='30%'><b>Start Date</b></td>" & Chr(13)
                lblCalendarEvents.Text &= "     <td bgcolor='#cccccc' align='left' width='30%'><b>End Date</b></td>" & Chr(13)
                lblCalendarEvents.Text &= " </tr>" & Chr(13) & strTemp & Chr(13)
            Else
                lblCalendarEvents.Text &= " <tr>" & Chr(13)
                lblCalendarEvents.Text &= "     <td bgcolor='#ffffff' align='center' colspan='3'>There are no department/section workmates with approved plans during the selected period</td>" & Chr(13)
                lblCalendarEvents.Text &= " </tr>" & Chr(13)
            End If

        End If

        'Public Holidays
        parameters.Clear()
        Dim ctyId As New SqlParameter("@ctyId", System.Web.HttpContext.Current.Session("ctyId"))
        parameters.Add(ctyId)
        objDR = GetSpDataReader("stpGetPublicHolidays_Detail", parameters.ToArray())

        lblCalendarEvents.Text &= " <tr>" & Chr(13)
        lblCalendarEvents.Text &= "     <td bgcolor='#eeeeee' align='center' colspan='3'><font color='#333333'><b>HOLIDAYS</b></font></td>" & Chr(13)
        lblCalendarEvents.Text &= " </tr>" & Chr(13)
        strTemp = ""

        While objDR.Read()
            intYear = objDR("pbhYear")
            intMonth = objDR("pbhMonth")
            intDay = objDR("pbhDay")
            bolRecurring = objDR("pbhRecurring")
            strName = objDR("pbhName") & ""

            datDate = DateSerial(intYear, intMonth, intDay)

            If bolRecurring Then
                datDate = DateSerial(Year(Now()), intMonth, intDay)
            End If

            If datDate >= datSelectedStartDate And datDate <= datSelectedEndDate Then
                strTemp &= " <tr>" & Chr(13)
                strTemp &= "     <td bgcolor='#ffffff' align='left' width='40%'>" & strName & "</td>" & Chr(13)
                strTemp &= "     <td bgcolor='#ffffff' align='left' width='30%'>" & FormatDateTime(datDate, DateFormat.LongDate) & "</td>" & Chr(13)
                strTemp &= "     <td bgcolor='#ffffff' align='left' width='30%'>&nbsp;</td>" & Chr(13)
                strTemp &= " </tr>" & Chr(13)
            End If
        End While

        objDR.Close()

        If Len(strTemp) > 0 Then
            lblCalendarEvents.Text &= " <tr>" & Chr(13)
            lblCalendarEvents.Text &= "     <td bgcolor='#cccccc' align='left' width='40%'><b>Holiday</b></td>" & Chr(13)
            lblCalendarEvents.Text &= "     <td bgcolor='#cccccc' align='left' width='30%'><b>Date</b></td>" & Chr(13)
            lblCalendarEvents.Text &= "     <td bgcolor='#cccccc' align='left' width='30%'><b>&nbsp;</b></td>" & Chr(13)
            lblCalendarEvents.Text &= " </tr>" & Chr(13) & strTemp & Chr(13)
        Else
            lblCalendarEvents.Text &= " <tr>" & Chr(13)
            lblCalendarEvents.Text &= "     <td bgcolor='#ffffff' align='center' colspan='3'>There are currently no holidays listed in the selected period</td>" & Chr(13)
            lblCalendarEvents.Text &= " </tr>" & Chr(13)
        End If

        lblCalendarEvents.Text &= "</table>" & Chr(13)


        objDR = Nothing

    End Sub
End Class
