﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class myprofile

    '''<summary>
    '''dtvProfile control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dtvProfile As Global.System.Web.UI.WebControls.DetailsView

    '''<summary>
    '''SqlProfile control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SqlProfile As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''grvList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grvList As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''SqlList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SqlList As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''grvLeaveDaysLog control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grvLeaveDaysLog As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''SqlLeaveDaysLog control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SqlLeaveDaysLog As Global.System.Web.UI.WebControls.SqlDataSource
End Class
