﻿Imports lvManLib.DataFunctions
Imports lvManLib.Security
Imports lvManLib.Menus

Partial Class doredirect
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim strMenuId As String
            Dim strRedirect As String
            strMenuId = Request.QueryString("mnu")


            If Len(strMenuId) = 0 Then Response.Redirect("/")
            If Not IsNumeric(strMenuId) Then Response.Redirect("/")

            strRedirect = GetRedirectFromId(strMenuId)

            If Len(strRedirect) = 0 Then Response.Redirect("/main.aspx?erm=nomnu") 'Menu Doesnt Exist

            If GetMenuPermissions(strMenuId) Then
                Response.Redirect(strRedirect)
            Else
                Response.Redirect("/main.aspx?erm=admnu") 'Access Denied
            End If


        End If
    End Sub

End Class
