﻿<%@ Page Title="change password | LeaveFlow" Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.changepassword" Codebehind="changepassword.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
    <style type="text/css">
        .style2
        {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">
    <table cellpadding="4" cellspacing="0" class="style2">
        <tr>
            <td colspan="2">
                <asp:Label ID="lblMessage" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td width="25%">
                Current Password:</td>
            <td width="75%">
                <asp:TextBox ID="txtPassword" runat="server" CssClass="textbox" 
                    TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="txtPassword" Display="Dynamic" ErrorMessage="***"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                New Password:</td>
            <td>
                <asp:TextBox ID="txtNewPassword" runat="server" CssClass="textbox" 
                    TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="txtNewPassword" Display="Dynamic" ErrorMessage="***"></asp:RequiredFieldValidator>                    
            </td>
        </tr>
        <tr>
            <td>
                Confirm Password:</td>
            <td>
                <asp:TextBox ID="txtConfPassword" runat="server" CssClass="textbox" 
                    TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="txtConfPassword" Display="Dynamic" ErrorMessage="***"></asp:RequiredFieldValidator>                    
                <asp:CompareValidator ID="CompareValidator1" runat="server" 
                    ControlToCompare="txtNewPassword" ControlToValidate="txtConfPassword" 
                    Display="Dynamic" ErrorMessage="** specified passwords do not match"></asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Button ID="cmdChangePassword" runat="server" CssClass="button" 
                    Text="Change Password" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>

