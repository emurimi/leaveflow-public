﻿Imports lvManLib.DataFunctions
Imports lvManLib.Security

Partial Class uploadrelateddocs
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            lblTransactionID.Text = GetTransactionID() 'should be requested
            lblTransactionID.Text = Request.QueryString("trn") & ""
            lblEntity.Text = Request.QueryString("ent") & "" 'shold be requested
            lblEntityID.Text = "0"

        End If
    End Sub

    Protected Sub FileUploadComplete(ByVal sender As Object, ByVal e As EventArgs)
        Dim strFileName As String = System.IO.Path.GetFileName(AsyncFileUpload1.FileName)
        Dim strFileExtension As String
        Dim strPath As String
        Dim strVirtualPath As String

        strFileExtension = Mid(strFileName, InStrRev(strFileName, ".") + 1)

        lblTitle.Text = Mid(strFileName, 1, InStrRev(strFileName, ".") - 1)
        Select Case lblEntity.Text
            Case "R" 'Leave Requests
                strVirtualPath = "/userDocs/requests/"
                strFileName = "LRQ"
            Case "C" 'Compensation Requests
                strVirtualPath = "/userDocs/compensation/"
                strFileName = "LCR"
            Case "P" 'Leave Plans
                strVirtualPath = "/userDocs/plans/"
                strFileName = "LEP"
            Case Else 'Other
                strVirtualPath = "/userDocs/other/"
                strFileName = "OTH"
        End Select


        strFileName &= Now.Ticks & "." & strFileExtension
        strPath = Server.MapPath(strVirtualPath) & strFileName
        AsyncFileUpload1.SaveAs(strPath)
        lblFileName.Text = strVirtualPath & strFileName
        SqlList.Insert()
        'Response.Redirect("uploadrelateddocs.aspx?ent=" & lblEntity.Text & "&trn=" & lblTransactionID.Text)
    End Sub
End Class
