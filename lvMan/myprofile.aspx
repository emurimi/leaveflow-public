﻿<%@ Page Title="LeaveFlow | my profile" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" Inherits="lvMan.myprofile" Codebehind="myprofile.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainHead" Runat="Server">
    <style type="text/css">
        .style2
        {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <table cellpadding="4" cellspacing="0" class="style2">
        <tr>
            <td style="width:50%">
                <b>Profile</b></td>
            <td style="width:50%">
                <b>Leave Days Log </b>(latest updates)</td>
        </tr>
        <tr>
            <td valign="top">
                <asp:DetailsView ID="dtvProfile" CellPadding="5" BorderColor="#EEEEEE" 
                    Width="100%" runat="server" AutoGenerateRows="False" 
                    DataSourceID="SqlProfile">
                    <Fields>
                        <asp:BoundField DataField="usrName" HeaderStyle-BackColor="#eeeeee" 
                            HeaderText="User Name:" SortExpression="usrName" >
                        <HeaderStyle BackColor="#EEEEEE"></HeaderStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="usrStaffID" HeaderStyle-BackColor="#eeeeee" 
                            HeaderText="Staff ID:" SortExpression="usrStaffID" >
                            <HeaderStyle BackColor="#EEEEEE"></HeaderStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="usrEmailAdd" HeaderStyle-BackColor="#eeeeee" 
                            HeaderText="Email Address:" SortExpression="usrEmailAdd" >
                            <HeaderStyle BackColor="#EEEEEE"></HeaderStyle>
                        </asp:BoundField>

                        <asp:BoundField DataField="usrFullName" HeaderStyle-BackColor="#eeeeee" 
                            HeaderText="Full Name:" SortExpression="usrFullName" >
                        <HeaderStyle BackColor="#EEEEEE"></HeaderStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="dptName" HeaderStyle-BackColor="#eeeeee" 
                            HeaderText="Department:" SortExpression="dptName" >
                        <HeaderStyle BackColor="#EEEEEE"></HeaderStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="secName" HeaderStyle-BackColor="#eeeeee" 
                            HeaderText="Section:" SortExpression="secName" >
                        <HeaderStyle BackColor="#EEEEEE"></HeaderStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="posName" HeaderStyle-BackColor="#eeeeee" 
                            HeaderText="Position:" SortExpression="posName" >
                        <HeaderStyle BackColor="#EEEEEE"></HeaderStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="graName" HeaderStyle-BackColor="#eeeeee" 
                            HeaderText="Grade:" SortExpression="graName" >
                        <HeaderStyle BackColor="#EEEEEE"></HeaderStyle>
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Gender:" SortExpression="usrSex">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("usrSex") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <InsertItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("usrSex") %>'></asp:TextBox>
                            </InsertItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# GetGender(DataBinder.Eval(Container.DataItem, "usrSex")) %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle BackColor="#EEEEEE" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status:" SortExpression="usrStatus">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("usrStatus") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <InsertItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("usrStatus") %>'></asp:TextBox>
                            </InsertItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# GetStatus(DataBinder.Eval(Container.DataItem, "usrStatus")) %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle BackColor="#EEEEEE" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="usrDateEmployed" HeaderStyle-BackColor="#eeeeee" 
                            HeaderText="Date Employed:" SortExpression="usrDateEmployed" 
                            DataFormatString="{0:dd MMM yyyy}" >
                            <HeaderStyle BackColor="#EEEEEE"></HeaderStyle>
                        </asp:BoundField>
                    </Fields>
                </asp:DetailsView>
                <asp:SqlDataSource ID="SqlProfile" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                    SelectCommandType="StoredProcedure"
                    SelectCommand="stpUsers_View">
                    <SelectParameters>
                        <asp:SessionParameter Name="usrId" SessionField="usrId" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <br /><br />
                <b>My Approvers:</b><br />
                <asp:GridView ID="grvList" Width="100%" CellPadding="4" BorderColor="#EEEEEE" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="SqlList">
                    <Columns>
                        <asp:TemplateField HeaderText="Approver">
                            <ItemTemplate>
                                <asp:Label ID="lblApprover" runat="server" Text='<%# GetUser(DataBinder.Eval(Container.DataItem, "apsApproverUsrId")) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Is Final Approver">
                            <ItemTemplate>
                                <asp:Label ID="lblFinalApprover" runat="server" Text='<%# FinalApprover(DataBinder.Eval(Container.DataItem, "apsFinalApprover")) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <RowStyle CssClass="greytable" />
                    <AlternatingRowStyle CssClass="whitetable" />
                    <PagerStyle CssClass="greytable" />
                    <HeaderStyle HorizontalAlign="Left" ForeColor="#FFFFFF" CssClass="tablecolors_title" />                            
                    <EmptyDataTemplate>
                        There are no approvers listed
                    </EmptyDataTemplate>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlList" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                    SelectCommand="SELECT [apsApprovalOrder], [apsFinalApprover], [apsApprovalOrder] FROM [tblApprovalSetup] WHERE 1=0">
                </asp:SqlDataSource>
            </td>
            <td valign="top">
                <asp:GridView ID="grvLeaveDaysLog" Width="100%" CellPadding="3" BorderColor="#eeeeee" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="SqlLeaveDaysLog">
                    <Columns>
                        <asp:BoundField DataField="ldlDate" HeaderText="Date" DataFormatString="{0:dd MMM yy}" SortExpression="ldlDate" />
                        <asp:BoundField DataField="ldlDescription" HeaderText="Description" SortExpression="ldlDescription" />
                        <asp:BoundField DataField="ldlUnits" ItemStyle-HorizontalAlign="Right" HeaderText="Units" DataFormatString="{0:N2}" SortExpression="ldlUnits" />
                        <asp:BoundField DataField="ldlBalance" ItemStyle-HorizontalAlign="Right" HeaderText="Balance" DataFormatString="{0:N2}" SortExpression="ldlBalance" />
                    </Columns>
                    <HeaderStyle BackColor="#cccccc" HorizontalAlign="Left" Font-Bold="True" />
                    <AlternatingRowStyle BackColor="#fcfcfc" />
                    <EmptyDataTemplate>
                        There are currently no records in your leave days log
                    </EmptyDataTemplate>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlLeaveDaysLog" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                    SelectCommandType="StoredProcedure"
                    SelectCommand="stpLeaveDaysLog_View">
                    <SelectParameters>
                        <asp:SessionParameter Name="usrId" SessionField="usrId" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
    </table>
</asp:Content>

