﻿Imports lvManLib.Config
Imports lvManLib.Security
Imports lvManLib.Menus
Imports lvManLib.DataFunctions
Imports System.Data.SqlClient

Partial Class MasterPage
    Inherits System.Web.UI.MasterPage


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not UserLoggedIn() Then Response.Redirect("/default.aspx?errm=tmo")

        If Not Page.IsPostBack Then
            Dim strCompanyName As String
            Dim strPage As String = LCase(HttpContext.Current.Request.ServerVariables("SCRIPT_NAME") & "")
            strCompanyName = GetConfig("CompanyName")
            lblCompany.Text = GetConfig("CompanyLogo")
            If Len(lblCompany.Text) > 0 Then
                lblCompany.Text = "<img alt='" & Replace(strCompanyName, "'", "") & "' title='" & Replace(strCompanyName, "'", "") & "' src='" & lblCompany.Text & "' />"
            Else
                lblCompany.Text = strCompanyName
            End If

            lblLoggedInAs.Text = Session("usrFullName")
            lblPageTitle.Text = GetPageName()

            Page.Title = lblPageTitle.Text & " | LeaveFlow"
            LoadLinks()

            If InStr(strPage, "/reports/") > 0 And InStr(strPage, "/rptfilter") = 0 Then
                trTitle.Visible = False
            End If

            If Not IsValidLicense() Then
                trContent.Visible = False
                trInvalid.Visible = True
            End If
        End If
    End Sub

    Sub LoadLinks()
        Dim strPage As String = LCase(Request.ServerVariables("SCRIPT_NAME"))
        If InStr(strPage, "/main.aspx") > 0 Then tdMain.Style.Add("background-image", "url('/images/topbluemainbg4.png')")
        If InStr(strPage, "leaverequest.aspx") > 0 Or InStr(strPage, "leaverecords.aspx") > 0 Or InStr(strPage, "leavemain.aspx") > 0 Then tdMyRequests.Style.Add("background-image", "url('/images/topbluemainbg4.png')")
        ''If InStr(strPage, "leavecompensation.aspx") > 0 Then tdLeaveCompensation.Style.Add("background-image", "url('/images/topbluemainbg4.png')")
        If InStr(strPage, "/approvals/") > 0 Then tdApprovals.Style.Add("background-image", "url('/images/topbluemainbg4.png')")
        If InStr(strPage, "/hradmin/") > 0 Then tdHRManagement.Style.Add("background-image", "url('/images/topbluemainbg4.png')")
        If InStr(strPage, "/sysadmin/") > 0 Then tdSystemAdmin.Style.Add("background-image", "url('/images/topbluemainbg4.png')")
        If InStr(strPage, "/reports/") > 0 Then tdReports.Style.Add("background-image", "url('/images/topbluemainbg4.png')")
        LoadDropDowns()
        LoadMenus()
        loadHrMenu()
        LoadReportsMenu()
    End Sub

    Sub LoadDropDowns()
        Dim strGroup As String = ""
        Dim strPrevGroup As String = ""
        Dim strSeperator As String = "<hr style='border: 1px dotted #D8E6F6' />"
        'Dim objDR As SqlDataReader
        Dim intCount As Integer = 0
        Dim strLeavePlans As String = GetConfig("EnableLeavePlans") & ""
        Dim strOrganizationType As String = GetConfig("OrganizationType") & "" '1 = Organization with many companies, 2 = Single company
        Dim strEnableBranches As String = GetConfig("EnableBranches") & ""
        Dim strEnableSections As String = GetConfig("EnableSections") & ""
        'Dim intMenuID As Integer
        Dim bolShowMenu As Boolean = True
        Dim intWidth As Integer = 820

        'My Leave
        litMyRequests.Text &= FormatDropDownLink("New Request", "/leaverequest.aspx")
        litMyRequests.Text &= FormatDropDownLink("My Requests", "/leavemain.aspx")
        litMyRequests.Text &= FormatDropDownLink("Leave Days Compensation", "/leavecompensation.aspx")
        litMyRequests.Text &= FormatDropDownLink("My Records", "/leaverecords.aspx", False)

        If Session("usrIsSystemAccount") & "" = "True" Then
            ppmMyRequests.Visible = False
            tdMyRequests.Visible = False
            tblLinks.Width -= 140
        End If

        If Session("Menu_SysAdmin") & "" <> "Y" Then
            tblLinks.Width -= 140
            tdSystemAdmin.Visible = False
        End If

        If Session("Menu_HRAdmin") & "" <> "Y" Then
            tblLinks.Width -= 140
            tdHRManagement.Visible = False
        End If

        If Session("Menu_Reports") & "" <> "Y" Then
            tblLinks.Width -= 140
            tdReports.Visible = False
        End If

        If Session("Menu_Approvals") & "" <> "Y" Then
            tdApprovals.Visible = False
            ppmApprovals.Visible = False
            tblLinks.Width -= 140
        End If



        'Approvals
        'check for permissions to approve allowances
        'strSql = "SELECT gmnmnuId FROM qryUserGroupMenus WHERE gmnmnuId = 55 AND ugpusrId = " & Session("usrId")
        'strTemp = ReturnSingleDbValue(strSql)

        litApprovals.Text &= FormatDropDownLink("Process Leave Requests", "/approvals/default.aspx")
        If strLeavePlans = "Y" Then litApprovals.Text &= FormatDropDownLink("Process Leave Plans", "/approvals/plans.aspx")
        litApprovals.Text &= FormatDropDownLink("Process Leave Compensation Requests", "/approvals/compensation.aspx")

    End Sub

    Sub LoadMenus()
        Dim strGroup As String = ""
        Dim strPrevGroup As String = ""
        Dim strSeperator As String = "<div style='padding-top:3px;padding-bottom:3px;'><hr style='border: 1px dashed #eeeeee;' /></div>"
        Dim objDR As SqlDataReader
        Dim intCount As Integer = 0
        Dim strLeavePlans As String = GetConfig("EnableLeavePlans") & ""
        Dim strOrganizationType As String = GetConfig("OrganizationType") & "" '1 = Organization with many companies, 2 = Single company
        Dim strEnableBranches As String = GetConfig("EnableBranches") & ""
        Dim strEnableSections As String = GetConfig("EnableSections") & ""
        Dim intMenuID As Integer
        Dim bolShowMenu As Boolean = True
        Dim intWidth As Integer = 820
        Dim parameters As New List(Of SqlParameter)()

        Dim mnuParent As New SqlParameter("@mnuParent", 6)
        parameters.Add(mnuParent)
        Dim usrId As New SqlParameter("@usrId", Session("usrId"))
        parameters.Add(usrId)

        objDR = GetSpDataReader("stpUser_Menus", parameters.ToArray())

        strGroup = ""
        strPrevGroup = "-"
        intCount = 0
        While objDR.Read
            intCount += 1
            intMenuID = objDR("mnuId")
            bolShowMenu = False
            strGroup = objDR("mnuGroup") & ""
            If strPrevGroup <> "-" And strGroup <> strPrevGroup Then litMenus.Text &= strSeperator
            If strPrevGroup <> "-" And strGroup <> strPrevGroup Then litMenus.Text &= "<b>" & strGroup & "</b><br />"

            Select Case intMenuID
                Case 17 'Companies
                    If strOrganizationType = "1" Then
                        bolShowMenu = True
                    Else
                        'Allow user to edit single company
                        litMenus.Text &= FormatDropDownLink("Company Setup", "/doredirect.aspx?mnu=" & intMenuID, False) & ""
                    End If

                Case 20 'Sections
                    If strEnableSections = "Y" Then
                        bolShowMenu = True
                    End If

                Case 48 'Branches
                    If strEnableBranches = "Y" Then
                        bolShowMenu = True
                    End If

                Case Else
                    bolShowMenu = True

            End Select

            If bolShowMenu Then litMenus.Text &= FormatDropDownLink(objDR("mnuName"), "/doredirect.aspx?mnu=" & intMenuID, False) & ""

            strPrevGroup = strGroup
        End While

        objDR.Close()
        objDR = Nothing

        'If intCount = 0 Then litMenus.Text = "No functions available to you at this time"
        If intCount = 0 Then ppmSystemAdmin.Visible = False
    End Sub

    Sub loadHrMenu()
        Dim strGroup As String = ""
        Dim strPrevGroup As String = ""
        Dim strSeperator As String = "<div style='padding-top:3px;padding-bottom:3px;'><hr style='border: 1px dashed #eeeeee;' /></div>"
        Dim objDR As SqlDataReader
        Dim intCount As Integer = 0
        Dim strLeavePlans As String = GetConfig("EnableLeavePlans") & ""
        Dim strOrganizationType As String = GetConfig("OrganizationType") & "" '1 = Organization with many companies, 2 = Single company
        Dim strEnableBranches As String = GetConfig("EnableBranches") & ""
        Dim strEnableSections As String = GetConfig("EnableSections") & ""
        Dim bolShowMenu As Boolean = True
        Dim intWidth As Integer = 820
        Dim parameters As New List(Of SqlParameter)()

        Dim mnuParent As New SqlParameter("@mnuParent", 1)
        parameters.Add(mnuParent)
        Dim usrId As New SqlParameter("@usrId", Session("usrId"))
        parameters.Add(usrId)

        objDR = GetSpDataReader("stpUser_Menus", parameters.ToArray())

        strGroup = ""
        strPrevGroup = ""
        intCount = 0
        While objDR.Read
            intCount += 1
            strGroup = objDR("mnuGroup") & ""
            If strPrevGroup <> "" And strGroup <> strPrevGroup Then hrMenus.Text &= strSeperator
            If strGroup <> strPrevGroup Then hrMenus.Text &= "<b>" & strGroup & "</b><br />"
            hrMenus.Text &= FormatDropDownLink(objDR("mnuName"), "/doredirect.aspx?mnu=" & objDR("mnuId"), False) & ""
            strPrevGroup = strGroup
        End While

        objDR.Close()
        objDR = Nothing

        'If intCount = 0 Then hrMenus.Text = "No functions available to you at this time"
        If intCount = 0 Then ppmHRManagement.Visible = False
    End Sub

    Sub LoadReportsMenu()
        Dim strGroup As String = ""
        Dim strPrevGroup As String = ""
        Dim strSeperator As String = "<div style='padding-top:3px;padding-bottom:3px;'><hr style='border: 1px dashed #eeeeee;' /></div>"
        Dim objDR As SqlDataReader
        Dim intCount As Integer = 0
        Dim strLeavePlans As String = GetConfig("EnableLeavePlans") & ""
        Dim strOrganizationType As String = GetConfig("OrganizationType") & "" '1 = Organization with many companies, 2 = Single company
        Dim strEnableBranches As String = GetConfig("EnableBranches") & ""
        Dim strEnableSections As String = GetConfig("EnableSections") & ""
        Dim bolShowMenu As Boolean = True
        Dim intWidth As Integer = 820
        Dim parameters As New List(Of SqlParameter)()

        Dim mnuParent As New SqlParameter("@mnuParent", 10)
        parameters.Add(mnuParent)
        Dim usrId As New SqlParameter("@usrId", Session("usrId"))
        parameters.Add(usrId)

        objDR = GetSpDataReader("stpUser_Menus", parameters.ToArray())

        strGroup = ""
        strPrevGroup = ""
        intCount = 0
        While objDR.Read
            intCount += 1
            strGroup = objDR("mnuGroup") & ""
            If strPrevGroup <> "" And strGroup <> strPrevGroup Then litReports.Text &= strSeperator
            If strGroup <> strPrevGroup Then litReports.Text &= "<b>" & strGroup & "</b><br />"
            litReports.Text &= FormatDropDownLink(objDR("mnuName"), "/doredirect.aspx?mnu=" & objDR("mnuId"), False) & ""
            strPrevGroup = strGroup
        End While

        objDR.Close()
        objDR = Nothing

        'If intCount = 0 Then hrMenus.Text = "No functions available to you at this time"
        If intCount = 0 Then ppmReports.Visible = False
    End Sub

    Function FormatDropDownLink(ByVal strName As String, ByVal strLink As String, Optional ByVal bolSeperator As Boolean = True) As String
        Dim strReturn As String = ""
        Dim strDiv As String = "menulinksdiv"
        If Not bolSeperator Then strDiv = "menulinksdiv_noline"
        strReturn = "<div class='" & strDiv & "'>"
        strReturn &= "<a href='" & strLink & "' class='menulinks'>" & strName & "</a>"
        strReturn &= "</div>"
        strReturn &= Chr(13)

        Return strReturn
    End Function
End Class

