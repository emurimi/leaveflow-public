﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="lvMan.lvCalendar" Codebehind="lvCalendar.ascx.vb" %>
<asp:TextBox id="txtDate" CssClass="textbox" runat="server"></asp:TextBox>
<input type="button" value="..." onclick="OnClick()" class="button" />
<br />
<div id="divCalendar" style="display:none;position:absolute;">
    <asp:Calendar ID="calCalendar" runat="server" BackColor="#FFFFCC" BorderColor="#FFCC66" 
        BorderWidth="1px" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" 
        ForeColor="#663399" Height="200px" ShowGridLines="True" Width="220px">
        <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True" />
        <TodayDayStyle BackColor="#FFCC66" ForeColor="White" />
        <SelectorStyle BackColor="#FFCC66" />
        <OtherMonthDayStyle ForeColor="#CC9966" />
        <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC" />
        <DayHeaderStyle BackColor="#FFCC66" Font-Bold="True" Height="1px" />
        <TitleStyle BackColor="#990000" Font-Bold="True" Font-Size="9pt" 
            ForeColor="#FFFFCC" />
    </asp:Calendar>
</div>
<script type="text/javascript">
    function OnClick()
    {
      if(divCalendar.style.display == "none")
        divCalendar.style.display = "";
      else
        divCalendar.style.display = "none";
    }
</script>
