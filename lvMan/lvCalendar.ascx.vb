﻿
Partial Class lvCalendar
    Inherits System.Web.UI.UserControl

    Protected Sub calCalendar_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles calCalendar.SelectionChanged
        txtDate.Text = calCalendar.SelectedDate.ToShortDateString()
        Dim div As System.Web.UI.Control = Page.FindControl("divCalendar")

        If TypeOf div Is HtmlGenericControl Then
            CType(div, HtmlGenericControl).Style.Add("display", "none")
        End If
    End Sub
End Class
