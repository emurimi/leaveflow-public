﻿<%@ Page Title="LeaveFlow | leave compensation" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" Inherits="lvMan.leavecompensation" Codebehind="leavecompensation.aspx.vb" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
<asp:UpdatePanel ID="upnAddEdit" runat="server"> 
    <ContentTemplate>
        </ContentTemplate>   
    </asp:UpdatePanel>      
        <table cellpadding="3" cellspacing="0" width="100%">
            <tr id="trCommands" runat="server">
                <td>
                    <table width="290" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td id="tdView" runat="server" width="144" height="29" align="center" valign="middle" background='/images/orangetab.png'>
                            <asp:LinkButton ID="cmdViewConfigs" CssClass="whitetext" runat="server">In Process</asp:LinkButton></td>
                        <td width="2"></td>
                        <td id="tdAddEdit" runat="server" width="144" align="center" valign="middle" background='/images/darkbluetab.png'>
                        <asp:LinkButton ID="cmdAddConfig" CssClass="whitetext" runat="server">New Request</asp:LinkButton>
                        </td>
                      </tr>
                    </table>                    
                </td>
            </tr>        
            <tr>
                <td>
                    <asp:Label ID="lblMessage" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                    <asp:Panel ID="panList" runat="server" Width="65%">
                        <asp:GridView ID="grvList" BorderColor="#EEEEEE" Width="100%" runat="server" 
                            AutoGenerateColumns="False" CellPadding="4" DataKeyNames="lcoId" DataSourceID="SqlList" 
                            AllowPaging="True" AllowSorting="True">
                            <Columns>
                                <asp:BoundField Visible="False" DataField="lcoId" HeaderText="lcoId" InsertVisible="False" ReadOnly="True" SortExpression="lcoId" />
                                <asp:BoundField DataField="lcoUnits" HeaderText="Units" SortExpression="lcoUnits" />
                                <asp:BoundField DataField="areName" HeaderText="Reason" SortExpression="areName" />
                              <asp:TemplateField HeaderText="Status" SortExpression="staName">
                                  <ItemTemplate>
                                      <asp:Label ID="Label2" runat="server" Text='<%# GetStatus(DataBinder.Eval(Container.DataItem, "staName")) %>'></asp:Label>
                                  </ItemTemplate>
                              </asp:TemplateField>                                
                                <asp:BoundField DataField="lcoDate" HeaderText="Date" DataFormatString="{0:d}" SortExpression="lcoDate" />
                                <asp:CommandField ItemStyle-Font-Bold="True" SelectText="View/Edit" ShowSelectButton="True" />
                            </Columns>
                            <RowStyle CssClass="greytable" />
                            <AlternatingRowStyle CssClass="whitetable" />
                            <PagerStyle CssClass="greytable" />
                            <HeaderStyle HorizontalAlign="Left" ForeColor="#FFFFFF" CssClass="tablecolors_title" />                            

                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlList" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                            InsertCommandType="StoredProcedure"
                            DeleteCommandType="StoredProcedure"
                            SelectCommandType="StoredProcedure"
                            UpdateCommandType="StoredProcedure"
                            DeleteCommand="stpLeaveCompensation_Delete" 
                            InsertCommand="stpLeaveCompensation_Add" 
                            SelectCommand="stpLeaveCompensationRequests_View" 
                            UpdateCommand="stpLeaveCompensation_Update">
                            <SelectParameters>
                                <asp:SessionParameter Name="usrId" SessionField="usrId" Type="Int32" />
                            </SelectParameters>
                            <DeleteParameters>
                                <asp:Parameter Name="lcoId" Type="Int32" />
                            </DeleteParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="lcostaId" DefaultValue="1" Type="Int32" />
                                <asp:ControlParameter ControlID="txtDays" PropertyName="Text" Name="lcoUnits" Type="Double" />
                                <asp:ControlParameter ControlID="lstReasons" PropertyName="SelectedValue" Name="lcoareId" Type="Int32" />
                                <asp:ControlParameter ControlID="txtDescription" PropertyName="Text" Name="lcoReasonDesc" Type="String" />
                                <asp:ControlParameter ControlID="grvList" PropertyName="SelectedValue" Name="lcoId" Type="Int32" />
                            </UpdateParameters>
                            <InsertParameters>
                                <asp:SessionParameter SessionField="usrId" Name="lcousrId" Type="Int32" />
                                <asp:ControlParameter ControlID="lblTransactionID" PropertyName="Text" Name="lcoTransactionID" Type="String" />
                                <asp:ControlParameter ControlID="txtDays" PropertyName="Text" Name="lcoUnits" Type="Double" />
                                <asp:ControlParameter ControlID="lstReasons" PropertyName="SelectedValue" Name="lcoareId" Type="Int32" />
                                <asp:ControlParameter ControlID="txtDescription" PropertyName="Text" Name="lcoReasonDesc" Type="String" />
                                <asp:Parameter DefaultValue="1" Name="lcostaId" Type="Int32" />
                            </InsertParameters>
                        </asp:SqlDataSource>
                    </asp:Panel>
                    
                    <asp:Panel ID="panAddEdit" runat="server" Width="100%" Visible="False">
                      <table width="500" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td valign="top">                    
                                <table cellpadding="3" id="tblAddEdit" runat="server" cellspacing="0" width="100%" class="tablewithborders">
                                    <tr id="trReason" runat="server">
                                        <td width="50%">
                                            Reason:<asp:Label ID="lblTransactionID" runat="server" Visible="False"></asp:Label>
                                        </td>
                                        <td width="50%">
                                            <asp:DropDownList ID="lstReasons" runat="server"  
                                                CssClass="listmenu" DataSourceID="SqlReason" DataTextField="areName" 
                                                DataValueField="areId">
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="SqlReason" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                SelectCommandType="StoredProcedure"
                                                SelectCommand="stpAdjustmentReasons_View">
                                            </asp:SqlDataSource>
                                        </td>
                                    </tr>
                                    <tr id="trDays" runat="server">
                                        <td width="50%">
                                            <asp:Label ID="lblUnits" runat="server" Text="Days:"></asp:Label>
                                        </td>
                                        <td width="50%">
                                            <asp:TextBox ID="txtDays" runat="server" CssClass="textbox"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rvaName" runat="server" 
                                                ControlToValidate="txtDays" Display="Dynamic" ErrorMessage="***" 
                                                ValidationGroup="AddItem"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr id="trDescription" runat="server">
                                        <td width="50%">
                                            Description:</td>
                                        <td width="50%">
                                            <asp:TextBox ID="txtDescription" Columns="30" Rows="5" TextMode="MultiLine" runat="server" CssClass="textbox"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                ControlToValidate="txtDescription" Display="Dynamic" ErrorMessage="***" 
                                                ValidationGroup="AddItem"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>      
                                    <tr id="trViewReason" runat="server" visible="False">
                                       <td width="50%">
                                            Reason:</td>
                                        <td width="50%">
                                            <asp:Label ID="lblReason" runat="server"></asp:Label>
                                        </td>
                                    </tr>                      
                                    <tr id="trViewDays" runat="server" visible="False">
                                        <td width="50%">
                                            <asp:Label ID="lblViewUnits" runat="server" Text="Days:"></asp:Label></td>
                                        <td width="50%">
                                            <asp:Label ID="lblDays" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr id="trViewDescription" runat="server" visible="False">
                                        <td width="50%">
                                            Description:</td>
                                        <td width="50%">
                                            <asp:Label ID="lblDescription" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="50%">
                                            Status:</td>
                                        <td width="50%">
                                            <asp:Label ID="lblStatus" runat="server" Text="Pending"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="50%">
                                            Related Document(s):</td>
                                        <td width="50%">
                                            <asp:Label ID="lblRelatedDocuments" runat="server" Font-Bold="True" 
                                                Text="Upload Related Documents"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="width: 100%; text-align: center" width="50%">
                                            <asp:GridView ID="grvList0" runat="server" AutoGenerateColumns="False" 
                                                BorderColor="White" DataKeyNames="ldcId" DataSourceID="SqlList0" 
                                                GridLines="None" ShowHeader="False" Width="70%">
                                                <Columns>
                                                    <asp:BoundField DataField="ldcId" HeaderStyle-HorizontalAlign="Left" 
                                                        HeaderText="ldcId" InsertVisible="False" ReadOnly="True" SortExpression="ldcId" 
                                                        Visible="False">
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="Title" SortExpression="ldcTitle">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("ldcTitle") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label1" runat="server" 
                                                                Text='<%# GetDocument(DataBinder.Eval(Container.DataItem, "ldcTitle"), DataBinder.Eval(Container.DataItem, "ldcPath"))  %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:CommandField EditText="Edit Title" ShowDeleteButton="True" 
                                                        ShowEditButton="True">
                                                        <ItemStyle Font-Bold="True" />
                                                    </asp:CommandField>
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    There are no files attached to this request
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                            <asp:SqlDataSource ID="SqlList0" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                DeleteCommandType="StoredProcedure"
                                                SelectCommandType="StoredProcedure"
                                                UpdateCommandType="StoredProcedure"
                                                DeleteCommand="stpLeaveDocuments_Delete" 
                                                SelectCommand="stpLeaveDocuments_View" 
                                                UpdateCommand="stpLeaveDocuments_Update">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="lblTransactionID" Name="ldcTransactionID" PropertyName="Text" Type="String" />
                                                    <asp:Parameter DefaultValue="C" Name="ldcEntity" Type="String" />
                                                </SelectParameters>
                                                <DeleteParameters>
                                                    <asp:Parameter Name="ldcId" Type="Int32" />
                                                </DeleteParameters>
                                                <UpdateParameters>
                                                    <asp:Parameter Name="ldcTitle" Type="String" />
                                                    <asp:Parameter Name="ldcId" Type="Int32" />
                                                </UpdateParameters>
                                            </asp:SqlDataSource>
                                        </td>
                                    </tr>
                                    <tr id="trConfirmRow" runat="server" visible="False">
                                        <td style="text-align: right">
                                            <asp:Button ID="cmdCancelConfirm" runat="server" CausesValidation="False" 
                                                CssClass="button2" Text="&lt;&lt; Revise Request" />
                                        </td>
                                        <td>
                                            <asp:Button ID="cmdConfirm" runat="server" CssClass="button2" 
                                                Text="Confirm - Add Request" />
                                        </td>
                                    </tr>
                                    <tr id="trAddRow" runat="server">
                                        <td style="text-align: right">
                                            <asp:Button ID="cmdCancel" runat="server" CausesValidation="False" 
                                                CssClass="button" Text="Cancel" />
                                        </td>
                                        <td>
                                            <asp:Button ID="cmdUpdate" runat="server" CssClass="button" Text="Add Request" 
                                                ValidationGroup="AddItem" />
                                        </td>
                                    </tr>
                                    <tr id="trApprovals1" runat="server">
                                        <td colspan="2"><b>Approval Details</b></td>
                                    </tr>
                                    <tr id="trApprovals2" runat="server">
                                        <td colspan="2">
                                            <asp:GridView ID="grvApprovals" BorderColor="#EEEEEE" runat="server" AutoGenerateColumns="False" 
                                                DataSourceID="SqlApprovalPath" Width="100%">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Approver">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblFullName" runat="server" Text='<%# GetApprover(DataBinder.Eval(Container.DataItem, "usrFullName") & "") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                  <asp:TemplateField HeaderText="Status" SortExpression="staName">
                                                      <ItemTemplate>
                                                          <asp:Label ID="Label2" runat="server" Text='<%# GetStatus(DataBinder.Eval(Container.DataItem, "staName")) %>'></asp:Label>
                                                      </ItemTemplate>
                                                  </asp:TemplateField>
                                                    <asp:BoundField DataField="appDateChanged" HeaderText="Date Updated" SortExpression="appDateChanged" />
                                                    <asp:BoundField DataField="appComments" HeaderText="Comments" SortExpression="appComments" />
                                                </Columns>
                                                  <RowStyle CssClass="greytable" />
                                                  <AlternatingRowStyle CssClass="whitetable" />
                                                  <PagerStyle CssClass="greytable" />
                                                  <HeaderStyle HorizontalAlign="Left" CssClass="tablecolors_title" />
                                                
                                            </asp:GridView>
                                            <asp:SqlDataSource ID="SqlApprovalPath" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                SelectCommandType="StoredProcedure"
                                                SelectCommand="stpGetApprovalPathStatus">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="grvList" Name="lrqId" PropertyName="SelectedValue" Type="Int32" />
                                                    <asp:Parameter Name="Category" DefaultValue="LC" Type="String" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>                                        
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                </table>
                              </td>
                            </tr>
                          </table>    
                         
                    </asp:Panel>    
                    
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
        </table>

    
</asp:Content>

