<%@ Page Title="LeaveFlow | leave request" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" Inherits="lvMan.leaverequest" Codebehind="leaverequest.aspx.vb" %>
<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Ajax.Net" Namespace="RJS.Web.WebControl" TagPrefix="rjs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainHead" Runat="Server">
    <style type="text/css">
        .style2
        {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <table cellpadding="5" cellspacing="0" width="100%" align="center">
        <tr>
            <td>
                <table width="436" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="144" height="29" align="center" valign="middle" background='/images/darkbluetab.png'>
                        <a href="leavemain.aspx" class="whitetext">My Requests</a></td>
                    <td width="2"></td>
                    <td width="144" height="29" align="center" valign="middle" background='/images/orangetab.png'>
                        <span class="whitetext">New Request</span>
                    </td>
                    <td width="2"></td>
                    <td width="144" height="29" align="center" valign="middle" background='/images/darkbluetab.png'>
                        <a href="leaverecords.aspx" class="whitetext">My Records</a>
                    </td>                
                  </tr>
                </table>             
            </td>
        </tr>
        <tr>
            <td>
                <table width="65%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="15"><img src="/images/lefttop.jpg" alt="LeaveFlow" width="15" height="15" /></td>
                      <td height="15" background="/images/topside.jpg">&nbsp;</td>
                      <td width="15"><img src="/images/righttop.jpg" alt="LeaveFlow" width="15" height="15" /></td>
                    </tr>
                    <tr>
                      <td background="/images/leftside.jpg">&nbsp;</td>
                      <td valign="top">
                        <asp:Label ID="lblExtraMessage" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                        <table cellpadding="0" cellspacing="0" width="100%" align="center">
                            <tr id="trApplication" runat="server">
                                <td>
                                    <table cellpadding="4" cellspacing="0" width="100%" align="center">
                                      <tr>
                                        <td colspan="2">
                                        <asp:Label ID="lblMessage" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                        </td>
                                      </tr>
                                      <tr id="trApplyFor" runat="server" Visible="False">
                                        <td width="50%">Application for:
                                        <asp:Label ID="lblTransactionID" runat="server" Visible="False"></asp:Label>
                                        </td>
                                        <td width="50%">
                                            <asp:DropDownList ID="lstApplicationFor" runat="server" CssClass="listmenu" 
                                                AutoPostBack="True" DataSourceID="SqlUsers" DataTextField="usrFullName" 
                                                DataValueField="usrId">
                                              </asp:DropDownList>
                                            <asp:SqlDataSource ID="SqlUsers" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                SelectCommandType="StoredProcedure"
                                                SelectCommand="stpUsers_MinView">
                                               </asp:SqlDataSource>
                                        </td>
                                      </tr>
                                      <tr>
                                          <td width="50%">
                                              Leave type&nbsp;</td>
                                          <td width="50%">
                                              <asp:DropDownList ID="lstLeaveType" DataTextField="ltyName" DataValueField="ltyId" runat="server" CssClass="listmenu" 
                                                  AutoPostBack="True" DataSourceID="SqlLeaveTypes">
                                              </asp:DropDownList>
                                              <asp:SqlDataSource ID="SqlLeaveTypes" runat="server" 
                                                  ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                  SelectCommandType="StoredProcedure"
                                                  SelectCommand="stpLeaveTypes_View">
                                                  <SelectParameters>
                                                      <asp:SessionParameter Name="ltySex" SessionField="usrSex" Type="String" />
                                                       <asp:SessionParameter Name="ltycomId" SessionField="comid" Type="Int32" />
                                                  </SelectParameters>
                                              </asp:SqlDataSource>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              Leave <asp:Label ID="lblLeaveUnits" runat="server"></asp:Label> available</td>
                                          <td>
                                              <asp:Label ID="lblDaysAvailable" runat="server"></asp:Label>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              Start date</td>
                                          <td>
                                              <asp:TextBox ID="txtStartDate" ReadOnly="True" runat="server" CssClass="textbox"></asp:TextBox>
                                              <rjs:PopCalendar ID="ppcStartDate" runat="server" Control="txtStartDate" 
                                                  AutoPostBack="False" Format="mm/dd/yyyy" HolidayMessage="This date is not available" 
                                                  Separator="/" SelectHoliday="False" />
                                              <asp:DropDownList ID="lstStartTime" CssClass="listmenu" runat="server"></asp:DropDownList>
                                              <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                  ControlToValidate="txtStartDate" ErrorMessage="**"></asp:RequiredFieldValidator>
                                              <rjs:PopCalendarMessageContainer ID="pmcSTartDate" runat="server" Calendar="ppcStartDate" />
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              End date</td>
                                          <td>
                                              <asp:TextBox ID="txtEndDate" ReadOnly="True" runat="server" CssClass="textbox"></asp:TextBox>
                                              <rjs:PopCalendar ID="ppcEndDate" runat="server" Control="txtEndDate" 
                                                  AutoPostBack="False" Format="mm/dd/yyyy" HolidayMessage="This date is not available" 
                                                  Separator="/" ShowWeekend="True" />
                                              <asp:DropDownList ID="lstEndTime" CssClass="listmenu" runat="server"></asp:DropDownList>
                                              <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                  ControlToValidate="txtEndDate" ErrorMessage="**"></asp:RequiredFieldValidator>
                                              <rjs:PopCalendarMessageContainer ID="pmcEndDate" runat="server" Calendar="ppcEndDate" /> 
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              <asp:Label ID="lblLeaveUnits2" runat="server"></asp:Label> deductible</td>
                                          <td>
                                              <asp:Label ID="lblDays" runat="server" Font-Bold="True"></asp:Label>
                                              <asp:Label ID="lblDaysDesc" runat="server"></asp:Label>
                                              <asp:Button ID="cmdCalculate" runat="server" CausesValidation="False" 
                                                  CssClass="button" Text="calculate &gt;&gt;" />
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              To be relieved by</td>
                                          <td>
                                              <asp:DropDownList ID="lstReliever" runat="server" CssClass="listmenu" 
                                                  DataSourceID="SqlRelievers" DataTextField="usrFullName" DataValueField="usrId">
                                              </asp:DropDownList>
                                              <asp:SqlDataSource ID="SqlRelievers" runat="server" 
                                                  ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                  SelectCommandType="StoredProcedure"
                                                  SelectCommand="stpUsers_MinView">
                                                  <SelectParameters>
                                                      <asp:SessionParameter Name="usrdptId" SessionField="dptId" Type="Int32" />
                                                      <asp:SessionParameter Name="usrId_Hide" SessionField="usrId" Type="Int32" />
                                                  </SelectParameters>
                                              </asp:SqlDataSource>
                                          </td>
                                      </tr>
                                    <tr>
                                          <td>
                                              Contacts</td>
                                          <td>
                                              <asp:TextBox ID="txtContacts" runat="server" Columns="30" CssClass="textbox" 
                                                  Rows="5" TextMode="MultiLine"></asp:TextBox>
                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                                  ControlToValidate="txtContacts" ErrorMessage="**"></asp:RequiredFieldValidator>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              Comments</td>
                                          <td>
                                              <asp:TextBox ID="txtComments" runat="server" Columns="30" CssClass="textbox" 
                                                  Rows="5" TextMode="MultiLine"></asp:TextBox>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              Related Document(s):</td>
                                          <td>
                                              <asp:Label ID="lblRelatedDocuments" runat="server" Font-Bold="True"
                                                  Text="Upload Related Documents"></asp:Label>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              &nbsp;</td>
                                          <td>
                                              &nbsp;</td>
                                      </tr>
                                      <tr>
                                          <td>
                                              <asp:SqlDataSource ID="SqlList" runat="server" 
                                                  ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                  InsertCommandType="StoredProcedure"
                                                  DeleteCommandType="StoredProcedure"
                                                  SelectCommandType="StoredProcedure"
                                                  UpdateCommandType="StoredProcedure"
                                                  DeleteCommand="stpLeaveRequests_Delete" 
                                                  InsertCommand="stpLeaveRequests_Add" 
                                                  SelectCommand="stpMyLeaveRequests_View" 
                                                  UpdateCommand="stpLeaveRequests_Update">
                                                  <SelectParameters>
                                                      <asp:Parameter DefaultValue="-1" Name="lrqId" Type="Int32" />
                                                  </SelectParameters>
                                                  <InsertParameters>
                                                      <asp:ControlParameter ControlID="lblTransactionID" Name="lrqTransactionID" PropertyName="Text" Type="String" />
                                                      <asp:ControlParameter ControlID="lstApplicationFor" Name="lrqusrId" PropertyName="SelectedValue" Type="Int32" />
                                                      <asp:ControlParameter ControlID="lstLeaveType" Name="lrqltyId" PropertyName="SelectedValue" Type="Int32" />
                                                      <asp:ControlParameter ControlID="txtComments" Name="lrqComments" PropertyName="Text" Type="String" />
                                                      <asp:ControlParameter ControlID="txtStartDate" Name="lrqStartDate" PropertyName="Text" Type="DateTime" />
                                                      <asp:ControlParameter ControlID="txtEndDate" Name="lrqEndDate" PropertyName="Text" Type="DateTime" />
                                                      <asp:Parameter Name="lrqstaId" Type="Int32" DefaultValue="1" />
                                                      <asp:ControlParameter ControlID="lstReliever" DefaultValue="" Name="lrqReliever" PropertyName="SelectedValue" Type="Int32" />
                                                      <asp:ControlParameter ControlID="txtContacts" Name="lrqContacts" PropertyName="Text" Type="String" />
                                                      <asp:SessionParameter SessionField="usrId" Name="lrqApplyingUsrId"  Type="Int32" />
                                                      <asp:ControlParameter ControlID="lstStartTime" Name="lrqStartTime" PropertyName="SelectedValue" Type="String" />
                                                      <asp:ControlParameter ControlID="lstEndTime" Name="lrqEndTime" PropertyName="SelectedValue" Type="String" />
                                                  </InsertParameters>
                                                  
                                                  <UpdateParameters>
                                                      <asp:ControlParameter ControlID="lstLeaveType" Name="lrqltyId" PropertyName="SelectedValue" Type="Int32" />
                                                      <asp:ControlParameter ControlID="txtComments" Name="lrqComments" PropertyName="Text" Type="String" />
                                                      <asp:ControlParameter ControlID="txtStartDate" Name="lrqStartDate" PropertyName="Text" Type="DateTime" />
                                                      <asp:ControlParameter ControlID="txtEndDate" Name="lrqEndDate" PropertyName="Text" Type="DateTime" />
                                                      <asp:ControlParameter ControlID="lstReliever" DefaultValue="" Name="lrqReliever" PropertyName="SelectedValue" Type="Int32" />
                                                      <asp:ControlParameter ControlID="txtContacts" Name="lrqContacts" PropertyName="Text" Type="String" />
                                                      <asp:ControlParameter ControlID="lstStartTime" Name="lrqStartTime" PropertyName="SelectedValue" Type="String" />
                                                      <asp:ControlParameter ControlID="lstEndTime" Name="lrqEndTime" PropertyName="SelectedValue" Type="String" />
                                                      <asp:ControlParameter ControlID="lbllrqId" Name="lrqId" PropertyName="Text" Type="String" />
                                                  </UpdateParameters>
                                              </asp:SqlDataSource>
                                          </td>
                                          <td>
                                              <asp:Button ID="cmdUpdate" runat="server" CssClass="button" 
                                                  Text="send request" />
                                &nbsp;<asp:Button ID="cmdCancel" runat="server" CausesValidation="False" CssClass="button" 
                                                  Text="cancel" />
                                          </td>
                                      </tr>
                                  </table>
                                
                                </td>
                            </tr>
                            <tr id="trCancelReason" runat="server" visible="False">
                                <td>
                                    <table cellpadding="4" cellspacing="0" class="style2">
                                        <tr>
                                            <td>
                                            Please specify the reason for cancellation:<br />
                                            <asp:TextBox ID="txtCancelReason" runat="server" TextMode="MultiLine" CssClass="textbox" Rows="5" Columns="30"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rvaCancelReason" runat="server" 
                                                  ControlToValidate="txtCancelReason" ValidationGroup="CancelReason" ErrorMessage="**"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="cmdCancelNow" ValidationGroup="CancelReason" runat="server" CssClass="button" 
                                                  Text="cancel now" />
                                &nbsp;<asp:Button ID="cmdCancelCancellation" runat="server" CausesValidation="False" CssClass="button" 
                                                  Text="back" />
                                            </td>
                                        </tr>
                                    </table> 
                                </td>
                            </tr>
                            <tr id="trConfirmation" runat="server" visible="False">
                                <td>
                                <table cellpadding="4" cellspacing="0" class="style2">
                                        <tr>
                                            <td>
                                    <table cellpadding="4" cellspacing="0" width="100%" align="center">
                                      <tr>
                                          <td width="50%">
                                              Leave type&nbsp;</td>
                                          <td width="50%">
                                              <asp:Label ID="lblConfLeaveType" runat="server"></asp:Label>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              Leave <asp:Label ID="lblConfLeaveUnits1" runat="server"></asp:Label> available</td>
                                          <td>
                                              <asp:Label ID="lblConfDaysAvailable" runat="server"></asp:Label>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              Start date</td>
                                          <td>
                                              <asp:Label ID="lblConfStartDate" runat="server"></asp:Label>
                                              &nbsp;<asp:Label ID="lblConfStartTime" runat="server"></asp:Label>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              End date</td>
                                          <td>
                                              <asp:Label ID="lblConfEndDate" runat="server"></asp:Label>
                                              &nbsp;<asp:Label ID="lblConfEndTime" runat="server"></asp:Label>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              <asp:Label ID="lblConfLeaveUnits2" runat="server"></asp:Label> deductible</td>
                                          <td>
                                              <asp:Label ID="lblConfDays" runat="server" Font-Bold="False"></asp:Label>
                                              <asp:Label ID="lblDaysDesc2" runat="server"></asp:Label>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              To be relieved by</td>
                                          <td>
                                              <asp:Label ID="lblConfReliever" runat="server"></asp:Label>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              Contacts</td>
                                          <td>
                                              <asp:Label ID="lblConfContacts" runat="server"></asp:Label>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              Comments</td>
                                          <td>
                                              <asp:Label ID="lblConfComments" runat="server"></asp:Label>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              Status</td>
                                          <td>
                                              <asp:Label ID="lblConfStatus" runat="server" Font-Bold="True"></asp:Label>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              Related Documents:</td>
                                          <td>
                                              &nbsp;</td>
                                      </tr>
                                      <tr>
                                          <td colspan="2" align="center">
                                                <asp:GridView ID="grvList" Width="70%" runat="server" AutoGenerateColumns="False" 
                                                    DataKeyNames="ldcId" DataSourceID="SqlList0" BorderColor="White" 
                                                    GridLines="None" ShowHeader="False">
                                                    <Columns>
                                                        <asp:BoundField HeaderStyle-HorizontalAlign="Left" DataField="ldcId" 
                                                            HeaderText="ldcId" InsertVisible="False" ReadOnly="True" SortExpression="ldcId" 
                                                            Visible="False" >
                                                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="Title" SortExpression="ldcTitle">
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("ldcTitle") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label1" runat="server" Text='<%# GetDocument(DataBinder.Eval(Container.DataItem, "ldcTitle"), DataBinder.Eval(Container.DataItem, "ldcPath"))  %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                        <asp:CommandField EditText="Edit Title" ShowDeleteButton="True" ShowEditButton="True">
                                                            <ItemStyle Font-Bold="True" />
                                                        </asp:CommandField>
                                                    </Columns>
                                                    <EmptyDataTemplate>
                                                    There are no files attached to this request
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                                <asp:SqlDataSource ID="SqlList0" runat="server" 
                                                    ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                    DeleteCommandType="StoredProcedure"
                                                    SelectCommandType="StoredProcedure"
                                                    UpdateCommandType="StoredProcedure"
                                                    DeleteCommand="stpLeaveDocuments_Delete" 
                                                    SelectCommand="stpLeaveDocuments_View" 
                                                    UpdateCommand="stpLeaveDocuments_Update">
                                                    <SelectParameters>
                                                        <asp:ControlParameter ControlID="lblTransactionID" Name="ldcTransactionID" PropertyName="Text" Type="String" />
                                                        <asp:Parameter Name="ldcEntity" DefaultValue="R" Type="String" />
                                                    </SelectParameters>
                                                    <DeleteParameters>
                                                        <asp:Parameter Name="ldcId" Type="Int32" />
                                                    </DeleteParameters>
                                                    <UpdateParameters>
                                                        <asp:Parameter Name="ldcTitle" Type="String" />
                                                        <asp:Parameter Name="ldcId" Type="Int32" />
                                                    </UpdateParameters>
                                                </asp:SqlDataSource>
                                          </td>
                                      </tr>
                                      <tr id="trConfirmRow" runat="server" visible="False">
                                          <td colspan="2" style="text-align: center">
                                              <table cellpadding="2" cellspacing="0" width="100%" bgcolor="#eeeeee">
                                                  <tr>
                                                      <td bgcolor="#CCCCCC" height="50">
                                                          <b>You have applied for leave as specified above.<br /><br />The following are the events as 
                                                          per the corporate calenfar during the applied period:</b></td>
                                                  </tr>
                                                  <tr>
                                                      <td>
                                              <asp:Label ID="lblCalendarEvents" runat="server"></asp:Label>
                                                      </td>
                                                  </tr>
                                                  <tr>
                                                      <td>
                                                          <asp:Button ID="cmdConfirmRequest" runat="server" CssClass="button2" 
                                                              Text="Confirm Request" />
                                                      &nbsp;<asp:Button ID="cmdCancelRequest" runat="server" CssClass="button2" 
                                                              Text="Modify Request" CausesValidation="False" />
                                                      </td>
                                                  </tr>
                                              </table>
                                          </td>
                                      </tr>
                                      </table>
                                
                                            </td>
                                        </tr>
                                        <tr id="trApprovalDetails" runat="server">
                                            <td>
                                                <b>Approval Details<asp:Label ID="lbllrqId" runat="server" Visible="False"></asp:Label>
                                                </b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="grvApprovals" BorderColor="#EEEEEE" runat="server" AutoGenerateColumns="False" 
                                                    DataSourceID="SqlApprovalPath" Width="100%">
                                                    <Columns>
                                                        <asp:BoundField DataField="usrFullName" HeaderText="Approver" SortExpression="usrFullName" />
                                                          <asp:TemplateField HeaderText="Status" SortExpression="staName">
                                                              <ItemTemplate>
                                                                  <asp:Label ID="Label2" runat="server" Text='<%# GetStatus(DataBinder.Eval(Container.DataItem, "staName")) %>'></asp:Label>
                                                              </ItemTemplate>
                                                          </asp:TemplateField>
                                                        <asp:BoundField DataField="appDateChanged" HeaderText="Date Updated" SortExpression="appDateChanged" />
                                                        <asp:BoundField DataField="appComments" HeaderText="Comments" SortExpression="appComments" />
                                                    </Columns>
                                                    <RowStyle CssClass="greytable" />
                                                    <AlternatingRowStyle CssClass="whitetable" />
                                                    <PagerStyle CssClass="greytable" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="tablecolors_title" />
                                                    
                                                </asp:GridView>
                                                <asp:SqlDataSource ID="SqlApprovalPath" runat="server" 
                                                    ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                    SelectCommandType="StoredProcedure"
                                                    SelectCommand="stpGetApprovalPathStatus">
                                                    <SelectParameters>
                                                        <asp:ControlParameter ControlID="lbllrqId" Name="lrqId" PropertyName="Text" Type="Int32" />
                                                        <asp:Parameter Name="Category" DefaultValue="LR" Type="String" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table> 
                  </td>
                  <td background="/images/rightside.jpg">&nbsp;</td>
                </tr>
                <tr>
                  <td><img src="/images/leftbottom.jpg" alt="LeaveFlow" width="15" height="15" /></td>
                  <td height="15" background="/images/bottomside.jpg">&nbsp;</td>
                  <td><img src="/images/rightbottom.jpg" alt="right bottom" width="15" height="15" /></td>
                </tr>
              </table>              
            </td>
        </tr>
    </table> 



</asp:Content>

