﻿Imports lvManLib.Leave
Imports lvManLib.Config
Imports lvManLib.DataFunctions
Imports lvManLib.Security
Imports System.Data.SqlClient

Partial Class main
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not UserLoggedIn() Then Response.Redirect("/default.aspx?errm=tmo")

        If Not Page.IsPostBack Then
            Dim strTemp As String
            Dim datDate As Date = DateSerial(Year(Now()), Month(Now()), Day(Now()))

            grvAlerts.DataBind()
            LoadMyStatus()
            LoadLatestUpdates()
            LoadMainAlerts()

            datDate = DateAdd(DateInterval.WeekOfYear, -2, datDate)
            lblDate.Text = datDate

            cmdBackToAlerts.Visible = False

            Select Case Request.QueryString("errm") & ""
                Case "usrlcs"
                    lblErrorMessage.Text = "That feature is disabled due to a license violation. Please contact your system administrator"
            End Select

            If Request.QueryString("erm") & "" = "admnu" Then lblErrorMessage.Text = "Sorry, access to that section has been denied"

            If lblErrorMessage.Text = "" Then
                'check whether user has approvers setup
                strTemp = GetSpDataSingleValue("stpCheckUserApprovers", , "usrId=" & Session("usrId"))

                If CType(strTemp, Integer) > 0 Then
                    lblErrorMessage.Text = "You currently have no approvers setup. Please alert HR of this anomaly."
                End If
            End If
        End If
    End Sub

    Sub LoadMainAlerts()
        Dim strTemp As String

        strTemp = GetSpDataSingleValue("stpGetApproverRequests", , "usrId=" & Session("usrId") & "*Category=LR")
        If CType(strTemp, Integer) > 0 Then lblMainAlerts.Text &= "- You have pending leave requests to approve/ attend to. <b><a href='/approvals/default.aspx' style='text-decoration:none;'>go ></a></b><hr style=""border:1px dashed #cccccc;width:100%"" />"

        strTemp = GetSpDataSingleValue("stpGetApproverRequests", , "usrId=" & Session("usrId") & "*Category=LP")
        If CType(strTemp, Integer) > 0 Then lblMainAlerts.Text &= "- You have pending leave plans to approve/ attend to. <b><a href='/approvals/plans.aspx' style='text-decoration:none;'>go ></a></b><hr style=""border:1px dashed #cccccc;width:100%"" />"

        strTemp = GetSpDataSingleValue("stpGetApproverRequests", , "usrId=" & Session("usrId") & "*Category=LC")
        If CType(strTemp, Integer) > 0 Then lblMainAlerts.Text &= "- You have pending leave compensation requests to approve/ attend to. <b><a href='/approvals/compensation.aspx' style='text-decoration:none;'>go ></a></b><hr style=""border:1px dashed #cccccc;width:100%"" />"

        'Leave allowance approval rights
        strTemp = GetSpDataSingleValue("stpGetMenuPermissions", , "intUserID=" & Session("usrId") & "*intMenuId=55")

        If Len(strTemp) > 0 Then
            'strTemp = GetSpDataSingleValue("stpGetApproverRequests", , "usrId=" & Session("usrId") & "*Category=LA")
            'disable allowances for now
            'If Len(strTemp) > 0 Then lblMainAlerts.Text &= "- There are pending leave allowance requests to approve/ attend to. <b><a href='/approvals/allowances.aspx' style='text-decoration:none;'>go ></a></b><hr style=""border:1px dashed #cccccc;width:100%"" />"
        End If

        'If Len(lblMainAlerts.Text) = 0 And grvAlerts.Rows.Count = 0 And Len(lblAlertText.Text) = 0 Then lblMainAlerts.Text = "There are currently no items to highlight"
    End Sub

    Sub LoadMyStatus()
        Dim strTemp As String
        'Load Available Days
        LoadLeaveDays()

        'Next Leave Date
        strTemp = GetSpDataSingleValue("stpGetNextLeaveDate", , "usrId=" & Session("usrId"))
        If Len(strTemp) > 0 Then lblUpcomingApprovedLeave.Text = "<br />You will be going for leave next on " & FormatDateTime(strTemp, DateFormat.LongDate)

        'Pending Leave Applications
        strTemp = GetSpDataSingleValue("stpGetPendingLeaveApplications", , "usrId=" & Session("usrId"))
        If Len(strTemp) > 0 Then lblPendingLeaveApplications.Text &= "<br />You made " & strTemp & " leave request(s) pending approval"
    End Sub

    Sub LoadLatestUpdates()
        Dim objDR As SqlDataReader
        Dim intCount As Integer = 0
        Dim datStartDate As Date = DateSerial(Year(Now()), Month(Now()), Day(Now()))
        datStartDate = DateAdd(DateInterval.WeekOfYear, -1, datStartDate)
        Dim datEndDate As Date = DateAdd(DateInterval.WeekOfYear, 1, datStartDate)
        Dim strID As String
        Dim strLevel As String
        Dim strLevelReport As String = GetConfig("LatestUpdatesLevel")
        Dim strSeperator As String = "<br /><hr style=""border:1px dashed #cccccc;width:100%"" />"
        Dim parameters As New List(Of SqlParameter)()

        'Org Leave calendar
        strLevel = strLevelReport
        Select Case strLevelReport
            Case "C" 'Company
                strID = Session("comId")
            Case "B" 'Branch
                strID = Session("braId")
            Case "D" 'Dept
                strID = Session("dptId")
            Case "S" 'Section
                strID = Session("secId")
            Case Else
                strID = Session("comId")
                strLevel = "C"
        End Select

        Dim ID As New SqlParameter("@ID", strID)
        parameters.Add(ID)
        Dim Level As New SqlParameter("@Level", strLevel)
        parameters.Add(Level)
        Dim lrcStartDate1 As New SqlParameter("@lrcStartDate1", datStartDate)
        parameters.Add(lrcStartDate1)
        Dim lrcStartDate2 As New SqlParameter("@lrcStartDate2", datEndDate)
        parameters.Add(lrcStartDate2)

        objDR = GetSpDataReader("stpLeaveCalendar_Dashboard", parameters.ToArray())

        While objDR.Read()
            intCount += 1
            lblNewsFeed.Text &= objDR("usrFullName") & " will be going on " & objDR("ltyName") & " from " & FormatDateTime(objDR("lrcStartDate"), DateFormat.ShortDate) & " to " & FormatDateTime(objDR("lrcEndDate"), DateFormat.ShortDate) & strSeperator
        End While

        objDR.Close()
        objDR = Nothing

        If intCount = 0 Then
            'lblNewsFeed.Text = "There are no current updates to highlight"
        End If


    End Sub

    Sub LoadLeaveDays()
        Dim dblUnitsAvailable As Double
        Dim strUnits As String
        Dim strComId As String

        strUnits = Session("comLeaveUnit")
        strComId = Session("comId")
        dblUnitsAvailable = CalculateAvailableDays(Session("usrId"), 0)

        If strUnits = "H" Then
            lblDaysAvailable.Text = FormatNumber(dblUnitsAvailable, 0) & " hours " & GetHoursToDays(dblUnitsAvailable, 1)
        Else
            lblDaysAvailable.Text = dblUnitsAvailable & " days"
        End If

        If Session("usrIsSystemAccount") & "" = "True" Then
            trMyStatus.Visible = False
        Else
            lblDaysAvailable.Text = "You have accrued " & lblDaysAvailable.Text
        End If
    End Sub

    Protected Sub grvAlerts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvAlerts.SelectedIndexChanged
        Dim strTemp As String

        strTemp = GetSpDataSingleValue("stpReadAlert", , "aleId=" & grvAlerts.SelectedValue)

        lblAlertText.Text = strTemp
        grvAlerts.Visible = False
        cmdBackToAlerts.Visible = True
    End Sub

    Protected Sub cmdBackToAlerts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBackToAlerts.Click
        lblAlertText.Text = ""
        grvAlerts.DataBind()
        grvAlerts.Visible = True
        cmdBackToAlerts.Visible = False
    End Sub

    Function ShowAlert(ByVal strTitle As String, ByVal datDate As Date, ByVal bolRead As Boolean) As String
        If Not bolRead Then strTitle = "<b>" & strTitle & "</b>"
        strTitle &= "<br /><i><font color='#666666'>" & FormatDateTime(datDate, DateFormat.LongDate) & "</font></i>"
        Return strTitle
    End Function
End Class
