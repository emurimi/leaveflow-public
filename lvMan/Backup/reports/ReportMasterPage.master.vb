﻿Imports lvManLib.Config
Imports lvManLib.General
Imports lvManLib.Menus

Partial Class reports_ReportMasterPage
    Inherits System.Web.UI.MasterPage


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim strCompanyName As String
            Dim strCompanyLogo As String
            strCompanyName = GetConfig("CompanyName")
            strCompanyLogo = GetConfig("CompanyLogo")
            If Len(strCompanyLogo) > 0 Then
                lblLogo.Text = "<img src='" & GetWebsiteRoot() & "/" & strCompanyLogo & "' />"
            End If

            lblCompanyName.Text = strCompanyName

        End If
    End Sub
End Class

