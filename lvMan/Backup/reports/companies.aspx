﻿<%@ Page Title="LeaveFlow | Companies" Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.reports_companies" Codebehind="companies.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">
    <table cellpadding="3" cellspacing="0" width="100%">
        <tr>
            <td class="bigtext">
            <asp:Label ID="lblTitle" Text="Companies" runat="server"></asp:Label>
            </td>
        </tr>  
        <tr>
            <td>
                <asp:GridView ID="grvList" Width="100%" CellPadding="4" BorderColor="#EEEEEE" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="SqlList">
                    <Columns>
                        <asp:BoundField DataField="comName" HeaderText="Name" SortExpression="comName" />
                        <asp:BoundField DataField="comDescription" HeaderText="Description" SortExpression="comDescription" />
                        <asp:BoundField DataField="ctyName" HeaderText="Country" SortExpression="ctyName" />
                        <asp:BoundField DataField="comLeaveUnit" HeaderText="Leave Unit" SortExpression="comLeaveUnit" />
                        <asp:BoundField DataField="comHoursPerDay" HeaderText="Hours per Day" SortExpression="comHoursPerDay" />
                        <asp:BoundField DataField="comLeaveAccrualPeriod" HeaderText="Leave Accrual Period" SortExpression="comLeaveAccrualPeriod" />
                        <asp:BoundField DataField="comFridayHolidayCompensation" HeaderText="Friday Holiday Compensation" SortExpression="comFridayHolidayCompensation" />
                        <asp:BoundField DataField="comSaturdayHolidayCompensation" HeaderText="Saturday Holiday Compensation" SortExpression="comSaturdayHolidayCompensation" />
                        <asp:BoundField DataField="comSundayHolidayCompensation" HeaderText="Sunday Holiday Compensation" SortExpression="comSundayHolidayCompensation" />
                        <asp:BoundField DataField="comStartTime" HeaderText="Work Start Time" SortExpression="comStartTime" />
                        <asp:BoundField DataField="comEndTime" HeaderText="Work End Time" SortExpression="comEndTime" />
                        <asp:BoundField DataField="comLBStartTime" HeaderText="Lunch Start Time" SortExpression="comLBStartTime" />
                        <asp:BoundField DataField="comLBEndTime" HeaderText="Lunch End Time" SortExpression="comLBEndTime" />
                        <asp:BoundField DataField="DeptNumber" DataFormatString="{0:N0}" HeaderText="No of Depts" SortExpression="DeptNumber" />
                        <asp:BoundField DataField="SecNumber" DataFormatString="{0:N0}" HeaderText="No of Sections" SortExpression="SecNumber" />
                        <asp:BoundField DataField="StaffNumber" DataFormatString="{0:N0}" HeaderText="No of Staff" SortExpression="StaffNumber" />
                    </Columns>
                    <RowStyle CssClass="greytable" />
                    <AlternatingRowStyle CssClass="whitetable" />
                    <PagerStyle CssClass="greytable" />
                    <HeaderStyle HorizontalAlign="Left" ForeColor="#FFFFFF" CssClass="tablecolors_title" />                            
                    <EmptyDataTemplate>
                    There are no records to show in this report
                    </EmptyDataTemplate>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlList" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                    SelectCommandType="StoredProcedure"
                    SelectCommand="stpCompanies_Report">
                </asp:SqlDataSource>
            </td> 
        </tr> 
     </table> 

</asp:Content>

