﻿<%@ Page Title="LeaveFlow | manual adjustments" Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.reports_manualadjustments" Codebehind="manualadjustments.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">
    <table cellpadding="3" cellspacing="0" width="100%">
        <tr>
            <td class="bigtext">
            <asp:Label ID="lblTitle" Text="Manual Adjustments" runat="server"></asp:Label>
            </td>
        </tr>  
        <tr>
            <td>
                <asp:GridView ID="grvList" Width="100%" CellPadding="4" BorderColor="#EEEEEE" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="SqlList">
                    <Columns>
                        <asp:BoundField DataField="comName" HeaderText="Company" SortExpression="comName" />
                        <asp:BoundField DataField="dptName" HeaderText="Department" SortExpression="dptName" />
                        <asp:BoundField DataField="secName" HeaderText="Section" SortExpression="secName" />
                        <asp:BoundField DataField="usrFullName" HeaderText="Name" SortExpression="usrFullName" />
                        <asp:BoundField DataField="usrStaffID" HeaderText="Staff ID" SortExpression="usrStaffID" />
                        <asp:BoundField DataField="ldaDate" HeaderText="Date" SortExpression="ldaDate" />
                        <asp:BoundField DataField="areName" HeaderText="Adjustment" SortExpression="areName" />
                        <asp:BoundField DataField="TheEffect" HeaderText="Effect" SortExpression="TheEffect" />
                        <asp:BoundField DataField="ldaUnits" HeaderText="Units" SortExpression="ldaUnits" />
                        <asp:BoundField DataField="AdjustmentBy" HeaderText="Entered By" SortExpression="AdjustmentBy" />
                        <asp:BoundField DataField="ldaDescription" HeaderText="Description" SortExpression="ldaDescription" />
                    </Columns>
                    <RowStyle CssClass="greytable" />
                    <AlternatingRowStyle CssClass="whitetable" />
                    <PagerStyle CssClass="greytable" />
                    <HeaderStyle HorizontalAlign="Left" ForeColor="#FFFFFF" CssClass="tablecolors_title" />                            
                    <EmptyDataTemplate>
                    There are no records to show in this report
                    </EmptyDataTemplate>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlList" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                    SelectCommandType="StoredProcedure"
                    SelectCommand="stpManualAdjustments_Report">
                    <SelectParameters>
                        <asp:QueryStringParameter QueryStringField="comId" Name="comId" DefaultValue="0" Type="Int32" />
                        <asp:QueryStringParameter QueryStringField="dptId" Name="dptId" DefaultValue="0" Type="Int32" />
                        <asp:QueryStringParameter QueryStringField="secId" Name="secId" DefaultValue="0" Type="Int32" />
                        <asp:QueryStringParameter QueryStringField="usrId" Name="usrId" DefaultValue="0" Type="Int32" />
                        <asp:QueryStringParameter QueryStringField="sdt" Name="ldaDate1" Type="DateTime" />
                        <asp:QueryStringParameter QueryStringField="edt" Name="ldaDate2" Type="DateTime" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td> 
        </tr> 
     </table> 
</asp:Content>

