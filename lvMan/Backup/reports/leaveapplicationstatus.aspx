﻿<%@ Page Title="LeaveFlow | leave application status" Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.reports_leaveapplicationstatus" Codebehind="leaveapplicationstatus.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">
   <table cellpadding="3" cellspacing="0" width="100%">
        <tr>
            <td class="bigtext">
            <asp:Label ID="lblTitle" runat="server">Leave Application Status</asp:Label>
            </td>
        </tr>  
        <tr>
            <td>
                <asp:GridView ID="grvList" Width="100%" CellPadding="4" BorderColor="#EEEEEE" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="SqlList">
                    <Columns>
                        <asp:BoundField DataField="comName" HeaderText="Company" SortExpression="comName" />
                        <asp:BoundField DataField="dptName" HeaderText="Department" SortExpression="dptName" />
                        <asp:BoundField DataField="secName" HeaderText="Section" SortExpression="secName" />
                        <asp:BoundField DataField="usrFullName" HeaderText="Name" SortExpression="usrFullName" />
                        <asp:BoundField DataField="usrStaffID" HeaderText="Staff ID" SortExpression="usrStaffID" />
                        <asp:BoundField DataField="lrqStartDate" HeaderText="Start" DataFormatString="{0:d}" SortExpression="lrqStartDate" />
                        <asp:BoundField DataField="lrqStartTime" HeaderText="Time" SortExpression="lrqStartTime" />
                        <asp:BoundField DataField="lrqEndDate" HeaderText="End" DataFormatString="{0:d}" SortExpression="lrqEndDate" />
                        <asp:BoundField DataField="lrqEndTime" HeaderText="Time" SortExpression="lrqEndTime" />
                        <asp:BoundField DataField="staName" HeaderText="Status" SortExpression="staName" />
                        <asp:BoundField DataField="lrqDate" HeaderText="Request Date" DataFormatString="{0:d}" SortExpression="lrqDate" />
                    </Columns>
                    <RowStyle CssClass="greytable" />
                    <AlternatingRowStyle CssClass="whitetable" />
                    <PagerStyle CssClass="greytable" />
                    <HeaderStyle HorizontalAlign="Left" ForeColor="#FFFFFF" CssClass="tablecolors_title" />                            
                    <EmptyDataTemplate>
                    There are no records to show in this report
                    </EmptyDataTemplate>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlList" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                    SelectCommandType="StoredProcedure"
                    SelectCommand="stpLeaveApplicationStatus_Report">
                    <SelectParameters>
                        <asp:QueryStringParameter QueryStringField="comId" Name="comId" DefaultValue="0" Type="Int32" />
                        <asp:QueryStringParameter QueryStringField="dptId" Name="dptId" DefaultValue="0" Type="Int32" />
                        <asp:QueryStringParameter QueryStringField="secId" Name="secId" DefaultValue="0" Type="Int32" />
                        <asp:QueryStringParameter QueryStringField="usrId" Name="usrId" DefaultValue="0" Type="Int32" />
                        <asp:QueryStringParameter QueryStringField="ltyId" Name="ltyId" DefaultValue="0" Type="Int32" />
                        <asp:ControlParameter ControlID="lblstaId" PropertyName="Text" Name="staId" DefaultValue="0" Type="Int32" />
                        <asp:QueryStringParameter QueryStringField="sdt" Name="lrqDate1" DefaultValue="0" Type="DateTime" />
                        <asp:QueryStringParameter QueryStringField="edt" Name="lrqDate2" DefaultValue="0" Type="DateTime" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:Label ID="lblstaId" runat="server" Visible="False"></asp:Label>
            </td> 
        </tr> 
     </table> 
</asp:Content>

