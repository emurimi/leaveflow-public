﻿<%@ Page Title="LeaveFlow | reports" Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.reports_rptfilter" Codebehind="rptfilter.aspx.vb" %>
<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Ajax.Net" Namespace="RJS.Web.WebControl" TagPrefix="rjs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">
    <table cellpadding="3" cellspacing="0" width="100%">
        <tr id="trTitle" runat="server" visible="False">
            <td class="bigtext">
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </td>
        </tr>  
        <tr>
            <td>
                <table cellpadding="4" cellspacing="0" width="60%" align="center">
                    <tr id="trCompany" runat="server">
                        <td width="5%">
                            <asp:CheckBox ID="chkCompany" runat="server" />
                        </td>
                        <td width="45%">
                            Company:</td>
                        <td width="50%">
                            <asp:DropDownList AutoPostBack="True" ID="lstCompanies" runat="server" CssClass="listmenu" 
                                DataSourceID="SqlCompanies" DataTextField="comName" DataValueField="comId">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="SqlCompanies" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                SelectCommandType="StoredProcedure"
                                SelectCommand="stpCompanyDetails">
                            </asp:SqlDataSource>                        
                        </td>
                    </tr>
                    <tr id="trBranch" runat="server">
                        <td width="5%">
                            <asp:CheckBox ID="chkBranch" runat="server" />
                        </td>
                        <td width="45%">
                            Branch:</td>
                        <td width="50%">
                            <asp:DropDownList ID="lstBranches" runat="server" AutoPostBack="True" 
                                CssClass="listmenu" DataSourceID="SqlBranches" DataTextField="braName" 
                                DataValueField="braId">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="SqlBranches" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                SelectCommandType="StoredProcedure"
                                SelectCommand="stpBranches_View">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="lstCompanies" Name="bracomId" PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>                        
                        </td>
                    </tr>

                    <tr id="trDepartment" runat="server">
                        <td width="5%">
                            <asp:CheckBox ID="chkDepartment" runat="server" />
                        </td>
                        <td width="45%">
                            Department:</td>
                        <td width="50%">
                            <asp:DropDownList ID="lstDepartments" runat="server" AutoPostBack="True" 
                                CssClass="listmenu" DataSourceID="SqlDepartments" DataTextField="dptName" 
                                DataValueField="dptId">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="SqlDepartments" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                SelectCommandType="StoredProcedure"
                                SelectCommand="stpDepartments_View">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="lstCompanies" Name="dptcomId" PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>                        
                        </td>
                    </tr>
                    <tr id="trSection" runat="server">
                        <td width="5%">
                            <asp:CheckBox ID="chkSection" runat="server" />
                        </td>
                        <td width="45%">
                            Section:</td>
                        <td width="50%">
                            <asp:DropDownList ID="lstSections" runat="server" CssClass="listmenu" 
                                DataSourceID="SqlSections" DataTextField="secName" DataValueField="secId">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="SqlSections" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                SelectCommandType="StoredProcedure"
                                SelectCommand="stpSections_View">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="lstDepartments" Name="secdptId" PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>                        
                        </td>
                    </tr>
                    <tr id="trUsers" runat="server">
                        <td width="5%">
                            <asp:CheckBox ID="chkUsers" runat="server" />
                        </td>
                        <td width="45%">
                            Staff:</td>
                        <td width="50%">
                            <asp:DropDownList ID="lstUsers" runat="server" CssClass="listmenu" 
                                DataSourceID="SqlUsers" DataTextField="usrFullName" DataValueField="usrId">
                            </asp:DropDownList>
                   
                            <asp:SqlDataSource ID="SqlUsers" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>"
                                SelectCommandType="StoredProcedure" 
                                SelectCommand="stpUsers_MinView">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="lstCompanies" DefaultValue="0" Name="comId" PropertyName="SelectedValue" Type="Int32" />
                                    <asp:ControlParameter ControlID="lstDepartments" DefaultValue="0" Name="usrdptId" PropertyName="SelectedValue" Type="Int32" />
                                    <asp:ControlParameter ControlID="lstSections" DefaultValue="0" Name="usrsecId" PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                   
                        </td>
                    </tr>                    
                    <tr id="trLeaveType" runat="server">
                        <td width="5%">
                            <asp:CheckBox ID="chkLeaveType" runat="server" />
                        </td>
                        <td width="45%">
                            Leave Type:</td>
                        <td width="50%">
                          <asp:DropDownList ID="lstLeaveType" DataTextField="ltyName" DataValueField="ltyId" runat="server" CssClass="listmenu" 
                              AutoPostBack="True" DataSourceID="SqlLeaveTypes">
                          </asp:DropDownList>
                          <asp:SqlDataSource ID="SqlLeaveTypes" runat="server" 
                              ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                              SelectCommandType="StoredProcedure"
                              SelectCommand="stpLeaveTypes_View">
                          </asp:SqlDataSource>                        
                        </td>
                    </tr>
                    <tr id="trStatus" runat="server">
                        <td width="5%">
                            <asp:CheckBox ID="chkStatus" runat="server" />
                        </td>
                        <td width="45%">
                            Status:</td>
                        <td width="50%">
                          <asp:DropDownList ID="lstStatus" DataTextField="staName" DataValueField="staId" 
                                runat="server" CssClass="listmenu" 
                              AutoPostBack="True" DataSourceID="SqlStatus">
                          </asp:DropDownList>
                            <asp:SqlDataSource ID="SqlStatus" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                SelectCommandType="StoredProcedure"
                                SelectCommand="stpStatus_List">
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr id="trStartDate" runat="server">
                        <td width="5%">
                            <asp:CheckBox ID="chkStartDate" runat="server" />
                        </td>
                        <td width="45%">
                           <asp:Label ID="lblStartDate" runat="server" Text="Start Date:"></asp:Label></td>
                        <td width="50%">
                              <asp:TextBox ID="txtStartDate" ReadOnly="True" runat="server" CssClass="textbox"></asp:TextBox>
                              <rjs:PopCalendar ID="ppcStartDate" runat="server" Control="txtStartDate" 
                                  AutoPostBack="False" Format="dd mm yyyy" HolidayMessage="This date is not available" 
                                  Separator="/" SelectHoliday="False" />
                              <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                  ControlToValidate="txtStartDate" ErrorMessage="**"></asp:RequiredFieldValidator>
                              <rjs:PopCalendarMessageContainer ID="pmcSTartDate" runat="server" Calendar="ppcStartDate" />
                        
                        </td>
                    </tr>
                    <tr id="trEndDate" runat="server">
                        <td width="5%">
                            <asp:CheckBox ID="chkEndDate" runat="server" />
                        </td>
                        <td width="45%">
                            <asp:Label ID="lblEndDate" runat="server" Text="End Date:"></asp:Label></td>
                        <td width="50%">
                          <asp:TextBox ID="txtEndDate" ReadOnly="True" runat="server" CssClass="textbox"></asp:TextBox>
                          <rjs:PopCalendar ID="ppcEndDate" runat="server" Control="txtEndDate" 
                              AutoPostBack="False" Format="dd mm yyyy" HolidayMessage="This date is not available" 
                              Separator="/" ShowWeekend="True" />
                          <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                              ControlToValidate="txtEndDate" ErrorMessage="**"></asp:RequiredFieldValidator>
                          <rjs:PopCalendarMessageContainer ID="pmcEndDate" runat="server" Calendar="ppcEndDate" /> 
                        
                        </td>
                    </tr>
                    <tr>
                        <td width="5%">
                            &nbsp;</td>
                        <td width="45%">
                            Format:</td>
                        <td width="50%">
                            <asp:DropDownList ID="lstFormat" runat="server" CssClass="listmenu">
                                <asp:ListItem Value="">Web Page</asp:ListItem>
                                <asp:ListItem Value="xls">Excel Document</asp:ListItem>
                                <asp:ListItem Value="pdf">PDF Document</asp:ListItem>
                                <asp:ListItem Value="doc">Word Document</asp:ListItem>
                                <asp:ListItem Value="csv">CSV Document</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            <asp:Button ID="cmdReport" runat="server" CssClass="button2" 
                                Text="go to report" />
                        </td>
                    </tr>
                    </table>
            </td>
        </tr>
     </table> 
</asp:Content>

