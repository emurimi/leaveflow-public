﻿<%@ Page Title="LeaveFlow | leave days statement" Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.reports_leavedaysstatement" Codebehind="leavedaysstatement.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">
    <table cellpadding="3" cellspacing="0" width="100%">
        <tr>
            <td class="bigtext">
            <asp:Label ID="lblTitle" Text="Leave Days Statement" runat="server"></asp:Label>
            </td>
        </tr>  
        <tr>
            <td>
                <table cellpadding="3" cellspacing="0" width="50%">
                    <tr>
                        <td width="35%"><b>Company:</b></td>
                        <td width="65%"><asp:Label ID="lblCompany" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td width="35%"><b>Department:</b></td>
                        <td width="65%"><asp:Label ID="lblDepartment" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td width="35%"><b>Name:</b></td>
                        <td width="65%"><asp:Label ID="lblName" runat="server"></asp:Label></td>
                    </tr>                    
                    <tr>
                        <td width="35%"><b>Staff ID:</b></td>
                        <td width="65%"><asp:Label ID="lblStaffID" runat="server"></asp:Label></td>
                    </tr>   
                    <tr>
                        <td width="35%"><b>Available <asp:Label ID="lblUnits" runat="server"></asp:Label>:</b></td>
                        <td width="65%"><asp:Label ID="lblAvailUnits" runat="server"></asp:Label></td>
                    </tr>                                       
                </table> 
                <asp:GridView ID="grvList" Width="100%" CellPadding="4" BorderColor="#EEEEEE" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="SqlList">
                    <Columns>
                        <asp:BoundField DataField="ldlDate" HeaderText="Date" DataFormatString="{0:d}" SortExpression="ldlDate" />
                        <asp:BoundField DataField="ldlDescription" HeaderText="Description" SortExpression="ldlDescription" />
                        <asp:BoundField DataField="ldlUnits" HeaderText="Units" DataFormatString="{0:N2}" SortExpression="ldlUnits" />
                        <asp:BoundField DataField="ldlBalance" HeaderText="Balance" DataFormatString="{0:N2}" SortExpression="ldlBalance" />
                    </Columns>
                    <RowStyle CssClass="greytable" />
                    <AlternatingRowStyle CssClass="whitetable" />
                    <PagerStyle CssClass="greytable" />
                    <HeaderStyle HorizontalAlign="Left" ForeColor="#FFFFFF" CssClass="tablecolors_title" />                            
                    <EmptyDataTemplate>
                    There are no records to show in this report
                    </EmptyDataTemplate>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlList" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                    SelectCommandType="StoredProcedure"
                    SelectCommand="stpLeaveDaysStatement_Report">
                    <SelectParameters>
                        <asp:QueryStringParameter QueryStringField="sdt" Name="ldlDate1" Type="DateTime" />
                        <asp:QueryStringParameter QueryStringField="edt" Name="ldlDate2" Type="DateTime" />
                        <asp:QueryStringParameter QueryStringField="usrId" Name="usrId" DefaultValue="0" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td> 
        </tr> 
     </table> 
</asp:Content>

