﻿Imports System.Text
Imports System.IO
Imports lvManLib.General
Imports lvManLib.Security

Partial Class reports_annualleavebalances
    Inherits System.Web.UI.Page
    Public strUserPermissions As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            strUserPermissions = SecureCurrentPage()
            LoadReport()
            CheckFormat()
        End If
    End Sub

    Sub CheckFormat()
        Dim strFormat As String = ""
        Dim strTitle As String
        strFormat = Request.QueryString("fmt") & ""

        If strFormat = "" Then Exit Sub

        strTitle = lblTitle.Text
        strTitle = Replace(strTitle, ":", "")
        strTitle = Replace(strTitle, "  ", " ")
        strTitle = Replace(strTitle, " ", "_")

        grvList.AllowPaging = False
        grvList.AllowSorting = False
        grvList.HeaderStyle.BackColor = Drawing.Color.DarkGray

        grvList.DataBind()

        Select Case strFormat
            Case "xls"
                ExportExcel(strTitle, grvList, Page)
            Case "doc"
                ExportWord(strTitle, grvList, Page)
            Case "csv"
                ExportCSV(strTitle, grvList, Page)
            Case "pdf"
                ExportPDF(strTitle, grvList, Page, True, True)
            Case Else
                'do nothing
        End Select
    End Sub

    Sub LoadReport()
        Dim strReport As String
        Dim strLeaveUnit As String = Session("comLeaveUnit")

        Select Case strLeaveUnit
            Case "H"
                grvList.Columns(5).HeaderText = "Hours"
            Case Else
                grvList.Columns(5).HeaderText = "Days"
        End Select
        strReport = "Annual Leave Balances"

        SqlList.DataBind()
        grvList.DataBind()
    End Sub
End Class
