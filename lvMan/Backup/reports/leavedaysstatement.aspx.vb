﻿Imports System.Text
Imports System.IO
Imports System.Data.SqlClient
Imports lvManLib.DataFunctions
Imports lvManLib.Config
Imports lvManLib.Leave
Imports lvManLib.General
Imports lvManLib.Security

Partial Class reports_leavedaysstatement
    Inherits System.Web.UI.Page
    Public strUserPermissions As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            strUserPermissions = SecureCurrentPage()
            LoadReport()
            CheckFormat()
        End If
    End Sub

    Sub CheckFormat()
        Dim strFormat As String = ""
        Dim strTitle As String
        strFormat = Request.QueryString("fmt") & ""

        If strFormat = "" Then Exit Sub

        strTitle = lblTitle.Text
        strTitle &= "_" & lblName.Text & "_" & lblStaffID.Text
        strTitle = Replace(strTitle, ":", "")
        strTitle = Replace(strTitle, "  ", " ")
        strTitle = Replace(strTitle, " ", "_")
        strTitle = Replace(strTitle, "'", "")

        grvList.AllowPaging = False
        grvList.AllowSorting = False
        grvList.HeaderStyle.BackColor = Drawing.Color.DarkGray

        grvList.DataBind()

        Select Case strFormat
            Case "xls"
                ExportExcel(strTitle, grvList, Page)
            Case "doc"
                ExportWord(strTitle, grvList, Page)
            Case "csv"
                ExportCSV(strTitle, grvList, Page)
            Case "pdf"
                ExportPDF(strTitle, grvList, Page, True, True)
            Case Else
                'do nothing
        End Select
    End Sub

    Sub LoadReport()
        Dim strReport As String
        Dim strusrId As String
        Dim objDR As SqlDataReader

        strReport = "Leave Days Statement"
        lblTitle.Text = strReport

        strusrId = Request.QueryString("usrId") & ""

        If Not IsNumeric(strusrId) Then strusrId = ""

        If Len(strusrId) = 0 Then Exit Sub

        objDR = GetSpDataReader("stpUserDetails", , "usrId=" & strusrId)

        If objDR.Read() Then
            lblCompany.Text = objDR("comName") & ""
            lblDepartment.Text = objDR("dptName") & ""
            lblName.Text = objDR("usrFullName") & ""
            lblStaffID.Text = objDR("usrStaffID") & ""
        End If

        objDR.Close()
        objDR = Nothing

        lblAvailUnits.Text = CalculateAvailableDays(strusrId, 0)
        If Len(lblAvailUnits.Text) > 0 Then lblAvailUnits.Text = FormatNumber(lblAvailUnits.Text, 2)

        lblUnits.Text = Session("comLeaveUnit")
        If lblUnits.Text = "H" Then
            lblUnits.Text = "hours"
        Else
            lblUnits.Text = "days"
        End If

    End Sub

End Class
