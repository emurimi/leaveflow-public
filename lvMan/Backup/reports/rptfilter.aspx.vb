﻿Imports lvManLib.Config

Partial Class reports_rptfilter
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            LoadReport()
            txtStartDate.Text = FormatDateTime(DateAdd(DateInterval.Month, -3, Now()), DateFormat.ShortDate)
            txtEndDate.Text = FormatDateTime(Now(), DateFormat.ShortDate)

            Dim strDateFormat As String = GetConfig("DateFormat") & ""

        End If
    End Sub

    Sub LoadReport()
        Select Case LCase(Request.QueryString("rep") & "")
            Case "levc"
                lblTitle.Text = "Leave Calendar"
                chkStartDate.Checked = True
                chkEndDate.Checked = True
                chkStartDate.Enabled = False
                chkEndDate.Enabled = False
                trStatus.Visible = False
                trLeaveType.Visible = False
                trBranch.Visible = False

            Case "anlb"
                lblTitle.Text = "Annual Leave Balances"
                trStartDate.Visible = False
                trEndDate.Visible = False
                trStatus.Visible = False
                trLeaveType.Visible = False
                trBranch.Visible = False

            Case "leas"
                lblTitle.Text = "Leave Application Status"
                chkStartDate.Checked = True
                chkEndDate.Checked = True
                chkStartDate.Enabled = False
                chkEndDate.Enabled = False
                trBranch.Visible = False

            Case "ston"
                lblTitle.Text = "Staff on Leave"
                trStatus.Visible = False
                trStartDate.Visible = False
                trEndDate.Visible = False
                trBranch.Visible = False

            Case "relv"
                lblTitle.Text = "Rejected Leave"
                chkStartDate.Checked = True
                chkEndDate.Checked = True
                chkStartDate.Enabled = False
                chkEndDate.Enabled = False
                trStatus.Visible = False
                trBranch.Visible = False

            Case "calv"
                lblTitle.Text = "Cancelled Leave"
                chkStartDate.Checked = True
                chkEndDate.Checked = True
                chkStartDate.Enabled = False
                chkEndDate.Enabled = False
                trStatus.Visible = False
                trBranch.Visible = False

            Case "plvd"
                lblTitle.Text = "Pending Leave Detail"
                chkStartDate.Checked = True
                chkEndDate.Checked = True
                chkStartDate.Enabled = False
                chkEndDate.Enabled = False
                trStatus.Visible = False
                trBranch.Visible = False

            Case "madj"
                lblTitle.Text = "Manual Adjustments"
                chkStartDate.Checked = True
                chkEndDate.Checked = True
                chkStartDate.Enabled = False
                chkEndDate.Enabled = False
                trStatus.Visible = False
                trLeaveType.Visible = False
                trBranch.Visible = False

            Case "ldst"
                lblTitle.Text = "Leave Days Statement"
                trStatus.Visible = False
                chkStartDate.Checked = True
                chkEndDate.Checked = True
                chkStartDate.Enabled = False
                chkEndDate.Enabled = False
                trLeaveType.Visible = False
                chkUsers.Checked = True
                chkUsers.Enabled = False
                trBranch.Visible = False

            Case "stap"
                lblTitle.Text = "Staff Approvers"
                trStatus.Visible = False
                chkStartDate.Checked = True
                chkEndDate.Checked = True
                chkStartDate.Enabled = False
                chkEndDate.Enabled = False
                trStartDate.Visible = False
                trEndDate.Visible = False
                trLeaveType.Visible = False
                chkUsers.Checked = True
                chkUsers.Enabled = False
                trBranch.Visible = True
                chkBranch.Checked = True
                chkBranch.Enabled = False
                chkCompany.Checked = True
                chkCompany.Enabled = False
                chkDepartment.Checked = True
                chkDepartment.Enabled = False
                chkSection.Checked = True
                chkSection.Enabled = False


            Case "vlnt"
                lblTitle.Text = "Value of Leave Not Taken"
                trStartDate.Visible = False
                trEndDate.Visible = False
                trStatus.Visible = False
                trLeaveType.Visible = False
                trBranch.Visible = False

            Case "leal"
                lblTitle.Text = "Leave Allowances"
                chkStartDate.Checked = True
                chkEndDate.Checked = True
                chkStartDate.Enabled = False
                chkEndDate.Enabled = False
                trLeaveType.Visible = False
                trBranch.Visible = False
                trStatus.Visible = False

            Case "lepl"
                lblTitle.Text = "Leave Plans"
                chkStartDate.Checked = True
                chkEndDate.Checked = True
                chkStartDate.Enabled = False
                chkEndDate.Enabled = False
                trLeaveType.Visible = False
                trBranch.Visible = False

            Case "usrs"
                lblTitle.Text = "Users"
                chkStartDate.Checked = False
                chkEndDate.Checked = False
                trStatus.Visible = False
                trLeaveType.Visible = False
                trUsers.Visible = False
                trBranch.Visible = False
                lblStartDate.Text = "Added From Date: "
                lblEndDate.Text = "To Date:"
            Case "apmt"
                lblTitle.Text = "Approval Matrix"
                trStartDate.Visible = False
                trEndDate.Visible = False
                trStatus.Visible = False
                trLeaveType.Visible = False
                trUsers.Visible = False
                chkDepartment.Checked = True
                chkDepartment.Enabled = False
                trBranch.Visible = False

            Case "stwa"
                lblTitle.Text = "Staff without approvers"
                trStartDate.Visible = False
                trEndDate.Visible = False
                trStatus.Visible = False
                trLeaveType.Visible = False
                trUsers.Visible = False
                trBranch.Visible = False

            Case "dpts"
                lblTitle.Text = "Departments"
                trStartDate.Visible = False
                trEndDate.Visible = False
                trStatus.Visible = False
                trLeaveType.Visible = False
                trUsers.Visible = False
                trSection.Visible = False
                trDepartment.Visible = False
                trBranch.Visible = False

            Case "secn"
                lblTitle.Text = "Sections"
                trStartDate.Visible = False
                trEndDate.Visible = False
                trStatus.Visible = False
                trLeaveType.Visible = False
                trUsers.Visible = False
                trSection.Visible = False
                trBranch.Visible = False

            Case "cmps"
                Response.Redirect("companies.aspx")
        End Select

        lstCompanies.DataBind()
        lstCompanies.Items.Insert(0, New ListItem("All Companies", "0"))
        lstCompanies.SelectedIndex = 0
        lstDepartments.DataBind()
        lstDepartments.Items.Insert(0, New ListItem("All Departments", "0"))
        lstDepartments.SelectedIndex = 0
        lstSections.DataBind()
        lstSections.Items.Insert(0, New ListItem("All Sections", "0"))
        lstSections.SelectedIndex = 0
        lstUsers.DataBind()
        lstUsers.Items.Insert(0, New ListItem("All Staff", "0"))
        lstUsers.SelectedIndex = 0

        If lstSections.Items.Count = 0 Then trSection.Visible = False
    End Sub

    Protected Sub cmdReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdReport.Click
        Dim strQueryString As String = ""

        strQueryString = "?fmt=" & lstFormat.SelectedValue

        If chkCompany.Checked Then strQueryString &= "&comId=" & lstCompanies.SelectedValue
        If chkDepartment.Checked And lstDepartments.Items.Count > 0 Then strQueryString &= "&dptId=" & lstDepartments.SelectedValue
        If chkSection.Checked And lstSections.Items.Count > 0 Then strQueryString &= "&secId=" & lstSections.SelectedValue
        If chkLeaveType.Checked Then strQueryString &= "&ltyId=" & lstLeaveType.SelectedValue
        If chkStatus.Checked Then strQueryString &= "&staId=" & lstStatus.SelectedValue
        If chkStartDate.Checked Then strQueryString &= "&sdt=" & Year(ppcStartDate.SelectedDate) & "-" & Month(ppcStartDate.SelectedDate) & "-" & Day(ppcStartDate.SelectedDate)
        If chkEndDate.Checked Then strQueryString &= "&edt=" & Year(ppcEndDate.SelectedDate) & "-" & Month(ppcEndDate.SelectedDate) & "-" & Day(ppcEndDate.SelectedDate)
        If chkUsers.Checked And lstUsers.Items.Count > 0 Then strQueryString &= "&usrId=" & lstUsers.SelectedValue
        If chkBranch.Checked And lstBranches.Items.Count > 0 Then strQueryString &= "&braId=" & lstBranches.SelectedValue

        Select Case LCase(Request.QueryString("rep") & "")
            Case "levc" 'Leave Calendar
                Response.Redirect("leavecalendar.aspx" & strQueryString)
            Case "anlb" 'Annual Leave Balances
                Response.Redirect("annualleavebalances.aspx" & strQueryString)
            Case "leas" 'Leave Application Status
                Response.Redirect("leaveapplicationstatus.aspx" & strQueryString)
            Case "ston" 'Staff on Leave
                Response.Redirect("leavecalendar.aspx" & strQueryString & "&rep=ston")
            Case "relv" 'Rejected Leave
                Response.Redirect("leaveapplicationstatus.aspx" & strQueryString & "&rep=relv")
            Case "calv" 'Cancelled Leave
                Response.Redirect("leavecalendar.aspx" & strQueryString & "&rep=calv")

            Case "plvd" 'Pending Leave Detail
                Response.Redirect("pendingleavestatus.aspx" & strQueryString)
            Case "madj" 'Manual Adjustments
                Response.Redirect("manualadjustments.aspx" & strQueryString)
            Case "ldst" 'Leave Days Statement
                Response.Redirect("leavedaysstatement.aspx" & strQueryString)
            Case "vlnt" 'Value of Leave Not Taken
                Response.Redirect("valueofleavenottaken.aspx" & strQueryString)
            Case "leal" 'Leave Allowances
                Response.Redirect("leaveallowances.aspx" & strQueryString)
            Case "lepl" 'Leave Plams
                Response.Redirect("leaveplans.aspx" & strQueryString)
            Case "usrs" 'Users
                Response.Redirect("users.aspx" & strQueryString)
            Case "apmt" 'Approval Matrix
                Response.Redirect("approvalmatrix.aspx" & strQueryString)
            Case "stwa" 'Staff without approvers
                Response.Redirect("staffwithoutapprovers.aspx" & strQueryString)
            Case "dpts" 'Departments
                Response.Redirect("departments.aspx" & strQueryString)
            Case "secn" 'Sections
                Response.Redirect("sections.aspx" & strQueryString)
            Case "stap" 'Staff Approvers
                Response.Redirect("staffapprovers.aspx" & strQueryString)
        End Select
    End Sub

    Protected Sub lstDepartments_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstDepartments.SelectedIndexChanged
        trSection.Visible = True
        lstSections.DataBind()
        If lstSections.Items.Count = 0 Then trSection.Visible = False
    End Sub
End Class
