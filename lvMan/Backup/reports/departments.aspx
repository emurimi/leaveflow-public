﻿<%@ Page Title="LeaveFlow | departments" Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.reports_departments" Codebehind="departments.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">
    <table cellpadding="3" cellspacing="0" width="100%">
        <tr>
            <td class="bigtext">
            <asp:Label ID="lblTitle" Text="Departments" runat="server"></asp:Label>
            </td>
        </tr>  
        <tr>
            <td>
                <asp:GridView ID="grvList" Width="100%" CellPadding="4" BorderColor="#EEEEEE" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="SqlList">
                    <Columns>
                        <asp:BoundField DataField="comName" HeaderText="Company" SortExpression="comName" />
                        <asp:BoundField DataField="dptName" HeaderText="Name" SortExpression="dptName" />
                        <asp:BoundField DataField="dptDescription" HeaderText="Description" SortExpression="dptDescription" />
                        <asp:BoundField DataField="StaffNumber" DataFormatString="{0:N0}" HeaderText="Number of Staff" SortExpression="StaffNumber" />
                    </Columns>
                    <RowStyle CssClass="greytable" />
                    <AlternatingRowStyle CssClass="whitetable" />
                    <PagerStyle CssClass="greytable" />
                    <HeaderStyle HorizontalAlign="Left" ForeColor="#FFFFFF" CssClass="tablecolors_title" />                            
                    <EmptyDataTemplate>
                    There are no records to show in this report
                    </EmptyDataTemplate>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlList" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                    SelectCommandType="StoredProcedure"                    
                    SelectCommand="stpDepartments_Report">
                    <SelectParameters>
                        <asp:QueryStringParameter QueryStringField="comId" Name="comId" DefaultValue="0" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td> 
        </tr> 
     </table> 

</asp:Content>

