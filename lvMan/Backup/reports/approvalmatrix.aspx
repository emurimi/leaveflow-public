﻿<%@ Page Title="LeaveFlow | approval matrix" Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.reports_approvalmatrix" Codebehind="approvalmatrix.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">
    <table cellpadding="3" cellspacing="0" width="100%">
        <tr>
            <td class="bigtext">
            <asp:Label ID="lblTitle" Text="Approval Matrix" runat="server"></asp:Label>
            </td>
        </tr>  
        <tr>
            <td>
                <asp:GridView ID="grvList" Width="100%" CellPadding="4" BorderColor="#EEEEEE" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="SqlList">
                    <Columns>
                        <asp:BoundField DataField="comName" HeaderText="Company" SortExpression="comName" />
                        <asp:BoundField DataField="dptName" HeaderText="Department" SortExpression="dptName" />
                        <asp:BoundField DataField="secName" HeaderText="Section" SortExpression="secName" />
                        <asp:BoundField DataField="posName" HeaderText="Position" SortExpression="posName" />
                        <asp:BoundField DataField="usrFullName" HeaderText="Staff Name" SortExpression="usrFullName" />
                        <asp:BoundField DataField="usrStaffID" HeaderText="Staff ID" SortExpression="usrStaffID" />
                        <asp:BoundField DataField="ApproverName" HeaderText="Approver Name" SortExpression="ApproverName" />
                        <asp:BoundField DataField="ApproverStaffID" HeaderText="Approver Staff ID" SortExpression="ApproverStaffID" />
                        <asp:BoundField DataField="ApproverEmailAdd" HeaderText="Approver Email Add" SortExpression="ApproverEmailAdd" />
                        <asp:BoundField DataField="apsApprovalOrder" HeaderText="Approval Order" SortExpression="apsApprovalOrder" />
                        <asp:BoundField DataField="apsFinalApprover" HeaderText="Final Approver" SortExpression="apsFinalApprover" />
                    </Columns>
                    <RowStyle CssClass="greytable" />
                    <AlternatingRowStyle CssClass="whitetable" />
                    <PagerStyle CssClass="greytable" />
                    <HeaderStyle HorizontalAlign="Left" ForeColor="#FFFFFF" CssClass="tablecolors_title" />                            
                    <EmptyDataTemplate>
                    There are no records to show in this report
                    </EmptyDataTemplate>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlList" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                    SelectCommandType="StoredProcedure"
                    SelectCommand="stpApprovalMatrix_Report">
                    <SelectParameters>
                        <asp:QueryStringParameter QueryStringField="comId" Name="comId" DefaultValue="0" Type="Int32" />
                        <asp:QueryStringParameter QueryStringField="dptId" Name="dptId" DefaultValue="0" Type="Int32" />
                        <asp:QueryStringParameter QueryStringField="secId" Name="secId" DefaultValue="0" Type="Int32" />
                        <asp:QueryStringParameter QueryStringField="usrId" Name="usrId" DefaultValue="0" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td> 
        </tr> 
     </table> 

</asp:Content>

