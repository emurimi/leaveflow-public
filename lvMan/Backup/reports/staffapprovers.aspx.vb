﻿Imports System.Text
Imports System.IO
Imports lvManLib.General
Imports lvManLib.Leave
Imports lvManLib.DataFunctions
Imports lvManLib.Security
Imports System.Data.SqlClient

Partial Class reports_staffapprovers
    Inherits System.Web.UI.Page
    Public strUserPermissions As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            strUserPermissions = SecureCurrentPage()

            LoadReport()
            CheckFormat()
        End If
    End Sub

    Sub CheckFormat()
        Dim strFormat As String = ""
        Dim strTitle As String
        strFormat = Request.QueryString("fmt") & ""

        If strFormat = "" Then Exit Sub

        strTitle = lblTitle.Text
        strTitle = Replace(strTitle, ":", "")
        strTitle = Replace(strTitle, "  ", " ")
        strTitle = Replace(strTitle, " ", "_")
        strTitle = Replace(strTitle, "'", "")

        grvList.AllowPaging = False
        grvList.AllowSorting = False
        grvList.HeaderStyle.BackColor = Drawing.Color.DarkGray

        grvList.DataBind()

        Select Case strFormat
            Case "xls"
                ExportExcel(strTitle, grvList, Page)
            Case "doc"
                ExportWord(strTitle, grvList, Page)
            Case "csv"
                ExportCSV(strTitle, grvList, Page)
            Case "pdf"
                ExportPDF(strTitle, grvList, Page, True, True)
            Case Else
                'do nothing
        End Select
    End Sub

    Sub LoadReport()
        Dim strReport As String
        Dim strcomId As String
        Dim strdptId As String
        Dim strsecId As String
        Dim strusrId As String
        Dim strrep As String
        Dim strSql As String
        Dim strbraId As String
        Dim strposId As String = "0"
        Dim strName As String = ""
        Dim objDR As SqlDataReader

        strReport = "Staff approvers"
        strrep = Request.QueryString("rep") & ""
        lblTitle.Text = strReport

        strcomId = Request.QueryString("comId") & ""
        strdptId = Request.QueryString("dptId") & ""
        strsecId = Request.QueryString("secId") & ""
        strusrId = Request.QueryString("usrId") & ""
        strbraId = Request.QueryString("braId") & ""

        If Not IsNumeric(strcomId) Then strcomId = "0"
        If Not IsNumeric(strdptId) Then strdptId = "0"
        If Not IsNumeric(strsecId) Then strsecId = "0"
        If Not IsNumeric(strusrId) Then strusrId = "0"
        If Not IsNumeric(strbraId) Then strbraId = "0"

        objDR = GetSpDataReader("stpUsers_MinView", , "usrId=" & strusrId)
        If objDR.Read() Then
            strName = objDR("usrFullName") & ""
            strposId = objDR("usrposId") & ""
        End If

        objDR.Close()
        objDR = Nothing

        lblTitle.Text &= " for " & strName

        strSql = GetApproversQuery(strcomId, strbraId, strdptId, strsecId, strposId, strusrId, "AA")


        SqlList.SelectCommand = strSql
        SqlList.DataBind()
        grvList.DataBind()
    End Sub

    Function GetUser(ByVal intUserId As Integer) As String
        Dim strSql As String
        Dim strTemp As String

        strSql = "SELECT usrFullName FROM tblUsers WHERE usrId = " & intUserId
        strTemp = ReturnSingleDbValue(strSql)

        If intUserId = CType(Request.QueryString("usrId") & "", Integer) Then strTemp &= " (skipped)"
        Return strTemp
    End Function
End Class
