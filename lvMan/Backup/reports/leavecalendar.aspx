﻿<%@ Page Title="LeaveFlow | leave calendar" Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.reports_leavecalendar" Codebehind="leavecalendar.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">
    <table cellpadding="3" cellspacing="0" width="100%">
        <tr>
            <td class="bigtext">
            <asp:Label ID="lblTitle" Text="Leave Calendar" runat="server"></asp:Label>
            </td>
        </tr>  
        <tr>
            <td>
                <asp:GridView ID="grvList" Width="100%" CellPadding="4" BorderColor="#EEEEEE" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="SqlList">
                    <Columns>
                        <asp:BoundField DataField="comName" HeaderText="Company" SortExpression="comName" />
                        <asp:BoundField DataField="dptName" HeaderText="Department" SortExpression="dptName" />
                        <asp:BoundField DataField="secName" HeaderText="Section" SortExpression="secName" />
                        <asp:BoundField DataField="usrFullName" HeaderText="Name" SortExpression="usrFullName" />
                        <asp:BoundField DataField="usrStaffID" HeaderText="Staff ID" SortExpression="usrStaffID" />
                        <asp:BoundField DataField="lrcStartDate" DataFormatString="{0:d}" HeaderText="Start Date" SortExpression="lrcStartDate" />
                        <asp:BoundField DataField="lrcStartTime" HeaderText="Time" SortExpression="lrcStartTime" />
                        <asp:BoundField DataField="lrcEndDate" DataFormatString="{0:d}" HeaderText="End Date" SortExpression="lrcEndDate" />
                        <asp:BoundField DataField="lrcEndTime" HeaderText="Time" SortExpression="lrcEndTime" />
                        <asp:BoundField DataField="lrcDays" HeaderText="Days" SortExpression="lrcDays" />
                        <asp:BoundField DataField="ltyName" HeaderText="Leave Type" SortExpression="ltyName" />
                        <asp:BoundField DataField="lrcCancelReason" Visible="False" HeaderText="Cancel Reason" SortExpression="lrcCancelReason" />
                    </Columns>
                    <RowStyle CssClass="greytable" />
                    <AlternatingRowStyle CssClass="whitetable" />
                    <PagerStyle CssClass="greytable" />
                    <HeaderStyle HorizontalAlign="Left" ForeColor="#FFFFFF" CssClass="tablecolors_title" />                            
                    <EmptyDataTemplate>
                    There are no records to show in this report
                    </EmptyDataTemplate>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlList" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                    SelectCommandType="StoredProcedure"
                    SelectCommand="stpLeaveCalendar_Report">
                    <SelectParameters>
                        <asp:QueryStringParameter QueryStringField="comId" Name="comId" DefaultValue="0" Type="Int32" />
                        <asp:QueryStringParameter QueryStringField="dptId" Name="dptId" DefaultValue="0" Type="Int32" />
                        <asp:QueryStringParameter QueryStringField="secId" Name="secId" DefaultValue="0" Type="Int32" />
                        <asp:QueryStringParameter QueryStringField="usrId" Name="usrId" DefaultValue="0" Type="Int32" />
                        <asp:ControlParameter ControlID="lblCurrent" PropertyName="Text" Name="CheckToday" DefaultValue="YES" Type="String" />
                        <asp:ControlParameter ControlID="chkCancelled" PropertyName="Checked" Name="lrcCancelled" Type="Boolean" />
                        <asp:QueryStringParameter QueryStringField="sdt" Name="lrcStartDate1" Type="DateTime" />
                        <asp:QueryStringParameter QueryStringField="edt" Name="lrcStartDate2" Type="DateTime" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:Label ID="lblCurrent" runat="server" Visible="False"></asp:Label>
                <asp:CheckBox id="chkCancelled" runat="server" Visible="False" />
            </td> 
        </tr> 
     </table> 
</asp:Content>

