﻿Imports System.Data.SqlClient
Imports lvManLib.Security
Imports lvManLib.DataFunctions
Imports lvManLib.Logic
Imports lvManLib.Config
Imports lvManLib.Leave
Imports lvManLib.General

Partial Class leavecompensation
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim strTemp As String

            'check whether user has approvers setup
            strTemp = GetSpDataSingleValue("stpCheckUserApprovers", , "usrId=" & Session("usrId"))

            If CType(strTemp, Integer) > 0 Then
                lblMessage.Text = "You currently have no approvers setup. Please alert HR of this anomaly, as you will not be able to apply for leave."
                cmdAddConfig.Enabled = False
                cmdAddConfig.ForeColor = Drawing.Color.Gray
            End If

            lblTransactionID.Text = GetTransactionID()
            lstReasons.DataBind()

            lblUnits.Text = Session("comLeaveUnit")
            If lblUnits.Text = "H" Then
                lblUnits.Text = "Hours:"
                lblViewUnits.Text = "Hours:"
            Else
                lblUnits.Text = "Days:"
                lblViewUnits.Text = "Days:"
            End If
        End If
    End Sub


    Protected Sub cmdUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
        If Page.IsValid Then
            LoadReadOnly()
            trConfirmRow.Visible = True
            grvList0.DataBind()
        End If

    End Sub

    Sub GetApprovals()
        Dim strTemp As String = ""
        Dim intCount As Integer

        Do Until Len(strTemp) > 0
            strTemp = GetSpDataSingleValue("stpGetLatestLeaveCompRequest", , "usrId=" & Session("usrId"))
            intCount += 1
            If Len(strTemp) = 0 Then
                System.Threading.Thread.Sleep(1000)
                If intCount > 10 Then strTemp = "0"
            End If
        Loop
        AlertApprovers(strTemp, "LC")

    End Sub

    Sub ClearFields()
        txtDays.Text = ""
        txtDescription.Text = ""
        lstReasons.SelectedIndex = 0
        cmdUpdate.Visible = True
        lblMessage.Text = ""
        trApprovals1.Visible = False
        trApprovals2.Visible = False
        lblTransactionID.Text = GetTransactionID()
        grvList0.Columns(2).Visible = True
        lblRelatedDocuments.Text = "<a href=""javascript:openwin_small('/uploadrelateddocs.aspx?trn=" & lblTransactionID.Text & "&ent=C')""><b>Attach Related Document(s)</b></a>"
    End Sub


    Protected Sub cmdAddMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddConfig.Click
        cmdUpdate.Text = "Add Request"
        ClearFields()
        trConfirmRow.Visible = False
        trAddRow.Visible = True
        cmdConfirm.Text = "Confirm - Add Request"
        LoadWrite()
        EditMode(True)
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        EditMode(False)
    End Sub

    Protected Sub cmdViewMenus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdViewConfigs.Click
        EditMode(False)
    End Sub

    Sub EditMode(ByVal bolEdit As Boolean, Optional ByVal bolClearFields As Boolean = True)
        EnableDisableFields(True)

        If bolEdit Then
            If bolClearFields Then ClearFields()
            panList.Visible = False
            panAddEdit.Visible = True
            tdAddEdit.Style.Item("background-image") = "url(/images/orangetab.png);"
            tdView.Style.Item("background-image") = "url(/images/darkbluetab.png);"
        Else
            cmdAddConfig.Text = "Add Request"
            panList.Visible = True
            panAddEdit.Visible = False
            tdAddEdit.Style.Item("background-image") = "url(/images/darkbluetab.png);"
            tdView.Style.Item("background-image") = "url(/images/orangetab.png);"
            grvList.DataBind()
        End If
    End Sub

    Sub EnableDisableFields(ByVal bolEnabled As Boolean)
        txtDescription.Enabled = bolEnabled
        lstReasons.Enabled = bolEnabled
        lstReasons.Enabled = bolEnabled
        txtDays.Enabled = bolEnabled
        cmdUpdate.Enabled = bolEnabled
    End Sub

    Protected Sub grvList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvList.SelectedIndexChanged
        GetUpdateForm(grvList.SelectedValue)
        trConfirmRow.Visible = False
    End Sub

    Sub GetUpdateForm(ByVal intlcoId As Integer)
        Dim objDR As SqlDataReader
        Dim intstaId As Integer

        objDR = GetSpDataReader("stpLeaveCompRequests_View", , "lcoId=" & intlcoId)
        cmdUpdate.Visible = True
        lblMessage.Text = ""

        If objDR.Read Then
            lblTransactionID.Text = objDR("lcoTransactionID") & ""
            txtDescription.Text = objDR("lcoReasonDesc") & ""
            txtDays.Text = objDR("lcoUnits") & ""
            intstaId = objDR("lcostaId")
            lstReasons.SelectedIndex = lstReasons.Items.IndexOf(lstReasons.Items.FindByValue(objDR("lcoareId")))
            lblStatus.Text = objDR("staName") & ""

            Select Case intstaId
                Case 1
                    cmdUpdate.Text = "Update Request"
                    cmdConfirm.Text = "Confirm - Update Request"

                Case 6
                    lblMessage.Text = "Please revise this leave compensation, as requested by your approver"
                    lblMessage.BackColor = Drawing.Color.Yellow
                    cmdUpdate.Text = "Revise Request"
                    cmdConfirm.Text = "Confirm - Revise Request"

                Case Else
                    cmdUpdate.Visible = False
                    lblMessage.Text = "This request is no longer pending, and thus may not be edited."
                    LoadReadOnly()

            End Select

            trApprovals1.Visible = True
            trApprovals2.Visible = True
            grvApprovals.DataBind()
            grvList0.DataBind()
            grvList0.Columns(2).Visible = False
            lblRelatedDocuments.Text = ""

        End If

        objDR.Close()
        objDR = Nothing
        EditMode(True, False)
    End Sub

    Function GetApprover(ByVal strName As String) As String
        If Len(strName) > 2 Then
            Return strName
        Else
            Return "Human Resources"
        End If
    End Function

    Function GetStatus(ByVal strStatus As String) As String
        Select Case strStatus
            Case "Pending"
                Return "<b><i>Pending</i></b>"
            Case "Rejected"
                Return "<font color='red'><b>Rejected</b></font>"
            Case "Approved"
                Return "<font color='green'><b>Approved</b></font>"
            Case "Cancelled"
                Return "<font color='orange'>Cancelled</font>"
            Case "On Hold"
                Return "<b>On Hold</b>"
            Case "Revise Suggested"
                Return "<font color='orange'><b>Revise Suggested</b></font>"
            Case Else
                Return strStatus
        End Select
    End Function

    Function GetDocument(ByVal strTitle As String, ByVal strPath As String) As String
        Dim strReturn As String
        If Len(strTitle & "") = 0 Then strTitle = "untitled document"
        strReturn = "<a href='" & strPath & "' target='_blank'>" & strTitle & "</a>"
        Return strReturn
    End Function

    Protected Sub cmdCancelConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancelConfirm.Click
        LoadWrite()
        trConfirmRow.Visible = False
    End Sub

    Protected Sub cmdConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdConfirm.Click
        Dim strTemp As String = ""
        Dim intCount As Integer

        lblMessage.Text = ""

        If Page.IsValid Then
            If cmdUpdate.Text = "Add Request" Then
                SqlList.Insert()
                GetApprovals()

                Do Until Len(strTemp) > 0
                    strTemp = GetSpDataSingleValue("stpGetLatestLeaveCompRequest", , "usrId=" & Session("usrId"))

                    intCount += 1
                    If Len(strTemp) = 0 Then
                        System.Threading.Thread.Sleep(1000)
                        If intCount > 10 Then strTemp = "0"
                    End If
                Loop

                ExecuteStoredProc("stpUpdateLeaveDocumentsID", , "ldcEntityID=" & strTemp & "*ldcTransactionID=" & lblTransactionID.Text)
            Else
                SqlList.Update()

                If cmdUpdate.Text = "Revise Request" Then
                    Dim strMailMessage As String = ""
                    Dim strApproversEmail As String = ""
                    Dim strApproversName As String = ""
                    Dim strMailTitle As String
                    Dim objDR As SqlDataReader
                    Dim strURL As String = GetConfig("SystemURL")

                    objDR = GetSpDataReader("stpApprovalPathCompensation_Revision", , "lrqId=" & grvList.SelectedValue)

                    If objDR.Read Then
                        strApproversEmail = objDR("usrEmailAdd") & ""
                        strApproversName = objDR("usrFullName") & ""
                    End If

                    objDR.Close()
                    objDR = Nothing

                    ExecuteStoredProc("stpApprovalPathCompensation_UpdateRevision", , "lrqId=" & grvList.SelectedValue)

                    If Len(strApproversEmail & "") > 0 Then
                        strMailMessage = GetMailTemplate("ALCRAA", Session("comId"), "[[NAME]]|[[APPROVER]]|[[URL]]", Session("usrFullName") & "|" & strApproversName & "|" & strURL)

                        If Len(strMailMessage) > 10 Then
                            strMailTitle = Mid(strMailMessage, 1, InStr(strMailMessage, "|}*") - 1)
                            strMailMessage = Mid(strMailMessage, InStr(strMailMessage, "|}*") + 3)
                            strMailMessage = Replace(strMailMessage, Chr(13), "<br />")
                            strMailMessage = Replace(strMailMessage, vbCrLf, "<br />")
                            SendMail(strMailTitle, strMailMessage, strApproversEmail)
                        End If
                    End If

                End If
            End If

            EditMode(False)
        End If
    End Sub

    Sub LoadConfirmForm()
        LoadReadOnly()
        trConfirmRow.Visible = True
    End Sub

    Sub LoadReadOnly()
        lblReason.Text = lstReasons.SelectedItem.Text
        lblDays.Text = txtDays.Text
        lblDescription.Text = txtDescription.Text
        trViewDays.Visible = True
        trViewDescription.Visible = True
        trViewReason.Visible = True
        trDescription.Visible = False
        trReason.Visible = False
        trDays.Visible = False
        trAddRow.Visible = False
    End Sub

    Sub LoadWrite()
        trViewDays.Visible = False
        trViewDescription.Visible = False
        trViewReason.Visible = False
        trDescription.Visible = True
        trReason.Visible = True
        trDays.Visible = True
    End Sub

End Class
