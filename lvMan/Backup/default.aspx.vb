Imports lvManLib.Security
Imports lvManLib.DataFunctions
Imports lvManLib.General
Imports lvManLib.Config
Imports System.Data.SqlClient

Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim strCompanyName As String
            Dim strLoginType As String


            strCompanyName = GetConfig("CompanyName")
            lblCompany.Text = GetConfig("CompanyLogo")
            strLoginType = GetConfig("LoginType")
            lblLoginType.Text = strLoginType

            Select Case strLoginType
                Case "U" 'UserName
                    lblUserName.Text = "User Name:"
                Case "E" 'Email Add
                    lblUserName.Text = "Email Address:"
                Case "W" 'Windows
                    'do nothing for now
                Case Else
                    lblUserName.Text = "User Name:"
                    lblLoginType.Text = "U"
            End Select

            If Len(lblCompany.Text) > 0 Then
                lblCompany.Text = "<img alt='" & Replace(strCompanyName, "'", "") & "' title='" & Replace(strCompanyName, "'", "") & "' src='" & lblCompany.Text & "' />"
            Else
                lblCompany.Text = strCompanyName
            End If

            If Len(Request.QueryString("errm") & "") > 0 Then lblMessage.Text = "Your login session may have timed out. Please login."

            If Not IsValidLicense() Then
                lblInvalidLicence.Visible = True
                tblValidBox.Visible = False
            End If
        End If
    End Sub

    Protected Sub cmdForgotPassword_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdForgotPassword.Click
        txtPassword.Text = "12345"
        trPassword.Visible = False
        cmdLogin.Visible = False
        cmdSendPassword.Visible = True
        cmdForgotPassword.Visible = False
        lblTitle.Text = "Forgot your password? Just supply the below and we will send instructions to your email address."
        lblUserName.Text = "Email address:"
        cmdCancelForgotPassword.Visible = True
    End Sub

    Protected Sub cmdCancelForgotPassword_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancelForgotPassword.Click
        lblTitle.Text = "Sign in to LeaveFlow"
        txtPassword.Text = ""
        trPassword.Visible = True
        cmdLogin.Visible = True
        cmdForgotPassword.Visible = True
        cmdSendPassword.Visible = False
        cmdCancelForgotPassword.Visible = False
    End Sub

    Protected Sub cmdSendPassword_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSendPassword.Click
        If Page.IsValid Then
            Dim strTemp As String
            Dim strNewPassword As String
            Dim strNewEncPassword As String
            Dim strMailMessage As String
            Dim strMailTitle As String
            Dim strName As String
            Dim strcomId As String
            Dim objDR As SqlDataReader
            Dim strURL As String = GetConfig("SystemURL") & ""
            If Len(strURL) = 0 Then strURL = "http://www.leaveflow.com"

            objDR = GetSpDataReader("stpUsers_MinView", , "usrEmailAdd=" & txtUserName.Text)

            If Not objDR.HasRows Then
                objDR.Close()
                objDR = Nothing
                lblMessage.Text = "The specified email address was not found on record."
                Exit Sub
            End If

            objDR.Read()

            strTemp = objDR("usrName")
            strNewPassword = "alm12" & Second(Now()) & Minute(Now()) & Day(Now()) & "Xuk!" & Hour(Now())
            strNewEncPassword = EncryptPassword(strNewPassword, LCase(strTemp))
            strcomId = objDR("dptcomId")
            If Len(strcomId) = 0 Then strcomId = "1"
            strName = objDR("usrFullName")

            ExecuteStoredProc("stpUser_ChangePassword_Self", , "usrPassword=" & strNewEncPassword & "*usrName=" & strTemp)


            strMailMessage = GetMailTemplate("UFP", strcomId, "[NAME]|[PASSWORD]|[URL]", strName & "|" & strNewPassword & "|" & strURL)

            strMailTitle = Mid(strMailMessage, 1, InStr(strMailMessage, "|}*") - 1)
            strMailMessage = Mid(strMailMessage, InStr(strMailMessage, "|}*") + 3)

            SendMail(strMailTitle, strMailMessage, txtUserName.Text)

            lblMessage.Text = "Your password has been sent to the specified email address."
        End If
    End Sub



    Protected Sub cmdLogin_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmdLogin.Click
        If Page.IsValid Then
            If ValidateUser(txtUserName.Text, txtPassword.Text, True, lblLoginType.Text) Then
                Response.Redirect("main.aspx")
            Else
                lblMessage.Text = "Login failed - Invalid username/ password or account not active"
            End If
        End If
    End Sub
End Class