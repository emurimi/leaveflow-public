<%@ Page Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.SystemAdministration_Users" title="System Users" Codebehind="users.aspx.vb" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
 
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 100%;
        }
        .style3
        {
            text-align: left;
        }
    </style>
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">

      
            <asp:Label ID="lblLoginType" runat="server" Visible="False"></asp:Label>
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>

                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td>    
                                            <asp:Label ID="lblMaxUsers" Font-Bold="True" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                
                                    <tr id="trCommands" runat="server">
                                        <td>
                                            <table width="290" border="0" cellspacing="0" cellpadding="0">
                                              <tr>
                                                <td id="tdViewUsers" runat="server" width="144" height="29" align="center" valign="middle" background='/images/orangetab.png'>
                                                    <asp:LinkButton CssClass="whitetext" ID="cmdViewUsers" CausesValidation="False" Font-Bold="True" runat="server">View Users</asp:LinkButton></td>
                                                <td width="2"></td>
                                                <td id="tdAddEditUsers" runat="server" width="144" align="center" valign="middle" background='/images/darkbluetab.png'>
                                                <asp:LinkButton CssClass="whitetext" CausesValidation="False" Font-Bold="True" ID="cmdAddUser" runat="server">Add 
                                                    User</asp:LinkButton></td>
                                              </tr>
                                            </table>                    
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" valign="top">
                                            <asp:Panel ID="panList" runat="server" Width="100%">
                                                <table width="100%" border="0" cellpadding="2" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <table cellpadding="2" cellspacing="0" class="style2">
                                                                <tr id="trBranchesCompanies" runat="server">
                                                                    <td>
                                                                        <asp:DropDownList ID="lstSelCompanies" runat="server" CssClass="listmenu" 
                                                                            DataSourceID="SqlCompanies" DataTextField="comName" DataValueField="comId" 
                                                                            AutoPostBack="True">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="lstSelBranches" runat="server" AutoPostBack="True" 
                                                                            CssClass="listmenu" DataSourceID="SqlBranches" DataTextField="braName" 
                                                                            DataValueField="braId">
                                                                        </asp:DropDownList>
                                                                        <asp:SqlDataSource ID="SqlBranches" runat="server" 
                                                                            ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                                            SelectCommandType="StoredProcedure"
                                                                            SelectCommand="stpBranches_View">
                                                                            <SelectParameters>
                                                                                <asp:ControlParameter ControlID="lstSelCompanies" Name="bracomId" PropertyName="SelectedValue" Type="Int32" />
                                                                            </SelectParameters>
                                                                        </asp:SqlDataSource>
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:DropDownList ID="lstSelDepartments" runat="server" CssClass="listmenu"  AutoPostBack="True"
                                                                            DataSourceID="SqlDepts" DataTextField="dptName" DataValueField="dptId">
                                                                        </asp:DropDownList>
                                                                        <asp:SqlDataSource ID="SqlDepts" runat="server" 
                                                                            ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                                            SelectCommandType="StoredProcedure"
                                                                            SelectCommand="stpDepartments_View">
                                                                            <SelectParameters>
                                                                                <asp:ControlParameter ControlID="lstSelCompanies" Name="dptcomId" PropertyName="SelectedValue" Type="Int32" />
                                                                            </SelectParameters>
                                                                        </asp:SqlDataSource>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="lstSelGroups" runat="server" CssClass="listmenu" AutoPostBack="True"
                                                                            DataSourceID="SqlGroups" DataTextField="grpName" DataValueField="grpId">
                                                                        </asp:DropDownList>
                                                                        <asp:SqlDataSource ID="SqlGroups" runat="server" 
                                                                            ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                                            SelectCommandType="StoredProcedure"
                                                                            SelectCommand="stpGroups_View">
                                                                        </asp:SqlDataSource>
                                                                    </td>
                                                                    <td>
                                                                        Name:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtSelName" runat="server" CssClass="textbox"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Button ID="cmdSearch" runat="server" CssClass="button" 
                                                                            Text="search &gt;&gt;" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>

                                                        <td>
                                                            <table width="100%" cellpadding="0" cellspacing="0" border="1" bordercolor="#cccccc" style="border-collapse:collapse">
                                                                <tr>
                                                                    <td>
                                                                        <asp:GridView ID="grvList" BorderColor="#EEEEEE" runat="server" Width="100%" AllowPaging="True" CssClass="gridviewcontents"
                                                                            AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="usrId" PageSize="50"
                                                                            DataSourceID="SqlList" RowStyle-Height="20">
                                                                            <RowStyle Height="20px" />
                                                                            <Columns>
                                                                                <asp:BoundField DataField="usrId" HeaderText="usrId" InsertVisible="False" ReadOnly="True" SortExpression="usrId" Visible="False" />
                                                                                <asp:BoundField DataField="usrName" HeaderText="User Name" SortExpression="usrName" />
                                                                                <asp:BoundField DataField="usrStaffID" HeaderText="Staff ID" SortExpression="usrStaffID" />
                                                                                <asp:BoundField DataField="usrFullName" HeaderText="Name" SortExpression="usrFullName" />
                                                                                <asp:BoundField DataField="braName" HeaderText="Branch" SortExpression="braName" />
                                                                                <asp:BoundField DataField="dptName" HeaderText="Department" SortExpression="dptName" />
                                                                                <asp:BoundField DataField="secName" HeaderText="Section" SortExpression="secName" />
                                                                                <asp:BoundField DataField="posName" HeaderText="Position" SortExpression="posName" />
                                                                                <asp:BoundField DataField="graName" HeaderText="Grade" SortExpression="graName" />
                                                                                <asp:BoundField DataField="usrSex" HeaderText="Sex" SortExpression="usrSex" />
                                                                                <asp:BoundField DataField="usrStatus" HeaderText="Empl Status" SortExpression="usrStatus" />
                                                                                <asp:CheckBoxField DataField="usrActive" HeaderText="Active" SortExpression="usrActive" />
                                                                                
                                                                                <asp:CommandField SelectText="View/Edit" ShowSelectButton="True" >
                                                                                    <ItemStyle Font-Bold="True" />
                                                                                </asp:CommandField>
                                                                            </Columns>
                                                                            <RowStyle CssClass="greytable" />
                                                                            <AlternatingRowStyle CssClass="whitetable" />
                                                                            <PagerStyle CssClass="greytable" />
                                                                            <HeaderStyle HorizontalAlign="Left" ForeColor="#FFFFFF" CssClass="tablecolors_title" />                            
                                                                            <EmptyDataTemplate>
                                                                                No system users found using the selected criteria
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>    

                                                                    
                                                                    </td>
                                                                </tr>
                                                            </table>

                                                        </td>
                                                    </tr>
                                                </table>
                                                <asp:SqlDataSource ID="SqlList" runat="server" 
                                                    ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                    SelectCommandType="StoredProcedure"
                                                    DeleteCommandType="StoredProcedure"
                                                    InsertCommandType="StoredProcedure"
                                                    UpdateCommandType="StoredProcedure"   
                                                    DeleteCommand="stpUsers_Delete"                                                 
                                                    SelectCommand="stpUsers_View" 
                                                    InsertCommand="stpUsers_Add" 
                                                    UpdateCommand="stpUsers_Update">
                                                    <SelectParameters>
                                                        <asp:ControlParameter ControlID="txtSelName" PropertyName="Text" Name="usrName" DbType="String" DefaultValue="NONE" />
                                                        <asp:ControlParameter ControlID="lstSelGroups" PropertyName="SelectedValue" Name="usrgrpId" DbType="Int32" DefaultValue="0" />
                                                        <asp:ControlParameter ControlID="lstSelCompanies" PropertyName="SelectedValue" Name="comId" DbType="Int32" DefaultValue="0" />
                                                        <asp:ControlParameter ControlID="lstSelBranches" PropertyName="SelectedValue" Name="usrbraId" DbType="Int32" DefaultValue="0" />
                                                        <asp:ControlParameter ControlID="lstSelDepartments" PropertyName="SelectedValue" Name="usrdptId" DbType="Int32" DefaultValue="0" />
                                                    </SelectParameters>
                                                    <DeleteParameters>
                                                        <asp:Parameter Name="usrId" Type="Int32" />
                                                    </DeleteParameters>
                                                    <UpdateParameters>
                                                        <asp:ControlParameter ControlID="txtUserName" Name="usrName" PropertyName="Text" Type="String" />
                                                        <asp:ControlParameter ControlID="txtStaffID" Name="usrStaffID" PropertyName="Text" Type="String" />
                                                        <asp:ControlParameter ControlID="txtFullName" Name="usrFullName" PropertyName="Text" Type="String" />
                                                        <asp:ControlParameter ControlID="lblPassword" Name="usrPassword" PropertyName="Text" Type="String" />
                                                        <asp:ControlParameter ControlID="lstDepartments" Name="usrdptId" PropertyName="SelectedValue" Type="Int32" />
                                                        <asp:ControlParameter ControlID="lstSections" DefaultValue="0" Name="usrsecId" PropertyName="SelectedValue" Type="Int32" />
                                                        <asp:ControlParameter ControlID="lstPositions" Name="usrposId" PropertyName="SelectedValue" Type="Int32" />
                                                        <asp:ControlParameter ControlID="lstGrades" Name="usrgraId" PropertyName="SelectedValue" Type="Int32" />
                                                        <asp:ControlParameter ControlID="lstGender" PropertyName="SelectedValue" Name="usrSex" Type="String" />
                                                        <asp:ControlParameter ControlID="lstStatus" PropertyName="SelectedValue" Name="usrStatus" Type="String" />
                                                        <asp:ControlParameter ControlID="chkActive" PropertyName="Checked" Name="usrActive" Type="Boolean" />
                                                        <asp:ControlParameter ControlID="txtDateEmployed" PropertyName="Text" Name="usrDateEmployed" Type="DateTime" />
                                                        <asp:ControlParameter ControlID="txtDescription" PropertyName="Text" Name="usrDescription" Type="String" />
                                                        <asp:ControlParameter ControlID="chkSystemAccount" PropertyName="Checked" Name="usrIsSystemAccount" Type="Boolean" />
                                                        <asp:ControlParameter ControlID="txtEmailAdd" Name="usrEmailAdd" PropertyName="Text" Type="String" />
                                                        <asp:ControlParameter ControlID="lstBranches" DefaultValue="0" Name="usrbraId" PropertyName="SelectedValue" Type="Int32" />
                                                        <asp:ControlParameter ControlID="chkPasswordChange" PropertyName="Checked" Name="usrChangePassword" Type="Boolean" />
                                                        <asp:ControlParameter ControlID="grvList" PropertyName="SelectedValue" Name="usrId" Type="Int32" />
                                                    </UpdateParameters>
                                                    <InsertParameters>
                                                    
                                                        <asp:ControlParameter ControlID="txtOpeningDays" Name="usrOpeningDays" PropertyName="Text" Type="Int32" />
                                                        <asp:ControlParameter ControlID="lstBranches" DefaultValue="0" Name="usrbraId" PropertyName="SelectedValue" Type="Int32" />
                                                        <asp:ControlParameter ControlID="txtUserName" Name="usrName" PropertyName="Text" Type="String" />
                                                        <asp:ControlParameter ControlID="txtStaffID" Name="usrStaffID" PropertyName="Text" Type="String" />
                                                        <asp:ControlParameter ControlID="txtFullName" Name="usrFullName" PropertyName="Text" Type="String" />
                                                        <asp:ControlParameter ControlID="txtEmailAdd" Name="usrEmailAdd" PropertyName="Text" Type="String" />
                                                        <asp:ControlParameter ControlID="lblPassword" Name="usrPassword" PropertyName="Text" Type="String" />
                                                        <asp:ControlParameter ControlID="lstDepartments" Name="usrdptId" PropertyName="SelectedValue" Type="Int32" />
                                                        <asp:ControlParameter ControlID="lstSections" DefaultValue="0" Name="usrsecId" PropertyName="SelectedValue" Type="Int32" />
                                                        <asp:ControlParameter ControlID="lstPositions" Name="usrposId" PropertyName="SelectedValue" Type="Int32" />
                                                        <asp:ControlParameter ControlID="lstStatus" PropertyName="SelectedValue" Name="usrStatus" Type="String" />
                                                        <asp:ControlParameter ControlID="lstGrades" Name="usrgraId" PropertyName="SelectedValue" Type="Int32" />
                                                        <asp:ControlParameter ControlID="lstGender" PropertyName="SelectedValue" Name="usrSex" Type="String" />
                                                        <asp:ControlParameter ControlID="chkActive" PropertyName="Checked" Name="usrActive" Type="Boolean" />
                                                        <asp:ControlParameter ControlID="txtDateEmployed" PropertyName="Text" Name="usrDateEmployed" Type="DateTime" />
                                                        <asp:ControlParameter ControlID="txtDescription" PropertyName="Text" Name="usrDescription" Type="String" />
                                                        <asp:ControlParameter ControlID="chkSystemAccount" PropertyName="Checked" Name="usrIsSystemAccount" Type="Boolean" />
                                                        <asp:ControlParameter ControlID="chkPasswordChange" PropertyName="Checked" Name="usrChangePassword" Type="Boolean" />
                                                    </InsertParameters>
                                                </asp:SqlDataSource>
                                            </asp:Panel>
                                            <asp:Panel ID="panAddEdit" runat="server" Width="100%" Visible="False">
                                                  <table width="98%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                      <td width="15"><img src="/images/lefttop.jpg" alt="LeaveFlow" width="15" height="15" /></td>
                                                      <td height="15" background="/images/topside.jpg">&nbsp;</td>
                                                      <td width="15"><img src="/images/righttop.jpg" alt="LeaveFlow" width="15" height="15" /></td>
                                                    </tr>
                                                    <tr>
                                                      <td background="/images/leftside.jpg">&nbsp;</td>
                                                      <td valign="top">                            
                                                        <asp:Label ID="lblMessage" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>                
                                                        <table cellpadding="3" id="tblAddEdit" runat="server" cellspacing="0" 
                                                            width="100%" class="tablewithborders">
                                                              <tr><td> 
                                                              <fieldset class="" ><legend><b>Identity</b></legend>
                                                                    <table width="100%">
                                                             
                                                                     <tr>
                                                                         <td width="25%">
                                                                             Staff ID:</td>
                                                                         <td width="25%">
                                                                             <asp:TextBox ID="txtStaffID" runat="server" CssClass="textbox"></asp:TextBox>
                                                                         </td>
                                                                         <td width="25%">
                                                                             Gender:</td>
                                                                         <td width="25%">
                                                                             <asp:DropDownList ID="lstGender" runat="server" CssClass="listmenu">
                                                                                 <asp:ListItem Selected="True" Value="M">Male</asp:ListItem>
                                                                                 <asp:ListItem Value="F">Female</asp:ListItem>
                                                                             </asp:DropDownList>
                                                                         </td>
                                                                     </tr>
                                                                     <tr>
                                                                         <td width="25%">
                                                                             Full Name:</td>
                                                                         <td width="25%">
                                                                             <asp:TextBox ID="txtFullName" runat="server" CssClass="textbox"></asp:TextBox>
                                                                             <asp:RequiredFieldValidator ID="rvaFullName" runat="server" 
                                                                                 ControlToValidate="txtFullName" Display="Dynamic" 
                                                                                 ErrorMessage="* please enter full name" ValidationGroup="AddUser"></asp:RequiredFieldValidator>
                                                                         </td>
                                                                         <td width="25%">
                                                                             <asp:Label ID="lblUserName" runat="server" Text="User Name:"></asp:Label>
                                                                         </td>
                                                                         <td width="25%">
                                                                             <asp:TextBox ID="txtUserName" runat="server" CssClass="textbox"></asp:TextBox>
                                                                             <asp:RequiredFieldValidator ID="rvaUserName" runat="server" 
                                                                                 ControlToValidate="txtUserName" Display="Dynamic" 
                                                                                 ErrorMessage="* please specify user name" ValidationGroup="AddUser"></asp:RequiredFieldValidator>
                                                                         </td>
                                                                     </tr>
                                                                     
                                                                     </table>
                                                                     </fieldset>
                                                                 </td></tr> 
                                                                 <tr><td>
                                                                 <fieldset  ><legend><b>Employment details</b></legend>
                                                                    <table width="100%">
                                                                 
                                                                 <tr>
                                                                     <td>
                                                                         Email Address:</td>
                                                                     <td>
                                                                         <asp:TextBox ID="txtEmailAdd" runat="server" CssClass="textbox"></asp:TextBox>
                                                                         <asp:RequiredFieldValidator ID="rvaFullName0" runat="server" 
                                                                             ControlToValidate="txtEmailAdd" Display="Dynamic" ErrorMessage="***" 
                                                                             ValidationGroup="AddUser"></asp:RequiredFieldValidator>
                                                                         <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                                                             ControlToValidate="txtEmailAdd" Display="Dynamic" 
                                                                             ErrorMessage="*** pls specify valid email" 
                                                                             ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                                                             ValidationGroup="AddUser"></asp:RegularExpressionValidator>
                                                                     </td>
                                                                     <td>
                                                                         Company:</td>
                                                                     <td>
                                                                         <asp:DropDownList ID="lstCompanies" runat="server" AutoPostBack="True" 
                                                                             CssClass="listmenu" DataSourceID="SqlCompanies" DataTextField="comName" 
                                                                             DataValueField="comId">
                                                                         </asp:DropDownList>
                                                                         <asp:SqlDataSource ID="SqlCompanies" runat="server" 
                                                                             ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                                             SelectCommand="stpCompanyDetails" SelectCommandType="StoredProcedure">
                                                                         </asp:SqlDataSource>
                                                                     </td>
                                                                 </tr>
                                                                 <tr>
                                                                     <td>
                                                                         <asp:Label ID="lblBranch" runat="server" Text="Branch:"></asp:Label>
                                                                     </td>
                                                                     <td>
                                                                         <asp:DropDownList ID="lstBranches" runat="server" CssClass="listmenu" 
                                                                             DataSourceID="SqlBranches2" DataTextField="braName" DataValueField="braId">
                                                                         </asp:DropDownList>
                                                                         <asp:SqlDataSource ID="SqlBranches2" runat="server" 
                                                                             ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                                             SelectCommand="stpBranches_View" SelectCommandType="StoredProcedure">
                                                                             <SelectParameters>
                                                                                 <asp:ControlParameter ControlID="lstCompanies" Name="bracomId" 
                                                                                     PropertyName="SelectedValue" Type="Int32" />
                                                                             </SelectParameters>
                                                                         </asp:SqlDataSource>
                                                                     </td>
                                                                     <td>
                                                                         Department:</td>
                                                                     <td>
                                                                         <asp:DropDownList ID="lstDepartments" runat="server" AutoPostBack="True" 
                                                                             CssClass="listmenu" DataSourceID="SqlDepartments0" DataTextField="dptName" 
                                                                             DataValueField="dptId">
                                                                         </asp:DropDownList>
                                                                         <asp:SqlDataSource ID="SqlDepartments0" runat="server" 
                                                                             ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                                             SelectCommand="stpDepartments_View" SelectCommandType="StoredProcedure">
                                                                             <SelectParameters>
                                                                                 <asp:ControlParameter ControlID="lstCompanies" Name="dptcomId" 
                                                                                     PropertyName="SelectedValue" Type="Int32" />
                                                                             </SelectParameters>
                                                                         </asp:SqlDataSource>
                                                                         <asp:RequiredFieldValidator ID="rvaDepartment0" runat="server" 
                                                                             ControlToValidate="lstDepartments" Display="Dynamic" 
                                                                             ErrorMessage="* please specify department" ValidationGroup="AddUser"></asp:RequiredFieldValidator>
                                                                     </td>
                                                                 </tr>
                                                                 <tr>
                                                                     <td>
                                                                         <asp:Label ID="lblSection" runat="server" Text="Section:"></asp:Label>
                                                                     </td>
                                                                     <td>
                                                                         <asp:DropDownList ID="lstSections" runat="server" CssClass="listmenu" 
                                                                             DataSourceID="SqlSections" DataTextField="secName" DataValueField="secId">
                                                                         </asp:DropDownList>
                                                                         <asp:SqlDataSource ID="SqlSections" runat="server" 
                                                                             ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                                             SelectCommand="stpSections_View" SelectCommandType="StoredProcedure">
                                                                             <SelectParameters>
                                                                                 <asp:ControlParameter ControlID="lstDepartments" Name="secdptId" 
                                                                                     PropertyName="SelectedValue" Type="Int32" />
                                                                             </SelectParameters>
                                                                         </asp:SqlDataSource>
                                                                     </td>
                                                                     <td>
                                                                         Position:</td>
                                                                     <td>
                                                                         <asp:DropDownList ID="lstPositions" runat="server" CssClass="listmenu" 
                                                                             DataSourceID="SqlPositions" DataTextField="posName" DataValueField="posId">
                                                                         </asp:DropDownList>
                                                                         <asp:SqlDataSource ID="SqlPositions" runat="server" 
                                                                             ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                                             SelectCommand="stpPositions_View" SelectCommandType="StoredProcedure">
                                                                         </asp:SqlDataSource>
                                                                     </td>
                                                                 </tr>
                                                                 <tr>
                                                                     <td>
                                                                         &nbsp;</td>
                                                                     <td>
                                                                         &nbsp;
                                                                     </td>
                                                                     <td>
                                                                         Grade:
                                                                     </td>
                                                                     <td>
                                                                         <asp:DropDownList ID="lstGrades" runat="server" CssClass="listmenu" 
                                                                             DataSourceID="SqlGrades" DataTextField="graName" DataValueField="graId">
                                                                         </asp:DropDownList>
                                                                         <asp:SqlDataSource ID="SqlGrades" runat="server" 
                                                                             ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                                             SelectCommand="stpGrades_View" SelectCommandType="StoredProcedure">
                                                                             <SelectParameters>
                                                                                 <asp:ControlParameter ControlID="lstCompanies" Name="gracomId" 
                                                                                     PropertyName="SelectedValue" Type="Int32" />
                                                                             </SelectParameters>
                                                                         </asp:SqlDataSource>
                                                                     </td>
                                                                 </tr>
                                                                 <tr>
                                                                     <td>
                                                                         Date employed:</td>
                                                                     <td>
                                                                         <asp:TextBox ID="txtDateEmployed" runat="server" CssClass="textbox" 
                                                                             onfocus="showCalendarControl(this);"></asp:TextBox>
                                                                     </td>
                                                                     <td>
                                                                         Description:</td>
                                                                     <td>
                                                                         <asp:TextBox ID="txtDescription" runat="server" CssClass="textbox"></asp:TextBox>
                                                                     </td>
                                                                 </tr>
                                                                 <tr>
                                                                     <td>
                                                                         Employment status:</td>
                                                                     <td>
                                                                         <asp:DropDownList ID="lstStatus" runat="server" CssClass="listmenu">
                                                                             <asp:ListItem Text="Employed" Value="E"></asp:ListItem>
                                                                             <asp:ListItem Text="Retired/ Resigned" Value="R"></asp:ListItem>
                                                                         </asp:DropDownList>
                                                                     </td>
                                                                     <td>
                                                                         Date of departure:</td>
                                                                     <td>
                                                                         <asp:Label ID="lblDateOfDeparture" runat="server"></asp:Label>
                                                                     </td>
                                                                 </tr>
                                                                 <tr>
                                                                     <td>
                                                                         Opening days:</td>
                                                                     <td>
                                                                         <asp:TextBox ID="txtOpeningDays" runat="server" CssClass="textbox"></asp:TextBox>
                                                                     </td>
                                                                     <td>
                                                                         &nbsp;</td>
                                                                     <td>
                                                                         &nbsp;</td>
                                                                 </tr>
                                                                 
                                                                 </table>
                                                                 </fieldset>
                                                                 </td></tr>

                                                                 <tr><td>
                                                                 <fieldset><legend> <b>Password</b>
                                                                         <asp:Label ID="lblChangePassword" runat="server" Visible="False">*Only specify 
                                                                    if you want to change the password</asp:Label></legend>
                                                                    <table width="100%">
                                                                 
                                                                 <tr>
                                                                     <td>
                                                                         Password:</td>
                                                                     <td>
                                                                         <asp:TextBox ID="txtPassword" runat="server" CssClass="textbox" 
                                                                             TextMode="Password"></asp:TextBox>
                                                                         <asp:RequiredFieldValidator ID="rvaPassword" runat="server" 
                                                                             ControlToValidate="txtPassword" Display="Dynamic" 
                                                                             ErrorMessage="* please specify password" ValidationGroup="AddUser"></asp:RequiredFieldValidator>
                                                                     </td>
                                                                     <td>
                                                                         Confirm Password:</td>
                                                                     <td>
                                                                         <asp:TextBox ID="txtConfirmPassword" runat="server" CssClass="textbox" 
                                                                             TextMode="Password"></asp:TextBox>
                                                                         <asp:RequiredFieldValidator ID="rvaConfirmPassword" runat="server" 
                                                                             ControlToValidate="txtConfirmPassword" Display="Dynamic" 
                                                                             ErrorMessage="* please specify password confirmation" ValidationGroup="AddUser"></asp:RequiredFieldValidator>
                                                                         <asp:CompareValidator ID="cvaPassword" runat="server" 
                                                                             ControlToCompare="txtPassword" ControlToValidate="txtConfirmPassword" 
                                                                             Display="Dynamic" ErrorMessage="* the passwords entered do not match!"></asp:CompareValidator>
                                                                         <asp:Label ID="lblPassword" runat="server" Visible="False"></asp:Label>
                                                                     </td>
                                                                 </tr>
                                                                 <tr>
                                                                     <td>
                                                                         Force password change:</td>
                                                                     <td>
                                                                         <asp:CheckBox ID="chkPasswordChange" runat="server" />
                                                                     </td>
                                                                     <td>
                                                                         &nbsp;</td>
                                                                     <td>
                                                                         &nbsp;</td>
                                                                 </tr>
                                                                 
                                                                 </table>
                                                                 </fieldset>
                                                                 </td></tr>
                                                                 <tr><td>
                                                                 <fieldset ><legend><b>Account status</b></legend>
                                                                    <table width="100%">
                                                                 
                                                                 <tr>
                                                                     <td>
                                                                         Is system account:</td>
                                                                     <td>
                                                                         <asp:CheckBox ID="chkSystemAccount" runat="server" />
                                                                     </td>
                                                                     <td>
                                                                         Active:</td>
                                                                     <td>
                                                                         <asp:CheckBox ID="chkActive" runat="server" />
                                                                     </td>
                                                                 </tr>
                                                                 <tr>
                                                                     <td>
                                                                         Date created:</td>
                                                                     <td>
                                                                         <asp:Label ID="lblDateCreated" runat="server"></asp:Label>
                                                                     </td>
                                                                     <td>
                                                                         &nbsp;</td>
                                                                     <td>
                                                                         &nbsp;</td>
                                                                 </tr>
                                                                 
                                                                 </table>
                                                                 </fieldset>
                                                                 </td></tr>
                                                                 <tr ID="trGroups" runat="server" visible="False"><td>
                                                                 <fieldset ><legend><b>User access rights</b></legend>
                                                                    <table width="100%">
                                                                 <tr >
                                                                     <td colspan="4">
                                                                         <table cellpadding="2" cellspacing="0" class="style1">
                                                                             <tr>
                                                                                 <td class="style3" width="50%">
                                                                                     </td>
                                                                                 <td style="text-align: center" width="50%">
                                                                                     &nbsp;</td>
                                                                             </tr>
                                                                             <tr>
                                                                                 <td style="text-align: center" width="50%">
                                                                                     <b>Available groups</b></td>
                                                                                 <td style="text-align: center" width="50%">
                                                                                     <b>Is a member of groups</b></td>
                                                                             </tr>
                                                                             <tr>
                                                                                 <td style="text-align: center">
                                                                                     <asp:ListBox ID="lstAvailableGroups" runat="server" CssClass="textbox" 
                                                                                         DataTextField="grpName" DataValueField="grpId" Rows="8" 
                                                                                         SelectionMode="Multiple"></asp:ListBox>
                                                                                 </td>
                                                                                 <td style="text-align: center">
                                                                                     <asp:ListBox ID="lstAllowedGroups" runat="server" CssClass="textbox" 
                                                                                         DataTextField="grpName" DataValueField="ugpId" Rows="8" 
                                                                                         SelectionMode="Multiple"></asp:ListBox>
                                                                                 </td>
                                                                             </tr>
                                                                             <tr>
                                                                                 <td style="text-align: center">
                                                                                     <asp:LinkButton ID="cmdAddMenu" runat="server" Font-Bold="True">ADD &gt;&gt;&gt;&gt;</asp:LinkButton>
                                                                                 </td>
                                                                                 <td style="text-align: center">
                                                                                     <asp:LinkButton ID="cmdRemoveGroup" runat="server" Font-Bold="True">&lt;&lt;&lt; REMOVE</asp:LinkButton>
                                                                                 </td>
                                                                             </tr>
                                                                         </table>
                                                                     </td>
                                                                 </tr>
                                                                 </table>
                                                                 </fieldset>
                                                                 </td></tr>
                                                                 <tr><td>
                                                                 <table>
                                                                 <tr>
                                                                     <td width="25%">
                                                                         <asp:Button ID="cmdCancel" runat="server" CausesValidation="False" 
                                                                             CssClass="button" Text="Cancel" />
                                                                     </td>
                                                                     <td style="text-align: left" width="25%">
                                                                         <asp:Button ID="cmdUpdate" runat="server" CssClass="button" Text="Add User" 
                                                                             ValidationGroup="AddUser" />
                                                                     </td>
                                                                     <td width="25%">
                                                                         &nbsp;</td>
                                                                     <td width="25%">
                                                                         &nbsp;</td>
                                                                 </tr>
                                                                 </table>
                                                                 </td></tr>
                                                                 
                                                        </table>
                                                  </td>
                                                  <td background="/images/rightside.jpg">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                  <td><img src="/images/leftbottom.jpg" alt="LeaveFlow" width="15" height="15" /></td>
                                                  <td height="15" background="/images/bottomside.jpg">&nbsp;</td>
                                                  <td><img src="/images/rightbottom.jpg" alt="right bottom" width="15" height="15" /></td>
                                                </tr>
                                              </table>    
                                            </asp:Panel>    

                                        </td>
                                    </tr>
                                 </table>
                      <table width="100%" cellpadding="0" cellspacing="0" border="0" id="tblContainer" runat="server" visible="False">
                          <tr>
                            <td width="7" valign="top"><img src='<%= ResolveUrl("~/images/left_curve.gif") %>' width="7" height="5" /></td>
                            <td valign="top" background='<%= ResolveUrl("~/images/centerline.gif") %>'><img src='<%= ResolveUrl("~/images/centerline.gif") %>' width="7" height="5" /></td>
                            <td width="5" valign="top"><img src='<%= ResolveUrl("~/images/topright_curve.gif") %>' width="5" height="5" /></td>
                          </tr>
                          <tr>
                            <td valign="top" background='<%= ResolveUrl("~/images/leftline.gif") %>'>&nbsp;</td>
                            <td valign="top">                                 
                            </td>
                            <td valign="top" background='<%= ResolveUrl("~/images/toprightline.gif") %>'><img src='<%= ResolveUrl("~/images/toprightline.gif") %>' width="5" height="5" /></td>
                          </tr>
                          <tr>
                            <td valign="top"><img src='<%= ResolveUrl("~/images/bottomleft_curve.gif") %>' width="7" height="7" /></td>
                            <td valign="top" background='<%= ResolveUrl("~/images/bottom_line.gif") %>'><img src='<%= ResolveUrl("~/images/bottom_line.gif") %>' width="5" height="7" /></td>
                            <td valign="top"><img src='<%= ResolveUrl("~/images/bottomright_curve.gif") %>' width="5" height="7" /></td>
                          </tr>                          
                      </table> 
                    </td>
                </tr>
           </table>
 <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>