﻿<%@ Page Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.SystemAdministration_Groups" title="System Groups" Codebehind="groups.aspx.vb" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">

            <table width="100%" cellpadding="3" cellspacing="0" border="0">
                <tr>
                    <td>
                        
                
                    </td>
                </tr>
                <tr>
                    <td width="100%">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr id="trCommands" runat="server">
                                <td>
                                    <table width="290" border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <td id="tdView" runat="server" width="144" height="29" align="center" valign="middle" background='/images/orangetab.png'>
                                            <asp:LinkButton CssClass="whitetext" ID="cmdViewGroups" runat="server">View Groups</asp:LinkButton></td>
                                        <td width="2"></td>
                                        <td id="tdAddEdit" runat="server" width="144" align="center" valign="middle" background='/images/darkbluetab.png'>
                                            <asp:LinkButton CssClass="whitetext" ID="cmdAddGroup" runat="server">Add Group</asp:LinkButton>
                                        </td>
                                      </tr>
                                    </table>                    
                                </td>
                            </tr>
                                  
                            <tr>
                                <td>
                                
                                        <asp:Panel ID="panList" runat="server" Width="100%">
                                            <asp:GridView ID="grvList" BorderColor="#EEEEEE" runat="server" Width="100%" AllowPaging="True"  CssClass="gridviewcontents"
                                                AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="grpId" 
                                                DataSourceID="SqlList" RowStyle-Height="20">
                                                <RowStyle Height="20px" />
                                                <Columns>
                                                    <asp:BoundField DataField="grpId" HeaderText="grpId" InsertVisible="False" ReadOnly="True" SortExpression="grpId" Visible="False" />
                                                    <asp:BoundField DataField="grpName" HeaderText="Name" SortExpression="grpName" />
                                                    <asp:BoundField DataField="grpDescription" HeaderText="Description" SortExpression="grpDescription" />
                                                    <asp:CommandField SelectText="View/Edit" ItemStyle-CssClass="gridlinks" ShowSelectButton="True" >
                                                        <ItemStyle CssClass="gridlinks" />
                                                    </asp:CommandField>
                                                </Columns>
                                                <RowStyle CssClass="greytable" />
                                                <AlternatingRowStyle CssClass="whitetable" />
                                                <PagerStyle CssClass="greytable" />
                                                <HeaderStyle HorizontalAlign="Left" ForeColor="#FFFFFF" CssClass="tablecolors_title" />                            

                                            </asp:GridView>    
                                            <asp:SqlDataSource ID="SqlList" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                SelectCommandType="StoredProcedure"
                                                DeleteCommandType="StoredProcedure"
                                                InsertCommandType="StoredProcedure"
                                                UpdateCommandType="StoredProcedure"                                                
                                                SelectCommand="stpGroups_View" 
                                                DeleteCommand="stpGroups_Delete" 
                                                InsertCommand="stpGroups_Add" 
                                                UpdateCommand="stpGroups_Update">
                                                <DeleteParameters>
                                                    <asp:Parameter Name="grpId" Type="Int32" />
                                                </DeleteParameters>
                                                <UpdateParameters>
                                                    <asp:ControlParameter ControlID="txtName" PropertyName="Text" Name="grpName" Type="String" />
                                                    <asp:ControlParameter ControlID="txtDescription" PropertyName="Text" Name="grpDescription" Type="String" />
                                                    <asp:ControlParameter ControlID="grvList" PropertyName="SelectedValue" Name="grpId" Type="Int32" />
                                                </UpdateParameters>
                                                <InsertParameters>
                                                    <asp:ControlParameter ControlID="txtName" PropertyName="Text" Name="grpName" Type="String" />
                                                    <asp:ControlParameter ControlID="txtDescription" PropertyName="Text" Name="grpDescription" Type="String" />
                                                </InsertParameters>
                                            </asp:SqlDataSource>
                                        </asp:Panel>

                                            
                                       
                                        <asp:Panel ID="panAddEdit" runat="server" Width="100%" Visible="False">
                                          <table width="400" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                              <td width="15"><img src="/images/lefttop.jpg" alt="LeaveFlow" width="15" height="15" /></td>
                                              <td height="15" background="/images/topside.jpg">&nbsp;</td>
                                              <td width="15"><img src="/images/righttop.jpg" alt="LeaveFlow" width="15" height="15" /></td>
                                            </tr>
                                            <tr>
                                              <td background="/images/leftside.jpg">&nbsp;</td>
                                              <td valign="top">                                        
                                                    <table cellpadding="3" id="tblAddEdit" runat="server" cellspacing="0" width="100%" class="tablewithborders">
                                                        <tr>
                                                            <td width="50%">
                                                                Group Name:</td>
                                                            <td width="50%">
                                                                <asp:TextBox ID="txtName" runat="server" CssClass="textbox"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rvaName" runat="server" ControlToValidate="txtName" 
                                                                    Display="Dynamic" ValidationGroup="AddGroup" 
                                                                    ErrorMessage="* please enter group name"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Description:</td>
                                                            <td>
                                                                <asp:TextBox ID="txtDescription" runat="server" Rows="4" TextMode="MultiLine" CssClass="textbox"></asp:TextBox>
                                                                    
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="cmdCancel" runat="server" CausesValidation="False" Text="Cancel" CssClass="button"></asp:Button>
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="cmdUpdate" runat="server" ValidationGroup="AddGroup" Text="Add Group" CssClass="button"></asp:Button>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td><asp:LinkButton ID="cmdDelete" OnClientClick="return confirm('Are you sure you want to delete this record?');" Font-Bold="True" runat="server">Delete Group</asp:LinkButton></td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr id="trGroupMenus1" runat="server">
                                                            <td colspan="2" bgcolor="#cccccc">
                                                                GROUP MENUS</td>
                                                        </tr>
                                                        <tr id="trGroupMenus2" runat="server">
                                                            <td>
                                                                Available Menus</td>
                                                            <td>
                                                                Allowed Menus</td>
                                                        </tr>
                                                        <tr id="trGroupMenus3" runat="server">
                                                            <td>
                                                                <asp:ListBox ID="lstAvailableMenus" CssClass="textbox" DataTextField="mnuText" DataValueField="mnuId" runat="server" Rows="15" 
                                                                    SelectionMode="Multiple"></asp:ListBox>
                                                            </td>
                                                            <td>
                                                                <asp:ListBox ID="lstAllowedMenus" CssClass="textbox" DataTextField="mnuText" DataValueField="gmnId" runat="server" Rows="15" 
                                                                    SelectionMode="Multiple"></asp:ListBox>
                                                            </td>
                                                        </tr>
                                                        <tr id="trGroupMenus4" runat="server">
                                                            <td>
                                                            <asp:CheckBox ID="chkAdd" runat="server" Text="Add" /> | 
                                                            <asp:CheckBox ID="chkEdit" runat="server" Text="Edit" /> | 
                                                            <asp:CheckBox ID="chkDelete" runat="server" Text="Delete" /> 
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr id="trGroupMenus5" runat="server">
                                                            <td>
                                                                <asp:LinkButton ID="cmdAddMenu" runat="server" Font-Bold="True">ADD &gt;&gt;&gt;&gt;</asp:LinkButton>
                                                            </td>
                                                            <td>
                                                                <asp:LinkButton ID="cmdRemoveMenu" runat="server" Font-Bold="True">&lt;&lt;&lt; REMOVE</asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                        
                                                    </table>
                                                  </td>
                                                  <td background="/images/rightside.jpg">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                  <td><img src="/images/leftbottom.jpg" alt="LeaveFlow" width="15" height="15" /></td>
                                                  <td height="15" background="/images/bottomside.jpg">&nbsp;</td>
                                                  <td><img src="/images/rightbottom.jpg" alt="right bottom" width="15" height="15" /></td>
                                                </tr>
                                              </table> 
                                        </asp:Panel>    
                                                
                                
                                
                                
                                </td>                    
                            </tr>
                       </table>
                    
                    

                    </td>
                </tr>

            </table>
</asp:Content>

