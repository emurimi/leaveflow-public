﻿Imports System.Data.SqlClient
Imports lvManLib.Security
Imports lvManLib.DataFunctions

Partial Class sysadmin_assets
    Inherits System.Web.UI.Page

    Public strUserPermissions As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        strUserPermissions = SecureCurrentPage()
        If InStr(strUserPermissions, "A") = 0 Then cmdAddConfig.Enabled = False
    End Sub

    Protected Sub cmdUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
        If Page.IsValid Then

            If cmdUpdate.Text = "Add Asset" Then
                SqlList.Insert()
            Else
                SqlList.Update()
            End If

            EditMode(False)
        End If
    End Sub


    Sub ClearFields()
        txtName.Text = ""
        txtDescription.Text = ""
        trAssignedTo1.Visible = False
        trAssignedTo2.Visible = False
        trAssignedTo3.Visible = False
        trAssignedTo4.Visible = False
    End Sub


    Sub GetUpdateForm(ByVal intassId As Integer)
        Dim objDR As SqlDataReader

        objDR = GetSpDataReader("stpAssets_View", , "assId=" & intassId)

        If objDR.Read Then
            txtName.Text = objDR("assName") & ""
            txtDescription.Text = objDR("assDescription") & ""
            lstAssetTypes.SelectedIndex = lstAssetTypes.Items.IndexOf(lstAssetTypes.Items.FindByValue(objDR("assastId")))
            lstCompanies.DataBind()
            lstCompanies.SelectedIndex = lstCompanies.Items.IndexOf(lstCompanies.Items.FindByValue(objDR("asscomId")))
            trAssignedTo1.Visible = True
            trAssignedTo2.Visible = True
            trAssignedTo3.Visible = True
            trAssignedTo4.Visible = True
            lstAvailableStaff.DataBind()
            lstAssignedStaff.DataBind()
        End If

        objDR.Close()
        objDR = Nothing

        cmdUpdate.Text = "Update Asset"
        cmdAddConfig.Text = cmdUpdate.Text
        EditMode(True, False)

    End Sub

    Protected Sub grvList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvList.SelectedIndexChanged
        GetUpdateForm(grvList.SelectedValue)
    End Sub

    Protected Sub cmdAddMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddConfig.Click
        cmdUpdate.Text = "Add Asset"
        EditMode(True)
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        EditMode(False)
    End Sub

    Protected Sub cmdViewMenus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdViewConfigs.Click
        EditMode(False)
    End Sub

    Sub EditMode(ByVal bolEdit As Boolean, Optional ByVal bolClearFields As Boolean = True)
        EnableDisableFields(True)

        If bolEdit Then
            If bolClearFields Then ClearFields()
            panList.Visible = False
            panAddEdit.Visible = True
            tdAddEdit.Style.Item("background-image") = "url(/images/orangetab.png);"
            tdView.Style.Item("background-image") = "url(/images/darkbluetab.png);"
            cmdDelete.Visible = False

            If cmdUpdate.Text = "Update Asset" Then
                If InStr(strUserPermissions, "E") = 0 Then EnableDisableFields(False)
                If InStr(strUserPermissions, "D") > 0 Then cmdDelete.Visible = True
            End If
        Else
            cmdAddConfig.Text = "Add Asset"
            panList.Visible = True
            panAddEdit.Visible = False
            tdAddEdit.Style.Item("background-image") = "url(/images/darkbluetab.png);"
            tdView.Style.Item("background-image") = "url(/images/orangetab.png);"
            grvList.DataBind()
        End If
    End Sub

    Sub EnableDisableFields(ByVal bolEnabled As Boolean)
        lstAssetTypes.Enabled = bolEnabled
        lstCompanies.Enabled = bolEnabled
        cmdAddSelected.Enabled = bolEnabled
        cmdRemoveSelected.Enabled = bolEnabled
        txtName.Enabled = bolEnabled
        txtDescription.Enabled = bolEnabled
        cmdUpdate.Enabled = bolEnabled
    End Sub

    Protected Sub cmdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        ExecuteStoredProc("stpAssets_Delete", , "assId=" & grvList.SelectedValue)
        EditMode(False)
    End Sub

    Protected Sub cmdAddSelected_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddSelected.Click
        Dim strSql As String
        Dim intCount As Integer

        For intCount = 0 To lstAvailableStaff.Items.Count - 1
            If lstAvailableStaff.Items(intCount).Selected Then
                strSql = "INSERT INTO tblUserAssets (uasassId, uasusrId) VALUES (" & grvList.SelectedValue & ", " & lstAvailableStaff.Items(intCount).Value & ")"
                ExecuteQuery(strSql)
            End If
        Next

        lstAvailableStaff.DataBind()
        lstAssignedStaff.DataBind()
    End Sub

    Protected Sub cmdRemoveSelected_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRemoveSelected.Click
        Dim strSql As String
        Dim intCount As Integer

        For intCount = 0 To lstAssignedStaff.Items.Count - 1
            If lstAssignedStaff.Items(intCount).Selected Then
                strSql = "UPDATE tblUserAssets SET uasRecordArchived = 1, uasEndDate = getDate() WHERE uasId = " & lstAssignedStaff.Items(intCount).Value
                ExecuteQuery(strSql)
            End If
        Next

        lstAvailableStaff.DataBind()
        lstAssignedStaff.DataBind()
    End Sub

    Protected Sub lstSelectCompanies_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstSelectCompanies.SelectedIndexChanged
        grvList.DataBind()
    End Sub
End Class
