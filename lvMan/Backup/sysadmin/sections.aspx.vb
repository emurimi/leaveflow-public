﻿Imports System.Data.SqlClient
Imports lvManLib.Security
Imports lvManLib.DataFunctions

Partial Class sysadmin_sections
    Inherits System.Web.UI.Page
    Public strUserPermissions As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        strUserPermissions = SecureCurrentPage()
        If InStr(strUserPermissions, "A") = 0 Then cmdAddConfig.Enabled = False
    End Sub

    Protected Sub cmdUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
        If Page.IsValid Then

            If cmdUpdate.Text = "Add Section" Then
                SqlList.Insert()
            Else
                SqlList.Update()
            End If

            EditMode(False)
        End If
    End Sub


    Sub ClearFields()
        txtName.Text = ""
        txtDescription.Text = ""
    End Sub


    Sub GetUpdateForm(ByVal intsecId As Integer)
        Dim objDR As SqlDataReader

        objDR = GetSpDataReader("stpSections_View", , "secId=" & intsecId)

        If objDR.Read Then
            txtName.Text = objDR("secName") & ""
            txtDescription.Text = objDR("secDesription") & ""
            lstDepartment.SelectedIndex = lstDepartment.Items.IndexOf(lstDepartment.Items.FindByValue(objDR("secdptId")))
        End If

        objDR.Close()
        objDR = Nothing

        cmdUpdate.Text = "Update Section"
        cmdAddConfig.Text = cmdUpdate.Text
        EditMode(True, False)

    End Sub

    Protected Sub grvList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvList.SelectedIndexChanged
        GetUpdateForm(grvList.SelectedValue)
    End Sub

    Protected Sub cmdAddMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddConfig.Click
        cmdUpdate.Text = "Add Section"
        EditMode(True)
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        EditMode(False)
    End Sub

    Protected Sub cmdViewMenus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdViewConfigs.Click
        EditMode(False)
    End Sub

    Sub EditMode(ByVal bolEdit As Boolean, Optional ByVal bolClearFields As Boolean = True)
        EnableDisableFields(True)

        If bolEdit Then
            If bolClearFields Then ClearFields()
            panList.Visible = False
            panAddEdit.Visible = True
            tdAddEdit.Style.Item("background-image") = "url(/images/orangetab.png);"
            tdView.Style.Item("background-image") = "url(/images/darkbluetab.png);"
            cmdDelete.Visible = False

            If cmdUpdate.Text = "Update Section" Then
                If InStr(strUserPermissions, "E") = 0 Then EnableDisableFields(False)
                If InStr(strUserPermissions, "D") > 0 Then cmdDelete.Visible = True
            End If
        Else
            cmdAddConfig.Text = "Add Section"
            panList.Visible = True
            panAddEdit.Visible = False
            tdAddEdit.Style.Item("background-image") = "url(/images/darkbluetab.png);"
            tdView.Style.Item("background-image") = "url(/images/orangetab.png);"
            grvList.DataBind()
        End If
    End Sub

    Sub EnableDisableFields(ByVal bolEnabled As Boolean)
        lstDepartment.Enabled = bolEnabled
        txtName.Enabled = bolEnabled
        txtDescription.Enabled = bolEnabled
        cmdUpdate.Enabled = bolEnabled
    End Sub

    Protected Sub cmdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        ExecuteStoredProc("stpSections_Delete", , "secId=" & grvList.SelectedValue)
        EditMode(False)
    End Sub

End Class
