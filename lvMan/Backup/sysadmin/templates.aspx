﻿<%@ Page Title="Setup Templates" Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.sysadmin_templates" Codebehind="templates.aspx.vb" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register TagPrefix="FCKeditorV2" Namespace="FredCK.FCKeditorV2" Assembly="FredCK.FCKeditorV2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
    <script type="text/javascript">
        function FCKeditor_OnComplete( editorInstance )
        {
            window.status = editorInstance.Description ;
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">
            <table width="100%" cellpadding="3" cellspacing="0" border="0">
                <tr>
                    <td width="100%">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td style="padding-bottom: 4px;">
                                
                                    <asp:DropDownList AutoPostBack="True" ID="lstSelectCompanies" runat="server" CssClass="listmenu" 
                                        DataSourceID="SqlCompanies" DataTextField="comName" DataValueField="comId">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlCompanies" runat="server" 
                                        ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                        SelectCommandType="StoredProcedure"
                                        SelectCommand="stpCompanyDetails">
                                    </asp:SqlDataSource>
                                
                                </td>
                            </tr>                           
                            <tr id="trCommands" runat="server">
                                <td>
                                    <table width="290" border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <td id="tdView" runat="server" width="144" height="29" align="center" valign="middle" background='/images/orangetab.png'>
                                            <asp:LinkButton CssClass="whitetext" ID="cmdViewGroups" runat="server">View Templates</asp:LinkButton></td>
                                        <td width="2"></td>
                                        <td id="tdAddEdit" runat="server" width="144" align="center" valign="middle" background='/images/darkbluetab.png'>
                                        <asp:LinkButton ID="cmdAddGroup" CssClass="whitetext" runat="server">Add Template</asp:LinkButton>
                                        </td>
                                      </tr>
                                    </table>                    
                                </td>
                            </tr>
                                 
                         
                            <tr>
                                <td>
                                
                                        <asp:Panel ID="panList" runat="server" Width="100%">
                                            <asp:GridView ID="grvList" BorderColor="#EEEEEE" runat="server" Width="100%" AllowPaging="True"  CssClass="gridviewcontents"
                                                AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="tedId" PageSize="30"
                                                DataSourceID="SqlList" RowStyle-Height="20">
                                                <RowStyle Height="20px" />
                                                <Columns>
                                                    <asp:BoundField DataField="tedId" Visible="False" HeaderText="tedId" InsertVisible="False" ReadOnly="True" SortExpression="tedId" />
                                                    <asp:BoundField DataField="tedTitle" HeaderText="Title/Subject" SortExpression="tedTitle" />
                                                    <asp:BoundField DataField="tedName" HeaderText="Name" SortExpression="tedName" />
                                                    <asp:CommandField SelectText="View/Edit" ShowSelectButton="True" />
                                                </Columns>
                                                <RowStyle CssClass="greytable" />
                                                <AlternatingRowStyle CssClass="whitetable" />
                                                <PagerStyle CssClass="greytable" />
                                                <HeaderStyle HorizontalAlign="Left" ForeColor="#FFFFFF" CssClass="tablecolors_title" />                            

                                            </asp:GridView>    
                                            <asp:SqlDataSource ID="SqlList" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                SelectCommandType="StoredProcedure"
                                                DeleteCommandType="StoredProcedure"
                                                InsertCommandType="StoredProcedure"
                                                UpdateCommandType="StoredProcedure"                                                
                                                SelectCommand="stpTemplateDefinitions_View" 
                                                DeleteCommand="stpTemplateDefinitions_Delete" 
                                                InsertCommand="stpTemplateDefinitions_Add" 
                                                UpdateCommand="stpTemplateDefinitions_Update">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="lstSelectCompanies" Name="tedcomId" PropertyName="SelectedValue" Type="Int32" />
                                                </SelectParameters>
                                                <DeleteParameters>
                                                    <asp:Parameter Name="tedId" Type="Int32" />
                                                </DeleteParameters>
                                                <UpdateParameters>
                                                    <asp:ControlParameter ControlID="lstCompanies" PropertyName="SelectedValue" Name="tedcomId" Type="Int32" />
                                                    <asp:ControlParameter ControlID="lstTemplate" PropertyName="SelectedValue" Name="tedShortName" Type="String" />
                                                    <asp:ControlParameter ControlID="lstTemplate" PropertyName="SelectedItem.Text" Name="tedName" Type="String" />
                                                    <asp:ControlParameter ControlID="txtTitle" PropertyName="Text" Name="tedTitle" Type="String" />
                                                    <asp:ControlParameter ControlID="fckText" PropertyName="Value" Name="tedText" Type="String" />
                                                    <asp:ControlParameter ControlID="grvList" PropertyName="SelectedValue" Name="tedId" Type="Int32" />
                                                </UpdateParameters>
                                                <InsertParameters>
                                                    <asp:ControlParameter ControlID="lstCompanies" PropertyName="SelectedValue" Name="tedcomId" Type="Int32" />
                                                    <asp:ControlParameter ControlID="lstTemplate" PropertyName="SelectedValue" Name="tedShortName" Type="String" />
                                                    <asp:ControlParameter ControlID="lstTemplate" PropertyName="SelectedItem.Text" Name="tedName" Type="String" />
                                                    <asp:ControlParameter ControlID="txtTitle" PropertyName="Text" Name="tedTitle" Type="String" />
                                                    <asp:ControlParameter ControlID="fckText" PropertyName="Value" Name="tedText" Type="String" />
                                                </InsertParameters>
                                            </asp:SqlDataSource>
                                        </asp:Panel>

                                            
                                       
                                        <asp:Panel ID="panAddEdit" runat="server" Width="100%" Visible="False">
                                          <table width="550" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                              <td width="15"><img src="/images/lefttop.jpg" alt="LeaveFlow" width="15" height="15" /></td>
                                              <td height="15" background="/images/topside.jpg">&nbsp;</td>
                                              <td width="15"><img src="/images/righttop.jpg" alt="LeaveFlow" width="15" height="15" /></td>
                                            </tr>
                                            <tr>
                                              <td background="/images/leftside.jpg">&nbsp;</td>
                                              <td valign="top">                                        
                                                    <table cellpadding="3" id="tblAddEdit" runat="server" cellspacing="0" width="100%" class="tablewithborders">
                                                        <tr>
                                                            <td width="50%">
                                                                Company:</td>
                                                            <td width="50%">
                                                                <asp:DropDownList ID="lstCompanies" runat="server" AutoPostBack="True" 
                                                                    CssClass="listmenu" DataSourceID="SqlCompanies" DataTextField="comName" 
                                                                    DataValueField="comId">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="50%">
                                                                Template:</td>
                                                            <td width="50%">
                                                                <asp:DropDownList ID="lstTemplate" runat="server" CssClass="listmenu">
                                                                    <asp:ListItem Text="Your User Account" Value="NUACCT"></asp:ListItem>
                                                                    <asp:ListItem Text="Forgot Password" Value="UFP"></asp:ListItem>
                                                                    
                                                                    <asp:ListItem Text="Leave request approved (sent to applicant)" Value="ULAP"></asp:ListItem>
                                                                    <asp:ListItem Text="Leave request rejected (sent to applicant)" Value="ULRJ"></asp:ListItem>
                                                                    <asp:ListItem Text="Leave request cancelled by self (sent to applicant)" Value="ULCN"></asp:ListItem>
                                                                    <asp:ListItem Text="Leave request needing attention (sent to approver)" Value="ALNA"></asp:ListItem>
                                                                    <asp:ListItem Text="Leave request approval actioned (sent to next approver)" Value="ALAA"></asp:ListItem>
                                                                    <asp:ListItem Text="Leave request revision suggested (sent to applicant)" Value="ALRVS"></asp:ListItem>
                                                                    <asp:ListItem Text="Leave request revised (sent to approver)" Value="ALRRAA"></asp:ListItem>
                                                                    
                                                                    <asp:ListItem Text="Leave plan approved (sent to applicant)" Value="ULPAP"></asp:ListItem>
                                                                    <asp:ListItem Text="Leave plan rejected (sent to applicant)" Value="ULPRJ"></asp:ListItem>
                                                                    <asp:ListItem Text="Leave plan cancelled by self (sent to applicant)" Value="ULPCN"></asp:ListItem>
                                                                    <asp:ListItem Text="Leave plan needing attention (sent to approver)" Value="ALPNA"></asp:ListItem>
                                                                    <asp:ListItem Text="Leave plan approval actioned (sent to next approver)" Value="ALPAA"></asp:ListItem>
                                                                    <asp:ListItem Text="Leave plan request revision suggested (sent to applicant)" Value="ALPRVS"></asp:ListItem>
                                                                    <asp:ListItem Text="Leave plan revised (sent to approver)" Value="ALPRNA"></asp:ListItem>

                                                                    <asp:ListItem Text="Leave compensation request approved (sent to applicant)" Value="ULCAP"></asp:ListItem>
                                                                    <asp:ListItem Text="Leave compensation request rejected (sent to applicant)" Value="ULCRJ"></asp:ListItem>
                                                                    <asp:ListItem Text="Leave compensation request cancelled by self (sent to applicant)" Value="ULCCN"></asp:ListItem>
                                                                    <asp:ListItem Text="Leave compensation request needing attention (sent to approver)" Value="ALCNA"></asp:ListItem>
                                                                    <asp:ListItem Text="Leave compensation request approval actioned (sent to next approver)" Value="ALCAA"></asp:ListItem>
                                                                    <asp:ListItem Text="Leave compensation request revision suggested (sent to applicant)" Value="ALCRVS"></asp:ListItem>
                                                                    <asp:ListItem Text="Leave compensation request revised (sent to approver)" Value="ALCRAA"></asp:ListItem>
                                                                    
                                                                    <asp:ListItem Text="Reminder of pending requests requiring action (sent to approver)" Value="ARPL"></asp:ListItem>
                                                                    <asp:ListItem Text="Reminder of pending requests requiring action (sent to HR)" Value="HRPL"></asp:ListItem>
                                                                    <asp:ListItem Text="Reminder of pending requests requiring action (sent to approver by HR)" Value="AHRL"></asp:ListItem>      

                                                                    <asp:ListItem Text="Alert of leave about to commence (to applicant)" Value="ALRCA"></asp:ListItem>
                                                                    <asp:ListItem Text="Alert of leave about to commence (to HR)" Value="ALRCHR"></asp:ListItem>
                                                                    <asp:ListItem Text="Alert of leave about to commence (to Reliever)" Value="ALRCH"></asp:ListItem>                                         
                                                                    
                                                                    <asp:ListItem Text="Leave allowance notification (sent to accounts)" Value="LANA"></asp:ListItem>                                         
                                                                    <asp:ListItem Text="Leave allowance paid (sent to applicant)" Value="LANAPU"></asp:ListItem>
                                                                    <asp:ListItem Text="Leave allowance not paid (sent to applicant)" Value="LANNPU"></asp:ListItem>
                                                                </asp:DropDownList>

                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Subject/ Title:</td>
                                                            <td>
                                                                <asp:TextBox ID="txtTitle" Columns="35" runat="server" TextMode="SingleLine" CssClass="textbox"></asp:TextBox>
                                                                    
                                                            </td>
                                                        </tr>                                                
                                                        <tr>
                                                            <td>
                                                                Text:</td>
                                                            <td>
                                                                <asp:TextBox Visible="False" ID="txtText" TextMode="MultiLine" Columns="50" Rows="30" runat="server"></asp:TextBox>
                                                                <FCKeditorV2:FCKeditor Height="300px" id="fckText" runat="server"></FCKeditorV2:FCKeditor>    
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="cmdCancel" runat="server" CausesValidation="False" Text="Cancel" CssClass="button"></asp:Button>
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="cmdUpdate" runat="server" ValidationGroup="AddGroup" 
                                                                    Text="Add Template" CssClass="button"></asp:Button>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                            
                                                            <asp:LinkButton ID="cmdDelete" Visible="False" OnClientClick="return confirm('Are you sure you want to delete this record?');" Font-Bold="True" runat="server">Delete Template</asp:LinkButton></td>
                                                            
                                                        </tr>
                                                        
                                                    </table>
                                                  </td>
                                                  <td background="/images/rightside.jpg">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                  <td><img src="/images/leftbottom.jpg" alt="LeaveFlow" width="15" height="15" /></td>
                                                  <td height="15" background="/images/bottomside.jpg">&nbsp;</td>
                                                  <td><img src="/images/rightbottom.jpg" alt="right bottom" width="15" height="15" /></td>
                                                </tr>
                                              </table>  
                                        </asp:Panel>    
                                                
                                
                                
                                
                                </td>                    
                            </tr>
                       </table>
                    
                    

                    </td>
                </tr>
            </table>
            
            <FCKeditorV2:FCKeditor id="FCKeditor1" Visible="False" runat="server"></FCKeditorV2:FCKeditor>
</asp:Content>

