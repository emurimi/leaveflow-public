<%@ Page Title="Setup Companies" Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.sysadmin_companies" Codebehind="companies.aspx.vb" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">
            <table width="100%" cellpadding="3" cellspacing="0" border="0">
                <tr>
                    <td width="100%">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr id="trCommands" runat="server">
                                <td>
                                    <table width="290" border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <td id="tdView" runat="server" width="144" height="29" align="center" valign="middle" background='/images/orangetab.png'>
                                            <asp:LinkButton CssClass="whitetext" ID="cmdViewConfigs" runat="server">View Companies</asp:LinkButton></td>
                                        <td width="2"></td>
                                        <td id="tdAddEdit" runat="server" width="144" align="center" valign="middle" background='/images/darkbluetab.png'>
                                        <asp:LinkButton CssClass="whitetext" ID="cmdAddConfig" runat="server">Add Company</asp:LinkButton>
                                        </td>
                                      </tr>
                                    </table>                    
                                </td>
                            </tr>
                            <tr>
                                <td width="100%" valign="top">
                                    <asp:Panel ID="panList" runat="server" Width="100%">
                                        <asp:GridView ID="grvList" BorderColor="#EEEEEE" runat="server" Width="100%" AllowPaging="True"  CssClass="gridviewcontents"
                                            AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="comId" 
                                            DataSourceID="SqlList" RowStyle-Height="20">
                                            <RowStyle Height="20px" />
                                            <Columns>
                                                <asp:BoundField DataField="comId" HeaderText="comId" InsertVisible="False" ReadOnly="True" SortExpression="comId" Visible="False" />
                                                <asp:BoundField DataField="comName" HeaderText="Name" SortExpression="comName" />
                                                <asp:BoundField DataField="ctyName" HeaderText="Country" SortExpression="ctyName" />
                                                <asp:CommandField ItemStyle-Font-Bold="True" SelectText="View/Edit" ShowSelectButton="True" >
                                                    <ItemStyle Font-Bold="True" />
                                                </asp:CommandField>
                                            </Columns>
                                            <RowStyle CssClass="greytable" />
                                            <AlternatingRowStyle CssClass="whitetable" />
                                            <PagerStyle CssClass="greytable" />
                                            <HeaderStyle HorizontalAlign="Left" ForeColor="#FFFFFF" CssClass="tablecolors_title" />                            

                                        </asp:GridView>    
                                        <asp:SqlDataSource ID="SqlList" runat="server" 
                                            ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                            SelectCommandType="StoredProcedure"
                                            DeleteCommandType="StoredProcedure"
                                            InsertCommandType="StoredProcedure"
                                            UpdateCommandType="StoredProcedure"                                            
                                            SelectCommand="stpCompanyDetails" 
                                            DeleteCommand="stpCompanies_Delete" 
                                            InsertCommand="stpCompanies_Add" 
                                            UpdateCommand="stpCompanies_Update">
                                            <DeleteParameters>
                                                <asp:Parameter Name="comId" Type="Int32" />
                                            </DeleteParameters>
                                            <UpdateParameters>
                                                <asp:ControlParameter ControlID="txtName" Name="comName" PropertyName="Text" Type="String" />
                                                <asp:ControlParameter ControlID="txtDescription" Name="comDescription" PropertyName="Text" Type="String" />
                                                <asp:ControlParameter ControlID="lstCountries" Name="comctyId" PropertyName="SelectedValue" Type="Int32" />
                                                <asp:ControlParameter ControlID="lstLeaveUnit" Name="comLeaveUnit" PropertyName="SelectedValue" Type="String" />
                                                <asp:Parameter Name="comLeaveAccrual" Type="Boolean" DefaultValue="True" />
                                                <asp:ControlParameter ControlID="lstLeaveAccrualPeriod" DefaultValue="" Name="comLeaveAccrualPeriod" PropertyName="SelectedValue" Type="String" />
                                                <asp:ControlParameter ControlID="lstFridayHoliday" Name="comFridayHolidayCompensation" PropertyName="SelectedValue" Type="Int32" />
                                                <asp:ControlParameter ControlID="lstSaturdayHoliday" Name="comSaturdayHolidayCompensation" PropertyName="SelectedValue" Type="Int32" />
                                                <asp:ControlParameter ControlID="lstSundayHoliday" Name="comSundayHolidayCompensation" PropertyName="SelectedValue" Type="Int32" />
                                                <asp:ControlParameter ControlID="grvList" Name="comId" PropertyName="SelectedValue" Type="Int32" />
                                                <asp:ControlParameter ControlID="lstHoursPerDay" Name="comHoursPerDay" PropertyName="SelectedValue" Type="Int32" />
                                                <asp:ControlParameter ControlID="lstStartTime" Name="comStartTime" PropertyName="SelectedValue" Type="String" />
                                                <asp:ControlParameter ControlID="lstEndTime" Name="comEndTime" PropertyName="SelectedValue" Type="String" />
                                                <asp:ControlParameter ControlID="lstLBStartTime" Name="comLBStartTime" PropertyName="SelectedValue" Type="String" />
                                                <asp:ControlParameter ControlID="lstLBEndTime" Name="comLBEndTime" PropertyName="SelectedValue" Type="String" />
                                                <asp:ControlParameter ControlID="txtHREmail" Name="comHREmail" PropertyName="Text" Type="String" />
                                            </UpdateParameters>
                                            <InsertParameters>
                                                <asp:ControlParameter ControlID="txtHREmail" Name="comHREmail" PropertyName="Text" Type="String" />
                                                <asp:ControlParameter ControlID="txtName" Name="comName" PropertyName="Text" Type="String" />
                                                <asp:ControlParameter ControlID="txtDescription" Name="comDescription" PropertyName="Text" Type="String" />
                                                <asp:ControlParameter ControlID="lstCountries" Name="comctyId" PropertyName="SelectedValue" Type="Int32" />
                                                <asp:ControlParameter ControlID="lstLeaveUnit" Name="comLeaveUnit" PropertyName="SelectedValue" Type="String" />
                                                <asp:ControlParameter ControlID="lstHoursPerDay" Name="comHoursPerDay" PropertyName="SelectedValue" Type="Int32" />
                                                <asp:Parameter Name="comLeaveAccrual" Type="Boolean" DefaultValue="True" />
                                                <asp:ControlParameter ControlID="lstLeaveAccrualPeriod" DefaultValue="" Name="comLeaveAccrualPeriod" PropertyName="SelectedValue" Type="String" />
                                                <asp:ControlParameter ControlID="lstFridayHoliday" Name="comFridayHolidayCompensation" PropertyName="SelectedValue" Type="Int32" />
                                                <asp:ControlParameter ControlID="lstSaturdayHoliday" Name="comSaturdayHolidayCompensation" PropertyName="SelectedValue" Type="Int32" />
                                                <asp:ControlParameter ControlID="lstSundayHoliday" Name="comSundayHolidayCompensation" PropertyName="SelectedValue" Type="Int32" />
                                                <asp:ControlParameter ControlID="lstStartTime" Name="comStartTime" PropertyName="SelectedValue" Type="String" />
                                                <asp:ControlParameter ControlID="lstEndTime" Name="comEndTime" PropertyName="SelectedValue" Type="String" />
                                                <asp:ControlParameter ControlID="lstLBStartTime" Name="comLBStartTime" PropertyName="SelectedValue" Type="String" />
                                                <asp:ControlParameter ControlID="lstLBEndTime" Name="comLBEndTime" PropertyName="SelectedValue" Type="String" />
                                            </InsertParameters>
                                        </asp:SqlDataSource>
                                    </asp:Panel>

                                        
                                   
                                    <asp:Panel ID="panAddEdit" runat="server" Width="100%" Visible="False">
                                      <table width="400" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          <td width="15"><img src="/images/lefttop.jpg" alt="LeaveFlow" width="15" height="15" /></td>
                                          <td height="15" background="/images/topside.jpg">&nbsp;</td>
                                          <td width="15"><img src="/images/righttop.jpg" alt="LeaveFlow" width="15" height="15" /></td>
                                        </tr>
                                        <tr>
                                          <td background="/images/leftside.jpg">&nbsp;</td>
                                          <td valign="top">                                    
                                                <table cellpadding="3" id="tblAddEdit" runat="server" cellspacing="0" width="100%" class="tablewithborders">
                                                    <tr>
                                                        <td width="50%">
                                                            Name:</td>
                                                        <td width="50%">
                                                            <asp:TextBox ID="txtName" runat="server" CssClass="textbox"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="rvaName" runat="server" ControlToValidate="txtName" 
                                                                Display="Dynamic" ValidationGroup="AddItem" 
                                                                ErrorMessage="* please enter name"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Country:</td>
                                                        <td>
                                                            <asp:DropDownList ID="lstCountries" runat="server" DataSourceID="SqlCountries" 
                                                                DataTextField="ctyName" DataValueField="ctyId" CssClass="listmenu">
                                                            </asp:DropDownList>
                                                            <asp:SqlDataSource ID="SqlCountries" runat="server" 
                                                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                                SelectCommandType="StoredProcedure"
                                                                SelectCommand="stpCountries_View">
                                                            </asp:SqlDataSource>
                                                                
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Leave Unit:</td>
                                                        <td>
                                                            <asp:DropDownList ID="lstLeaveUnit" runat="server" CssClass="listmenu">
                                                                <asp:ListItem Value="D">Days</asp:ListItem>
                                                                <asp:ListItem Value="H">Hours</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Leave Accrual Period:</td>
                                                        <td>
                                                            <asp:DropDownList ID="lstLeaveAccrualPeriod" runat="server" CssClass="listmenu">
                                                                <asp:ListItem Value="D">Daily</asp:ListItem>
                                                                <asp:ListItem Value="W">Weekly</asp:ListItem>
                                                                <asp:ListItem Value="M">Monthly</asp:ListItem>
                                                                <asp:ListItem Value="Y">Yearly</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Work hours per day:</td>
                                                        <td>
                                                            <asp:DropDownList ID="lstHoursPerDay" runat="server" CssClass="listmenu">
                                                                <asp:ListItem Value="1">1</asp:ListItem>
                                                                <asp:ListItem Value="2">2</asp:ListItem>
                                                                <asp:ListItem Value="3">3</asp:ListItem>
                                                                <asp:ListItem Value="4">4</asp:ListItem>
                                                                <asp:ListItem Value="5">5</asp:ListItem>
                                                                <asp:ListItem Value="6">6</asp:ListItem>
                                                                <asp:ListItem Value="7">7</asp:ListItem>
                                                                <asp:ListItem Value="8">8</asp:ListItem>
                                                                <asp:ListItem Value="9">9</asp:ListItem>
                                                                <asp:ListItem Value="10">10</asp:ListItem>
                                                                <asp:ListItem Value="11">11</asp:ListItem>
                                                                <asp:ListItem Value="12">12</asp:ListItem>

                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Work start time:</td>
                                                        <td>
                                                            <asp:DropDownList ID="lstStartTime" runat="server" CssClass="listmenu">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Work end time:</td>
                                                        <td>
                                                            <asp:DropDownList ID="lstEndTime" runat="server" CssClass="listmenu">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Lunch break start:</td>
                                                        <td>
                                                            <asp:DropDownList ID="lstLBStartTime" runat="server" CssClass="listmenu">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Lunch break end:</td>
                                                        <td>
                                                            <asp:DropDownList ID="lstLBEndTime" runat="server" CssClass="listmenu">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            HR Email Address:</td>
                                                        <td>
                                                            <asp:TextBox ID="txtHREmail" runat="server" CssClass="textbox"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtHREmail" 
                                                                Display="Dynamic" ValidationGroup="AddItem" 
                                                                ErrorMessage="* please specify"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>                                                    
                                                    <tr>
                                                        <td>
                                                            on Friday Holiday:</td>
                                                        <td>
                                                            <asp:DropDownList ID="lstFridayHoliday" runat="server" CssClass="listmenu">
                                                                <asp:ListItem Value="0">No action</asp:ListItem>
                                                                <asp:ListItem Value="-1">Count Thursday as holiday</asp:ListItem>
                                                                <asp:ListItem Value="1">Count Saturday as holiday</asp:ListItem>
                                                                <asp:ListItem Value="2">Count Sunday as holiday</asp:ListItem>
                                                                <asp:ListItem Value="3">Count Monday as holiday</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            on Saturday Holiday:</td>
                                                        <td>
                                                            <asp:DropDownList ID="lstSaturdayHoliday" runat="server" CssClass="listmenu">
                                                                <asp:ListItem Value="0">No action</asp:ListItem>
                                                                <asp:ListItem Value="-1">Count Friday as holiday</asp:ListItem>
                                                                <asp:ListItem Value="1">Count Sunday as holiday</asp:ListItem>
                                                                <asp:ListItem Value="2">Count Monday as holiday</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            on Sunday Holiday:</td>
                                                        <td>
                                                            <asp:DropDownList ID="lstSundayHoliday" runat="server" CssClass="listmenu">
                                                                <asp:ListItem Value="0">No action</asp:ListItem>
                                                                <asp:ListItem Value="-1">Count Saturday as holiday</asp:ListItem>
                                                                <asp:ListItem Value="-2">Count Friday as holiday</asp:ListItem>
                                                                <asp:ListItem Value="1">Count Monday as holiday</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Description:</td>
                                                        <td>
                                                            <asp:TextBox ID="txtDescription" runat="server" Rows="4" CssClass="textbox" 
                                                                Columns="30" TextMode="MultiLine"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="cmdCancel" runat="server" CausesValidation="False" Text="Cancel" CssClass="button"></asp:Button>
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="cmdUpdate" runat="server" ValidationGroup="AddItem" 
                                                                CssClass="button" Text="Add Company"></asp:Button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton ID="cmdDelete" OnClientClick="return confirm('Are you sure you want to delete this record?');" Font-Bold="True" runat="server">Delete Company</asp:LinkButton>
                                                        </td>
                                                        <td>
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>
                                              </td>
                                              <td background="/images/rightside.jpg">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td><img src="/images/leftbottom.jpg" alt="LeaveFlow" width="15" height="15" /></td>
                                              <td height="15" background="/images/bottomside.jpg">&nbsp;</td>
                                              <td><img src="/images/rightbottom.jpg" alt="right bottom" width="15" height="15" /></td>
                                            </tr>
                                          </table>                                                    
                                    </asp:Panel>    
                                </td>
                            </tr>
                        </table>                    
                    </td>
                </tr>

            </table>

</asp:Content>

