﻿Imports System.Data.SqlClient
Imports lvManLib.Security
Imports lvManLib.DataFunctions
Imports lvManLib.General
Imports lvManLib.Config

Partial Class sysadmin_companies
    Inherits System.Web.UI.Page

    Public strUserPermissions As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        strUserPermissions = SecureCurrentPage()
        If InStr(strUserPermissions, "A") = 0 Then cmdAddConfig.Enabled = False

        If Not Page.IsPostBack Then
            Dim strOrganizationType As String = GetConfig("OrganizationType") & "" '1 = Organization with many companies, 2 = Single company

            If grvList.Rows.Count > 0 And strOrganizationType = "2" Then
                cmdAddConfig.Enabled = False
                cmdAddConfig.ForeColor = Drawing.Color.Gray
            End If

            LoadTime(lstStartTime)
            LoadTime(lstEndTime)
            LoadTime(lstLBStartTime)
            LoadTime(lstLBEndTime)
        End If
    End Sub

    Protected Sub cmdUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
        If Page.IsValid Then

            If cmdUpdate.Text = "Add Company" Then
                SqlList.Insert()
            Else
                SqlList.Update()
            End If

            EditMode(False)
        End If
    End Sub


    Sub ClearFields()
        txtName.Text = ""
        txtDescription.Text = ""
        txtHREmail.Text = ""

    End Sub


    Sub GetUpdateForm(ByVal intcomId As Integer)
        Dim objDR As SqlDataReader

        objDR = GetSpDataReader("stpCompanyDetails", , "comId=" & intcomId)

        If objDR.Read Then
            txtName.Text = objDR("comName") & ""
            txtDescription.Text = objDR("comDescription") & ""
            txtHREmail.Text = objDR("comHREmail") & ""
            lstHoursPerDay.SelectedIndex = lstHoursPerDay.Items.IndexOf(lstHoursPerDay.Items.FindByValue(objDR("comHoursPerDay")))
            lstCountries.SelectedIndex = lstCountries.Items.IndexOf(lstCountries.Items.FindByValue(objDR("comctyId")))
            lstFridayHoliday.SelectedIndex = lstFridayHoliday.Items.IndexOf(lstFridayHoliday.Items.FindByValue(objDR("comFridayHolidayCompensation")))
            lstLeaveAccrualPeriod.SelectedIndex = lstLeaveAccrualPeriod.Items.IndexOf(lstLeaveAccrualPeriod.Items.FindByValue(objDR("comLeaveAccrualPeriod")))
            lstLeaveUnit.SelectedIndex = lstLeaveUnit.Items.IndexOf(lstLeaveUnit.Items.FindByValue(objDR("comLeaveUnit")))
            lstSaturdayHoliday.SelectedIndex = lstSaturdayHoliday.Items.IndexOf(lstSaturdayHoliday.Items.FindByValue(objDR("comSaturdayHolidayCompensation")))
            lstSundayHoliday.SelectedIndex = lstSundayHoliday.Items.IndexOf(lstSundayHoliday.Items.FindByValue(objDR("comSundayHolidayCompensation")))

            lstStartTime.SelectedIndex = lstStartTime.Items.IndexOf(lstStartTime.Items.FindByValue(objDR("comStartTime")))
            lstEndTime.SelectedIndex = lstEndTime.Items.IndexOf(lstEndTime.Items.FindByValue(objDR("comEndTime")))
            lstLBStartTime.SelectedIndex = lstLBStartTime.Items.IndexOf(lstLBStartTime.Items.FindByValue(objDR("comLBStartTime")))
            lstLBEndTime.SelectedIndex = lstLBEndTime.Items.IndexOf(lstLBEndTime.Items.FindByValue(objDR("comLBEndTime")))

        End If

        objDR.Close()
        objDR = Nothing

        cmdUpdate.Text = "Update Company"
        cmdAddConfig.Text = cmdUpdate.Text
        EditMode(True, False)

    End Sub

    Protected Sub grvList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvList.SelectedIndexChanged
        GetUpdateForm(grvList.SelectedValue)
    End Sub

    Protected Sub cmdAddMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddConfig.Click
        cmdUpdate.Text = "Add Company"
        EditMode(True)
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        EditMode(False)
    End Sub

    Protected Sub cmdViewMenus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdViewConfigs.Click
        EditMode(False)
    End Sub

    Sub EditMode(ByVal bolEdit As Boolean, Optional ByVal bolClearFields As Boolean = True)
        EnableDisableFields(True)

        If bolEdit Then
            If bolClearFields Then ClearFields()
            panList.Visible = False
            panAddEdit.Visible = True
            tdAddEdit.Style.Item("background-image") = "url(/images/orangetab.png);"
            tdView.Style.Item("background-image") = "url(/images/darkbluetab.png);"
            cmdDelete.Visible = False

            If cmdUpdate.Text = "Update Company" Then
                If InStr(strUserPermissions, "E") = 0 Then EnableDisableFields(False)
                If InStr(strUserPermissions, "D") > 0 Then cmdDelete.Visible = True
            End If
        Else
            cmdAddConfig.Text = "Add Company"
            panList.Visible = True
            panAddEdit.Visible = False
            tdAddEdit.Style.Item("background-image") = "url(/images/darkbluetab.png);"
            tdView.Style.Item("background-image") = "url(/images/orangetab.png);"
            grvList.DataBind()
        End If
    End Sub

    Sub EnableDisableFields(ByVal bolEnabled As Boolean)
        lstCountries.Enabled = bolEnabled
        lstFridayHoliday.Enabled = bolEnabled
        lstLeaveAccrualPeriod.Enabled = bolEnabled
        lstLeaveUnit.Enabled = bolEnabled
        lstSaturdayHoliday.Enabled = bolEnabled
        lstSundayHoliday.Enabled = bolEnabled
        txtName.Enabled = bolEnabled
        txtDescription.Enabled = bolEnabled
        cmdUpdate.Enabled = bolEnabled
    End Sub

    Protected Sub cmdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        ExecuteStoredProc("stpCompanies_Delete", , "comId=" & grvList.SelectedValue)
        EditMode(False)
    End Sub

End Class
