﻿Imports System.Data.SqlClient
Imports lvManLib.Security
Imports lvManLib.DataFunctions
Imports lvManLib.Config

Partial Class sysadmin_hrusers
    Inherits System.Web.UI.Page

    Public strUserPermissions As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        strUserPermissions = SecureCurrentPage()
        If InStr(strUserPermissions, "A") = 0 Then cmdAddConfig.Enabled = False

        If Not Page.IsPostBack Then
            Dim strEnableBranches As String = GetConfig("EnableBranches") & ""
            lstCompanies.DataBind()
            lstCompanies.Items.Insert(0, New ListItem("All companies", "0"))
            lstCompanies.SelectedIndex = 0
            lstSelectCompanies.DataBind()
            lstSelectCompanies.Items.Insert(0, New ListItem("All companies", "0"))
            lstSelectCompanies.SelectedIndex = 0

            lstBranches.DataBind()
            lstBranches.Items.Insert(0, New ListItem("All branches", "0"))
            lstBranches.SelectedIndex = 0

            If strEnableBranches = "N" Then
                trBranches.Visible = False
                grvList.Columns(2).Visible = False
            End If
        End If
    End Sub

    Protected Sub cmdUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
        If Page.IsValid Then

            If cmdUpdate.Text = "Add HR User" Then
                SqlList.Insert()
            Else
                SqlList.Update()
            End If

            EditMode(False)
        End If
    End Sub

    Sub ClearFields()
        lstCompanies.SelectedIndex = lstSelectCompanies.SelectedIndex
    End Sub


    Sub GetUpdateForm(ByVal inthruId As Integer)
        Dim strSql As String
        Dim objDR As SqlDataReader

        strSql = "SELECT * FROM tblHRUsers WHERE hruId = " & inthruId
        objDR = GetDataReader(strSql)

        If objDR.Read Then
            lstCompanies.SelectedIndex = lstCompanies.Items.IndexOf(lstCompanies.Items.FindByValue(objDR("hrucomId")))
            lstBranches.DataBind()
            lstBranches.Items.Insert(0, New ListItem("Select branch", "0"))
            lstBranches.SelectedIndex = 0
            lstBranches.SelectedIndex = lstBranches.Items.IndexOf(lstBranches.Items.FindByValue(objDR("hrubraId")))
            Try
                lstUsers.SelectedIndex = lstUsers.Items.IndexOf(lstUsers.Items.FindByValue(objDR("hruusrId")))
            Catch ex As Exception

            End Try

        End If

        objDR.Close()
        objDR = Nothing

        cmdUpdate.Text = "Update HR User"
        cmdAddConfig.Text = cmdUpdate.Text
        EditMode(True, False)
    End Sub

    Protected Sub grvList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvList.SelectedIndexChanged
        GetUpdateForm(grvList.SelectedValue)
    End Sub

    Protected Sub cmdAddMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddConfig.Click
        cmdUpdate.Text = "Add HR User"
        EditMode(True)
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        EditMode(False)
    End Sub

    Protected Sub cmdViewMenus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdViewConfigs.Click
        EditMode(False)
    End Sub

    Sub EditMode(ByVal bolEdit As Boolean, Optional ByVal bolClearFields As Boolean = True)
        EnableDisableFields(True)

        If bolEdit Then
            If bolClearFields Then ClearFields()
            panList.Visible = False
            panAddEdit.Visible = True
            tdAddEdit.Style.Item("background-image") = "url(/images/orangetab.png);"
            tdView.Style.Item("background-image") = "url(/images/darkbluetab.png);"
            cmdDelete.Visible = False

            If cmdUpdate.Text = "Update HR User" Then
                If InStr(strUserPermissions, "E") = 0 Then EnableDisableFields(False)
                If InStr(strUserPermissions, "D") > 0 Then cmdDelete.Visible = True
            End If
        Else
            cmdAddConfig.Text = "Add HR User"
            panList.Visible = True
            panAddEdit.Visible = False
            tdAddEdit.Style.Item("background-image") = "url(/images/darkbluetab.png);"
            tdView.Style.Item("background-image") = "url(/images/orangetab.png);"
            grvList.DataBind()
        End If
    End Sub

    Sub EnableDisableFields(ByVal bolEnabled As Boolean)
        lstCompanies.Enabled = bolEnabled
        lstBranches.Enabled = bolEnabled
        lstUsers.Enabled = bolEnabled
        cmdUpdate.Enabled = bolEnabled
    End Sub

    Protected Sub cmdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        Dim strSql As String
        strSql = "UPDATE tblHRUsers SET hruRecordArchived = 1 WHERE hruId = " & grvList.SelectedValue
        ExecuteQuery(strSql)
        EditMode(False)
    End Sub

    Protected Sub lstCompanies_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstCompanies.SelectedIndexChanged
        lstBranches.DataBind()
        lstBranches.Items.Insert(0, New ListItem("Select branch", "0"))
        lstBranches.SelectedIndex = 0
    End Sub

    Protected Sub lstSelectCompanies_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstSelectCompanies.SelectedIndexChanged
        lstCompanies.SelectedIndex = lstSelectCompanies.SelectedIndex
    End Sub
End Class
