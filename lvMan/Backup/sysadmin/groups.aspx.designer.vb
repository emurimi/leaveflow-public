﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class SystemAdministration_Groups

    '''<summary>
    '''trCommands control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trCommands As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''tdView control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdView As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''cmdViewGroups control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdViewGroups As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''tdAddEdit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdAddEdit As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''cmdAddGroup control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdAddGroup As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''panList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents panList As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''grvList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grvList As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''SqlList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SqlList As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''panAddEdit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents panAddEdit As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''tblAddEdit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblAddEdit As Global.System.Web.UI.HtmlControls.HtmlTable

    '''<summary>
    '''txtName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtName As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''rvaName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rvaName As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''txtDescription control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDescription As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''cmdCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdCancel As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''cmdUpdate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdUpdate As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''cmdDelete control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdDelete As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''trGroupMenus1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trGroupMenus1 As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''trGroupMenus2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trGroupMenus2 As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''trGroupMenus3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trGroupMenus3 As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''lstAvailableMenus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lstAvailableMenus As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''lstAllowedMenus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lstAllowedMenus As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''trGroupMenus4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trGroupMenus4 As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''chkAdd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkAdd As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''chkEdit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkEdit As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''chkDelete control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkDelete As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''trGroupMenus5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trGroupMenus5 As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''cmdAddMenu control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdAddMenu As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''cmdRemoveMenu control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdRemoveMenu As Global.System.Web.UI.WebControls.LinkButton
End Class
