﻿Imports lvManLib.DataFunctions
Imports System.Data.SqlClient
Imports lvManLib.Security

Partial Class SystemAdministration_GenericAdmin
    Inherits System.Web.UI.Page
    Public strUserPermissions As String = ""
    Public strDataSet As String
    Public strTable As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strTitle As String

        strUserPermissions = SecureCurrentPage()
        If InStr(strUserPermissions, "A") = 0 Then cmdAddGen.Enabled = False

        strDataSet = Request.QueryString("dst") & ""
        strTitle = ""
        strTable = ""

        Select Case strDataSet
            Case "reg"
                strTable = "tblRegions"
                strTitle = "Regions"
            Case "ast"
                strTable = "tblAssetTypes"
                strTitle = "Asset Classes"
            Case "lat"
                strTable = "tblLeaveAllowanceTypes"
                strTitle = "Allowance Types"
            Case Else
                Response.Redirect("/main.aspx")

        End Select

        If Not Page.IsPostBack Then
            cmdAddGen.Text = "Add " & strTitle
            cmdViewGen.Text = "View " & strTitle
            cmdUpdate.Text = "Add " & strTitle
        End If


        SqlList.SelectCommand = "SELECT " & strDataSet & "Id As genId, " & strDataSet & "Name As genName, " & strDataSet & "Description As genDescription FROM [" & strTable & "] WHERE " & strDataSet & "RecordArchived = 0 ORDER BY [" & strDataSet & "Name]"
        SqlList.InsertCommand = "INSERT INTO [" & strTable & "] ([" & strDataSet & "Name], [" & strDataSet & "Description]) VALUES (@genName, @genDescription)"
        SqlList.UpdateCommand = "UPDATE [" & strTable & "] SET [" & strDataSet & "Name] = @genName, [" & strDataSet & "Description] = @genDescription WHERE [" & strDataSet & "Id] = @genId"
        SqlList.DeleteCommand = "DELETE FROM [" & strTable & "] WHERE [" & strDataSet & "Id] = @genId"

    End Sub

    Protected Sub cmdUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
        If Page.IsValid Then

            If InStr(cmdUpdate.Text, "Add") > 0 Then
                SqlList.Insert()
            Else
                SqlList.Update()
            End If

            EditMode(False)
        End If
    End Sub

    Sub ClearFields()
        txtDescription.Text = ""
        txtName.Text = ""
    End Sub

    Sub GetUpdateForm(ByVal intgenId As Integer)
        Dim strSql As String
        Dim objDR As SqlDataReader

        strSql = "SELECT * FROM " & strTable & " WHERE " & strDataSet & "Id = " & intgenId
        objDR = GetDataReader(strSql)

        If objDR.Read Then
            txtDescription.Text = objDR(strDataSet & "Description") & ""
            txtName.Text = objDR(strDataSet & "Name") & ""
        End If

        objDR.Close()
        objDR = Nothing

        cmdUpdate.Text = Replace(cmdUpdate.Text, "Add", "Update")
        cmdAddGen.Text = cmdUpdate.Text
        EditMode(True, False)

    End Sub

    Protected Sub grvList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvList.SelectedIndexChanged
        GetUpdateForm(grvList.SelectedValue)
    End Sub

    Protected Sub cmdAddMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddGen.Click
        cmdUpdate.Text = Replace(cmdUpdate.Text, "Update", "Add")
        EditMode(True)
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        EditMode(False)
    End Sub

    Protected Sub cmdViewMenus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdViewGen.Click
        EditMode(False)
    End Sub

    Sub EditMode(ByVal bolEdit As Boolean, Optional ByVal bolClearFields As Boolean = True)
        EnableDisableFields(True)

        If bolEdit Then
            If bolClearFields Then ClearFields()
            panList.Visible = False
            panAddEdit.Visible = True
            tdAddEdit.Style.Item("background-image") = "url(/images/orangetab.png);"
            tdView.Style.Item("background-image") = "url(/images/darkbluetab.png);"
            cmdDelete.Visible = False

            If InStr(cmdUpdate.Text, "Update") > 0 Then
                If InStr(strUserPermissions, "E") = 0 Then EnableDisableFields(False)
                If InStr(strUserPermissions, "D") > 0 Then cmdDelete.Visible = True
            End If
        Else
            cmdAddGen.Text = Replace(cmdUpdate.Text, "Update", "Add")
            panList.Visible = True
            panAddEdit.Visible = False
            tdAddEdit.Style.Item("background-image") = "url(/images/darkbluetab.png);"
            tdView.Style.Item("background-image") = "url(/images/orangetab.png);"
            grvList.DataBind()
        End If
    End Sub

    Sub EnableDisableFields(ByVal bolEnabled As Boolean)
        txtDescription.Enabled = bolEnabled
        txtName.Enabled = bolEnabled
        cmdUpdate.Enabled = bolEnabled
    End Sub

    Protected Sub cmdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        Dim strSql As String
        strSql = "UPDATE " & strTable & " SET " & strDataSet & "RecordArchived = 1 WHERE " & strDataSet & "Id = " & grvList.SelectedValue
        ExecuteQuery(strSql)
        EditMode(False)
    End Sub

End Class
