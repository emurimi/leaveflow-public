﻿Imports System.Data.SqlClient
Imports lvManLib.Security
Imports lvManLib.DataFunctions

Partial Class SystemAdministration_Groups
    Inherits System.Web.UI.Page
    Public strUserPermissions As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        strUserPermissions = SecureCurrentPage()
        If InStr(strUserPermissions, "A") = 0 Then cmdAddGroup.Enabled = False
    End Sub

    Protected Sub cmdUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
        If Page.IsValid Then

            If cmdUpdate.Text = "Add Group" Then
                SqlList.Insert()
            Else
                SqlList.Update()
            End If

            EditMode(False)
        End If
    End Sub

    Sub ClearFields()
        txtDescription.Text = ""
        txtName.Text = ""
    End Sub


    Sub GetUpdateForm(ByVal intgrpId As Integer)
        Dim objDR As SqlDataReader

        objDR = GetSpDataReader("stpGroups_View", , "grpId=" & intgrpId)

        If objDR.Read Then
            txtDescription.Text = objDR("grpDescription") & ""
            txtName.Text = objDR("grpName") & ""
        End If

        objDR.Close()
        objDR = Nothing

        cmdUpdate.Text = "Update Group"
        cmdAddGroup.Text = "Update Group"

        UpdateGroupMenus()

        EditMode(True, False)

    End Sub

    Sub UpdateGroupMenus()
        Dim strSql As String
        Dim intCount As Integer
        Dim strText As String
        Dim strRights As String
        Dim strRightsDesc As String

        trGroupMenus1.Visible = True
        trGroupMenus2.Visible = True
        trGroupMenus3.Visible = True
        trGroupMenus4.Visible = True
        trGroupMenus5.Visible = True

        lstAllowedMenus.Items.Clear()

        strSql = "SELECT mnuId, ISNULL((SELECT mnuName FROM tblMenus As tblMN WHERE tblMN.mnuId = tblMenus.mnuParent), '') + ' --- ' + mnuName As mnuText FROM tblMenus WHERE mnuId NOT IN (SELECT gmnmnuId FROM tblGroupMenus WHERE gmngrpId = " & grvList.SelectedValue & ") AND mnuParent > 0 ORDER BY mnuParent, mnuName"
        BindListBox(strSql, lstAvailableMenus)

        strSql = "SELECT gmnId, (SELECT mnuName FROM tblMenus WHERE mnuId = gmnmnuId) + '+' + CAST(gmnAdd AS varchar(1)) + CAST(gmnEdit AS varchar(1)) + CAST(gmnDelete AS varchar(1)) As mnuText FROM tblGroupMenus WHERE gmngrpId = " & grvList.SelectedValue & " ORDER BY mnuText"
        BindListBox(strSql, lstAllowedMenus)

        Try
            For intCount = 0 To lstAllowedMenus.Items.Count - 1
                strText = lstAllowedMenus.Items(intCount).Text & ""
                strRights = ""
                If InStr(strText, "+") > 0 Then strRights = Mid(strText, InStr(strText, "+") + 1)
                strRightsDesc = " view | "

                If Mid(strRights, 1, 1) = "1" Then strRightsDesc &= "add | "
                If Mid(strRights, 2, 1) = "1" Then strRightsDesc &= "edt | "
                If Mid(strRights, 3, 1) = "1" Then strRightsDesc &= "del | "

                If Len(strRightsDesc) > 0 Then strRightsDesc = Mid(strRightsDesc, 1, Len(strRightsDesc) - 2)
                strText = Mid(strText, 1, InStr(strText, "+") - 1)
                strText &= " [" & strRightsDesc & "]"
                lstAllowedMenus.Items(intCount).Text = strText
            Next

        Catch ex As Exception
            'do nothing
        End Try
    End Sub

    Protected Sub grvList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvList.SelectedIndexChanged
        GetUpdateForm(grvList.SelectedValue)
    End Sub

    Protected Sub cmdAddGroup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddGroup.Click
        cmdUpdate.Text = "Add Group"
        EditMode(True)
        trGroupMenus1.Visible = False
        trGroupMenus2.Visible = False
        trGroupMenus3.Visible = False
        trGroupMenus4.Visible = False
        trGroupMenus5.Visible = False
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        EditMode(False)
    End Sub


    Protected Sub cmdAddMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddMenu.Click
        Dim strSql As String
        Dim intCount As Integer
        Dim intAdd As Integer = 0
        Dim intEdit As Integer = 0
        Dim intDelete As Integer = 0

        If chkAdd.Checked Then intAdd = 1
        If chkEdit.Checked Then intEdit = 1
        If chkDelete.Checked Then intDelete = 1

        For intCount = 0 To lstAvailableMenus.Items.Count - 1
            If lstAvailableMenus.Items(intCount).Selected Then
                strSql = "INSERT INTO tblGroupMenus (gmngrpId, gmnmnuId, gmnAdd, gmnEdit, gmnDelete) VALUES "
                strSql &= "(" & grvList.SelectedValue & ", " & lstAvailableMenus.Items(intCount).Value & ", " & intAdd & ", " & intEdit & ", " & intDelete & ")"
                ExecuteQuery(strSql)
            End If

        Next

        chkAdd.Checked = False
        chkEdit.Checked = False
        chkDelete.Checked = False
        UpdateGroupMenus()
    End Sub

    Protected Sub cmdRemoveMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRemoveMenu.Click
        Dim strSql As String
        Dim intCount As Integer

        For intCount = 0 To lstAllowedMenus.Items.Count - 1
            If lstAllowedMenus.Items(intCount).Selected Then
                strSql = "DELETE FROM tblGroupMenus WHERE gmnId = " & lstAllowedMenus.Items(intCount).Value
                ExecuteQuery(strSql)
            End If
        Next
        UpdateGroupMenus()
    End Sub

    Protected Sub cmdViewGroups_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdViewGroups.Click
        EditMode(False)
    End Sub

    Sub EditMode(ByVal bolEdit As Boolean, Optional ByVal bolClearFields As Boolean = True)
        EnableDisableFields(True)

        If bolEdit Then
            If bolClearFields Then ClearFields()
            panList.Visible = False
            panAddEdit.Visible = True
            tdAddEdit.Style.Item("background-image") = "url(/images/orangetab.png);"
            tdView.Style.Item("background-image") = "url(/images/darkbluetab.png);"
            cmdDelete.Visible = False

            If cmdUpdate.Text = "Update Group" Then
                If InStr(strUserPermissions, "E") = 0 Then EnableDisableFields(False)
                If InStr(strUserPermissions, "D") > 0 Then cmdDelete.Visible = True
            End If
        Else
            cmdAddGroup.Text = "Add Group"
            panList.Visible = True
            panAddEdit.Visible = False
            tdAddEdit.Style.Item("background-image") = "url(/images/darkbluetab.png);"
            tdView.Style.Item("background-image") = "url(/images/orangetab.png);"
            grvList.DataBind()
        End If
    End Sub

    Sub EnableDisableFields(ByVal bolEnabled As Boolean)
        txtDescription.Enabled = bolEnabled
        txtName.Enabled = bolEnabled
        cmdUpdate.Enabled = bolEnabled
        cmdAddMenu.Enabled = bolEnabled
        cmdRemoveMenu.Enabled = bolEnabled
    End Sub

    Protected Sub cmdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        ExecuteStoredProc("stpGroups_Delete", , "grpId=" & grvList.SelectedValue)
        EditMode(False)
    End Sub

End Class
