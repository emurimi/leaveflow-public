﻿Imports System.Data.SqlClient
Imports lvManLib.Security
Imports lvManLib.DataFunctions
Imports lvManLib.Config
Imports lvManLib.General

Partial Class SystemAdministration_Users
    Inherits System.Web.UI.Page
    Public strUserPermissions As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        strUserPermissions = SecureCurrentPage()

        If InStr(strUserPermissions, "A") = 0 Then cmdAddUser.Enabled = False
        'If InStr(strUserPermissions, "D") = 0 Then cmdDelete.Enabled = False

        If Not Page.IsPostBack Then
            Dim strLeavePlans As String = GetConfig("EnableLeavePlans") & ""
            Dim strOrganizationType As String = GetConfig("OrganizationType") & "" '1 = Organization with many companies, 2 = Single company
            Dim strEnableBranches As String = GetConfig("EnableBranches") & ""
            Dim strEnableSections As String = GetConfig("EnableSections") & ""

            lblLoginType.Text = GetConfig("LoginType")

            If strEnableBranches = "N" Then
                lstSelBranches.Visible = False
                lblBranch.Visible = False
                lstBranches.Visible = False
                If strOrganizationType = "2" Then trBranchesCompanies.Visible = False
                grvList.Columns(4).Visible = False
            End If

            If strEnableSections = "N" Then
                lblSection.Visible = False
                lstSections.Visible = False
                grvList.Columns(6).Visible = False
            End If

            Select Case lblLoginType.Text
                Case "U" 'UserName
                    'do nothing

                Case "E" 'Email Add
                    lblUserName.Visible = False
                    txtUserName.Visible = False
                    rvaUserName.Enabled = False
                    grvList.Columns(1).Visible = False

                Case "W" 'Windows
                    lblUserName.Text = "Windows User Name:"

                Case Else
                    'do nothing

            End Select

            lstSelCompanies.DataBind()
            lstSelCompanies.Items.Insert(0, New ListItem("All Companies", "0"))
            lstSelCompanies.SelectedIndex = 0

            RefreshLists()

            lstSelGroups.DataBind()
            lstSelGroups.Items.Insert(0, New ListItem("All User Groups", "0"))
            lstSelGroups.SelectedIndex = 0

            lstCompanies.DataBind()
            lstGrades.DataBind()
            If lstGrades.Items.Count = 0 Then
                lstGrades.Items.Add("No grades available")
                lstGrades.Items(0).Value = "0"
            End If

            lstDepartments.DataBind()
            lstSections.DataBind()
            lstBranches.DataBind()

            SqlList.DataBind()
            grvList.DataBind()

        End If

    End Sub

    Protected Sub cmdUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
        If Page.IsValid Then
            Dim strSql As String = ""
            Dim strTemp As String
            Dim strURL As String = GetConfig("SystemURL")
            Dim strUserName As String = ""

            If cmdUpdate.Text <> "Add User" Then
                strTemp = GetSpDataSingleValue("stpUser_CheckUnique", , "usrEmailAdd=" & txtEmailAdd.Text & "*usrId=" & grvList.SelectedValue)
            Else
                strTemp = GetSpDataSingleValue("stpUser_CheckUnique", , "usrEmailAdd=" & txtEmailAdd.Text)
            End If

            If Len(strTemp) > 0 Then
                lblMessage.Text = "The selected email address is already in use by " & strTemp & ". Please choose another."
                Exit Sub
            End If

            
            Select Case lblLoginType.Text
                Case "U" 'UserName
                    If cmdUpdate.Text <> "Add User" Then
                        strTemp = GetSpDataSingleValue("stpUser_CheckUnique", , "usrName=" & txtUserName.Text & "*usrId=" & grvList.SelectedValue)
                    Else
                        strTemp = GetSpDataSingleValue("stpUser_CheckUnique", , "usrName=" & txtUserName.Text)
                    End If
                    If Len(strTemp) > 0 Then
                        lblMessage.Text = "The selected username is already in use by " & strTemp & ". Please choose another."
                        Exit Sub
                    End If
                    strUserName = txtUserName.Text

                Case "E" 'Email Add
                    If Len(txtUserName.Text & "") = 0 Then txtUserName.Text = txtEmailAdd.Text
                    strUserName = txtEmailAdd.Text

                Case "W" 'Windows
                    'do nothing

                Case Else
                    'do nothing

            End Select

            
            If Len(txtPassword.Text & "") > 0 Then
                lblPassword.Text = EncryptPassword(txtPassword.Text, txtUserName.Text)
            End If


            If cmdUpdate.Text = "Add User" Then
                Dim strMailMessage As String
                If Not IsNumeric(txtOpeningDays.Text) Then
                    lblMessage.Text = "Please specify a numeric value for the opening days (i.e. number of leave days due at this point in time)"
                    Exit Sub
                End If


                SqlList.Insert()

                strTemp = GetSpDataSingleValue("stpGetUserId", , "usrName=" & txtUserName.Text)

                If Len(strTemp) > 0 Then
                    ExecuteStoredProc("stpLeaveDaysManifest_Add", , "ldmusrId=" & strTemp & "*ldmUnits=" & txtOpeningDays.Text)
                End If

                strMailMessage = GetMailTemplate("NUACCT", lstCompanies.SelectedValue, "[USER]|[USERNAME]|[PASSWORD]|[URL]|", txtFullName.Text & "|" & strUserName & "|" & txtPassword.Text & "|" & strURL & "|")
                SendMail("Your LeaveFlow User Account", strMailMessage, txtEmailAdd.Text)

            Else
                SqlList.Update()
                If Len(txtPassword.Text & "") > 0 Then
                    ExecuteStoredProc("stpUser_ChangePassword", , "usrId=" & grvList.SelectedValue & "*usrPassword=" & txtPassword.Text)
                End If
            End If

            EditMode(False)
        End If
    End Sub

    Sub ClearFields()
        txtConfirmPassword.Text = ""
        txtFullName.Text = ""
        txtUserName.Text = ""
        txtPassword.Text = ""
        txtEmailAdd.Text = ""
        txtDateEmployed.Text = ""
        txtDescription.Text = ""
        txtStaffID.Text = ""
        lblDateCreated.Text = DateSerial(Year(Now()), Month(Now()), Day(Now()))
        chkActive.Checked = False
        lblMessage.Text = ""
        lstCompanies.SelectedIndex = 0
        lstGrades.DataBind()
        If lstGrades.Items.Count = 0 Then
            lstGrades.Items.Add("No grades available")
            lstGrades.Items(0).Value = "0"
        End If
        lstDepartments.DataBind()
        lstSections.DataBind()
        lstBranches.DataBind()

        litScript.Text = "<script>document.getElementById(""" & txtPassword.ClientID & """).value = """";</script>"
    End Sub

    Sub EnableDisableFields(ByVal bolEnabled As Boolean)
        txtConfirmPassword.Enabled = bolEnabled
        txtFullName.Enabled = bolEnabled
        txtUserName.Enabled = bolEnabled
        txtPassword.Enabled = bolEnabled
        txtEmailAdd.Enabled = bolEnabled
        cmdUpdate.Enabled = bolEnabled
        chkActive.Enabled = bolEnabled
        cmdAddMenu.Enabled = bolEnabled
        cmdRemoveGroup.Enabled = bolEnabled
        txtDateEmployed.Enabled = bolEnabled
        txtDescription.Enabled = bolEnabled
        txtStaffID.Enabled = bolEnabled
    End Sub


    Sub GetUpdateForm(ByVal intusrId As Integer)
        Dim objDR As SqlDataReader

        objDR = GetSpDataReader("stpUsers_View", , "usrId=" & intusrId)

        If objDR.Read Then
            txtConfirmPassword.Text = objDR("usrPassword") & ""
            txtFullName.Text = objDR("usrFullName") & ""
            txtPassword.Text = txtConfirmPassword.Text
            txtUserName.Text = objDR("usrName") & ""
            txtDescription.Text = objDR("usrDescription") & ""
            txtStaffID.Text = objDR("usrStaffID") & ""
            txtDateEmployed.Text = objDR("usrDateEmployed") & ""
            lblDateCreated.Text = objDR("usrDateAdded") & ""
            txtEmailAdd.Text = objDR("usrEmailAdd") & ""


            ' If Len(txtDateEmployed.Text) > 0 Then txtDateEmployed.Text = FormatDateTime(txtDateEmployed.Text, DateFormat.ShortDate)
            lstStatus.SelectedIndex = lstStatus.Items.IndexOf(lstStatus.Items.FindByValue(objDR("usrStatus") & ""))

            lstCompanies.SelectedIndex = lstCompanies.Items.IndexOf(lstCompanies.Items.FindByValue(objDR("comId") & ""))
            lstDepartments.DataBind()
            lstBranches.DataBind()
            lstBranches.SelectedIndex = lstBranches.Items.IndexOf(lstBranches.Items.FindByValue(objDR("usrbraId") & ""))
            lstDepartments.SelectedIndex = lstDepartments.Items.IndexOf(lstDepartments.Items.FindByValue(objDR("usrdptId") & ""))
            lstSections.DataBind()
            lstSections.SelectedIndex = lstSections.Items.IndexOf(lstSections.Items.FindByValue(objDR("usrsecId") & ""))
            lstGrades.SelectedIndex = lstGrades.Items.IndexOf(lstGrades.Items.FindByValue(objDR("usrgraId") & ""))
            lstGender.SelectedIndex = lstGender.Items.IndexOf(lstGender.Items.FindByValue(objDR("usrSex") & ""))
            lstPositions.SelectedIndex = lstPositions.Items.IndexOf(lstPositions.Items.FindByValue(objDR("usrposId") & ""))

            If objDR("usrActive") Then chkActive.Checked = True
            If objDR("usrIsSystemAccount") Then chkSystemAccount.Checked = True

            trGroups.Visible = True
        End If

        objDR.Close()
        objDR = Nothing

        LoadGroups(intusrId)

        rvaConfirmPassword.ValidationGroup = "UpdateUser"
        rvaPassword.ValidationGroup = "UpdateUser"
        cmdUpdate.Text = "Update User"
        cmdAddUser.Text = "Update User"
        EditMode(True, False)
        lblChangePassword.Visible = True
        litScript.Text = "<script>document.getElementById(""" & txtPassword.ClientID & """).value = """";</script>"
        lblMessage.Text = ""
    End Sub

    Sub LoadGroups(ByVal intusrId As Integer)
        Dim strSql As String

        strSql = "SELECT grpId, grpName FROM tblGroups WHERE grpRecordArchived = 0 AND grpId NOT IN (SELECT ugpgrpId FROM tblUserGroups WHERE ugpusrId = " & intusrId & ")"
        BindListBox(strSql, lstAvailableGroups)

        strSql = "SELECT ugpId, (SELECT grpName FROM tblGroups WHERE grpId = ugpgrpId) As grpName FROM tblUserGroups WHERE ugpusrId = " & intusrId
        BindListBox(strSql, lstAllowedGroups)
    End Sub

    Protected Sub grvList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvList.SelectedIndexChanged
        GetUpdateForm(grvList.SelectedValue)
    End Sub

    Protected Sub cmdAddUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddUser.Click
        rvaConfirmPassword.ValidationGroup = "AddUser"
        rvaPassword.ValidationGroup = "AddUser"
        cmdUpdate.Text = "Add User"
        lblChangePassword.Visible = False
        EditMode(True)
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        EditMode(False)
    End Sub

    Sub EditMode(ByVal bolEdit As Boolean, Optional ByVal bolClearFields As Boolean = True)
        EnableDisableFields(True)

        If bolEdit Then
            If bolClearFields Then ClearFields()
            panList.Visible = False
            panAddEdit.Visible = True
            tdAddEditUsers.Style.Item("background-image") = "url(/images/orangetab.png);"
            tdViewUsers.Style.Item("background-image") = "url(/images/darkbluetab.png);"
            'cmdDelete.Visible = False
            trGroups.Visible = False
            If cmdUpdate.Text = "Update User" Then
                If InStr(strUserPermissions, "E") = 0 Then EnableDisableFields(False)
                'cmdDelete.Visible = True
                trGroups.Visible = True
            End If
        Else
            cmdAddUser.Text = "Add User"
            panList.Visible = True
            panAddEdit.Visible = False
            tdAddEditUsers.Style.Item("background-image") = "url(/images/darkbluetab.png);"
            tdViewUsers.Style.Item("background-image") = "url(/images/orangetab.png);"
            grvList.DataBind()
        End If
    End Sub

    Protected Sub cmdViewUsers_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdViewUsers.Click
        EditMode(False)
    End Sub

    

    Protected Sub cmdAddMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddMenu.Click
        Dim intCount As Integer
        Dim strSql As String

        For intCount = 0 To lstAvailableGroups.Items.Count - 1
            If lstAvailableGroups.Items(intCount).Selected Then
                strSql = "INSERT INTO tblUserGroups (ugpusrId, ugpgrpId) VALUES (" & grvList.SelectedValue & ", " & lstAvailableGroups.Items(intCount).Value & ")"
                ExecuteQuery(strSql)
            End If
        Next

        LoadGroups(grvList.SelectedValue)
    End Sub

    Protected Sub cmdRemoveGroup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRemoveGroup.Click
        Dim intCount As Integer
        Dim strSql As String

        For intCount = 0 To lstAllowedGroups.Items.Count - 1
            If lstAllowedGroups.Items(intCount).Selected Then
                strSql = "DELETE FROM tblUserGroups WHERE ugpId = " & lstAllowedGroups.Items(intCount).Value
                ExecuteQuery(strSql)
            End If
        Next

        LoadGroups(grvList.SelectedValue)
    End Sub

    Protected Sub cmdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        grvList.DataBind()
        SqlList.DataBind()
    End Sub

    Protected Sub lstSelCompanies_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstSelCompanies.SelectedIndexChanged
        RefreshLists()
    End Sub

    Sub RefreshLists()
        lstSelBranches.DataBind()
        lstSelDepartments.DataBind()
        lstSelBranches.Items.Insert(0, New ListItem("All Branches", "0"))
        lstSelBranches.SelectedIndex = 0
        lstSelDepartments.Items.Insert(0, New ListItem("All Departments", "0"))
        lstSelDepartments.SelectedIndex = 0
        RefreshSections()
    End Sub

    Sub RefreshSections()
        lstSections.DataBind()
        lstSections.Items.Insert(0, New ListItem("All Sections", "0"))
        lstSections.SelectedIndex = 0
    End Sub

    Protected Sub lstCompanies_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstCompanies.SelectedIndexChanged
        lstGrades.DataBind()
        If lstGrades.Items.Count = 0 Then
            lstGrades.Items.Add("No grades available")
            lstGrades.Items(0).Value = "0"
        End If
        lstDepartments.DataBind()
        lstSections.DataBind()
        lstBranches.DataBind()

    End Sub
End Class
