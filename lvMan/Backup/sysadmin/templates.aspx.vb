﻿Imports System.Data.SqlClient
Imports lvManLib.Security
Imports lvManLib.DataFunctions

Partial Class sysadmin_templates
    Inherits System.Web.UI.Page
    Public strUserPermissions As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        strUserPermissions = SecureCurrentPage()
        If InStr(strUserPermissions, "A") = 0 Then cmdAddGroup.Enabled = False

        If Not Page.IsPostBack Then
            'FCKeditor1.BasePath = "/FCKEditor/"
            'FCKeditor1.ToolbarSet = "Basic"
            fckText.BasePath = "/FCKEditor/"
            fckText.ToolbarSet = "Basic"

            lstCompanies.DataBind()
            lstCompanies.Items.Insert(0, New ListItem("Spans all companies", "0"))
            lstCompanies.SelectedIndex = 0

            lstSelectCompanies.DataBind()
            lstSelectCompanies.Items.Insert(0, New ListItem("Spans all companies", "0"))
            lstSelectCompanies.SelectedIndex = 0
        End If
    End Sub

    Protected Sub cmdUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
        If Page.IsValid Then

            If cmdUpdate.Text = "Add Template" Then
                SqlList.Insert()
            Else
                SqlList.Update()
            End If

            EditMode(False)
        End If
    End Sub

    Sub ClearFields()
        txtText.Text = ""
        txtTitle.Text = ""
    End Sub


    Sub GetUpdateForm(ByVal inttedId As Integer)
        Dim objDR As SqlDataReader

        objDR = GetSpDataReader("stpTemplateDefinitions_View", , "tedId=" & inttedId)

        If objDR.Read Then
            fckText.Value = objDR("tedText") & ""
            txtTitle.Text = objDR("tedTitle") & ""
            lstCompanies.SelectedIndex = lstCompanies.Items.IndexOf(lstCompanies.Items.FindByValue(objDR("tedcomId")))
            lstTemplate.SelectedIndex = lstTemplate.Items.IndexOf(lstTemplate.Items.FindByValue(objDR("tedShortName")))
        End If

        objDR.Close()
        objDR = Nothing

        cmdUpdate.Text = "Update Template"
        cmdAddGroup.Text = cmdUpdate.Text
        EditMode(True, False)

    End Sub
    Protected Sub grvList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvList.SelectedIndexChanged
        GetUpdateForm(grvList.SelectedValue)
    End Sub

    Protected Sub cmdAddGroup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddGroup.Click
        cmdUpdate.Text = "Add Template"
        EditMode(True)
        'FCKeditor1.Visible = True
        'FCKeditor2.Visible = True
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        EditMode(False)
    End Sub

    Protected Sub cmdViewGroups_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdViewGroups.Click
        EditMode(False)
    End Sub

    Sub EditMode(ByVal bolEdit As Boolean, Optional ByVal bolClearFields As Boolean = True)
        EnableDisableFields(True)

        If bolEdit Then
            If bolClearFields Then ClearFields()
            panList.Visible = False
            panAddEdit.Visible = True
            tdAddEdit.Style.Item("background-image") = "url(/images/orangetab.png);"
            tdView.Style.Item("background-image") = "url(/images/darkbluetab.png);"
            cmdDelete.Visible = False

            If cmdUpdate.Text = "Update Template" Then
                If InStr(strUserPermissions, "E") = 0 Then EnableDisableFields(False)
                If InStr(strUserPermissions, "D") > 0 Then cmdDelete.Visible = True
            End If
        Else
            panList.Visible = True
            panAddEdit.Visible = False
            tdAddEdit.Style.Item("background-image") = "url(/images/darkbluetab.png);"
            tdView.Style.Item("background-image") = "url(/images/orangetab.png);"
            grvList.DataBind()
        End If
    End Sub

    Sub EnableDisableFields(ByVal bolEnabled As Boolean)
        txtTitle.Enabled = bolEnabled
        cmdUpdate.Enabled = bolEnabled
    End Sub

    Protected Sub cmdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        ExecuteStoredProc("stpTemplateDefinitions_Delete", , "tedId=" & grvList.SelectedValue)
        EditMode(False)
    End Sub
End Class
