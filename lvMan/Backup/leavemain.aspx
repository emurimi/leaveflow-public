﻿<%@ Page Title="LeaveFlow | my requests" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" Inherits="lvMan.leavemain" Codebehind="leavemain.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <table width="100%"  border="0" cellpadding="5" cellspacing="0" class="dash">
        <tr valign="top">
          <td width="603" bgcolor="#FCFCFC" colspan="2">
            <table width="436" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="144" height="29" align="center" valign="middle" background='/images/orangetab.png'>
                    <span class="whitetext">My Requests</span></td>
                <td width="2"></td>
                <td width="144" height="29" align="center" valign="middle" background='/images/darkbluetab.png'>
                    <a href="leaverequest.aspx" class="whitetext">New Request</a>
                </td>
                <td width="2"></td>
                <td width="144" height="29" align="center" valign="middle" background='/images/darkbluetab.png'>
                    <a href="leaverecords.aspx" class="whitetext">My Records</a>
                </td>                
              </tr>
            </table>           
          </td>
        </tr>
        <tr valign="top">
          <td width="603" bgcolor="#FCFCFC">
              <table cellpadding="3" cellspacing="0" width="100%">
                  <tr>
                      <td>
                          <asp:GridView ID="grvList" BorderColor="#EEEEEE" CellPadding="4" runat="server" 
                                  AutoGenerateColumns="False" DataKeyNames="lrqId" DataSourceID="SqlList" 
                                  AllowPaging="True" Width="100%">
                                  <Columns>
                                      <asp:BoundField DataField="lrqId" HeaderText="lrqId" InsertVisible="False"  ReadOnly="True" SortExpression="lrqId" Visible="False" />
                                      <asp:BoundField DataField="ltyName" HeaderText="Type" SortExpression="ltyName" />
                                      <asp:BoundField DataField="lrqDate" HeaderText="Request Date" DataFormatString="{0:d}" SortExpression="lrqDate" />
                                      <asp:BoundField DataField="lrqStartDate" DataFormatString="{0:d}" HeaderText="Start Date" SortExpression="lrqStartDate" />
                                      <asp:BoundField DataField="lrqEndDate" HeaderText="End Date" DataFormatString="{0:d}" SortExpression="lrqEndDate" />
                                      <asp:TemplateField HeaderText="Status" SortExpression="staName">
                                          <ItemTemplate>
                                              <asp:Label ID="Label2" runat="server" Text='<%# GetStatus(DataBinder.Eval(Container.DataItem, "staName")) %>'></asp:Label>
                                          </ItemTemplate>
                                      </asp:TemplateField>
                                      <asp:CommandField SelectText="View Details" ShowSelectButton="True">
                                        <ItemStyle CssClass="darckgrey" Font-Bold="True" />
                                      </asp:CommandField>
                                  </Columns>
                                  <RowStyle CssClass="greytable" />
                                  <AlternatingRowStyle CssClass="whitetable" />
                                  <PagerStyle CssClass="greytable" />
                                  <HeaderStyle HorizontalAlign="Left" CssClass="tablecolors_title" />
                                  <EmptyDataTemplate>
                                    There are currently no leave requests related to you
                                  </EmptyDataTemplate>
                              </asp:GridView>
                              <asp:SqlDataSource ID="SqlList" runat="server" 
                                  ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                  SelectCommandType="StoredProcedure"
                                  SelectCommand="stpMyLeaveRequests_View">
                                  <SelectParameters>
                                      <asp:SessionParameter Name="usrId" SessionField="usrId" Type="Int32" />
                                  </SelectParameters>
                              </asp:SqlDataSource>                      
                      
                      </td>
                  </tr>
                  <tr id="trLeaveDetails" runat="server" visible="False">
                      <td>
                          &nbsp;</td>
                  </tr>
              </table>
            </td>

        </tr>
      </table>

</asp:Content>

