﻿Imports lvManLib.Config
Imports lvManLib.DataFunctions
Imports lvManLib.Leave
Imports lvManLib.General
Imports lvManLib.Logic
Imports System.Data.SqlClient

Partial Class approvals_compensation
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            lstStatus.DataBind()
        End If
    End Sub

    Protected Sub grvList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvList.SelectedIndexChanged
        lbllrqId.Text = grvList.SelectedValue
        grvApprovals.DataBind()
        trLeaveDetails.Visible = True
        trList.Visible = False
        LoadApplication()
    End Sub

    Sub HideDetails()
        trLeaveDetails.Visible = False
        trList.Visible = True
        grvList.SelectedIndex = -1
    End Sub

    Protected Sub cmdBacktoList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBacktoList.Click
        HideDetails()
        lblMessage.Text = ""
    End Sub

    Sub LoadApplication()
        Dim objDR As SqlDataReader
        Dim parameters As New List(Of SqlParameter)()
        Dim lcoId As New SqlParameter("@lcoId", lbllrqId.Text)
        parameters.Add(lcoId)

        objDR = GetSpDataReader("stpCompensationRequest_ViewDetails", parameters.ToArray())

        If objDR.Read() Then
            lblApplicant.Text = objDR("usrFullName") & ""
            lblDays.Text = objDR("lcoUnits") & ""
            lblDescription.Text = objDR("lcoReasonDesc") & ""
            lblReason.Text = objDR("areName") & ""
            lblStatus.Text = objDR("staName") & ""
            lblusrId.Text = objDR("lcousrId") & ""
            lblcomId.Text = objDR("comId")
            lblDate.Text = objDR("lcoDate")
            lblusrEmailAdd.Text = objDR("usrEmailAdd") & ""
        End If
        objDR.Close()

        parameters.Clear()
        Dim lrqId As New SqlParameter("@lrqId", lbllrqId.Text)
        parameters.Add(lrqId)
        Dim appApproverUsrId As New SqlParameter("@appApproverUsrId", Session("usrId"))
        parameters.Add(appApproverUsrId)
        Dim Category As New SqlParameter("@Category", "LC")
        parameters.Add(Category)

        objDR = GetSpDataReader("stpGetApproverComments", parameters.ToArray())
        trFinalApprover.Visible = False
        txtComments.Text = ""
        lstStatus.SelectedIndex = 0

        If objDR.Read() Then
            lstStatus.SelectedIndex = lstStatus.Items.IndexOf(lstStatus.Items.FindByValue(objDR("appstaId")))
            txtComments.Text = objDR("appComments") & ""
            If objDR("appFinalApprover") Then
                lblTransactionID.Text = GetTransactionID()
                trFinalApprover.Visible = True
            End If
        Else
            If CheckHRApprover(grvList.SelectedValue, "compensation") Then
                lblTransactionID.Text = GetTransactionID()
                trFinalApprover.Visible = True
                lblHRUser.Text = "1"
                txtComments.Text = "HR Officer action"
                lblMessage.Text = "You have opened this compensation request as a HR officer. Final approver permissions apply."
            Else
                lblMessage.Text = "An error occured loading this approval. Please contact HR for help"
            End If
        End If
        objDR.Close()

        objDR = Nothing

        If Session("comLeaveUnit") = "H" Then
            lblLeaveUnit.Text = "Hours"
        Else
            lblLeaveUnit.Text = "Days"
        End If

    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        HideDetails()
        lblMessage.Text = ""
    End Sub

    Protected Sub cmdUpdateStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUpdateStatus.Click
        Dim strMailMessage As String
        Dim strEmailAdd As String
        Dim strMailTitle As String
        Dim strURL As String = GetConfig("SystemURL")
        Dim parameters As New List(Of SqlParameter)()

        Dim usrId As New SqlParameter("@usrId", lblusrId.Text)
        parameters.Add(usrId)

        strEmailAdd = GetSpDataSingleValue("stpGetUserEmailAdd", parameters.ToArray())

        If lblHRUser.Text = "1" Then
            'stpCreateApproval
            parameters.Clear()
            Dim Category As New SqlParameter("@Category", "LC")
            parameters.Add(Category)
            Dim applrqId As New SqlParameter("@applrqId", grvList.SelectedValue)
            parameters.Add(applrqId)
            Dim appOrder As New SqlParameter("@appOrder", 10)
            parameters.Add(appOrder)
            Dim appApproverUsrId As New SqlParameter("@appApproverUsrId", Session("usrId"))
            parameters.Add(appApproverUsrId)
            Dim appFinalApprover As New SqlParameter("@appFinalApprover", True)
            parameters.Add(appFinalApprover)
            Dim appComments As New SqlParameter("@appComments", txtComments.Text)
            parameters.Add(appComments)

            ExecuteStoredProc("stpCreateApproval", parameters.ToArray())
        Else
            parameters.Clear()
            Dim Category As New SqlParameter("@Category", "LC")
            parameters.Add(Category)
            Dim applrqId As New SqlParameter("@applrqId", grvList.SelectedValue)
            parameters.Add(applrqId)
            Dim appApproverUsrId As New SqlParameter("@appApproverUsrId", Session("usrId"))
            parameters.Add(appApproverUsrId)
            Dim appComments As New SqlParameter("@appComments", txtComments.Text)
            parameters.Add(appComments)
            Dim appstaId As New SqlParameter("@appstaId", lstStatus.SelectedValue)
            parameters.Add(appstaId)

            ExecuteStoredProc("stpApproval_Update", parameters.ToArray())
        End If


        Select Case lstStatus.SelectedValue
            Case 1 'Still pending
                'do nothing
            Case 2 'On Hold
                'much like pending - do nothing
            Case 3 'Cancelled
                'cant do that here!
            Case 4 'Rejected
                'alert applicant
                strMailMessage = GetMailTemplate("ULCRJ", lblcomId.Text, "[NAME]|[APPROVER]|[URL]|[COMMENTS]", lblApplicant.Text & "|" & Session("usrFullName") & "|" & strURL & "|" & txtComments.Text)
                strMailTitle = Mid(strMailMessage, 1, InStr(strMailMessage, "|}*") - 1)
                strMailMessage = Mid(strMailMessage, InStr(strMailMessage, "|}*") + 3)

                'strMailMessage = "Your leave request has been rejected by " & Session("usrFullName") & ". Please visit the leave system for details."
                If Len(strEmailAdd) > 2 Then SendMail(strMailTitle, strMailMessage, strEmailAdd)

                'flag request as rejected
                parameters.Clear()
                Dim lcostaId As New SqlParameter("@lcostaId", 4)
                parameters.Add(lcostaId)
                Dim lcoId As New SqlParameter("@lcoId", grvList.SelectedValue)
                parameters.Add(lcoId)

                ExecuteStoredProc("stpLeaveCompensationRequest_UpdateStatus", parameters.ToArray())

            Case 5 'Approved
                'alert next approver
                If lblHRUser.Text = "0" Then
                    AlertApprovers(grvList.SelectedValue, "LC")
                End If

            Case 6 'Revision suggested
                'alert applicant
                strMailMessage = GetMailTemplate("ALCRVS", lblcomId.Text, "[NAME]|[APPROVER]|[URL]|[COMMENTS]", lblApplicant.Text & "|" & Session("usrFullName") & "|" & strURL & "|" & txtComments.Text)
                strMailTitle = Mid(strMailMessage, 1, InStr(strMailMessage, "|}*") - 1)
                strMailMessage = Mid(strMailMessage, InStr(strMailMessage, "|}*") + 3)

                If Len(strEmailAdd) > 2 Then SendMail(strMailTitle, strMailMessage, strEmailAdd)

                'flag request as revise
                parameters.Clear()
                Dim lcostaId As New SqlParameter("@lcostaId", 6)
                parameters.Add(lcostaId)
                Dim lcoId As New SqlParameter("@lcoId", grvList.SelectedValue)
                parameters.Add(lcoId)

                ExecuteStoredProc("stpLeaveCompensationRequest_UpdateStatus", parameters.ToArray())

        End Select


        lblMessage.Text = "You have set " & lblApplicant.Text & "'s compensation request to: " & lstStatus.SelectedItem.Text

        If trFinalApprover.Visible And lstStatus.SelectedValue = 5 Then
            FinalApproval()
            lblMessage.Text &= ". " & lblApplicant.Text & " will now be compensated for the requested period."

            strMailMessage = GetMailTemplate("ULCAP", lblcomId.Text, "[NAME]|[APPROVER]|[URL]|[COMMENTS]", lblApplicant.Text & "|" & Session("usrFullName") & "|" & strURL & "|" & txtComments.Text)
            strMailTitle = Mid(strMailMessage, 1, InStr(strMailMessage, "|}*") - 1)
            strMailMessage = Mid(strMailMessage, InStr(strMailMessage, "|}*") + 3)
            If Len(strEmailAdd) > 2 Then SendMail(strMailTitle, strMailMessage, strEmailAdd)
        End If

        HideDetails()
        grvList.DataBind()

    End Sub

    Sub FinalApproval()
        Dim parameters As New List(Of SqlParameter)()

        'Update main request record
        parameters.Clear()
        Dim lcostaId As New SqlParameter("@lcostaId", 5)
        parameters.Add(lcostaId)
        Dim lcoId As New SqlParameter("@lcoId", grvList.SelectedValue)
        parameters.Add(lcoId)

        ExecuteStoredProc("stpLeaveCompensationRequest_UpdateStatus", parameters.ToArray())

        'compensate the requested period
        EffectLeaveDayChanges(lblusrId.Text, lblDays.Text, "A", "Leave Days Compensation", "CHK")

    End Sub

    Function GetStatus(ByVal strStatus As String) As String
        Select Case strStatus
            Case "Pending"
                Return "<b><i>Pending</i></b>"
            Case "Rejected"
                Return "<font color='red'><b>Rejected</b></font>"
            Case "Approved"
                Return "<font color='green'><b>Approved</b></font>"
            Case "Cancelled"
                Return "<font color='orange'>Cancelled</font>"
            Case "On Hold"
                Return "<b>On Hold</b>"
            Case "Revise Suggested"
                Return "<font color='orange'><b>Revise Suggested</b></font>"
            Case Else
                Return strStatus
        End Select
    End Function
End Class
