﻿<%@ Page Title="LeaveFlow | approve compensation requests" Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.approvals_compensation" Codebehind="compensation.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">
   <table width="100%"  border="0" cellpadding="5" cellspacing="0" class="dash">
        <tr valign="top" id="trNavigation" runat="server" visible="False">
          <td width="603" bgcolor="#FCFCFC" colspan="2">
            <table width="580" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="144" height="29" align="center" valign="middle" background='/images/darkbluetab.png'>
                    <a href="default.aspx" class="whitetext">Leave Requests</a></td>
                <td width="2"></td>
                <td width="144" height="29" align="center" valign="middle" background='/images/orangetab.png'>
                    <span class="whitetext">Compensation Reqs</span>
                </td>
                <td width="2"></td>
                <td width="144" height="29" align="center" valign="middle" background='/images/darkbluetab.png'>
                    <a href="plans.aspx" class="whitetext">Leave Plans</a>
                </td>                
                <td width="2"></td>
                <td width="144" height="29" align="center" valign="middle" background='/images/darkbluetab.png'>
                    <a href="allowances.aspx" class="whitetext">Allowances</a>
                </td>                 
              </tr>
            </table>           
          </td>
        </tr>
        <tr valign="top">
          <td width="603" bgcolor="#FCFCFC">
            <table cellpadding="3" cellspacing="0" width="100%">
  
                <tr>
                    <td>
                    <asp:Label ID="lblMessage" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr id="trList" runat="server">
                    <td>
                        <asp:GridView ID="grvList" BorderColor="#EEEEEE" CellPadding="4" runat="server" 
                          AutoGenerateColumns="False" DataKeyNames="lcoId" DataSourceID="SqlList" 
                          AllowPaging="True" Width="100%">
                            <Columns>
                                
                                <asp:BoundField DataField="lcoId" HeaderText="lcoId" Visible="False" InsertVisible="False" ReadOnly="True" SortExpression="lcoId" />
                                <asp:BoundField DataField="usrFullName" HeaderText="Name" SortExpression="usrFullName" />
                                <asp:BoundField DataField="lcoUnits" HeaderText="Units" SortExpression="lcoUnits" />
                                <asp:BoundField DataField="areName" HeaderText="Reason" SortExpression="areName" />
                                <asp:BoundField DataField="staName" HeaderText="Status" SortExpression="staName" />
                                <asp:BoundField DataField="lcoDate" HeaderText="lcoDate" DataFormatString="{0:d}" SortExpression="lcoDate" />
                                <asp:CommandField ItemStyle-Font-Bold="True" SelectText="View Details" ShowSelectButton="True" />
                            </Columns>
                            <RowStyle CssClass="greytable" />
                            <PagerStyle CssClass="greytable" />
                            <HeaderStyle HorizontalAlign="Left" CssClass="tablecolors_title" />
                            <EmptyDataTemplate>
                                There are currently no leave applications awaiting your approval
                            </EmptyDataTemplate>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlList" runat="server" 
                             ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                            SelectCommandType="StoredProcedure"
                            SelectCommand="stpLeaveCompensationRequests_View" 
                            DeleteCommandType="StoredProcedure"
                            DeleteCommand="stpLeaveCompensationRequests_Delete" 
                            InsertCommandType="StoredProcedure"
                            InsertCommand="stpLeaveCompensationRequests_Add" 
                            UpdateCommandType="StoredProcedure"
                            UpdateCommand="stpLeaveCompensationRequest_Update">
                            <SelectParameters>
                                <asp:SessionParameter Name="usrId" SessionField="usrId" Type="Int32" />
                            </SelectParameters>                            
                            <DeleteParameters>
                                <asp:Parameter Name="lcoId" Type="Int32" />
                            </DeleteParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="lcousrId" Type="Int32" />
                                <asp:Parameter Name="lcoUnits" Type="Double" />
                                <asp:Parameter Name="lcoareId" Type="Int32" />
                                <asp:Parameter Name="lcoReasonDesc" Type="String" />
                                <asp:Parameter Name="lcostaId" Type="Int32" />
                                <asp:Parameter Name="lcoDate" Type="DateTime" />
                                <asp:Parameter Name="lcoId" Type="Int32" />
                            </UpdateParameters>
                            <InsertParameters>
                                <asp:Parameter Name="lcousrId" Type="Int32" />
                                <asp:Parameter Name="lcoUnits" Type="Double" />
                                <asp:Parameter Name="lcoareId" Type="Int32" />
                                <asp:Parameter Name="lcoReasonDesc" Type="String" />
                                <asp:Parameter Name="lcostaId" Type="Int32" />
                                <asp:Parameter Name="lcoDate" Type="DateTime" />
                            </InsertParameters>
                        </asp:SqlDataSource>
                    </td>
                </tr>
                <tr id="trLeaveDetails" runat="server" visible="False">
                    <td>
                        <table cellpadding="4" cellspacing="0" width="100%">
                            <tr>
                                <td>
                        <table cellpadding="4" cellspacing="0" width="100%" align="center">
                          <tr>
                              <td colspan="2" style="width: 100%">
                                  <asp:LinkButton ID="cmdBacktoList" runat="server" Font-Bold="True">&lt;&lt; BACK TO LIST</asp:LinkButton>
                                  <asp:Label ID="lblHRUser" runat="server" Visible="False"></asp:Label>
                              </td>
                          </tr>
                          <tr id="trFinalApprover" runat="server" visible="False">
                            <td colspan="2">You are the final approver for this leave compensation request.
                            <asp:Label ID="lblTransactionID" Visible="False" runat="server"></asp:Label>
                            <asp:Label ID="lblusrId" Visible="False" runat="server"></asp:Label>
                            <asp:Label ID="lblltyId" Visible="False" runat="server"></asp:Label>
                            <asp:Label ID="lblusrEmailAdd" Visible="False" runat="server"></asp:Label>
                            </td>
                          </tr>
                          <tr>
                              <td width="30%">
                                  Change status:</td>
                              <td width="70%">
                                  <asp:DropDownList ID="lstStatus" runat="server" CssClass="listmenu" 
                                      DataSourceID="SqlStatus" DataTextField="staName" DataValueField="staId">
                                  </asp:DropDownList>
                                  <asp:SqlDataSource ID="SqlStatus" runat="server" 
                                      ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                      SelectCommandType="StoredProcedure"
                                      SelectCommand="stpStatus_List">
                                      <SelectParameters>
                                          <asp:Parameter DefaultValue="3" Name="staId_Hide" Type="Int32" />
                                      </SelectParameters>
                                  </asp:SqlDataSource>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  Comments:</td>
                              <td>
                                  <asp:TextBox ID="txtComments" runat="server" Columns="30" CssClass="textbox" 
                                      Rows="4" TextMode="MultiLine"></asp:TextBox>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;</td>
                              <td>
                                  <asp:Button ID="cmdUpdateStatus" runat="server" CssClass="button" 
                                      Text="UPDATE STATUS" />
        &nbsp;<asp:Button ID="cmdCancel" runat="server" CausesValidation="False" CssClass="button" 
                                      Text="CANCEL" />
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;</td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td>
                                  <b>DETAILS BELOW:</b></td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td>
                                  Applicant&nbsp;</td>
                              <td>
                                  <asp:Label ID="lblApplicant" runat="server"></asp:Label>
                                  <asp:Label ID="lblcomId" Visible="False" runat="server"></asp:Label>
                              </td>
                          </tr>                  
                          <tr>
                              <td>
                                  Reason</td>
                              <td>
                                  <asp:Label ID="lblReason" runat="server"></asp:Label>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <asp:Label ID="lblLeaveUnit" runat="server"></asp:Label></td>
                              <td>
                                  <asp:Label ID="lblDays" runat="server"></asp:Label>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  Description</td>
                              <td>
                                  <asp:Label ID="lblDescription" runat="server"></asp:Label>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  Status</td>
                              <td>
                                  <asp:Label ID="lblStatus" runat="server" Font-Bold="True"></asp:Label>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  Date applied</td>
                              <td>
                                  <asp:Label ID="lblDate" runat="server" Font-Bold="True"></asp:Label>
                              </td>
                          </tr>                          
                          </table>
                    
                                </td>
                            </tr>
                            <tr>
                                <td height="25" bgcolor="#eeeeee">
                                    <b>Approval Details<asp:Label ID="lbllrqId" runat="server" Visible="False"></asp:Label>
                                    </b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="grvApprovals" runat="server" AutoGenerateColumns="False" 
                                        DataSourceID="SqlApprovalPath" Width="100%">
                                        <Columns>
                                            <asp:BoundField DataField="usrFullName" HeaderText="Approver" SortExpression="usrFullName" />
                                            <asp:TemplateField HeaderText="Status" SortExpression="staName">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text='<%# GetStatus(DataBinder.Eval(Container.DataItem, "staName")) %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:BoundField DataField="appDateChanged" HeaderText="Date Updated" SortExpression="appDateChanged" />
                                            <asp:BoundField DataField="appComments" HeaderText="Comments" SortExpression="appComments" />
                                        </Columns>
                                        <RowStyle CssClass="greytable" />
                                        <PagerStyle CssClass="greytable" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="tablecolors_title" />                                            
                                    </asp:GridView>
                                    <asp:SqlDataSource ID="SqlApprovalPath" runat="server" 
                                        ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                        SelectCommandType="StoredProcedure"
                                        SelectCommand="stpGetApprovalPathStatus">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="lbllrqId" Name="applrqId" PropertyName="Text" Type="Int32" />
                                            <asp:Parameter Name="Category" DefaultValue="LC" Type="String" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
              </td>
        </tr> 
     </table>
</asp:Content>

