﻿<%@ Page Title="LeaveFlow | make approvals" Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.approvals_default" Codebehind="default.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">
    <table width="100%"  border="0" cellpadding="5" cellspacing="0" class="dash">
        <tr valign="top" id="trNavigation" runat="server" visible="False">
          <td width="603" bgcolor="#FCFCFC" colspan="2">
            <table width="580" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="144" height="29" align="center" valign="middle" background='/images/orangetab.png'>
                    <span class="whitetext">Leave Requests</span></td>
                <td width="2"></td>
                <td width="144" height="29" align="center" valign="middle" background='/images/darkbluetab.png'>
                    <a href="compensation.aspx" class="whitetext">Compensation Reqs</a>
                </td>
                <td width="2"></td>
                <td width="144" height="29" align="center" valign="middle" background='/images/darkbluetab.png'>
                    <a href="plans.aspx" class="whitetext">Leave Plans</a>
                </td>              
                <td width="2"></td>
                <td width="144" height="29" align="center" valign="middle" background='/images/darkbluetab.png'>
                    <a href="allowances.aspx" class="whitetext">Allowances</a>
                </td>                   
              </tr>
            </table>           
          </td>
        </tr>
        <tr valign="top">
          <td width="603" bgcolor="#FCFCFC">
            <table cellpadding="3" cellspacing="0" width="100%">
 
                <tr>
                    <td>
                    <asp:Label ID="lblMessage" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                    <asp:Label ID="lblTransactionID2" Visible="False" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr id="trList" runat="server">
                    <td>
                        <asp:GridView ID="grvList" BorderColor="#EEEEEE" CellPadding="4" runat="server" AllowSorting="False" 
                          AutoGenerateColumns="False" DataKeyNames="lrqId" DataSourceID="SqlList" 
                          AllowPaging="True" Width="100%">
                            <Columns>
                                <asp:BoundField DataField="lrqId" HeaderText="lrqId" InsertVisible="False"  ReadOnly="True" SortExpression="lrqId" Visible="False" />
                                <asp:BoundField DataField="usrFullName" HeaderText="Applicant" SortExpression="usrFullName" />
                                <asp:BoundField DataField="ltyName" HeaderText="Type" SortExpression="ltyName" />
                                <asp:BoundField DataField="lrqDate" HeaderText="Request Date" DataFormatString="{0:d}" SortExpression="lrqDate" />
                                <asp:BoundField DataField="lrqStartDate" DataFormatString="{0:d}" HeaderText="Start Date" SortExpression="lrqStartDate" />
                                <asp:BoundField DataField="lrqEndDate" HeaderText="End Date" DataFormatString="{0:d}" SortExpression="lrqEndDate" />
                                <asp:TemplateField HeaderText="Status" SortExpression="staName">
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Text='<%# GetStatus(DataBinder.Eval(Container.DataItem, "staName")) %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="lrqStartDate" HeaderText="Days" SortExpression="lrqStartDate" />
                                <asp:CommandField SelectText="View Details" ShowSelectButton="True">
                                <ItemStyle CssClass="darckgrey" Font-Bold="True" />
                                </asp:CommandField>
                            </Columns>
                            <RowStyle CssClass="greytable" />
                            <PagerStyle CssClass="greytable" />
                            <HeaderStyle HorizontalAlign="Left" CssClass="tablecolors_title" />
                            <EmptyDataTemplate>
                                There are currently no leave applications awaiting your approval
                            </EmptyDataTemplate>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlList" runat="server" 
                             ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                             SelectCommandType="StoredProcedure"
                             SelectCommand="stpLeaveRequests_View">
                            <SelectParameters>
                                <asp:SessionParameter Name="lrqusrId" SessionField="usrId" Type="Int32" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </td>
                </tr>
                <tr id="trLeaveDetails" runat="server" visible="False">
                    <td>
                        <table cellpadding="4" cellspacing="0" width="100%">
                            <tr>
                                <td>
                        <table cellpadding="4" cellspacing="0" width="100%" align="center">
                          <tr>
                              <td colspan="2" style="width: 100%">
                                  <asp:LinkButton ID="cmdBacktoList" runat="server" Font-Bold="True">&lt;&lt; BACK TO LIST</asp:LinkButton>
                                  <asp:Label ID="lblHRUser" runat="server" Visible="False"></asp:Label>
                              </td>
                          </tr>
                          <tr id="trFinalApprover" runat="server" visible="False">
                            <td colspan="2"><font color="red"><b>You are the final approver for this leave request.</b></font>
                            <asp:Label ID="lblTransactionID" Visible="False" runat="server"></asp:Label>
                            <asp:Label ID="lblusrId" Visible="False" runat="server"></asp:Label>
                            <asp:Label ID="lblltyId" Visible="False" runat="server"></asp:Label>
                            <asp:Label ID="lblLeaveDays" Visible="False" runat="server"></asp:Label>
                            <asp:Label ID="lblusrEmailAdd" Visible="False" runat="server"></asp:Label>
                            </td>
                          </tr>
                          <tr>
                              <td width="35%">
                                  Change status:</td>
                              <td width="65%">
                                  <asp:DropDownList ID="lstStatus" runat="server" CssClass="listmenu" 
                                      DataSourceID="SqlStatus" DataTextField="staName" DataValueField="staId">
                                  </asp:DropDownList>
                                  <asp:SqlDataSource ID="SqlStatus" runat="server" 
                                      ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                      SelectCommandType="StoredProcedure"
                                      SelectCommand="stpStatus_List">
                                      <SelectParameters>
                                          <asp:Parameter DefaultValue="3" Name="staId_Hide" Type="Int32" />
                                      </SelectParameters>
                                  </asp:SqlDataSource>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  Comments:</td>
                              <td>
                                  <asp:TextBox ID="txtComments" runat="server" Columns="30" CssClass="textbox" 
                                      Rows="4" TextMode="MultiLine"></asp:TextBox>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;</td>
                              <td>
                                  <asp:Button ID="cmdUpdateStatus" runat="server" CssClass="button" 
                                      Text="UPDATE STATUS" />
                                   &nbsp;<asp:Button ID="cmdCancel" runat="server" CausesValidation="False" CssClass="button" 
                                      Text="CANCEL" />
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;</td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td>
                                  <b>DETAILS BELOW:</b></td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td>
                                  Applicant&nbsp;</td>
                              <td>
                                  <asp:Label ID="lblApplicant" runat="server"></asp:Label>
                                  <asp:Label ID="lblcomId" Visible="False" runat="server"></asp:Label>
                              </td>
                          </tr>                  
                          <tr>
                              <td>
                                  Leave type&nbsp;</td>
                              <td>
                                  <asp:Label ID="lblConfLeaveType" runat="server"></asp:Label>

                                  <asp:Label ID="lblConfLeaveTypeDeduct" Visible="False" runat="server"></asp:Label>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  Leave <asp:Label ID="lblConfLeaveUnits1" runat="server"></asp:Label> available</td>
                              <td>
                                  <asp:Label ID="lblConfDaysAvailable" runat="server"></asp:Label>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  Start date</td>
                              <td>
                                  <asp:Label ID="lblConfStartDate" runat="server"></asp:Label>
                                  &nbsp;<asp:Label ID="lblConfStartTime" runat="server"></asp:Label>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  End date</td>
                              <td>
                                  <asp:Label ID="lblConfEndDate" runat="server"></asp:Label>
                                  &nbsp;<asp:Label ID="lblConfEndTime" runat="server"></asp:Label>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <asp:Label ID="lblConfLeaveUnits2" runat="server"></asp:Label> deductible</td>
                              <td>
                                  <asp:Label ID="lblConfDays" runat="server" Font-Bold="False"></asp:Label>
                                  
                                  <asp:Label ID="lblDaysVersion" runat="server" Font-Bold="False"></asp:Label>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  To be relieved by</td>
                              <td>
                                  <asp:Label ID="lblConfReliever" runat="server"></asp:Label>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  Contacts</td>
                              <td>
                                  <asp:Label ID="lblConfContacts" runat="server"></asp:Label>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  Comments</td>
                              <td>
                                  <asp:Label ID="lblConfComments" runat="server"></asp:Label>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  Status</td>
                              <td>
                                  <asp:Label ID="lblConfStatus" runat="server" Font-Bold="True"></asp:Label>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  Related Documents:</td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td colspan="2" align="center">
                                    <asp:GridView ID="grvList0" Width="70%" runat="server" AutoGenerateColumns="False" 
                                        DataKeyNames="ldcId" DataSourceID="SqlList0" BorderColor="White" 
                                        GridLines="None" ShowHeader="False">
                                        <Columns>
                                            <asp:BoundField HeaderStyle-HorizontalAlign="Left" DataField="ldcId" 
                                                HeaderText="ldcId" InsertVisible="False" ReadOnly="True" SortExpression="ldcId" 
                                                Visible="False" >
                                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="Title" SortExpression="ldcTitle">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("ldcTitle") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# GetDocument(DataBinder.Eval(Container.DataItem, "ldcTitle"), DataBinder.Eval(Container.DataItem, "ldcPath"))  %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                        There are no files attached to this request
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                    <asp:SqlDataSource ID="SqlList0" runat="server" 
                                        ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>"
                                        DeleteCommandType="StoredProcedure" 
                                        DeleteCommand="stpLeaveDocuments_Delete" 
                                        SelectCommandType="StoredProcedure" 
                                        SelectCommand="stpLeaveDocuments_View" 
                                        UpdateCommandType="StoredProcedure" 
                                        UpdateCommand="stpLeaveDocuments_Update">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="lblTransactionID2" Name="ldcTransactionID" PropertyName="Text" Type="String" />
                                            <asp:Parameter Name="ldcEntity" DefaultValue="R" Type="String" />
                                        </SelectParameters>
                                        <DeleteParameters>
                                            <asp:Parameter Name="ldcId" Type="Int32" />
                                        </DeleteParameters>
                                        <UpdateParameters>
                                            <asp:Parameter Name="ldcTitle" Type="String" />
                                            <asp:Parameter Name="ldcId" Type="Int32" />
                                        </UpdateParameters>
                                    </asp:SqlDataSource>
                                </td>
                          </tr>
                          </table>
                    
                                </td>
                            </tr>
                            <tr>
                                <td height="25" bgcolor="#eeeeee"><b>Other events in the same dates</b></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblCalendarEvents" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td height="25" bgcolor="#eeeeee">
                                    <b>Approval Details<asp:Label ID="lbllrqId" runat="server" Visible="False"></asp:Label>
                                    </b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="grvApprovals" runat="server" AutoGenerateColumns="False" 
                                        DataSourceID="SqlApprovalPath" Width="100%">
                                        <Columns>
                                            <asp:BoundField DataField="usrFullName" HeaderText="Approver" SortExpression="usrFullName" />
                                            <asp:TemplateField HeaderText="Status" SortExpression="staName">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text='<%# GetStatus(DataBinder.Eval(Container.DataItem, "staName")) %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:BoundField DataField="appDateChanged" HeaderText="Date Updated" SortExpression="appDateChanged" />
                                            <asp:BoundField DataField="appComments" HeaderText="Comments" SortExpression="appComments" />
                                        </Columns>
                                        <RowStyle CssClass="greytable" />
                                        <PagerStyle CssClass="greytable" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="tablecolors_title" />                                        
                                    </asp:GridView>
                                    <asp:SqlDataSource ID="SqlApprovalPath" runat="server" 
                                        ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                        SelectCommandType="StoredProcedure"
                                        SelectCommand="stpGetApprovalPathStatus">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="lbllrqId" Name="lrqId" PropertyName="Text" Type="Int32" />
                                            <asp:Parameter Name="Category" DefaultValue="LR" Type="String" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
              </td>
        </tr> 
     </table>

</asp:Content>

