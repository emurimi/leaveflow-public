﻿<%@ Page Title="LeaveFlow | leave records" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" Inherits="lvMan.leaverecords" Codebehind="leaverecords.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    <table cellpadding="5" cellspacing="0" width="100%" align="center">
        <tr>
            <td>
                <table width="436" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="144" height="29" align="center" valign="middle" background='/images/darkbluetab.png'>
                        <a href="leavemain.aspx" class="whitetext">My Requests</a></td>
                    <td width="2"></td>
                    <td width="144" height="29" align="center" valign="middle" background='/images/darkbluetab.png'>
                        <a href="leaverequest.aspx" class="whitetext">New Request</a>
                    </td>
                    <td width="2"></td>
                    <td width="144" height="29" align="center" valign="middle" background='/images/orangetab.png'>
                        <span class="whitetext">My Records</span>
                    </td>                
                  </tr>
                </table>             
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="grvList" BorderColor="#EEEEEE" CellPadding="4" runat="server" 
                      AutoGenerateColumns="False" DataKeyNames="lrcId" DataSourceID="SqlList" 
                      AllowPaging="True" Width="80%">
                    <Columns>
                        <asp:BoundField DataField="lrcId" HeaderText="lrcId" InsertVisible="False" ReadOnly="True" SortExpression="lrcId" Visible="False" />
                        <asp:BoundField DataField="ltyName" HeaderText="Leave Type" SortExpression="ltyName" />
                        <asp:BoundField DataField="lrcStartDate" HeaderText="Start Date" DataFormatString="{0:dd MMM yy}" SortExpression="lrcStartDate" />
                        <asp:BoundField DataField="lrcEndDate" HeaderText="End Date" DataFormatString="{0:dd MMM yy}" SortExpression="lrcEndDate" />
                        <asp:BoundField DataField="lrcDays" HeaderText="Days" SortExpression="lrcDays" />
                    </Columns>
                    <RowStyle CssClass="greytable" />
                    <AlternatingRowStyle CssClass="whitetable" />
                    <PagerStyle CssClass="greytable" />
                    <HeaderStyle HorizontalAlign="Left" ForeColor="#FFFFFF" CssClass="tablecolors_title" />
                    <EmptyDataTemplate>
                        There are currently no leave records related to you
                    </EmptyDataTemplate>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlList" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                    SelectCommandType="StoredProcedure"                      
                    SelectCommand="stpMyLeaveRecords_View">
                    <SelectParameters>
                        <asp:SessionParameter Name="usrId" SessionField="usrId" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
             </td> 
          </tr> 
          <tr>
            <td height="50">&nbsp;</td>
          </tr>
       </table> 
</asp:Content>

