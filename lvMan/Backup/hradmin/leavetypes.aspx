<%@ Page Title="LeaveFlow | leave types" Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.hradmin_leavetypes" Codebehind="leavetypes.aspx.vb" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
    <style type="text/css">
        .style2
        {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">

        <table cellpadding="3" cellspacing="0" class="style2">
            <tr>
                <td>
                    <asp:DropDownList ID="lstSelectCompanies" AutoPostBack="True" runat="server" CssClass="listmenu" 
                        DataSourceID="SqlCompanies" DataTextField="comName" DataValueField="comId">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlCompanies" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                        SelectCommandType="StoredProcedure"
                        SelectCommand="stpCompanyDetails">
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr id="trCommands" runat="server">
                <td>
                    <table width="290" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td id="tdView" runat="server" width="144" height="29" align="center" valign="middle" background='/images/orangetab.png'>
                            <asp:LinkButton ID="cmdViewConfigs" CssClass="whitetext" runat="server">View Types</asp:LinkButton></td>
                        <td width="2"></td>
                        <td id="tdAddEdit" runat="server" width="144" align="center" valign="middle" background='/images/darkbluetab.png'>
                        <asp:LinkButton ID="cmdAddConfig" CssClass="whitetext" runat="server">Add Types</asp:LinkButton>
                        </td>
                      </tr>
                    </table>                    
                </td>
            </tr>        
            <tr>
                <td>
                    <asp:Panel ID="panList" runat="server" Width="100%">
                        <asp:GridView ID="grvList" BorderColor="#EEEEEE" Width="100%" runat="server" 
                            AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ltyId" DataSourceID="SqlList" 
                            AllowPaging="True" AllowSorting="True">
                            <Columns>
                                <asp:BoundField DataField="ltyId" HeaderText="ltyId" InsertVisible="False" ReadOnly="True" SortExpression="ltyId" Visible="False" />
                                <asp:BoundField DataField="ltyName" HeaderText="Leave Type" SortExpression="ltyName" />
                                <asp:BoundField DataField="ltyDescription" HeaderText="Description" SortExpression="ltyDescription" />
                                <asp:BoundField DataField="ltyMax" HeaderText="Max Days" SortExpression="ltyMax" />
                                <asp:BoundField DataField="ltySex" HeaderText="Gender" SortExpression="ltySex" />
                                <asp:CommandField SelectText="View/Edit" ShowSelectButton="True" />
                            </Columns>
                            <RowStyle CssClass="greytable" />
                            <PagerStyle CssClass="greytable" />
                            <AlternatingRowStyle CssClass="whitetable" />
                            <HeaderStyle HorizontalAlign="Left" ForeColor="#FFFFFF" CssClass="tablecolors_title" />    
                            <EmptyDataTemplate>
                                There are currently no records to display
                            </EmptyDataTemplate>
                                                    
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlList" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                            DeleteCommandType="StoredProcedure"
                            DeleteCommand="stpLeaveTypes_Delete"
                            InsertCommandType="StoredProcedure" 
                            InsertCommand="stpLeaveTypes_Add"
                            SelectCommandType="StoredProcedure" 
                            SelectCommand="stpLeaveTypes_View" 
                            UpdateCommandType="StoredProcedure"
                            UpdateCommand="stpLeaveTypes_Update">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="lstSelectCompanies" Name="ltycomId" PropertyName="SelectedValue" Type="Int32" />
                            </SelectParameters>
                            <DeleteParameters>
                                <asp:Parameter Name="ltyId" Type="Int32" />
                            </DeleteParameters>
                            <UpdateParameters>
                                <asp:ControlParameter ControlID="txtName" PropertyName="Text" Name="ltyName" Type="String" />
                                <asp:ControlParameter ControlID="txtDescription" PropertyName="Text" Name="ltyDescription" Type="String" />
                                <asp:ControlParameter ControlID="chkPaid" Name="ltyPaid" PropertyName="Checked" Type="Boolean" />
                                <asp:ControlParameter ControlID="chkCount" Name="ltyCount" PropertyName="Checked" Type="Boolean" />
                                <asp:ControlParameter ControlID="txtMax" Name="ltyMax" PropertyName="Text" Type="Int32" />
                                <asp:ControlParameter ControlID="lstCompanies" PropertyName="SelectedValue" Name="ltycomId" Type="Int32" />
                                <asp:ControlParameter ControlID="chkCountAfterMax" Name="ltyCountAfterMax" PropertyName="Checked" Type="Boolean" />
                                <asp:ControlParameter ControlID="lstGender" PropertyName="SelectedValue" Name="ltySex" Type="String" />
                                <asp:ControlParameter ControlID="grvList" PropertyName="SelectedValue" Name="ltyId" Type="Int32" />
                            </UpdateParameters>
                            <InsertParameters>
                                <asp:ControlParameter ControlID="txtName" PropertyName="Text" Name="ltyName" Type="String" />
                                <asp:ControlParameter ControlID="txtDescription" PropertyName="Text" Name="ltyDescription" Type="String" />
                                <asp:ControlParameter ControlID="chkPaid" Name="ltyPaid" PropertyName="Checked" Type="Boolean" />
                                <asp:ControlParameter ControlID="chkCount" Name="ltyCount" PropertyName="Checked" Type="Boolean" />
                                <asp:ControlParameter ControlID="txtMax" Name="ltyMax" PropertyName="Text" Type="Int32" />
                                <asp:ControlParameter ControlID="lstCompanies" PropertyName="SelectedValue" Name="ltycomId" Type="Int32" />
                                <asp:ControlParameter ControlID="chkCountAfterMax" Name="ltyCountAfterMax" PropertyName="Checked" Type="Boolean" />
                                <asp:ControlParameter ControlID="lstGender" PropertyName="SelectedValue" Name="ltySex" Type="String" />
                            </InsertParameters>
                        </asp:SqlDataSource>
                    </asp:Panel>
                    
                    <asp:Panel ID="panAddEdit" runat="server" Width="100%" Visible="False">
                      <table width="400" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="15"><img src="/images/lefttop.jpg" alt="LeaveFlow" width="15" height="15" /></td>
                          <td height="15" background="/images/topside.jpg">&nbsp;</td>
                          <td width="15"><img src="/images/righttop.jpg" alt="LeaveFlow" width="15" height="15" /></td>
                        </tr>
                        <tr>
                          <td background="/images/leftside.jpg">&nbsp;</td>
                          <td valign="top">                    
                                <table cellpadding="3" id="tblAddEdit" runat="server" cellspacing="0" width="100%" class="tablewithborders">
                                    <tr id="trCompany" runat="server">
                                        <td width="50%">
                                            Company:</td>
                                        <td width="50%">
                                            <asp:DropDownList ID="lstCompanies" runat="server" CssClass="listmenu" 
                                                DataSourceID="SqlCompanies" DataTextField="comName" DataValueField="comId">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="50%">
                                            Name:</td>
                                        <td width="50%">
                                            <asp:TextBox ID="txtName" runat="server" CssClass="textbox"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rvaName" runat="server" 
                                                ControlToValidate="txtName" Display="Dynamic" 
                                                ErrorMessage="* please enter name" ValidationGroup="AddItem"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Description:</td>
                                        <td>
                                            <asp:TextBox ID="txtDescription" runat="server" Rows="4" CssClass="textbox" 
                                                Columns="30" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Paid:</td>
                                        <td>
                                            <asp:CheckBox ID="chkPaid" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Deduct units used:</td>
                                        <td>
                                            <asp:CheckBox ID="chkCount" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Maximum units:</td>
                                        <td>
                                            <asp:TextBox ID="txtMax" runat="server" CssClass="textbox"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Deduct after maximum:</td>
                                        <td>
                                            <asp:CheckBox ID="chkCountAfterMax" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Gender:</td>
                                        <td>
                                            <asp:DropDownList ID="lstGender" runat="server" CssClass="listmenu">
                                                <asp:ListItem Value="A">All</asp:ListItem>
                                                <asp:ListItem Value="M">Male</asp:ListItem>
                                                <asp:ListItem Value="F">Female</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="cmdCancel" runat="server" CausesValidation="False" 
                                                CssClass="button" Text="Cancel" />
                                        </td>
                                        <td>
                                            <asp:Button ID="cmdUpdate" runat="server" CssClass="button" 
                                                Text="Add Leave Type" ValidationGroup="AddItem" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="cmdDelete" OnClientClick="return confirm('Are you sure you want to delete this record?');" Font-Bold="True" runat="server">Delete Leave Type</asp:LinkButton>
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                </table>
                              </td>
                              <td background="/images/rightside.jpg">&nbsp;</td>
                            </tr>
                            <tr>
                              <td><img src="/images/leftbottom.jpg" alt="LeaveFlow" width="15" height="15" /></td>
                              <td height="15" background="/images/bottomside.jpg">&nbsp;</td>
                              <td><img src="/images/rightbottom.jpg" alt="right bottom" width="15" height="15" /></td>
                            </tr>    
                           </table>                             
                    </asp:Panel>    
                    
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
</asp:Content>

