﻿<%@ Page Title="Cancel Leave Record" Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.hradmin_cancelleaverecord" Codebehind="cancelleaverecord.aspx.vb" %>
<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Ajax.Net" Namespace="RJS.Web.WebControl" TagPrefix="rjs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">
    <table cellpadding="5" cellspacing="0" width="100%" align="center">
        <tr>
            <td>
                <asp:Label ID="lblMessage" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr id="trCancelMain1" runat="server">
            <td>
                <table width="100%" cellpadding="2" cellspacing="0">
                    <tr>
                        <td width="30%">Company:</td>
                        <td width="70%">
                            <asp:DropDownList ID="lstCompanies" AutoPostBack="True" runat="server" CssClass="listmenu" 
                                DataSourceID="SqlCompanies" DataTextField="comName" DataValueField="comId">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="SqlCompanies" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                SelectCommandType="StoredProcedure"
                                SelectCommand="stpCompanyDetails">
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td>Department:</td>
                        <td>
                            <asp:DropDownList ID="lstDepartments" runat="server" CssClass="listmenu" 
                                DataSourceID="SqlDepartments" DataTextField="dptName" 
                                DataValueField="dptId">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="SqlDepartments" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                SelectCommandType="StoredProcedure"
                                SelectCommand="stpDepartments_View">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="lstCompanies" Name="dptcomId" PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>   

                        </td>
                    </tr>
                    <tr>
                        <td>
                            Start Date between:</td>
                        <td>
                                <asp:TextBox ID="txtStartDate" ReadOnly="True" runat="server" CssClass="textbox"></asp:TextBox>
                                <rjs:PopCalendar ID="ppcStartDate" runat="server" Control="txtStartDate" 
                                    AutoPostBack="False" Format="mm dd yyyy" HolidayMessage="This date is not available" 
                                    Separator="/" SelectHoliday="False" />
                                              
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="Search" 
                                    ControlToValidate="txtStartDate" ErrorMessage="**" Display="Dynamic"></asp:RequiredFieldValidator>
                                <rjs:PopCalendarMessageContainer ID="pmcSTartDate" runat="server" Calendar="ppcStartDate" />

                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            and:</td>
                        <td>
                                <asp:TextBox ID="txtEndDate" ReadOnly="True" runat="server" CssClass="textbox"></asp:TextBox>
                                <rjs:PopCalendar ID="ppcEndDate" runat="server" Control="txtEndDate" 
                                    AutoPostBack="False" Format="mm dd yyyy" HolidayMessage="This date is not available" 
                                    Separator="/" ShowWeekend="True" />
                                              
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="Search" 
                                    ControlToValidate="txtEndDate" ErrorMessage="**" Display="Dynamic"></asp:RequiredFieldValidator>
                                <rjs:PopCalendarMessageContainer ID="pmcEndDate" runat="server" Calendar="ppcEndDate" /> 
                        </td>
                    </tr> 
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <asp:Button ID="cmdSearch" runat="server" CssClass="button" ValidationGroup="Search" Text="Search >" />
                        </td>
                    </tr>
                </table>

                
            </td>
        </tr>
        <tr id="trCancelMain2" runat="server">
            <td>
                <asp:GridView ID="grvList" BorderColor="#EEEEEE" CellPadding="4" runat="server" 
                      AutoGenerateColumns="False" DataKeyNames="lrcId" DataSourceID="SqlList" 
                      AllowPaging="True" Width="80%">
                    <Columns>
                        <asp:BoundField DataField="lrcId" HeaderText="lrcId" InsertVisible="False" ReadOnly="True" SortExpression="lrcId" Visible="False" />
                        <asp:BoundField DataField="usrFullName" HeaderText="Name" SortExpression="usrFullName" />
                        <asp:BoundField DataField="ltyName" HeaderText="Leave Type" SortExpression="ltyName" />
                        <asp:BoundField DataField="lrcStartDate" HeaderText="Start Date" DataFormatString="{0:dd MMM yy}" SortExpression="lrcStartDate" />
                        <asp:BoundField DataField="lrcEndDate" HeaderText="End Date" DataFormatString="{0:dd MMM yy}" SortExpression="lrcEndDate" />
                        <asp:BoundField DataField="lrcDays" HeaderText="Days" SortExpression="lrcDays" />
                        <asp:CommandField SelectText="Cancel Record" ShowSelectButton="True">
                            <ItemStyle CssClass="darckgrey" Font-Bold="True" />
                        </asp:CommandField>
                    </Columns>
                    <EmptyDataTemplate>
                        There are currently no leave records that meet the specified criteria
                    </EmptyDataTemplate>
                    <RowStyle CssClass="greytable" />
                    <AlternatingRowStyle CssClass="whitetable" />
                    <PagerStyle CssClass="greytable" />
                    <HeaderStyle HorizontalAlign="Left" ForeColor="#FFFFFF" CssClass="tablecolors_title" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlList" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                    SelectCommandType="StoredProcedure"                                         
                    SelectCommand="stpLeaveRecords_List">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="txtStartDate" Name="StartDate" PropertyName="Text" Type="DateTime" />
                        <asp:ControlParameter ControlID="txtEndDate" Name="EndDate" PropertyName="Text" Type="DateTime" />
                        <asp:ControlParameter ControlID="lstDepartments" Name="dptId" PropertyName="SelectedValue" DbType="Int32" />
                        <asp:ControlParameter ControlID="lstCompanies" Name="comId" PropertyName="SelectedValue" DbType="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
             </td> 
          </tr> 
          <tr id="trCancelDetail" runat="server" visible="False">
            <td height="50">
                <asp:Label ID="lblusrId" Visible="False" runat="server"></asp:Label>
                <table cellpadding="4" cellspacing="0" width="60%">
                    <tr>
                        <td>
                            Name:</td>
                        <td>
                            <asp:Label ID="lblName" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Leave Type:</td>
                        <td>
                            <asp:Label ID="lblLeaveType" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Start Date:</td>
                        <td>
                            <asp:Label ID="lblStartDate" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            End Date:</td>
                        <td>
                            <asp:Label ID="lblEndDate" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Days:</td>
                        <td>
                            <asp:Label ID="lblDays" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Cancel Reason:</td>
                        <td>
                            <asp:TextBox ID="txtReason" runat="server" Columns="30" CssClass="textbox" 
                                Rows="5" TextMode="MultiLine"></asp:TextBox>
                                              
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="Cancel" 
                                    ControlToValidate="txtReason" ErrorMessage="**" Display="Dynamic"></asp:RequiredFieldValidator>
                                </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            <asp:CheckBox ID="chkReturnDays" runat="server" 
                                Text="Return all leave days to balance" />
                            <br />
                            * You may return days partially using the Manual Adjustments feature</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            <asp:Button ID="cmdCancelNow" runat="server" CssClass="button" 
                                Text="Cancel Leave Now" ValidationGroup="Cancel" />
&nbsp;<asp:Button ID="cmdBack" runat="server" CssClass="button" Text="&lt; Back" />
                        </td>
                    </tr>
                </table>
              </td>
          </tr>
       </table> 

</asp:Content>

