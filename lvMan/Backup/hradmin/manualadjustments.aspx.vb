﻿Imports System.Data.SqlClient
Imports lvManLib.Security
Imports lvManLib.DataFunctions
Imports lvManLib.Logic

Partial Class hradmin_manualadjustments
    Inherits System.Web.UI.Page
    Public strUserPermissions As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        strUserPermissions = SecureCurrentPage()
        If InStr(strUserPermissions, "A") = 0 Then cmdAddConfig.Enabled = False
    End Sub

    Protected Sub cmdUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
        If Page.IsValid Then
            Dim strEffect As String
            Dim parameters As New List(Of SqlParameter)()
            Dim areId As New SqlParameter("@areId", lstReasons.SelectedValue)
            parameters.Add(areId)

            strEffect = GetSpDataSingleValue("stpAdjustmentReasons_GetEffect", parameters.ToArray())

            If cmdUpdate.Text = "Add Adjustment" Then
                SqlList.Insert()
                EffectLeaveDayChanges(lstUsers.SelectedValue, txtDays.Text, strEffect, lstReasons.SelectedItem.Text, "CHK")
            Else
                SqlList.Update()
            End If

            EditMode(False)
        End If
    End Sub

    Sub ClearFields()
        txtDays.Text = ""
        txtDescription.Text = ""
        lstCompanies.DataBind()
        lstCompanies.SelectedIndex = lstSelectCompanies.SelectedIndex
        lstDepartments.DataBind()
        lstDepartments.SelectedIndex = 0
    End Sub

    Protected Sub cmdAddMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddConfig.Click
        cmdUpdate.Text = "Add Adjustment"
        EditMode(True)
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        EditMode(False)
    End Sub

    Protected Sub cmdViewMenus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdViewConfigs.Click
        EditMode(False)
    End Sub

    Sub EditMode(ByVal bolEdit As Boolean, Optional ByVal bolClearFields As Boolean = True)
        EnableDisableFields(True)

        If bolEdit Then
            If bolClearFields Then ClearFields()
            panList.Visible = False
            panAddEdit.Visible = True
            tdAddEdit.Style.Item("background-image") = "url(/images/orangetab.png);"
            tdView.Style.Item("background-image") = "url(/images/darkbluetab.png);"

            If cmdUpdate.Text = "Update Adjustment" Then
                If InStr(strUserPermissions, "E") = 0 Then EnableDisableFields(False)
            End If
        Else
            cmdAddConfig.Text = "Add Adjustment"
            panList.Visible = True
            panAddEdit.Visible = False
            tdAddEdit.Style.Item("background-image") = "url(/images/darkbluetab.png);"
            tdView.Style.Item("background-image") = "url(/images/orangetab.png);"
            grvList.DataBind()
        End If
    End Sub

    Sub EnableDisableFields(ByVal bolEnabled As Boolean)
        txtDescription.Enabled = bolEnabled
        lstCompanies.Enabled = bolEnabled
        lstReasons.Enabled = bolEnabled
        lstDepartments.Enabled = bolEnabled
        txtDays.Enabled = bolEnabled
        cmdUpdate.Enabled = bolEnabled
    End Sub

End Class
