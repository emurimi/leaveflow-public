﻿Imports lvManLib.DataFunctions
Imports lvManLib.Logic
Imports lvManLib.Security
Imports System.Data.SqlClient

Partial Class hradmin_cancelleaverecord
    Inherits System.Web.UI.Page
    Public strUserPermissions As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            strUserPermissions = SecureCurrentPage()
            If InStr(strUserPermissions, "E") = 0 And InStr(strUserPermissions, "A") = 0 Then cmdCancelNow.Visible = False

            Dim datDate As Date = FormatDateTime(Now(), DateFormat.ShortDate)
            txtStartDate.Text = DateAdd(DateInterval.Month, -1, datDate)
            txtEndDate.Text = DateAdd(DateInterval.Month, 1, datDate)
            lstCompanies.DataBind()
            grvList.DataBind()

            lstCompanies.Items.Insert(0, New ListItem("All companies", "0"))
            lstCompanies.SelectedIndex = 0
            lstDepartments.DataBind()
            lstDepartments.Items.Insert(0, New ListItem("All departments", "0"))
            lstDepartments.SelectedIndex = 0
        End If
    End Sub


    Protected Sub cmdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        grvList.DataBind()
    End Sub

    Protected Sub lstCompanies_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstCompanies.SelectedIndexChanged
        lstDepartments.DataBind()
        lstDepartments.Items.Insert(0, New ListItem("All departments", "0"))
        lstDepartments.SelectedIndex = 0
    End Sub

    Protected Sub grvList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvList.SelectedIndexChanged
        Dim objDR As SqlDataReader
        Dim parameters As New List(Of SqlParameter)()
        Dim lrcId As New SqlParameter("@lrcId", grvList.SelectedValue)
        parameters.Add(lrcId)

        objDR = GetSpDataReader("stpLeaveRecord_MinView", parameters.ToArray())

        If objDR.Read() Then
            lblDays.Text = objDR("lrcDays")
            lblEndDate.Text = FormatDateTime(objDR("lrcEndDate"), DateFormat.LongDate)
            lblStartDate.Text = FormatDateTime(objDR("lrcStartDate"), DateFormat.LongDate)
            lblLeaveType.Text = objDR("ltyName")
            lblName.Text = objDR("usrFullName")
            lblusrId.Text = objDR("lrcusrId")
        End If

        chkReturnDays.Checked = False
        txtReason.Text = ""
        lblMessage.Text = ""

        objDR.Close()
        objDR = Nothing
        ShowForm()
    End Sub

    Protected Sub cmdBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBack.Click
        ShowList()
    End Sub

    Protected Sub cmdCancelNow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancelNow.Click
        If Page.IsValid Then
            Dim parameters As New List(Of SqlParameter)()
            Dim lrcId As New SqlParameter("@lrcId", grvList.SelectedValue)
            parameters.Add(lrcId)
            Dim lrcCancelReason As New SqlParameter("@lrcCancelReason", txtReason.Text)
            parameters.Add(lrcCancelReason)

            ExecuteStoredProc("stpLeaveRecords_Cancel", parameters.ToArray())

            lblMessage.Text = "The selected leave days have been canceled"

            If chkReturnDays.Checked Then
                EffectLeaveDayChanges(lblusrId.Text, lblDays.Text, "A", "Canceled leave record")
                lblMessage.Text &= " and the days have been returned to the user's balance"
            End If

            ShowList()
            grvList.DataBind()

        End If
    End Sub

    Sub ShowList()
        trCancelMain1.Visible = True
        trCancelMain2.Visible = True
        trCancelDetail.Visible = False
    End Sub

    Sub ShowForm()
        trCancelMain1.Visible = False
        trCancelMain2.Visible = False
        trCancelDetail.Visible = True
    End Sub
End Class
