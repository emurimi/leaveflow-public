﻿<%@ Page Title="Manage Pay Grades" Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.sysadmin_grades" Codebehind="grades.aspx.vb" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">

            <table width="100%" cellpadding="3" cellspacing="0" border="0">
                <tr>
                    <td width="100%">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td style="padding-bottom: 4px;">
                                
                                    <asp:DropDownList AutoPostBack="True" ID="lstSelectCompanies" runat="server" CssClass="listmenu" 
                                        DataSourceID="SqlCompanies" DataTextField="comName" DataValueField="comId">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlCompanies" runat="server" 
                                        ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                        SelectCommandType="StoredProcedure"
                                        SelectCommand="stpCompanyDetails">
                                    </asp:SqlDataSource>
                                
                                </td>
                            </tr>
                            <tr id="trCommands" runat="server">
                                <td>
                                    <table width="290" border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <td id="tdView" runat="server" width="144" height="29" align="center" valign="middle" background='/images/orangetab.png'>
                                            <asp:LinkButton CssClass="whitetext" ID="cmdViewConfigs" runat="server">View Pay Grades</asp:LinkButton></td>
                                        <td width="2"></td>
                                        <td id="tdAddEdit" runat="server" width="144" align="center" valign="middle" background='/images/darkbluetab.png'>
                                        <asp:LinkButton CssClass="whitetext" ID="cmdAddConfig" runat="server">Add Pay Grade</asp:LinkButton>
                                        </td>
                                      </tr>
                                    </table>                    
                                </td>
                            </tr>
                                   
                            <tr>
                                <td width="100%" valign="top">
                                    <asp:Panel ID="panList" runat="server" Width="100%">
                                        <asp:GridView ID="grvList" BorderColor="#EEEEEE" runat="server" Width="100%" AllowPaging="True"  CssClass="gridviewcontents"
                                            AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="graId" 
                                            DataSourceID="SqlList" CellPadding="4" RowStyle-Height="20">
                                            <RowStyle Height="20px" />
                                            <Columns>
                                                <asp:BoundField DataField="graId" HeaderText="graId" InsertVisible="False" Visible="False" ReadOnly="True" SortExpression="graId" />
                                                <asp:BoundField DataField="graName" HeaderText="Name" SortExpression="graName" />
                                                <asp:BoundField DataField="comName" HeaderText="Company" SortExpression="comName" />
                                                <asp:CommandField SelectText="View/Edit" ShowSelectButton="True" 
                                                    ItemStyle-Font-Bold="True" >
                                                    <ItemStyle Font-Bold="True" />
                                                </asp:CommandField>
                                            </Columns>
                                            <RowStyle CssClass="greytable" />
                                            <AlternatingRowStyle CssClass="whitetable" />
                                            <PagerStyle CssClass="greytable" />
                                            <HeaderStyle HorizontalAlign="Left" ForeColor="#FFFFFF" CssClass="tablecolors_title" />                            
                                            <EmptyDataTemplate>
                                                There are currently no records to display
                                            </EmptyDataTemplate>

                                        </asp:GridView>    
                                        <asp:SqlDataSource ID="SqlList" runat="server" 
                                            ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                            SelectCommandType="StoredProcedure"
                                            SelectCommand="stpGrades_View" 
                                            DeleteCommandType="StoredProcedure"
                                            DeleteCommand="stpGrades_Delete" 
                                            InsertCommandType="StoredProcedure"
                                            InsertCommand="stpGrades_Add" 
                                            UpdateCommandType="StoredProcedure"                                           
                                            UpdateCommand="stpGrades_Update">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="lstSelectCompanies" Name="gracomId" PropertyName="SelectedValue" Type="Int32" />
                                            </SelectParameters>
                                            <DeleteParameters>
                                                <asp:Parameter Name="graId" Type="Int32" />
                                            </DeleteParameters>
                                            <UpdateParameters>
                                                <asp:ControlParameter ControlID="txtMaxCarryOver" PropertyName="Text" Name="graMaxYearCarryOver" Type="Double" />
                                                <asp:ControlParameter ControlID="lstCarryOverExpiry" PropertyName="SelectedValue" Name="graCarryOverExpiryMonth" Type="Int32" />
                                                <asp:ControlParameter ControlID="lstCompanies" PropertyName="SelectedValue" Name="gracomId" Type="Int32" />
                                                <asp:ControlParameter ControlID="txtName" PropertyName="Text" Name="graName" Type="String" />
                                                <asp:ControlParameter ControlID="txtDescription" PropertyName="Text" Name="graDescription" Type="String" />
                                                <asp:ControlParameter ControlID="grvList" PropertyName="SelectedValue" Name="graId" Type="Int32" />
                                            </UpdateParameters>
                                            <InsertParameters>
                                                <asp:ControlParameter ControlID="txtMaxCarryOver" PropertyName="Text" Name="graMaxYearCarryOver" Type="Double" />
                                                <asp:ControlParameter ControlID="lstCarryOverExpiry" PropertyName="SelectedValue" Name="graCarryOverExpiryMonth" Type="Int32" />
                                                <asp:ControlParameter ControlID="lstCompanies" PropertyName="SelectedValue" Name="gracomId" Type="Int32" />
                                                <asp:ControlParameter ControlID="txtName" PropertyName="Text" Name="graName" Type="String" />
                                                <asp:ControlParameter ControlID="txtDescription" PropertyName="Text" Name="graDescription" Type="String" />
                                            </InsertParameters>
                                        </asp:SqlDataSource>
                                    </asp:Panel>

                                        
                                   
                                    <asp:Panel ID="panAddEdit" runat="server" Width="100%" Visible="False">
                                      <table width="400" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          <td width="15"><img src="/images/lefttop.jpg" alt="LeaveFlow" width="15" height="15" /></td>
                                          <td height="15" background="/images/topside.jpg">&nbsp;</td>
                                          <td width="15"><img src="/images/righttop.jpg" alt="LeaveFlow" width="15" height="15" /></td>
                                        </tr>
                                        <tr>
                                          <td background="/images/leftside.jpg">&nbsp;</td>
                                          <td valign="top">                                    
                                                <table cellpadding="3" id="tblAddEdit" runat="server" cellspacing="0" width="100%" class="tablewithborders">
                                                    <tr>
                                                        <td width="50%">
                                                            Name:</td>
                                                        <td width="50%">
                                                            <asp:TextBox ID="txtName" runat="server" CssClass="textbox"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="rvaName" runat="server" ControlToValidate="txtName" 
                                                                Display="Dynamic" ValidationGroup="AddItem" 
                                                                ErrorMessage="* please enter name"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr id="trCompany" runat="server">
                                                        <td>
                                                            Company:</td>
                                                        <td>
                                                            <asp:DropDownList ID="lstCompanies" runat="server" DataSourceID="SqlCompanies" 
                                                                DataTextField="comName" DataValueField="comId" CssClass="listmenu">
                                                            </asp:DropDownList>
                                                                
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Max carry over days (0 for no limit):</td>
                                                        <td>
                                                            <asp:TextBox ID="txtMaxCarryOver" runat="server" CssClass="textbox"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Expiry month for carry over days:</td>
                                                        <td>
                                                            <asp:DropDownList ID="lstCarryOverExpiry" runat="server" CssClass="listmenu">
                                                                <asp:ListItem Text="No Expiry" Value="0"></asp:ListItem>
                                                                <asp:ListItem Text="Jan" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="Feb" Value="2"></asp:ListItem>
                                                                <asp:ListItem Text="Mar" Value="3"></asp:ListItem>
                                                                <asp:ListItem Text="Apr" Value="4"></asp:ListItem>
                                                                <asp:ListItem Text="May" Value="5"></asp:ListItem>
                                                                <asp:ListItem Text="Jun" Value="6"></asp:ListItem>
                                                                <asp:ListItem Text="Jul" Value="7"></asp:ListItem>
                                                                <asp:ListItem Text="Aug" Value="8"></asp:ListItem>
                                                                <asp:ListItem Text="Sep" Value="9"></asp:ListItem>
                                                                <asp:ListItem Text="Oct" Value="10"></asp:ListItem>
                                                                <asp:ListItem Text="Nov" Value="11"></asp:ListItem>
                                                                <asp:ListItem Text="Dec" Value="12"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Description:</td>
                                                        <td>
                                                            <asp:TextBox ID="txtDescription" runat="server" Columns="30" CssClass="textbox" 
                                                                Rows="4" TextMode="MultiLine"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="cmdCancel" runat="server" CausesValidation="False" Text="Cancel" CssClass="button"></asp:Button>
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="cmdUpdate" runat="server" ValidationGroup="AddItem" 
                                                                CssClass="button" Text="Add Grade"></asp:Button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton ID="cmdDelete" OnClientClick="return confirm('Are you sure you want to delete this record?');" Font-Bold="True" runat="server">Delete Grade</asp:LinkButton>
                                                        </td>
                                                        <td>
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>
                                          </td>
                                          <td background="/images/rightside.jpg">&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td><img src="/images/leftbottom.jpg" alt="LeaveFlow" width="15" height="15" /></td>
                                          <td height="15" background="/images/bottomside.jpg">&nbsp;</td>
                                          <td><img src="/images/rightbottom.jpg" alt="right bottom" width="15" height="15" /></td>
                                        </tr>
                                      </table>                                         
                                    </asp:Panel>    
                                </td>
                            </tr>
                        </table>                    
                    </td>
                </tr>

            </table>
</asp:Content>

