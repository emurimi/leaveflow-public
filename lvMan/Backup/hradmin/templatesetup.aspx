﻿<%@ Page Title="Setup Templates" Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.hradmin_templatesetup" Codebehind="templatesetup.aspx.vb" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">

        <table cellpadding="3" cellspacing="0" width="100%">
            <tr>
                <td>
                    <asp:DropDownList ID="lstSelectCompanies" AutoPostBack="True" runat="server" CssClass="listmenu" 
                        DataSourceID="SqlCompanies" DataTextField="comName" DataValueField="comId">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlCompanies" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                        SelectCommandType="StoredProcedure"
                        SelectCommand="stpCompanyDetails">
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr id="trCommands" runat="server">
                <td>
                    <table width="290" border="0" cellspacing="2" cellpadding="0">
                      <tr>
                        <td id="tdView" runat="server" width="144" height="29" align="center" valign="middle" background='/images/orangetab.png'>
                            <asp:LinkButton ID="cmdViewConfigs" runat="server">View Templates</asp:LinkButton></td>
                        <td id="tdAddEdit" runat="server" width="144" align="center" valign="middle" background='/images/darkbluetab.png'>
                        <asp:LinkButton ID="cmdAddConfig" runat="server">Add Template</asp:LinkButton>
                        </td>
                      </tr>
                    </table>                    
                </td>
            </tr>        
            <tr>
                <td height="1" valign="top" bgcolor="#34a7db"><img src='<%= ResolveUrl("~/images/spacer.gif") %>' width="1" height="1" /></td>
            </tr>                                    
            
            <tr>
                <td>
                    <asp:Panel ID="panList" runat="server" Width="100%">
                        <asp:GridView ID="grvList" Width="100%" runat="server" AutoGenerateColumns="False" 
                            BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" 
                            CellPadding="4" DataKeyNames="tedId" DataSourceID="SqlList" ForeColor="Black" 
                            GridLines="Vertical" AllowPaging="True" AllowSorting="True">
                            <RowStyle BackColor="#F7F7DE" />
                            <Columns>
                                <asp:BoundField DataField="tedId" HeaderText="tedId" InsertVisible="False" Visible="False" ReadOnly="True" SortExpression="tedId" />
                                <asp:BoundField DataField="tedName" HeaderText="Name" SortExpression="tedName" />
                                <asp:CommandField SelectText="View/Edit" ShowSelectButton="True" ItemStyle-Font-Bold="True" />
                            </Columns>
                            <FooterStyle BackColor="#CCCC99" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlList" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>"
                            DeleteCommandType="StoredProcedure" 
                            DeleteCommand="stpTemplateDefinitions_Delete" 
                            InsertCommandType="StoredProcedure" 
                            InsertCommand="stpTemplateDefinitions_Add" 
                            SelectCommandType="StoredProcedure" 
                            SelectCommand="stpTemplateDefinitions_View" 
                            UpdateCommandType="StoredProcedure" 
                            UpdateCommand="stpTemplateDefinitions_Update">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="lstSelectCompanies" Name="tedcomId" PropertyName="SelectedValue" Type="Int32" />
                            </SelectParameters>
                            <DeleteParameters>
                                <asp:Parameter Name="tedId" Type="Int32" />
                            </DeleteParameters>
                            <UpdateParameters>
                                <asp:ControlParameter ControlID="lstCompanies" PropertyName="SelectedValue" Name="tedcomId" Type="Int32" />
                                <asp:ControlParameter ControlID="lstTemplateType" PropertyName="SelectedValue" Name="tedShortName" Type="String" />
                                <asp:ControlParameter ControlID="lstTemplateType" PropertyName="SelectedItem.Text" Name="tedName" Type="String" />
                                <asp:ControlParameter ControlID="txtText" PropertyName="Text" Name="tedText" Type="String" />
                                <asp:ControlParameter ControlID="grvList" PropertyName="SelectedValue" Name="tedId" Type="Int32" />
                            </UpdateParameters>
                            <InsertParameters>
                                <asp:ControlParameter ControlID="lstCompanies" PropertyName="SelectedValue" Name="tedcomId" Type="Int32" />
                                <asp:ControlParameter ControlID="lstTemplateType" PropertyName="SelectedValue" Name="tedShortName" Type="String" />
                                <asp:ControlParameter ControlID="lstTemplateType" PropertyName="SelectedItem.Text" Name="tedName" Type="String" />
                                <asp:ControlParameter ControlID="txtText" PropertyName="Text" Name="tedText" Type="String" />
                            </InsertParameters>
                        </asp:SqlDataSource>
                    </asp:Panel>
                    
                    <asp:Panel ID="panAddEdit" runat="server" Width="100%" Visible="False">
                        <table cellpadding="3" id="tblAddEdit" runat="server" cellspacing="0" width="100%" class="tablewithborders">
                            <tr>
                                <td width="50%">
                                    Company:</td>
                                <td width="50%">
                                    <asp:DropDownList ID="lstCompanies" runat="server" CssClass="listmenu" 
                                        DataSourceID="SqlCompanies" DataTextField="comName" DataValueField="comId">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>Template Type:
                                </td>
                                <td>
                                    <asp:DropDownList ID="lstTemplateType" runat="server" CssClass="listmenu">
                                        <asp:ListItem Value="DA">Days Adjusted</asp:ListItem>
                                        <asp:ListItem Value="FP">Forgot Password</asp:ListItem>
                                        <asp:ListItem Value="LA">Leave Application</asp:ListItem>
                                        <asp:ListItem Value="LP">Leave Approved</asp:ListItem>
                                        <asp:ListItem Value="LR">Leave Rejected</asp:ListItem>
                                        <asp:ListItem Value="NA">Next Approver</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Text:</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:TextBox ID="txtText" runat="server" Columns="60" CssClass="textbox" 
                                        Rows="10" TextMode="MultiLine"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rvaName0" runat="server" 
                                        ControlToValidate="txtText" Display="Dynamic" ErrorMessage="***" 
                                        ValidationGroup="AddItem"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="cmdCancel" runat="server" CausesValidation="False" 
                                        CssClass="button" Text="Cancel" />
                                </td>
                                <td>
                                    <asp:Button ID="cmdUpdate" runat="server" CssClass="button" Text="Add Template" 
                                        ValidationGroup="AddItem" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:LinkButton ID="cmdDelete" OnClientClick="return confirm('Are you sure you want to delete this record?');" Font-Bold="True" runat="server">Delete Template</asp:LinkButton>
                                </td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                        </table>
                    </asp:Panel>    
                    
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
</asp:Content>

