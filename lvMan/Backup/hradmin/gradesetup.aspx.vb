﻿Imports System.Data.SqlClient
Imports lvManLib.Security
Imports lvManLib.DataFunctions
Imports lvManLib.Config

Partial Class hradmin_gradesetup
    Inherits System.Web.UI.Page

    Public strUserPermissions As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        strUserPermissions = SecureCurrentPage()
        If InStr(strUserPermissions, "A") = 0 Then cmdAddConfig.Enabled = False

        If Not Page.IsPostBack Then
            Dim strOrganizationType As String = GetConfig("OrganizationType") & "" '1 = Organization with many companies, 2 = Single company

            If strOrganizationType = "2" Then
                trCompany.Visible = False
                lstSelectCompanies.Visible = False
                grvList.Columns(2).Visible = False
            End If

            lstCompanies.DataBind()
            lstCompanies.Items.Insert(0, New ListItem("Spans all companies", "0"))
            lstCompanies.SelectedIndex = 0

            lstSelectCompanies.DataBind()
            lstSelectCompanies.Items.Insert(0, New ListItem("Spans all companies", "0"))
            lstSelectCompanies.SelectedIndex = 0
        End If
    End Sub

    Protected Sub cmdUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
        If Page.IsValid Then
            If cmdUpdate.Text = "Add Entitlement" Then
                SqlList.Insert()
            Else
                SqlList.Update()
            End If
            EditMode(False)
        End If
    End Sub

    Sub ClearFields()
        txtMinimumMonths.Text = ""
        txtAccruedUnits.Text = ""
        'lstGrades.SelectedIndex = 0
    End Sub


    Sub GetUpdateForm(ByVal intgreId As Integer)
        Dim objDR As SqlDataReader
        Dim parameters As New List(Of SqlParameter)()
        Dim greId As New SqlParameter("@greId", intgreId)
        parameters.Add(greId)

        objDR = GetSpDataReader("stpGradeEntitlements_View", parameters.ToArray())

        If objDR.Read Then
            txtMinimumMonths.Text = objDR("graMinEmploymentMonths") & ""
            txtAccruedUnits.Text = objDR("graAccruedUnits") & ""
            lstCompanies.SelectedIndex = lstCompanies.Items.IndexOf(lstCompanies.Items.FindByValue(objDR("gracomId") & ""))
            lstGrades.DataBind()
            lstGrades.SelectedIndex = lstGrades.Items.IndexOf(lstGrades.Items.FindByValue(objDR("gragraId") & ""))
        End If

        objDR.Close()
        objDR = Nothing

        cmdUpdate.Text = "Update Entitlement"
        cmdAddConfig.Text = cmdUpdate.Text
        EditMode(True, False)
    End Sub

    Protected Sub grvList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvList.SelectedIndexChanged
        GetUpdateForm(grvList.SelectedValue)
    End Sub

    Protected Sub cmdAddMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddConfig.Click
        cmdUpdate.Text = "Add Entitlement"
        EditMode(True)
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        EditMode(False)
    End Sub

    Protected Sub cmdViewMenus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdViewConfigs.Click
        EditMode(False)
    End Sub

    Sub EditMode(ByVal bolEdit As Boolean, Optional ByVal bolClearFields As Boolean = True)
        EnableDisableFields(True)

        If bolEdit Then
            If bolClearFields Then ClearFields()
            panList.Visible = False
            panAddEdit.Visible = True
            tdAddEdit.Style.Item("background-image") = "url(/images/orangetab.png);"
            tdView.Style.Item("background-image") = "url(/images/darkbluetab.png);"
            cmdDelete.Visible = False

            If cmdUpdate.Text = "Update Entitlement" Then
                If InStr(strUserPermissions, "E") = 0 Then EnableDisableFields(False)
                If InStr(strUserPermissions, "D") > 0 Then cmdDelete.Visible = True
            End If
        Else
            cmdAddConfig.Text = "Add Entitlement"
            panList.Visible = True
            panAddEdit.Visible = False
            tdAddEdit.Style.Item("background-image") = "url(/images/darkbluetab.png);"
            tdView.Style.Item("background-image") = "url(/images/orangetab.png);"
            grvList.DataBind()
        End If
    End Sub

    Sub EnableDisableFields(ByVal bolEnabled As Boolean)
        txtAccruedUnits.Enabled = bolEnabled
        lstCompanies.Enabled = bolEnabled
        lstGrades.Enabled = bolEnabled
        txtMinimumMonths.Enabled = bolEnabled
        cmdUpdate.Enabled = bolEnabled
    End Sub

    Protected Sub cmdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        Dim parameters As New List(Of SqlParameter)()
        Dim greId As New SqlParameter("@greId", grvList.SelectedValue)
        parameters.Add(greId)
        ExecuteStoredProc("stpGradeEntitlements_Delete")
        EditMode(False)
    End Sub

    Protected Sub lstSelectCompanies_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstSelectCompanies.SelectedIndexChanged
        grvList.DataBind()
    End Sub
End Class
