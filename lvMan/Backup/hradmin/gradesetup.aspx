<%@ Page Title="Setup Grades" Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.hradmin_gradesetup" Codebehind="gradesetup.aspx.vb" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">

        <table cellpadding="3" cellspacing="0" width="100%">
            <tr>
                <td>
                    <asp:DropDownList ID="lstSelectCompanies" AutoPostBack="True" runat="server" CssClass="listmenu" 
                        DataSourceID="SqlCompanies" DataTextField="comName" DataValueField="comId">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlCompanies" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                        SelectCommandType="StoredProcedure"
                        SelectCommand="stpCompanyDetails">
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr id="trCommands" runat="server">
                <td>
                    <table width="290" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td id="tdView" runat="server" width="144" height="29" align="center" valign="middle" background='/images/orangetab.png'>
                            <asp:LinkButton ID="cmdViewConfigs" CssClass="whitetext" runat="server">View Entitlements</asp:LinkButton></td>
                        <td width="2"></td>
                        <td id="tdAddEdit" runat="server" width="144" align="center" valign="middle" background='/images/darkbluetab.png'>
                            <asp:LinkButton ID="cmdAddConfig" CssClass="whitetext" runat="server">Add Entitlement</asp:LinkButton>
                        </td>
                      </tr>
                    </table>                    
                </td>
            </tr>        
            <tr>
                <td>
                    <asp:Panel ID="panList" runat="server" Width="100%">
                        <asp:GridView ID="grvList" BorderColor="#EEEEEE" Width="100%" runat="server" 
                            AutoGenerateColumns="False" CellPadding="4" DataKeyNames="greId" DataSourceID="SqlList" 
                            AllowPaging="True" AllowSorting="True">
                            <Columns>
                                <asp:BoundField DataField="greId" HeaderText="greId" Visible="False" InsertVisible="False" ReadOnly="True" SortExpression="greId" />
                                <asp:BoundField DataField="graName" HeaderText="Grade" SortExpression="graName" />
                                <asp:BoundField DataField="graMinEmploymentMonths" HeaderText="Min Employment Months" SortExpression="graMinEmploymentMonths" />
                                <asp:BoundField DataField="graAccruedUnits" HeaderText="Accrued Units" SortExpression="graAccruedUnits" />
                                <asp:CommandField SelectText="View/Edit" ItemStyle-Font-Bold="True" ShowSelectButton="True" >
                                    <ItemStyle Font-Bold="True" />
                                </asp:CommandField>
                            </Columns>
                            <RowStyle CssClass="greytable" />
                            <AlternatingRowStyle CssClass="whitetable" />
                            <PagerStyle CssClass="greytable" />
                            <HeaderStyle HorizontalAlign="Left" ForeColor="#FFFFFF" CssClass="tablecolors_title" />                            
                            <EmptyDataTemplate>
                                There are currently no records to display
                            </EmptyDataTemplate>
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlList" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                            DeleteCommandType="StoredProcedure"
                            DeleteCommand="stpGradeEntitlements_Delete" 
                            InsertCommandType="StoredProcedure"
                            InsertCommand="stpGradeEntitlements_Add" 
                            SelectCommandType="StoredProcedure"
                            SelectCommand="stpGradeEntitlements_View" 
                            UpdateCommandType="StoredProcedure"
                            UpdateCommand="stpGradeEntitlements_Update">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="lstSelectCompanies" Name="gragraId" PropertyName="SelectedValue" Type="Int32" />
                            </SelectParameters>
                            <DeleteParameters>
                                <asp:Parameter Name="greId" Type="Int32" />
                            </DeleteParameters>
                            <UpdateParameters>
                                <asp:ControlParameter ControlID="lstGrades" PropertyName="SelectedValue" Name="gragraId" Type="Int32" />
                                <asp:ControlParameter ControlID="txtMinimumMonths" PropertyName="Text" Name="graMinEmploymentMonths" Type="Double" />
                                <asp:ControlParameter ControlID="txtAccruedUnits" PropertyName="Text" Name="graAccruedUnits" Type="Double" />
                                <asp:ControlParameter ControlID="grvList" PropertyName="SelectedValue" Name="greId" Type="Int32" />
                            </UpdateParameters>
                            <InsertParameters>
                                <asp:ControlParameter ControlID="lstGrades" PropertyName="SelectedValue" Name="gragraId" Type="Int32" />
                                <asp:ControlParameter ControlID="txtMinimumMonths" PropertyName="Text" Name="graMinEmploymentMonths" Type="Double" />
                                <asp:ControlParameter ControlID="txtAccruedUnits" PropertyName="Text" Name="graAccruedUnits" Type="Double" />
                            </InsertParameters>
                        </asp:SqlDataSource>
                    </asp:Panel>
                    
                    <asp:Panel ID="panAddEdit" runat="server" Width="100%" Visible="False">
                      <table width="500" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="15"><img src="/images/lefttop.jpg" alt="LeaveFlow" width="15" height="15" /></td>
                          <td height="15" background="/images/topside.jpg">&nbsp;</td>
                          <td width="15"><img src="/images/righttop.jpg" alt="LeaveFlow" width="15" height="15" /></td>
                        </tr>
                        <tr>
                          <td background="/images/leftside.jpg">&nbsp;</td>
                          <td valign="top">                    
                                <table cellpadding="3" id="tblAddEdit" runat="server" cellspacing="0" width="100%" class="tablewithborders">
                                    <tr id="trCompany" runat="server">
                                        <td width="50%">
                                            Company:</td>
                                        <td width="50%">
                                            <asp:DropDownList ID="lstCompanies" runat="server" CssClass="listmenu" 
                                                DataSourceID="SqlCompanies" DataTextField="comName" DataValueField="comId">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="50%">
                                            Grade:</td>
                                        <td width="50%">
                                            <asp:DropDownList ID="lstGrades" runat="server" CssClass="listmenu" 
                                                DataSourceID="SqlGrades" DataTextField="graName" DataValueField="graId">
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="SqlGrades" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                SelectCommandType="StoredProcedure"
                                                SelectCommand="stpGrades_View">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="lstCompanies" Name="gracomId" PropertyName="SelectedValue" Type="Int32" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="50%">
                                            Minimum months employed:</td>
                                        <td width="50%">
                                            <asp:TextBox ID="txtMinimumMonths" runat="server" CssClass="textbox"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rvaName" runat="server" 
                                                ControlToValidate="txtMinimumMonths" Display="Dynamic" ErrorMessage="***" 
                                                ValidationGroup="AddItem"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Leave units accrued (per accrual period):</td>
                                        <td>
                                            <asp:TextBox ID="txtAccruedUnits" runat="server" CssClass="textbox"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rvaName0" runat="server" 
                                                ControlToValidate="txtAccruedUnits" Display="Dynamic" ErrorMessage="***" 
                                                ValidationGroup="AddItem"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="cmdCancel" runat="server" CausesValidation="False" 
                                                CssClass="button" Text="Cancel" />
                                        </td>
                                        <td>
                                            <asp:Button ID="cmdUpdate" runat="server" CssClass="button" 
                                                Text="Add Entitlement" ValidationGroup="AddItem" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="cmdDelete" OnClientClick="return confirm('Are you sure you want to delete this record?');" Font-Bold="True" runat="server">Delete Entitlement</asp:LinkButton>
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                </table>
                              </td>
                              <td background="/images/rightside.jpg">&nbsp;</td>
                            </tr>
                            <tr>
                              <td><img src="/images/leftbottom.jpg" alt="LeaveFlow" width="15" height="15" /></td>
                              <td height="15" background="/images/bottomside.jpg">&nbsp;</td>
                              <td><img src="/images/rightbottom.jpg" alt="right bottom" width="15" height="15" /></td>
                            </tr>
                          </table>                                  
                    </asp:Panel>    
                    
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
 
</asp:Content>

