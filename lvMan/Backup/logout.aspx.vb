﻿
Partial Class logout
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim objCookie As System.Web.HttpCookie

        objCookie = Request.Cookies("smartLv")
        Try
            objCookie.Expires = DateAdd(DateInterval.Day, -1, Now())
            Response.Cookies.Add(objCookie)
        Catch ex As Exception
            'do nothing
        End Try

        Session.Abandon()
        Response.Redirect("default.aspx")
    End Sub
End Class
