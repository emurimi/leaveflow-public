﻿Imports lvManLib.Config
Imports lvManLib.DataFunctions
Imports lvManLib.Leave
Imports lvManLib.General
Imports lvManLib.Security
Imports System.Data.SqlClient

Partial Class leaverequest
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not UserLoggedIn() Then Response.Redirect("/default.aspx?errm=tmo")

        If Not Page.IsPostBack Then
            Dim datDate As Date
            Dim strLRQ As String
            Dim strTemp As String
            Dim strDateFormat As String = GetConfig("DateFormat") & ""

            'If logged in user is liz, then allow to apply on behalf of someone
            If LCase(Session("usrName")) = "liz@m-kopa.com" Then
                trApplyFor.Visible = True
            End If

            Select Case strDateFormat
                Case "UK"
                    ppcStartDate.Format = "dd/mm/yyyy"
                    ppcEndDate.Format = "dd/mm/yyyy"
                Case "UN"
                    ppcStartDate.Format = "yyyy/mm/dd"
                    ppcEndDate.Format = "yyyy/mm/dd"
                Case "US"
                    ppcStartDate.Format = "mm/dd/yyyy"
                    ppcEndDate.Format = "mm/dd/yyyy"
            End Select

            'check whether user has approvers setup
            strTemp = GetSpDataSingleValue("stpCheckUserApprovers", , "usrId=" & lstApplicationFor.SelectedValue)

            If CType(strTemp, Integer) > 0 Then
                lblMessage.Text = "You currently have no approvers setup. Please alert HR of this anomaly, as you will not be able to apply for leave."
                cmdCancel.Visible = False
                cmdUpdate.Visible = False
            End If

            strLRQ = Request.QueryString("lrq") & ""

            lblTransactionID.Text = GetTransactionID()
            lblRelatedDocuments.Text = "<a href=""javascript:openwin_small('/uploadrelateddocs.aspx?trn=" & lblTransactionID.Text & "&ent=R')"" class=""blacktext""><b>Attach Related Document(s)</b></a>"
            datDate = DateAdd(DateInterval.Weekday, 20, Now())

            txtStartDate.Text = FormatDateTime(datDate, DateFormat.ShortDate)
            datDate = DateAdd(DateInterval.Day, 5, datDate)
            txtEndDate.Text = FormatDateTime(datDate, DateFormat.ShortDate)

            lstApplicationFor.DataBind()
            lstApplicationFor.SelectedIndex = lstApplicationFor.Items.IndexOf(lstApplicationFor.Items.FindByValue(Session("usrId")))
            lstLeaveType.DataBind()
            lblDaysAvailable.Text = CalculateAvailableDays(lstApplicationFor.SelectedValue, lstLeaveType.SelectedValue)

            lstReliever.DataBind()
            lstReliever.Items.Insert(0, New ListItem("No Reliever", "0"))
            lstReliever.SelectedIndex = 0

            LoadTime(lstStartTime, Session("comId"))
            LoadTime(lstEndTime, Session("comId"))
            lstStartTime.SelectedIndex = 0
            lstEndTime.SelectedIndex = lstEndTime.Items.Count - 1

            lblLeaveUnits.Text = Session("comLeaveUnit")
            If lblLeaveUnits.Text = "H" Then
                lblLeaveUnits.Text = "hours"
                lblLeaveUnits2.Text = "Hours"
            Else
                lblLeaveUnits.Text = "days"
                lblLeaveUnits2.Text = "Days"
            End If

            lblDays.Text = CountLeaveDays(txtStartDate.Text, txtEndDate.Text, Session("comLeaveUnit"), lstApplicationFor.SelectedValue, lstStartTime.SelectedValue, lstEndTime.SelectedValue, lstLeaveType.SelectedValue)

            If Mid(UCase(lblLeaveUnits.Text), 1, 1) = "H" Then
                lblDaysDesc.Text = GetHoursToDays(lblDays.Text, Session("comId"))
                lblDaysDesc2.Text = lblDaysDesc.Text
            End If

            If IsNumeric(strLRQ) Then
                strTemp = GetSpDataSingleValue("stpGetLeaveRequestStatus", , "lrqId=" & strLRQ & "*usrId=" & Session("usrId"))

                If Len(strTemp) = 0 Then
                    lblMessage.Text = "You may only cancel a leave request that you have access to"
                    Exit Sub
                End If

                If Len(strTemp) > 0 Then
                    lbllrqId.Text = strLRQ
                    LoadApplication()

                    If Request.QueryString("act") & "" = "cancl" Then 'User has decided to cancel request
                        Dim strMailMessage As String
                        Dim strMailTitle As String
                        Dim strEmailAdd As String = Session("usrEmailAdd")
                        Dim strURL As String = GetConfig("SystemURL")

                        trCancelReason.Visible = True

                        If 1 = 0 Then
                            ExecuteStoredProc("stpLeaveRequest_UpdateStatus", , "lrqstaId=3*lrqId=" & strLRQ)
                            lblMessage.Text = "You have cancelled this leave request"
                            lblMessage.BackColor = Drawing.Color.Yellow
                            LoadApplication()

                            strMailMessage = GetMailTemplate("ULCN", Session("comId"), "[NAME]|[URL]", Session("usrFullName") & "|" & strURL)
                            strMailTitle = "Leave Request Cancelled" 'Mid(strMailMessage, 1, InStr(strMailMessage, "|}*") - 1)
                            ''strMailMessage = Mid(strMailMessage, InStr(strMailMessage, "|}*") + 3)

                            If Len(strEmailAdd) > 2 Then SendMail(strMailTitle, strMailMessage, strEmailAdd)
                        End If

                        Exit Sub
                    End If

                    If strTemp = "6" Then 'If Status = 6
                        'Allow Revision of this LRQ
                        lblMessage.Text = "Please revise this leave application, as requested by your approver<br /><br />You may also choose to <a onclick=""return confirm('Are you sure you want to cancel this request?');"" href='leaverequest.aspx?act=cancl&lrq=" & Request.QueryString("lrq") & "'><b>CANCEL THIS REQUEST</b></a>"
                        lblMessage.BackColor = Drawing.Color.Yellow
                        cmdUpdate.Text = "send revised request"
                        trApplication.Visible = True
                        trConfirmation.Visible = False
                    End If
                End If
            End If
        End If

        LoadHolidaysUsedDays()
    End Sub

    Protected Sub cmdUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
        If Page.IsValid Then
            If ValidApplication() Then
                ShowConfirmation()
                If cmdUpdate.Text = "send revised request" Then cmdConfirmRequest.Text = "Confirm Revised Request"
            End If
        End If
    End Sub

    Sub LoadApplication()
        Dim objDR As SqlDataReader
        Dim strUserID As String = ""
        Dim intStatus As Integer

        objDR = GetSpDataReader("stpLeaveRequest_View", , "lrqId=" & lbllrqId.Text)

        If objDR.Read() Then
            strUserID = objDR("lrqusrId")
            lstApplicationFor.SelectedIndex = lstApplicationFor.Items.IndexOf(lstApplicationFor.Items.FindByValue(strUserID))
            lblTransactionID.Text = objDR("lrqTransactionID") & ""
            lblConfComments.Text = objDR("lrqComments") & ""
            lblConfContacts.Text = objDR("lrqContacts") & ""
            lblConfStartTime.Text = objDR("lrqStartTime") & ""
            lblConfEndTime.Text = objDR("lrqEndTime") & ""
            lblConfEndDate.Text = FormatDateTime(objDR("lrqEndDate") & "", DateFormat.ShortDate)
            lblConfLeaveType.Text = objDR("ltyName") & ""
            lblConfReliever.Text = objDR("Reliever") & ""
            lblConfStartDate.Text = FormatDateTime(objDR("lrqStartDate") & "", DateFormat.ShortDate)
            lblConfStatus.Text = objDR("staName")
            intStatus = objDR("lrqstaId")
            txtContacts.Text = objDR("lrqContacts")

            Select Case intStatus
                Case 3, 4, 5 'Canceled, Rejected, Approved
                    'do nothing
                Case Else 'Allow Cancel
                    lblConfStatus.Text &= " &nbsp;&nbsp;|&nbsp;&nbsp; <i><a onclick=""return confirm('Are you sure you want to cancel this request?');"" href='leaverequest.aspx?act=cancl&lrq=" & lbllrqId.Text & "'>Cancel Leave Request</a></i>"
            End Select


            lblConfDaysAvailable.Text = CalculateAvailableDays(objDR("lrqusrId"), objDR("lrqltyId"))

            lstLeaveType.SelectedIndex = lstLeaveType.Items.IndexOf(lstLeaveType.Items.FindByValue(objDR("lrqltyId")))
            lstReliever.SelectedIndex = lstReliever.Items.IndexOf(lstReliever.Items.FindByValue(objDR("lrqReliever")))
            txtStartDate.Text = lblConfStartDate.Text
            txtEndDate.Text = lblConfEndDate.Text
            txtComments.Text = lblConfComments.Text
            txtContacts.Text = lblConfContacts.Text
            lstStartTime.SelectedIndex = lstStartTime.Items.IndexOf(lstStartTime.Items.FindByValue(lblConfStartTime.Text))
            lstEndTime.SelectedIndex = lstEndTime.Items.IndexOf(lstEndTime.Items.FindByValue(lblConfEndTime.Text))
        End If

        objDR.Close()
        objDR = Nothing

        lblConfDays.Text = CountLeaveDays(lblConfStartDate.Text, lblConfEndDate.Text, Session("comLeaveUnit") & "", strUserID, lblConfStartTime.Text, lblConfEndTime.Text, lstLeaveType.SelectedValue)
        If UCase(Session("comLeaveUnit")) = "H" Then
            lblDaysDesc2.Text = GetHoursToDays(lblConfDays.Text, Session("comId"))
        End If

        If Session("comLeaveUnit") & "" = "H" Then
            lblConfLeaveUnits1.Text = "hours"
            lblConfLeaveUnits2.Text = "Hours"
        Else
            lblConfLeaveUnits1.Text = "days"
            lblConfLeaveUnits2.Text = "Days"
        End If

        trApplication.Visible = False
        trConfirmation.Visible = True
        grvApprovals.DataBind()
        grvList.DataBind()
        grvList.Columns(2).Visible = False
    End Sub

    Function ValidApplication() As Boolean
        Dim bolValid As Boolean
        Dim datStartDate As Date
        Dim datEndDate As Date
        Dim dblDays As Double
        Dim dblDaysAvail As Double
        Dim strAllowExcessLeave As String
        Dim strStartTime As String
        Dim strEndTime As String
        Dim dblMinumumLeaveUnits As Double = 0
        bolValid = True


        strStartTime = Replace(lstStartTime.SelectedValue, ":", "")
        strEndTime = Replace(lstEndTime.SelectedValue, ":", "")

        'Check Dates
        If Len(txtStartDate.Text) = 0 Or Len(txtEndDate.Text) = 0 Then
            lblMessage.Text = "Please enter a valid start date and end date."
            Return False
        End If

        If Not IsDate(txtStartDate.Text) Or Not IsDate(txtEndDate.Text) Then
            lblMessage.Text = "Please enter a valid start date and end date."
            Return False
        End If

        If txtStartDate.Text = txtEndDate.Text And CType(strStartTime, Integer) >= CType(strEndTime, Integer) Then
            lblMessage.Text = "Please enter a valid start time and end time."
            Return False
        End If

        datStartDate = CType(txtStartDate.Text, Date)
        datEndDate = CType(txtEndDate.Text, Date)

        'Check number of days available vs applied for
        dblDays = CountLeaveDays(datStartDate, datEndDate, lblLeaveUnits.Text, lstApplicationFor.SelectedValue, lstStartTime.SelectedValue, lstEndTime.SelectedValue, lstLeaveType.SelectedValue)
        dblDaysAvail = CalculateAvailableDays(lstApplicationFor.SelectedValue, lstLeaveType.SelectedValue)

        If dblDays < dblMinumumLeaveUnits Then
            lblMessage.Text = "The applied for units are less than the minimal units of " & FormatNumber(dblMinumumLeaveUnits, 1) & " " & lblLeaveUnits2.Text
            Return False
        End If

        'look for duplicate leave

        If dblDays > dblDaysAvail Then
            strAllowExcessLeave = LCase(GetConfig("AllowExcessLeave") & "")
            If strAllowExcessLeave = "false" Then
                lblMessage.Text = "Sorry, you have applied for more days than are available. Please correct."
                Return False
            End If
        End If

        Return bolValid
    End Function

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Response.Redirect("main.aspx")
    End Sub

    Protected Sub cmdCalculate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCalculate.Click
        lblDays.Text = CountLeaveDays(txtStartDate.Text, txtEndDate.Text, lblLeaveUnits.Text, lstApplicationFor.SelectedValue, lstStartTime.SelectedValue, lstEndTime.SelectedValue, lstLeaveType.SelectedValue)
        If Mid(UCase(lblLeaveUnits.Text), 1, 1) = "H" Then
            lblDaysDesc.Text = GetHoursToDays(lblDays.Text, Session("comId"))
            lblDaysDesc2.Text = lblDaysDesc.Text
        End If
    End Sub

    Protected Sub lstLeaveType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstLeaveType.SelectedIndexChanged
        lblDaysAvailable.Text = CalculateAvailableDays(lstApplicationFor.SelectedValue, lstLeaveType.SelectedValue)
    End Sub

    Protected Sub lstApplicationFor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstApplicationFor.SelectedIndexChanged
        lblDaysAvailable.Text = CalculateAvailableDays(lstApplicationFor.SelectedValue, lstLeaveType.SelectedValue)
    End Sub

    Sub LoadHolidaysUsedDays()
        Dim objDR As SqlDataReader
        Dim intYear As Integer
        Dim intMonth As Integer
        Dim intDay As Integer
        Dim bolRecurring As Boolean
        Dim strName As String
        Dim datStartDate As Date
        Dim datEndDate As Date
        Dim datDate As Date

        objDR = GetSpDataReader("stpPublicHolidaysByCountry_View", , "ctyId=" & Session("ctyId"))

        While objDR.Read()
            intYear = objDR("pbhYear")
            intMonth = objDR("pbhMonth")
            intDay = objDR("pbhDay")
            bolRecurring = objDR("pbhRecurring")
            strName = objDR("pbhName") & ""

            ppcStartDate.AddSpecialDay(intDay, intMonth, intYear, strName)
            ppcEndDate.AddSpecialDay(intDay, intMonth, intYear, strName)

            If bolRecurring Then
                If intYear <> Year(Now()) Then ppcStartDate.AddSpecialDay(intDay, intMonth, Year(Now()), strName)
                If intYear <> Year(Now()) - 1 Then ppcStartDate.AddSpecialDay(intDay, intMonth, Year(Now()) - 1, strName)
                If intYear <> Year(Now()) + 1 Then ppcStartDate.AddSpecialDay(intDay, intMonth, Year(Now()) + 1, strName)

                If intYear <> Year(Now()) Then ppcEndDate.AddSpecialDay(intDay, intMonth, Year(Now()), strName)
                If intYear <> Year(Now()) - 1 Then ppcEndDate.AddSpecialDay(intDay, intMonth, Year(Now()) - 1, strName)
                If intYear <> Year(Now()) + 1 Then ppcEndDate.AddSpecialDay(intDay, intMonth, Year(Now()) + 1, strName)
            End If
        End While

        objDR.Close()

        objDR = GetSpDataReader("stpLeaveRecords_UserMinView", , "usrId=" & lstApplicationFor.SelectedValue)

        While objDR.Read()
            datStartDate = objDR("lrcStartDate")
            datEndDate = objDR("lrcEndDate")
            datDate = datStartDate

            Do While datDate <= datEndDate
                ppcStartDate.AddHoliday(Day(datDate), Month(datDate), Year(datDate), "Date already used on leave")
                ppcEndDate.AddHoliday(Day(datDate), Month(datDate), Year(datDate), "Date already used on leave")
                datDate = DateAdd(DateInterval.Day, 1, datDate)
            Loop
        End While

        objDR.Close()
        objDR = Nothing

    End Sub

    Protected Sub ppcStartDate_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ppcStartDate.SelectionChanged
        LoadHolidaysUsedDays()
    End Sub

    Protected Sub ppcEndDate_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ppcEndDate.SelectionChanged
        LoadHolidaysUsedDays()
    End Sub

    Protected Sub cmdConfirmRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdConfirmRequest.Click
        lblExtraMessage.Text = ""

        If ValidApplication() Then
            Dim strTemp As String = ""
            Dim intCount As Integer = 0

            If LCase(cmdConfirmRequest.Text) = "confirm revised request" Then
                Dim strMailMessage As String
                Dim strApproversEmail As String = ""
                Dim strApproversName As String = ""
                Dim strMailTitle As String
                Dim objDR As SqlDataReader
                Dim strURL As String = GetConfig("SystemURL")

                objDR = GetSpDataReader("stpApprovalPath_RevisionView", , "lrqId=" & lbllrqId.Text)

                If objDR.Read Then
                    strApproversEmail = objDR("usrEmailAdd") & ""
                    strApproversName = objDR("usrFullName") & ""
                End If

                objDR.Close()
                objDR = Nothing

                SqlList.Update()

                ExecuteStoredProc("stpApprovalPath_RevisionUpdate", , "staId=1*staIdCheck=6*lrqId=" & lbllrqId.Text)

                'Deprecated?
                'strSql = "UPDATE tblLeaveRequests SET lrqstaId = 1 WHERE lrqId = " & lbllrqId.Text

                If Len(strApproversEmail & "") > 0 Then
                    strMailMessage = GetMailTemplate("ALRRAA", Session("comId"), "[NAME]|[APPROVER]|[URL]", Session("usrFullName") & "|" & strApproversName & "|" & strURL)

                    If Len(strMailMessage) > 10 Then
                        strMailTitle = Mid(strMailMessage, 1, InStr(strMailMessage, "|}*") - 1)
                        strMailMessage = Mid(strMailMessage, InStr(strMailMessage, "|}*") + 3)
                        strMailMessage = Replace(strMailMessage, Chr(13), "<br />")
                        strMailMessage = Replace(strMailMessage, vbCrLf, "<br />")
                        SendMail(strMailTitle, strMailMessage, strApproversEmail)
                    End If
                End If

            Else
                Try
                    SqlList.Insert()
                Catch ex As Exception
                    lblExtraMessage.Text = "Your leave request has already been submitted." & ex.Message
                    Exit Sub
                End Try

                Do Until Len(strTemp) > 0
                    strTemp = GetSpDataSingleValue("stpGetLastLeaveRequestID", , "usrId=" & lstApplicationFor.SelectedValue)
                    intCount += 1
                    If Len(strTemp) = 0 Then
                        System.Threading.Thread.Sleep(1000)
                        If intCount > 10 Then strTemp = "0"
                    End If
                Loop

                ExecuteStoredProc("stpLeaveDocuments_UpdateEntity", , "ldcEntityID=" & strTemp & "*ldcTransactionID=" & lblTransactionID.Text)

                AlertApprovers(strTemp, "LR")
                lbllrqId.Text = strTemp
            End If

            LoadApplication()
            trConfirmRow.Visible = False
            trApprovalDetails.Visible = True
        End If
    End Sub

    Protected Sub cmdCancelRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancelRequest.Click
        trApplication.Visible = True
        trConfirmation.Visible = False
        trConfirmRow.Visible = False
    End Sub

    Sub ShowConfirmation()

        lblConfComments.Text = txtComments.Text
        lblConfContacts.Text = txtContacts.Text
        lblConfStartTime.Text = lstStartTime.SelectedItem.Text
        lblConfEndTime.Text = lstEndTime.SelectedItem.Text
        lblConfEndDate.Text = txtEndDate.Text
        lblConfLeaveType.Text = lstLeaveType.SelectedItem.Text
        lblConfReliever.Text = lstReliever.SelectedItem.Text
        lblConfStartDate.Text = txtStartDate.Text
        lblConfStatus.Text = "Pending"
        lblConfDaysAvailable.Text = lblDaysAvailable.Text
        lblConfDays.Text = lblDays.Text
        lblConfLeaveUnits1.Text = lblLeaveUnits.Text
        lblConfLeaveUnits2.Text = lblLeaveUnits2.Text

        trApplication.Visible = False
        trConfirmation.Visible = True
        trConfirmRow.Visible = True
        trApprovalDetails.Visible = False
        LoadCalendar()

    End Sub

    Sub LoadCalendar()
        LoadClashingDates(lblCalendarEvents, lstApplicationFor.SelectedValue, txtStartDate.Text, txtEndDate.Text)
    End Sub

    Function GetDocument(ByVal strTitle As String, ByVal strPath As String) As String
        Dim strReturn As String
        If Len(strTitle & "") = 0 Then strTitle = "untitled document"
        strReturn = "<a href='" & strPath & "' target='_blank'>" & strTitle & "</a>"
        Return strReturn
    End Function

    Function GetStatus(ByVal strStatus As String) As String
        Select Case strStatus
            Case "Pending"
                Return "<b><i>Pending</i></b>"
            Case "Rejected"
                Return "<font color='red'><b>Rejected</b></font>"
            Case "Approved"
                Return "<font color='green'><b>Approved</b></font>"
            Case "Cancelled"
                Return "<font color='orange'>Cancelled</font>"
            Case "On Hold"
                Return "<b>On Hold</b>"
            Case "Revise Suggested"
                Return "<font color='orange'><b>Revise Suggested</b></font>"
            Case Else
                Return strStatus
        End Select
    End Function

    Protected Sub cmdCancelCancellation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancelCancellation.Click
        Response.Redirect("leaverequest.aspx?lrq=" & Request.QueryString("lrq"))
    End Sub

    Protected Sub cmdCancelNow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancelNow.Click
        Dim strMailMessage As String
        Dim strLRQ As String = Request.QueryString("lrq")
        Dim strMailTitle As String
        Dim strEmailAdd As String = Session("usrEmailAdd")
        Dim strURL As String = GetConfig("SystemURL")
        Dim strReason As String = txtCancelReason.Text


        ExecuteStoredProc("stpLeaveRequest_Cancel", , "lrqId=" & strLRQ & "*lrqCancelReason=" & strReason)
        lblExtraMessage.Text = "You have cancelled this leave request"
        lblExtraMessage.BackColor = Drawing.Color.Yellow
        LoadApplication()

        strMailMessage = GetMailTemplate("ULCN", Session("comId"), "[NAME]|[URL]|[REASON]", Session("usrFullName") & "|" & strURL & "|" & strReason)
        strMailTitle = "Leave Request Cancelled" 'Mid(strMailMessage, 1, InStr(strMailMessage, "|}*") - 1)
        ''strMailMessage = Mid(strMailMessage, InStr(strMailMessage, "|}*") + 3)

        'Also need to notify someone else?

        If Len(strEmailAdd) > 2 Then SendMail(strMailTitle, strMailMessage, strEmailAdd)
        trCancelReason.Visible = False
    End Sub
End Class
