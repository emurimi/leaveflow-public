<%@ Page Language="VB" AutoEventWireup="true" Inherits="lvMan._Default" Codebehind="default.aspx.vb" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head id="Head1" runat="server">
    <title>LeaveFlow | automated time-off management</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
</head>
<body>
<form id="Form2" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
<br />
<table width="100%" cellpadding="0" cellspacing="0" align="center">
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td height="100" style="padding-left:40px;padding-right:20px;">
                        &nbsp;</td>
                  </tr>
   
                </table></td>
              </tr>
              <tr>
                <td>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td height="300">
                        <table width="40%"  border="0" cellspacing="20" cellpadding="0" align="center" 
                            bgcolor="#FFFFFF" style="border: 4px solid #eeeeee">
                          <tr>
                            <td>
                        <table cellpadding="0" cellspacing="0" class="style1">
                            <tr>
                                <td width="50%">
                                 <asp:Label ID="lblCompany" runat="server" CssClass="darktitle"></asp:Label>
                                 </td>
                                <td width="50%">
                                    <div align="right">
                                        <img src="/imagebank/leaveman.jpg" alt="LeaveFlow" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                  </tr>
                          <tr>
                            <td>
                            <asp:Label ID="lblInvalidLicence" Visible="False" ForeColor="Red" runat="server" CssClass="darktitle" Text="This copy of LeaveFlow has an invalid license. Please contact your supplier for help."></asp:Label>
                            <table width="89%"  border="0" cellspacing="0" cellpadding="5" align="center" id="tblValidBox" runat="server">
                              <tr>
                                <td valign="top">
                                    <asp:Label ID="lblTitle" runat="server" CssClass="darktitle" 
                                        Text="Sign in to LeaveFlow"></asp:Label>
                                  </td>
                              </tr>
                              <tr>
                                <td valign="top">
                                  <table width="100%"  border="0" cellspacing="0" cellpadding="3" align="center">
                                    <tr>
                                      <td colspan="2" valign="top">
                                          <asp:Label ID="lblMessage" runat="server" Font-Bold="True" ForeColor="#FF3300"></asp:Label>
                                        </td>
                                      </tr>
                                    <tr>
                                      <td width="40%" valign="top"><div align="left"><asp:Label ID="lblUserName" runat="server"></asp:Label></div>
                                        <asp:Label ID="lblLoginType" runat="server" Visible="False"></asp:Label>
                                      </td>
                                      <td>
                                          <asp:TextBox ID="txtUserName" runat="server" CssClass="textbox"></asp:TextBox>
                                          <asp:RequiredFieldValidator ControlToValidate="txtUserName" ID="RequiredFieldValidator1" runat="server" 
                                          ErrorMessage="<img src='/imagebank/validation.gif'alt='please specify' />">
                                          </asp:RequiredFieldValidator>
                                          
                                        </td>
                                    </tr>
                                    <tr id="trPassword" runat="server">
                                      <td valign="top"><div align="left">Password:</div></td>
                                      <td>
                                          <asp:TextBox ID="txtPassword" runat="server" CssClass="textbox" 
                                              TextMode="Password"></asp:TextBox>
                                          <asp:RequiredFieldValidator ControlToValidate="txtPassword" ID="RequiredFieldValidator2" runat="server" 
                                          ErrorMessage="<img src='/imagebank/validation.gif'alt='please specify' />">
                                          </asp:RequiredFieldValidator>                          
                                        </td>
                                    </tr>
                                    <tr>
                                      <td valign="top">&nbsp;</td>
                                      <td width="40%">
                                          <asp:ImageButton ID="cmdLogin" runat="server" 
                                              ImageUrl="~/imagebank/btn_sign-in.png" />
                                      <asp:Button ID="cmdSendPassword" Text="send password" CssClass="button2" 
                                              runat="server" Visible="False" />
                                          <asp:Button ID="cmdCancelForgotPassword" Text="<< cancel" CssClass="button2" 
                                              runat="server" Visible="False" CausesValidation="False" />
                                        </td>
                                    </tr>
                                    <tr>
                                      <td valign="top">&nbsp;</td>
                                      <td width="90%">
                                          &nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td valign="top">&nbsp;</td>
                                      <td width="90%">
                                          <asp:LinkButton ID="cmdForgotPassword" runat="server" CausesValidation="False" 
                                              Font-Bold="True" CssClass="blacktext">Forgot Password</asp:LinkButton>
                                        </td>
                                    </tr>
                                  </table>
                                  </td>
                              </tr>
                            </table>        
                    </td>
                  </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td height="40" style="padding-left:40px;">
                    <br />
                    <br />
                    <br />
                  </td>
              </tr>
              <tr>
                <td height="40" style="padding-left:40px;">&nbsp;</td>
              </tr>
              <tr>
                <td height="40" align="center" style="border-top: 1px dashed #eeeeee">
                    &copy; <%=Year(Now())%> <a href="http://www.leaveflow.com" class="blacktext" target="_blank">LeaveFlow.com</a>.&nbsp;&nbsp;&nbsp; All Rights Reserved &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a class="blacktext" href="mailto:automate@leaveflow.com">automate@leaveflow.com</a> 
                </td>
              </tr>
            </table>
        </td> 
    </tr> 
</table> 
</form>

</body>
</html>


