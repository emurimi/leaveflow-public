﻿Imports lvManLib.Menus

Partial Class InnerMasterPage
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            ''If DoBuildMenu() Then BuildMenu()
        End If
    End Sub

    Function DoBuildMenu() As Boolean
        If InStr(LCase(Request.ServerVariables("SCRIPT_NAME")), "/approvals") > 0 Then Return False
        If InStr(LCase(Request.ServerVariables("SCRIPT_NAME")), "changepassword") > 0 Then Return False
        Return True
    End Function

    Sub BuildMenu()
        Dim strMenu As String
        Dim strMenuName As String
        Dim astrMenu As String()
        Dim intCount As Integer
        Dim astrMenuText As String()
        Dim intCurrentMenuId As Integer
        Dim strCSS As String

        strMenuName = GetPageNameFromId(GetCurrentParentId)
        ''Page.Title = "LeaveFlow | " & LCase(strMenuName) '& " / " & strMenuName

        If Len(strMenuName) = 0 Then Page.Title = "LeaveFlow"
        intCurrentMenuId = GetCurrentPageId()

        strMenu = GetCurrentMenuString()

        If Len(strMenu) = 0 Then Exit Sub

        astrMenu = Split(strMenu, ";")

        litMenus.Text = "<table width='96%'  border='0' cellspacing='0' cellpadding='0'>" & Chr(13)
        For intCount = 0 To UBound(astrMenu)
            astrMenuText = Split(astrMenu(intCount), ",")
            strCSS = "sidebox"
            If intCurrentMenuId = CType(astrMenuText(0), Integer) Then strCSS = "sidebox2"
            litMenus.Text &= "<tr>" & Chr(13)
            litMenus.Text &= "<td "
            litMenus.Text &= " class='" & strCSS & "'>" & Chr(13)
            litMenus.Text &= "<a href='/doredirect.aspx?mnu=" & astrMenuText(0) & "' class='blacktext2'>" & Chr(13)
            litMenus.Text &= astrMenuText(1)
            litMenus.Text &= "</a>"
            litMenus.Text &= "</td>" & Chr(13)
            litMenus.Text &= "</tr>" & Chr(13)
        Next
        litMenus.Text &= "</table>" & Chr(13)
    End Sub

End Class

