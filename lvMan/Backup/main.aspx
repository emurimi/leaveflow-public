﻿<%@ Page Title="leave man" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" Inherits="lvMan.main" Codebehind="main.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainHead" Runat="Server">


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" Runat="Server">
    
    <table cellpadding="5" cellspacing="0" width="100%">
        <tr id="trMyStatus" runat="server">
            <td colspan="2" style="font-size:larger;">
                <asp:Label ID="lblDaysAvailable" runat="server"></asp:Label>
                <asp:Label ID="lblNextLeaveDate" runat="server"></asp:Label>
                <asp:Label ID="lblPendingLeaveApplications" runat="server"></asp:Label>
                <asp:Label ID="lblUpcomingApprovedLeave" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="border-bottom: 1px dashed #cccccc">
            &nbsp;<asp:Label ID="lblErrorMessage" runat="server" CssClass="bigtext" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td width="50%" valign="top">
                <table cellpadding="5" cellspacing="0" width="100%" style="border: 1px solid #cccccc">
                    <tr>
                        <td class="titlebar">
                            Items requiring your attention</td>
                    </tr>
                    <tr>
                        <td class="contentbox">
                            <asp:Label ID="lblMainAlerts" runat="server"></asp:Label>
                            <asp:GridView ID="grvAlerts" Width="100%" ShowHeader="False" runat="server" 
                                AutoGenerateColumns="False" GridLines="None"
                                DataKeyNames="aleId" DataSourceID="SqlAlerts">
                                <Columns>
                                    <asp:BoundField DataField="aleId" HeaderText="aleId" InsertVisible="False" ReadOnly="True" SortExpression="aleId" Visible="False" />
                                    <asp:TemplateField HeaderText="aleTitle" SortExpression="aleTitle">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAleTitle" runat="server" Text='<%# ShowAlert(DataBinder.Eval(Container.DataItem, "aleTitle"), DataBinder.Eval(Container.DataItem, "aleDate"), DataBinder.Eval(Container.DataItem, "aleRead")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField SelectText="View" ShowSelectButton="True" />
                                </Columns>
                                
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlAlerts" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                SelectCommand="SELECT [aleId], [aleTitle], [aleDate], [aleRead] FROM [tblAlerts] WHERE (([aleRecordArchived] = @aleRecordArchived) AND ([aleDate] >= @aleDate) AND aleusrId = @usrId) ORDER BY [aleDate] DESC">
                                <SelectParameters>
                                    <asp:Parameter DefaultValue="False" Name="aleRecordArchived" Type="Boolean" />
                                    <asp:SessionParameter SessionField="usrId" Name="usrId" Type="Int32" />
                                    <asp:ControlParameter ControlID="lblDate" Name="aleDate" PropertyName="Text" Type="DateTime" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                            <asp:Label ID="lblAlertText" runat="server"></asp:Label>
                            <asp:LinkButton ID="cmdBackToAlerts" Font-Bold="True" runat="server">&lt;&lt; Back to alerts</asp:LinkButton>
                            <asp:Label ID="lblDate" runat="server" Visible="False"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="50%" valign="top">
                <table cellpadding="5" cellspacing="0" width="100%" style="border: 1px solid #cccccc">
                    <tr>
                        <td class="titlebar">
                            Latest Updates</td>
                    </tr>
                    <tr>
                        <td class="contentbox">
                            <asp:Label ID="lblNewsFeed" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    
</asp:Content>

