﻿Imports lvManLib.DataFunctions
Imports lvManLib.Leave
Imports System.Data.SqlClient

Partial Class myprofile
    Inherits System.Web.UI.Page


    Function GetStatus(ByVal strStatus As String) As String
        Select Case strStatus
            Case "E"
                Return "Actively Employed"
            Case Else
                Return "Retired/ Resigned"
        End Select
    End Function

    Function GetGender(ByVal strGender As String) As String
        Select Case strGender
            Case "M"
                Return "Male"
            Case Else
                Return "Female"
        End Select
    End Function

    Function FinalApprover(ByVal bolFinal As Boolean) As String
        If bolFinal Then
            Return "Yes"
        Else
            Return "No"
        End If
    End Function

    Sub LoadApprovers()
        Dim strcomId As String = ""
        Dim strdptId As String = ""
        Dim strsecId As String = ""
        Dim strusrId As String = ""
        Dim strSql As String
        Dim strbraId As String = ""
        Dim strposId As String = ""
        Dim objDR As SqlDataReader

        strusrId = Session("usrId")
        objDR = GetSpDataReader("stpUsers_View", , "usrId=" & strusrId)

        If objDR.Read Then
            strcomId = objDR("comId") & ""
            strdptId = objDR("dptId") & ""
            strsecId = objDR("secId") & ""
            strbraId = objDR("braId") & ""
            strposId = objDR("posId") & ""
        End If

        objDR.Close()
        objDR = Nothing

        If Len(strsecId & "") = 0 Then strsecId = "0"

        strSql = GetApproversQuery(strcomId, strbraId, strdptId, strsecId, strposId, strusrId, "AA")

        SqlList.SelectCommand = strSql
        SqlList.DataBind()
        grvList.DataBind()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            LoadApprovers()
        End If
    End Sub

    Function GetUser(ByVal intUserId As Integer) As String
        Dim strTemp As String

        strTemp = GetSpDataSingleValue("stpGetUserNameFromID", , "usrId=" & intUserId)

        If intUserId = CType(Session("usrId") & "", Integer) Then strTemp &= " (not applicable)"
        Return strTemp
    End Function
End Class
