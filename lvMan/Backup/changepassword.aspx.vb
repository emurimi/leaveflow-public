﻿Imports lvManLib.DataFunctions
Imports lvManLib.Security
Imports lvManLib.General

Partial Class changepassword
    Inherits System.Web.UI.Page


    Protected Sub cmdChangePassword_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdChangePassword.Click
        If Page.IsValid Then
            Dim strCurrentPassword As String
            Dim strPassword As String

            strCurrentPassword = GetSpDataSingleValue("stpGetUserPassword", , "usrName=" & Session("usrName"))

            strPassword = txtPassword.Text
            strPassword = EncryptPassword(strPassword, LCase(Session("usrName") & ""))

            If strCurrentPassword <> strPassword And strCurrentPassword <> txtPassword.Text Then
                lblMessage.Text = "The specified password does not match your current password."
                Exit Sub
            End If

            If txtPassword.Text = txtNewPassword.Text Then
                lblMessage.Text = "Please specify a new password that is different from your current password."
                Exit Sub
            End If

            strPassword = txtNewPassword.Text
            strPassword = EncryptPassword(strPassword, LCase(Session("usrName") & ""))

            ExecuteStoredProc("stpUser_ChangePassword_Self", , "usrPassword=" & strPassword & "*usrId=" & Session("usrId"))

            txtConfPassword.Text = ""
            txtPassword.Text = ""
            txtNewPassword.Text = ""

            lblMessage.Text = "<script>alert('Your password has been changed successfully');window.location.href='main.aspx';</script>"
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Select Case Request.QueryString("erm") & ""
                Case "chng"
                    lblMessage.Text = "Please change your password before proceeding"
            End Select
        End If
    End Sub
End Class
