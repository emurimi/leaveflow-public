﻿Imports lvManLib.Config
Imports lvManLib.DataFunctions
Imports lvManLib.Leave
Imports lvManLib.General
Imports lvManLib.Logic
Imports System.Data.SqlClient

Partial Class approvals_default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            lstStatus.DataBind()
        End If
    End Sub

    Protected Sub grvList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvList.SelectedIndexChanged
        lbllrqId.Text = grvList.SelectedValue
        grvApprovals.DataBind()
        trLeaveDetails.Visible = True
        trList.Visible = False
        LoadApplication()
    End Sub

    Sub HideDetails()
        trLeaveDetails.Visible = False
        trList.Visible = True
        grvList.SelectedIndex = -1
    End Sub

    Protected Sub cmdBacktoList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBacktoList.Click
        HideDetails()
        lblMessage.Text = ""
    End Sub

    Sub LoadApplication()
        Dim objDR As SqlDataReader
        Dim datStartDate As Date
        Dim datEndDate As Date
        Dim parameters As New List(Of SqlParameter)()
        Dim lrqId As New SqlParameter("@lrqId", lbllrqId.Text)
        parameters.Add(lrqId)

        objDR = GetSpDataReader("stpLeaveRequest_View", parameters.ToArray())
        lblHRUser.Text = "0"

        If objDR.Read() Then
            datStartDate = objDR("lrqStartDate")
            datEndDate = objDR("lrqEndDate")
            lblTransactionID2.Text = objDR("lrqTransactionID") & ""
            lblApplicant.Text = objDR("usrFullName") & ""
            lblConfComments.Text = objDR("lrqComments") & ""
            lblConfContacts.Text = objDR("lrqContacts") & ""
            lblConfEndDate.Text = Year(datEndDate) & "-" & Month(datEndDate) & "-" & Day(datEndDate) 'FormatDateTime(datEndDate, DateFormat.ShortDate)
            lblConfLeaveType.Text = objDR("ltyName") & ""
            lblConfLeaveTypeDeduct.Text = objDR("ltyCount") & ""
            lblConfReliever.Text = objDR("Reliever") & ""
            lblConfStartDate.Text = Year(datStartDate) & "-" & Month(datStartDate) & "-" & Day(datStartDate) 'FormatDateTime(datStartDate, DateFormat.ShortDate)
            lblConfStartTime.Text = objDR("lrqStartTime") & ""
            lblConfEndTime.Text = objDR("lrqEndTime") & ""
            lblConfStatus.Text = objDR("staName") & ""
            lblusrId.Text = objDR("lrqusrId") & ""
            lblltyId.Text = objDR("lrqltyId")
            lblcomId.Text = objDR("comId")
            lblConfDaysAvailable.Text = CalculateAvailableDays(lblusrId.Text, lblltyId.Text)
            lblusrEmailAdd.Text = objDR("usrEmailAdd") & ""
        End If
        objDR.Close()

        parameters.Clear()
        Dim lrqId2 As New SqlParameter("@lrqId", lbllrqId.Text)
        parameters.Add(lrqId2)
        Dim appApproverUsrId As New SqlParameter("@appApproverUsrId", Session("usrId"))
        parameters.Add(appApproverUsrId)
        Dim Category As New SqlParameter("@Category", "LR")
        parameters.Add(Category)

        objDR = GetSpDataReader("stpGetApproverComments", parameters.ToArray())

        trFinalApprover.Visible = False
        txtComments.Text = ""
        lstStatus.SelectedIndex = 0

        If objDR.Read() Then
            lstStatus.SelectedIndex = lstStatus.Items.IndexOf(lstStatus.Items.FindByValue(objDR("appstaId")))
            txtComments.Text = objDR("appComments") & ""
            If objDR("appFinalApprover") Then
                lblTransactionID.Text = GetTransactionID()
                trFinalApprover.Visible = True
            End If
        Else
            If CheckHRApprover(grvList.SelectedValue, "leave") Then
                lblTransactionID.Text = GetTransactionID()
                trFinalApprover.Visible = True
                lblHRUser.Text = "1"
                txtComments.Text = "HR Officer action"
                lblMessage.Text = "You have opened this leave request as a HR officer. Final approver permissions apply."
            Else
                lblMessage.Text = "An error occured loading this approval. Please contact HR for help"
            End If
        End If
        objDR.Close()

        objDR = Nothing

        lblConfDays.Text = CountLeaveDays(lblConfStartDate.Text, lblConfEndDate.Text, Session("comLeaveUnit"), lblusrId.Text, lblConfStartTime.Text, lblConfEndTime.Text, lblltyId.Text)

        If Session("comLeaveUnit") = "H" Then
            lblConfLeaveUnits1.Text = "hours"
            lblConfLeaveUnits2.Text = "Hours"
            lblLeaveDays.Text = GetHoursToDayNumber(lblConfDays.Text, lblcomId.Text)
            lblDaysVersion.Text = GetHoursToDays(lblConfDays.Text, lblcomId.Text)
        Else
            lblConfLeaveUnits1.Text = "days"
            lblConfLeaveUnits2.Text = "Days"
            lblLeaveDays.Text = lblConfDays.Text
        End If

        LoadClashingDates(lblCalendarEvents, lblusrId.Text, datStartDate, datEndDate)
        grvList0.DataBind()
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        HideDetails()
        lblMessage.Text = ""
    End Sub

    Protected Sub cmdUpdateStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUpdateStatus.Click
        Dim strMailMessage As String
        Dim strEmailAdd As String
        Dim strMailTitle As String
        Dim strAllowance As String = ""
        Dim strURL As String = GetConfig("SystemURL")
        Dim parameters As New List(Of SqlParameter)()

        Dim lrqId As New SqlParameter("@lrqId", lbllrqId.Text)
        parameters.Add(lrqId)
        Dim Category As New SqlParameter("@Category", "LR")
        parameters.Add(Category)

        strEmailAdd = GetSpDataSingleValue("stpGetUserEmailAdd", parameters.ToArray())

        If lblHRUser.Text = "1" Then
            parameters.Clear()
            Dim Category1 As New SqlParameter("@Category", "LR")
            parameters.Add(Category1)
            Dim applrqId As New SqlParameter("@applrqId", grvList.SelectedValue)
            parameters.Add(applrqId)
            Dim appOrder As New SqlParameter("@appOrder", 10)
            parameters.Add(appOrder)
            Dim appApproverUsrId As New SqlParameter("@appApproverUsrId", Session("usrId"))
            parameters.Add(appApproverUsrId)
            Dim appFinalApprover As New SqlParameter("@appFinalApprover", True)
            parameters.Add(appFinalApprover)
            Dim appComments As New SqlParameter("@appComments", txtComments.Text)
            parameters.Add(appComments)

            ExecuteStoredProc("stpCreateApproval", parameters.ToArray())

        Else
            parameters.Clear()
            Dim Category2 As New SqlParameter("@Category", "LR")
            parameters.Add(Category2)
            Dim applrqId As New SqlParameter("@applrqId", grvList.SelectedValue)
            parameters.Add(applrqId)
            Dim appApproverUsrId As New SqlParameter("@appApproverUsrId", Session("usrId"))
            parameters.Add(appApproverUsrId)
            Dim appComments As New SqlParameter("@appComments", txtComments.Text)
            parameters.Add(appComments)
            Dim appstaId As New SqlParameter("@appstaId", lstStatus.SelectedValue)
            parameters.Add(appstaId)

            ExecuteStoredProc("stpApproval_Update", parameters.ToArray())
        End If


        Select Case lstStatus.SelectedValue
            Case 1 'Still pending
                'do nothing
            Case 2 'On Hold
                'much like pending - do nothing
            Case 3 'Cancelled
                'cant do that here!
            Case 4 'Rejected
                'alert applicant
                strMailMessage = GetMailTemplate("ULRJ", lblcomId.Text, "[NAME]|[APPROVER]|[URL]|[COMMENTS]", lblApplicant.Text & "|" & Session("usrFullName") & "|" & strURL & "|" & txtComments.Text)
                strMailTitle = "Leave Request Declined" ''Mid(strMailMessage, 1, InStr(strMailMessage, "|}*") - 1)
                ''strMailMessage = Mid(strMailMessage, InStr(strMailMessage, "|}*") + 3)

                'strMailMessage = "Your leave request has been rejected by " & Session("usrFullName") & ". Please visit the leave system for details."
                If Len(strEmailAdd) > 2 Then SendMail(strMailTitle, strMailMessage, strEmailAdd)

                'flag request as rejected
                parameters.Clear()
                Dim lcostaId As New SqlParameter("@lrqstaId", 4)
                parameters.Add(lcostaId)
                Dim lcoId As New SqlParameter("@lrqId", grvList.SelectedValue)
                parameters.Add(lcoId)

                ExecuteStoredProc("stpLeaveRequest_UpdateStatus", parameters.ToArray())

            Case 5 'Approved
                'alert next approver
                If lblHRUser.Text = "0" Then
                    AlertApprovers(grvList.SelectedValue)
                End If

            Case 6 'Revision Suggested
                'alert applicant
                strMailMessage = GetMailTemplate("ALRVS", lblcomId.Text, "[NAME]|[APPROVER]|[URL]|[COMMENTS]", lblApplicant.Text & "|" & Session("usrFullName") & "|" & strURL & "|" & txtComments.Text)
                strMailTitle = "Leave Request Revision Suggested" 'Mid(strMailMessage, 1, InStr(strMailMessage, "|}*") - 1)
                ''strMailMessage = Mid(strMailMessage, InStr(strMailMessage, "|}*") + 3)

                If Len(strEmailAdd) > 2 Then SendMail(strMailTitle, strMailMessage, strEmailAdd)

                'flag request as revise
                parameters.Clear()
                Dim lcostaId As New SqlParameter("@lrqstaId", 6)
                parameters.Add(lcostaId)
                Dim lcoId As New SqlParameter("@lrqId", grvList.SelectedValue)
                parameters.Add(lcoId)

                ExecuteStoredProc("stpLeaveRequest_UpdateStatus", parameters.ToArray())
        End Select


        lblMessage.Text = "You have set " & lblApplicant.Text & "'s leave request to: " & lstStatus.SelectedItem.Text

        If trFinalApprover.Visible And lstStatus.SelectedValue = 5 Then
            FinalApproval()
            lblMessage.Text &= ". " & lblApplicant.Text & " will now be able to go on leave on the selected date(s)."

            strMailMessage = GetMailTemplate("ULAP", lblcomId.Text, "[NAME]|[APPROVER]|[URL]|[COMMENTS]|[ALLOWANCE]", lblApplicant.Text & "|" & Session("usrFullName") & "|" & strURL & "|" & txtComments.Text & "|" & strAllowance)
            strMailTitle = "Leave Application Approved" ''Mid(strMailMessage, 1, InStr(strMailMessage, "|}*") - 1)
            ''strMailMessage = Mid(strMailMessage, InStr(strMailMessage, "|}*") + 3)
            If Len(strEmailAdd) > 2 Then SendMail(strMailTitle, strMailMessage, strEmailAdd)
        End If

        HideDetails()
        grvList.DataBind()

    End Sub

    Sub FinalApproval()
        Dim parameters As New List(Of SqlParameter)()
        Dim strSql As String
        Dim strReturn As String

        strSql = "UPDATE tblLeaveRequests SET lrqstaId = 5 WHERE lrqId = " & lbllrqId.Text
        ExecuteQuery(strSql)

        'Update main request record
        parameters.Clear()
        Dim lrcusrId As New SqlParameter("@lrcusrId", lblusrId.Text)
        parameters.Add(lrcusrId)
        Dim lrcltyId As New SqlParameter("@lrcltyId", lblltyId.Text)
        parameters.Add(lrcltyId)
        Dim lrcTransactionID As New SqlParameter("@lrcTransactionID", lblTransactionID.Text)
        parameters.Add(lrcTransactionID)
        Dim lrclrqId As New SqlParameter("@lrclrqId", lbllrqId.Text)
        parameters.Add(lrclrqId)
        Dim lrcStartDateReq As New SqlParameter("@lrcStartDateReq", lblConfStartDate.Text)
        parameters.Add(lrcStartDateReq)
        Dim lrcEndDateReq As New SqlParameter("@lrcEndDateReq", lblConfEndDate.Text)
        parameters.Add(lrcEndDateReq)
        Dim lrcStartDate As New SqlParameter("@lrcStartDate", lblConfStartDate.Text)
        parameters.Add(lrcStartDate)
        Dim lrcEndDate As New SqlParameter("@lrcEndDate", lblConfEndDate.Text)
        parameters.Add(lrcEndDate)
        Dim lrcDays As New SqlParameter("@lrcDays", lblConfDays.Text)
        parameters.Add(lrcDays)
        Dim lrcComments As New SqlParameter("@lrcComments", txtComments.Text)
        parameters.Add(lrcComments)

        strReturn = ExecuteStoredProc("stpLeaveRecord_Add", parameters.ToArray())

        Response.Write("<!--FF|" & strReturn & "-->")

        'Update Days Manifest
        'Check if days are not deductible so as to get deductible days
        If lblConfLeaveTypeDeduct.Text = "False" Then
            Dim dblDeductible As Double = CType(lblConfDays.Text, Double)
            Dim dblDaysAvailable As Double = CType(lblConfDaysAvailable.Text, Double)
            Dim dblEffectiveDaysDeductible As Double = dblDeductible - dblDaysAvailable
            If dblEffectiveDaysDeductible > 0 Then
                EffectLeaveDayChanges(lblusrId.Text, dblEffectiveDaysDeductible, "D", lblConfLeaveType.Text & " (excess of available non-deductible)", "CHK")
            Else
                EffectLeaveDayChanges(lblusrId.Text, 0, "D", lblConfLeaveType.Text & " (" & lblConfDays.Text & " available non-deductible used)", "CHK")
            End If
        Else
            EffectLeaveDayChanges(lblusrId.Text, lblConfDays.Text, "D", lblConfLeaveType.Text, "CHK")
        End If


    End Sub

    Function GetDocument(ByVal strTitle As String, ByVal strPath As String) As String
        Dim strReturn As String
        If Len(strTitle & "") = 0 Then strTitle = "untitled document"
        strReturn = "<a href='" & strPath & "' target='_blank'>" & strTitle & "</a>"
        Return strReturn
    End Function

    Function GetStatus(ByVal strStatus As String) As String
        Select Case strStatus
            Case "Pending"
                Return "<b><i>Pending</i></b>"
            Case "Rejected"
                Return "<font color='red'><b>Rejected</b></font>"
            Case "Approved"
                Return "<font color='green'><b>Approved</b></font>"
            Case "Cancelled"
                Return "<font color='orange'>Cancelled</font>"
            Case "On Hold"
                Return "<b>On Hold</b>"
            Case "Revise Suggested"
                Return "<font color='orange'><b>Revise Suggested</b></font>"
            Case Else
                Return strStatus
        End Select
    End Function
End Class
