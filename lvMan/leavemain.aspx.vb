﻿
Partial Class leavemain
    Inherits System.Web.UI.Page

    Protected Sub grvList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvList.SelectedIndexChanged
        Response.Redirect("leaverequest.aspx?lrq=" & grvList.SelectedValue)
    End Sub

    Function GetStatus(ByVal strStatus As String) As String
        Select Case strStatus
            Case "Pending"
                Return "<b><i>Pending</i></b>"
            Case "Rejected"
                Return "<font color='red'><b>Rejected</b></font>"
            Case "Approved"
                Return "<font color='green'><b>Approved</b></font>"
            Case "Cancelled"
                Return "<font color='orange'>Cancelled</font>"
            Case "On Hold"
                Return "<b>On Hold</b>"
            Case "Revise Suggested"
                Return "<font color='orange'><b>Revise Suggested</b></font>"
            Case Else
                Return strStatus
        End Select
    End Function

End Class
