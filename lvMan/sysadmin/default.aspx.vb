﻿Imports System.Data.SqlClient
Imports lvManLib.DataFunctions
Imports lvManLib.Config
Imports lvManLib.Security

Partial Class sysadmin_default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Not UserLoggedIn() Then Response.Redirect("/default.aspx?errm=tmo")
            LoadMenus()
        End If
    End Sub

    Sub LoadMenus()
        Dim strGroup As String = ""
        Dim strPrevGroup As String = ""
        Dim strSeperator As String = "<div style='padding-top:10px;padding-bottom:10px;'><hr style='border: 1px dashed #eeeeee;' /></div>"
        Dim objDR As SqlDataReader
        Dim intCount As Integer = 0
        Dim strLeavePlans As String = GetConfig("EnableLeavePlans") & ""
        Dim strOrganizationType As String = GetConfig("OrganizationType") & "" '1 = Organization with many companies, 2 = Single company
        Dim strEnableBranches As String = GetConfig("EnableBranches") & ""
        Dim strEnableSections As String = GetConfig("EnableSections") & ""
        Dim intMenuID As Integer
        Dim bolShowMenu As Boolean = True
        Dim intWidth As Integer = 820
        Dim parameters As New List(Of SqlParameter)()

        Dim mnuParent As New SqlParameter("@mnuParent", 6)
        parameters.Add(mnuParent)
        Dim usrId As New SqlParameter("@usrId", Session("usrId"))
        parameters.Add(usrId)

        objDR = GetSpDataReader("stpUser_Menus", parameters.ToArray())

        strGroup = ""
        strPrevGroup = ""
        intCount = 0
        While objDR.Read
            intCount += 1
            intMenuID = objDR("mnuId")
            bolShowMenu = False
            strGroup = objDR("mnuGroup") & ""
            If strPrevGroup <> "" And strGroup <> strPrevGroup Then litMenus.Text &= strSeperator
            If strGroup <> strPrevGroup Then litMenus.Text &= "<b>" & strGroup & "</b><br /><br />"

            Select Case intMenuID
                Case 17 'Companies
                    If strOrganizationType = "1" Then
                        bolShowMenu = True
                    Else
                        'Allow user to edit single company
                        litMenus.Text &= FormatDropDownLink("Company Setup", "/doredirect.aspx?mnu=" & intMenuID, False) & " | "
                    End If

                Case 20 'Sections
                    If strEnableSections = "Y" Then
                        bolShowMenu = True
                    End If

                Case 48 'Branches
                    If strEnableBranches = "Y" Then
                        bolShowMenu = True
                    End If

                Case Else
                    bolShowMenu = True

            End Select

            If bolShowMenu Then litMenus.Text &= FormatDropDownLink(objDR("mnuName"), "/doredirect.aspx?mnu=" & intMenuID, False) & " | "

            strPrevGroup = strGroup
        End While

        objDR.Close()
        objDR = Nothing

        If intCount = 0 Then litMenus.Text = "No functions available to you at this time"
    End Sub

    Function FormatDropDownLink(ByVal strName As String, ByVal strLink As String, Optional ByVal bolSeperator As Boolean = True) As String
        Dim strReturn As String
        Dim strSeperator As String = "<hr style='border: 1px dashed #D8E6F6' />"
        If Not bolSeperator Then strSeperator = ""

        strReturn = "<a href='" & strLink & "' class='menulinks'>" & strName & "</a>" & strSeperator & Chr(13)

        Return strReturn
    End Function

End Class
