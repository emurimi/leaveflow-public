﻿<%@ Page Title="Setup Countries" Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.sysadmin_countries" Codebehind="countries.aspx.vb" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">

            <table width="100%" cellpadding="3" cellspacing="0" border="0">
                <tr>
                    <td width="100%">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr id="trCommands" runat="server">
                                <td>
                                    <table width="290" border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <td id="tdView" runat="server" width="144" height="29" align="center" valign="middle" background='/images/orangetab.png'>
                                            <asp:LinkButton CssClass="whitetext" ID="cmdViewConfigs" runat="server">View Countries</asp:LinkButton></td>
                                        <td width="2"></td>
                                        <td id="tdAddEdit" runat="server" width="144" align="center" valign="middle" background='/images/darkbluetab.png'>
                                        <asp:LinkButton CssClass="whitetext" ID="cmdAddConfig" runat="server">Add Country</asp:LinkButton>
                                        </td>
                                      </tr>
                                    </table>                    
                                </td>
                            </tr>
                                  
                            <tr>
                                <td width="100%" valign="top">
                                    <asp:Panel ID="panList" runat="server" Width="100%">
                                        <asp:GridView ID="grvList" BorderColor="#EEEEEE" runat="server" Width="100%" AllowPaging="True"  CssClass="gridviewcontents"
                                            AllowSorting="True" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="ctyId" 
                                            DataSourceID="SqlList" RowStyle-Height="20">
                                            <RowStyle Height="20px" />
                                            <Columns>
                                                <asp:BoundField DataField="ctyId" HeaderText="ctyId" InsertVisible="False" ReadOnly="True" Visible="False" SortExpression="ctyId" />
                                                <asp:BoundField DataField="ctyName" HeaderText="Name" SortExpression="ctyName" />
                                                <asp:BoundField DataField="ctyCurrency" HeaderText="Currency" SortExpression="ctyCurrency" />                                                
                                                <asp:BoundField DataField="regName" HeaderText="Region" SortExpression="regName" />
                                                <asp:CommandField ShowSelectButton="True" SelectText="View/Edit" 
                                                    ItemStyle-Font-Bold="True" >
                                                    <ItemStyle Font-Bold="True" />
                                                </asp:CommandField>
                                            </Columns>
                                             <RowStyle CssClass="greytable" />
                                            <AlternatingRowStyle CssClass="whitetable" />
                                            <PagerStyle CssClass="greytable" />
                                            <HeaderStyle HorizontalAlign="Left" ForeColor="#FFFFFF" CssClass="tablecolors_title" />                            

                                        </asp:GridView>    
                                        <asp:SqlDataSource ID="SqlList" runat="server" 
                                            ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                            SelectCommandType="StoredProcedure"
                                            DeleteCommandType="StoredProcedure"
                                            InsertCommandType="StoredProcedure"
                                            UpdateCommandType="StoredProcedure"                                            
                                            SelectCommand="stpCountries_View" 
                                            DeleteCommand="stpCountries_Delete" 
                                            InsertCommand="stpCountries_Add" 
                                            UpdateCommand="stpCountries_Update">
                                            
                                            <DeleteParameters>
                                                <asp:Parameter Name="ctyId" Type="Int32" />
                                            </DeleteParameters>
                                            <UpdateParameters>
                                                <asp:ControlParameter ControlID="lstRegions" Name="ctyregId" PropertyName="SelectedValue" Type="Int32" />
                                                <asp:ControlParameter ControlID="txtName" Name="ctyName" PropertyName="Text" Type="String" />
                                                <asp:ControlParameter ControlID="txtDescription" Name="ctyDescription" PropertyName="Text" Type="String" />
                                                <asp:ControlParameter ControlID="grvList" Name="ctyId" PropertyName="SelectedValue" Type="Int32" />
                                                <asp:ControlParameter ControlID="txtCurrency" Name="ctyCurrency" PropertyName="Text" Type="String" />
                                            </UpdateParameters>
                                            <InsertParameters>
                                                <asp:ControlParameter ControlID="lstRegions" Name="ctyregId" PropertyName="SelectedValue" Type="Int32" />
                                                <asp:ControlParameter ControlID="txtName" Name="ctyName" PropertyName="Text" Type="String" />
                                                <asp:ControlParameter ControlID="txtCurrency" Name="ctyCurrency" PropertyName="Text" Type="String" />
                                                <asp:ControlParameter ControlID="txtDescription" Name="ctyDescription" PropertyName="Text" Type="String" />                                            
                                            </InsertParameters>
                                        </asp:SqlDataSource>
                                    </asp:Panel>

                                        
                                   
                                    <asp:Panel ID="panAddEdit" runat="server" Width="100%" Visible="False">
                                      <table width="400" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          
                                          
                                          
                                        </tr>
                                        <tr>
                                          
                                          <td valign="top">                                    
                                                <table cellpadding="3" id="tblAddEdit" runat="server" cellspacing="0" width="100%" class="tablewithborders">
                                                    <tr>
                                                        <td width="50%">
                                                            Name:</td>
                                                        <td width="50%">
                                                            <asp:TextBox ID="txtName" runat="server" CssClass="textbox"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="rvaName" runat="server" ControlToValidate="txtName" 
                                                                Display="Dynamic" ValidationGroup="AddItem" 
                                                                ErrorMessage="* please enter name"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="50%">
                                                            Currency:</td>
                                                        <td width="50%">
                                                            <asp:TextBox ID="txtCurrency" runat="server" CssClass="textbox"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCurrency" 
                                                                Display="Dynamic" ValidationGroup="AddItem" 
                                                                ErrorMessage="* please enter currency short name"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>                                                    
                                                    <tr>
                                                        <td>
                                                            Region:</td>
                                                        <td>
                                                            <asp:DropDownList ID="lstRegions" runat="server" DataSourceID="SqlRegions" 
                                                                DataTextField="regName" DataValueField="regId" CssClass="listmenu">
                                                            </asp:DropDownList>
                                                            <asp:SqlDataSource ID="SqlRegions" runat="server" 
                                                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                                SelectCommandType="StoredProcedure"
                                                                SelectCommand="stpRegions_View">
                                                            </asp:SqlDataSource>
                                                                
                                                        </td>
                                                    </tr>
                                                    <tr id="trHide" runat="server" visible="False">
                                                        <td>
                                                            Description:</td>
                                                        <td>
                                                            <asp:TextBox ID="txtDescription" runat="server" Rows="4" CssClass="textbox" 
                                                                Columns="30" TextMode="MultiLine"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="cmdCancel" runat="server" CausesValidation="False" Text="Cancel" CssClass="button"></asp:Button>
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="cmdUpdate" runat="server" ValidationGroup="AddItem" 
                                                                CssClass="button" Text="Add Country"></asp:Button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton ID="cmdDelete" OnClientClick="return confirm('Are you sure you want to delete this record?');" Font-Bold="True" runat="server">Delete Country</asp:LinkButton>
                                                        </td>
                                                        <td>
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>
                                              </td>
                                              
                                            </tr>
                                            <tr>
                                              
                                              
                                              
                                            </tr>
                                          </table>                                                   
                                    </asp:Panel>    
                                </td>
                            </tr>
                        </table>                    
                    </td>
                </tr>

            </table>
</asp:Content>

