﻿<%@ Page Title="Setup Assets" Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.sysadmin_assets" Codebehind="assets.aspx.vb" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">

            <table width="100%" cellpadding="3" cellspacing="0" border="0">
                <tr>
                    <td>
                        <asp:DropDownList ID="lstSelectCompanies" runat="server" AutoPostBack="True" 
                            CssClass="listmenu" DataSourceID="SqlCompanies" DataTextField="comName" 
                            DataValueField="comId">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="SqlCompanies" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>"
                            SelectCommandType="StoredProcedure" 
                            SelectCommand="stpCompanyDetails">
                        </asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td width="100%">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr id="trCommands" runat="server">
                                <td>
                                    <table width="290" border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <td id="tdView" runat="server" width="144" height="29" align="center" valign="middle" background='/images/orangetab.png'>
                                            <asp:LinkButton CssClass="whitetext" ID="cmdViewConfigs" runat="server">View Assets</asp:LinkButton></td>
                                        <td width="2"></td>
                                        <td id="tdAddEdit" runat="server" width="144" align="center" valign="middle" background='/images/darkbluetab.png'>
                                            <asp:LinkButton CssClass="whitetext" ID="cmdAddConfig" runat="server">Add Asset</asp:LinkButton>
                                        </td>
                                      </tr>
                                    </table>                    
                                </td>
                            </tr>
                                  
                            <tr>
                                <td width="100%" valign="top">
                                    <asp:Panel ID="panList" runat="server" Width="100%">
                                        <asp:GridView ID="grvList" BorderColor="#EEEEEE" runat="server" Width="100%" 
                                            AllowPaging="True"  CssClass="gridviewcontents"
                                            AllowSorting="True" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="assId" 
                                            DataSourceID="SqlList" RowStyle-Height="20">
                                            <RowStyle Height="20px" />
                                            <Columns>
                                                <asp:BoundField DataField="assId" HeaderText="assId" Visible="False" InsertVisible="False" ReadOnly="True" SortExpression="assId" />
                                                <asp:BoundField DataField="comName" HeaderText="Company" SortExpression="comName" />
                                                <asp:BoundField DataField="astName" HeaderText="Type" SortExpression="astName" />
                                                <asp:BoundField DataField="assName" HeaderText="Name" SortExpression="Name" />
                                                <asp:BoundField DataField="assDescription" HeaderText="Description" SortExpression="Description" />
                                                 <asp:CommandField SelectText="View/Edit" ItemStyle-Font-Bold="True" ShowSelectButton="True" >
                                                </asp:CommandField>
                                            </Columns>
                                             <RowStyle CssClass="greytable" />
                                            <AlternatingRowStyle CssClass="whitetable" />
                                            <PagerStyle CssClass="greytable" />
                                            <HeaderStyle HorizontalAlign="Left" ForeColor="#FFFFFF" CssClass="tablecolors_title" />                            

                                        </asp:GridView>    
                                        <asp:SqlDataSource ID="SqlList" runat="server" 
                                            ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                            SelectCommandType="StoredProcedure"
                                            DeleteCommandType="StoredProcedure"
                                            InsertCommandType="StoredProcedure"
                                            UpdateCommandType="StoredProcedure"
                                            SelectCommand="stpAssets_View" 
                                            DeleteCommand="stpAssets_Delete" 
                                            InsertCommand="stpAssets_Add" 
                                            UpdateCommand="stpAssets_Update">
                                            
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="lstSelectCompanies" Name="comId" PropertyName="SelectedValue" Type="Int32" />
                                            </SelectParameters>
                                            <DeleteParameters>
                                                <asp:Parameter Name="assId" Type="Int32" />
                                            </DeleteParameters>
                                            <UpdateParameters>
                                                <asp:ControlParameter ControlID="lstAssetTypes" PropertyName="SelectedValue" Name="assastId" Type="Int32" />
                                                <asp:ControlParameter ControlID="lstCompanies" PropertyName="SelectedValue" Name="asscomId" Type="Int32" />
                                                <asp:ControlParameter ControlID="txtName" PropertyName="Text" Name="assName" Type="String" />
                                                <asp:ControlParameter ControlID="txtDescription" PropertyName="Text" Name="assDescription" Type="String" />
                                                <asp:ControlParameter ControlID="grvList" PropertyName="SelectedValue" Name="assId" Type="Int32" />
                                            </UpdateParameters>
                                            <InsertParameters>
                                                <asp:ControlParameter ControlID="lstAssetTypes" PropertyName="SelectedValue" Name="assastId" Type="Int32" />
                                                <asp:ControlParameter ControlID="lstCompanies" PropertyName="SelectedValue" Name="asscomId" Type="Int32" />
                                                <asp:ControlParameter ControlID="txtName" PropertyName="Text" Name="assName" Type="String" />
                                                <asp:ControlParameter ControlID="txtDescription" PropertyName="Text" Name="assDescription" Type="String" />
                                            </InsertParameters>
                                        </asp:SqlDataSource>
                                    </asp:Panel>

                                        
                                   
                                    <asp:Panel ID="panAddEdit" runat="server" Width="100%" Visible="False">
                                      <table width="400" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          
                                          
                                          
                                        </tr>
                                        <tr>
                                          
                                          <td valign="top">                                    
                                                <table cellpadding="3" id="tblAddEdit" runat="server" cellspacing="0" width="100%" class="tablewithborders">
                                                    <tr>
                                                        <td width="50%">
                                                            Company:</td>
                                                        <td width="50%">
                                                            <asp:DropDownList ID="lstCompanies" AutoPostBack="True" runat="server" CssClass="listmenu" 
                                                                DataSourceID="SqlCompanies" DataTextField="comName" DataValueField="comId">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="50%">
                                                            Asset Type:</td>
                                                        <td width="50%">
                                                            <asp:DropDownList ID="lstAssetTypes" runat="server" CssClass="listmenu" 
                                                                DataSourceID="SqlAssetTypes" DataTextField="astName" DataValueField="astId">
                                                            </asp:DropDownList>
                                                            <asp:SqlDataSource ID="SqlAssetTypes" runat="server" 
                                                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                                SelectCommandType="StoredProcedure"
                                                                SelectCommand="stpAssetTypes_View">
                                                            </asp:SqlDataSource>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="50%">
                                                            Name:</td>
                                                        <td width="50%">
                                                            <asp:TextBox ID="txtName" runat="server" CssClass="textbox"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="rvaName" runat="server" 
                                                                ControlToValidate="txtName" Display="Dynamic" 
                                                                ErrorMessage="* please enter name" ValidationGroup="AddItem"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Description:</td>
                                                        <td>
                                                            <asp:TextBox ID="txtDescription" runat="server" Rows="4" CssClass="textbox" 
                                                                Columns="30" TextMode="MultiLine"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr id="trAssignedTo1" runat="server" visible="False">
                                                        <td>
                                                            <b>ASSIGNED TO:</b></td>
                                                        <td>
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr id="trAssignedTo2" runat="server" visible="False">
                                                        <td style="text-align: center">
                                                            Available Staff:</td>
                                                        <td style="text-align: center">
                                                            Assigned Staff:</td>
                                                    </tr>
                                                    <tr id="trAssignedTo3" runat="server" visible="False">
                                                        <td style="text-align: center">
                                                            <asp:LinkButton ID="cmdAddSelected" runat="server" Font-Bold="True">Add Selected &gt;&gt;</asp:LinkButton>
                                                        </td>
                                                        <td style="text-align: center">
                                                            <asp:LinkButton ID="cmdRemoveSelected" runat="server" Font-Bold="True">&lt;&lt; Del Selected</asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                    <tr id="trAssignedTo4" runat="server" visible="False">
                                                        <td style="text-align: center">
                                                            <asp:ListBox SelectionMode="Multiple" ID="lstAvailableStaff" runat="server" CssClass="listmenu" 
                                                                DataSourceID="SqlAllStaff" DataTextField="usrStaffName" DataValueField="usrId" 
                                                                Rows="10"></asp:ListBox>
                                                            <asp:SqlDataSource ID="SqlAllStaff" runat="server" 
                                                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                                SelectCommand="SELECT [usrId], [usrFullName] + ' (' + [usrStaffID] + ')' As usrStaffName FROM [tblUsers] WHERE [usrdptId] IN (SELECT dptId FROM tblDepartments WHERE dptcomId = @comId) AND usrId NOT IN (SELECT uasusrId FROM tblUserAssets WHERE uasassId = @assId AND uasRecordArchived = 0) AND usrStatus = 'E' ORDER BY [usrFullName]">
                                                                <SelectParameters>
                                                                    <asp:ControlParameter ControlID="lstCompanies" Name="comId"  PropertyName="SelectedValue" Type="Int32" />
                                                                    <asp:ControlParameter ControlID="grvList" Name="assId" PropertyName="SelectedValue" Type="Int32" />
                                                                </SelectParameters>
                                                            </asp:SqlDataSource>
                                                        </td>
                                                        <td style="text-align: center">
                                                            <asp:ListBox SelectionMode="Multiple" ID="lstAssignedStaff" runat="server" CssClass="listmenu" 
                                                                Rows="10" DataSourceID="SqlAssignedStaff" DataTextField="usrStaffName" 
                                                                DataValueField="uasId"></asp:ListBox>
                                                            <asp:SqlDataSource ID="SqlAssignedStaff" runat="server" 
                                                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                                SelectCommand="SELECT [uasId], (SELECT [usrFullName] + ' (' + [usrStaffID] + ')' As usrStaffName FROM tblUsers WHERE usrId = [uasusrId]) As usrStaffName FROM [tblUserAssets] WHERE (([uasassId] = @uasassId) AND ([uasRecordArchived] = @uasRecordArchived))">
                                                                <SelectParameters>
                                                                    <asp:ControlParameter ControlID="grvList" Name="uasassId" PropertyName="SelectedValue" Type="Int32" />
                                                                    <asp:Parameter DefaultValue="False" Name="uasRecordArchived" Type="Boolean" />
                                                                </SelectParameters>
                                                            </asp:SqlDataSource>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="cmdCancel" runat="server" CausesValidation="False" 
                                                                CssClass="button" Text="Cancel" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="cmdUpdate" runat="server" CssClass="button" Text="Add Asset" 
                                                                ValidationGroup="AddItem" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton ID="cmdDelete" OnClientClick="return confirm('Are you sure you want to delete this record?');" Font-Bold="True" runat="server">Delete Asset</asp:LinkButton>
                                                        </td>
                                                        <td>
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>
                                              </td>
                                              
                                            </tr>
                                            <tr>
                                              
                                              
                                              
                                            </tr>
                                          </table>                                                   
                                    </asp:Panel>    
                                </td>
                            </tr>
                        </table>                    
                    </td>
                </tr>

            </table>
</asp:Content>

