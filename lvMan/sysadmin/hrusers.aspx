﻿<%@ Page Title="HR Users" Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.sysadmin_hrusers" Codebehind="hrusers.aspx.vb" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">
            <table width="100%" cellpadding="3" cellspacing="0" border="0">
                <tr>
                    <td width="100%">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td style="padding-bottom: 4px;">
                                
                                    <asp:DropDownList AutoPostBack="True" ID="lstSelectCompanies" runat="server" CssClass="listmenu" 
                                        DataSourceID="SqlCompanies" DataTextField="comName" DataValueField="comId">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlCompanies" runat="server" 
                                        ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                        SelectCommandType="StoredProcedure"
                                        SelectCommand="stpCompanyDetails">
                                    </asp:SqlDataSource>
                                
                                </td>
                            </tr>
                            <tr id="trCommands" runat="server">
                                <td>
                                    <table width="290" border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <td id="tdView" runat="server" width="144" height="29" align="center" valign="middle" background='/images/orangetab.png'>
                                            <asp:LinkButton CssClass="whitetext" ID="cmdViewConfigs" runat="server">View HR Users</asp:LinkButton></td>
                                        <td width="2"></td>
                                        <td id="tdAddEdit" runat="server" width="144" align="center" valign="middle" background='/images/darkbluetab.png'>
                                        <asp:LinkButton CssClass="whitetext" ID="cmdAddConfig" runat="server">Add HR User</asp:LinkButton>
                                        </td>
                                      </tr>
                                    </table>                    
                                </td>
                            </tr>
                                
                            <tr>
                                <td width="100%" valign="top">
                                    <asp:Panel ID="panList" runat="server" Width="100%">
                                        <asp:GridView ID="grvList" BorderColor="#EEEEEE" runat="server" Width="100%" 
                                            AllowPaging="True" CellPadding="4" CssClass="gridviewcontents"
                                            AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="hruId" 
                                            DataSourceID="SqlList" RowStyle-Height="20">
                                            <RowStyle Height="20px" />
                                            <Columns>
                                                <asp:BoundField DataField="hruId" HeaderText="hruId" InsertVisible="False" Visible="False" ReadOnly="True" SortExpression="hruId" />
                                                <asp:BoundField DataField="comName" HeaderText="Company" SortExpression="comName" />
                                                <asp:BoundField DataField="braName" HeaderText="Branch" SortExpression="braName" />
                                                <asp:BoundField DataField="usrFullName" HeaderText="System User" SortExpression="usrFullName" />
                                                <asp:CommandField SelectText="View/ Edit" ShowSelectButton="True" />
                                            </Columns>
                                            <RowStyle CssClass="greytable" />
                                            <AlternatingRowStyle CssClass="whitetable" />
                                            <PagerStyle CssClass="greytable" />
                                            <HeaderStyle HorizontalAlign="Left" ForeColor="#FFFFFF" CssClass="tablecolors_title" />                            

                                        </asp:GridView>    
                                        <asp:SqlDataSource ID="SqlList" runat="server" 
                                            ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                            SelectCommandType="StoredProcedure"
                                            DeleteCommandType="StoredProcedure"
                                            InsertCommandType="StoredProcedure"
                                            UpdateCommandType="StoredProcedure"                                            
                                            SelectCommand="stpHRUsers_View" 
                                            DeleteCommand="stpHRUsers_Delete" 
                                            InsertCommand="stpHRUsers_Add" 
                                            UpdateCommand="stpHRUsers_Update">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="lstSelectCompanies" PropertyName="SelectedValue" Name="comId" Type="Int32" />
                                            </SelectParameters>
                                            <DeleteParameters>
                                                <asp:Parameter Name="hruId" Type="Int32" />
                                            </DeleteParameters>
                                            <UpdateParameters>
                                                <asp:ControlParameter ControlID="lstCompanies" PropertyName="SelectedValue" Name="hrucomId" Type="Int32" />
                                                <asp:ControlParameter DefaultValue="0" ControlID="lstBranches" PropertyName="SelectedValue" Name="hrubraId" Type="Int32" />
                                                <asp:ControlParameter ControlID="lstUsers" PropertyName="SelectedValue" Name="hruusrId" Type="Int32" />
                                                <asp:ControlParameter ControlID="grvList" PropertyName="SelectedValue" Name="hruId" Type="Int32" />
                                            </UpdateParameters>
                                            <InsertParameters>
                                                <asp:ControlParameter ControlID="lstCompanies" PropertyName="SelectedValue" Name="hrucomId" Type="Int32" />
                                                <asp:ControlParameter DefaultValue="0" ControlID="lstBranches" PropertyName="SelectedValue" Name="hrubraId" Type="Int32" />
                                                <asp:ControlParameter ControlID="lstUsers" PropertyName="SelectedValue" Name="hruusrId" Type="Int32" />
                                            </InsertParameters>
                                        </asp:SqlDataSource>
                                    </asp:Panel>

                                        
                                   
                                    <asp:Panel ID="panAddEdit" runat="server" Width="100%" Visible="False">
                                      <table width="400" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          
                                          
                                          
                                        </tr>
                                        <tr>
                                          
                                          <td valign="top">                                    
                                                <table cellpadding="3" id="tblAddEdit" runat="server" cellspacing="0" width="100%" class="tablewithborders">
                                                    <tr>
                                                        <td>
                                                            Company:</td>
                                                        <td>
                                                            <asp:DropDownList ID="lstCompanies" AutoPostBack="True" runat="server" DataSourceID="SqlCompanies" 
                                                                DataTextField="comName" DataValueField="comId" CssClass="listmenu">
                                                            </asp:DropDownList>
                                                                
                                                        </td>
                                                    </tr>
                                                    <tr id="trBranches" runat="server">
                                                        <td>
                                                            Branch:</td>
                                                        <td>
                                                            <asp:DropDownList ID="lstBranches" AutoPostBack="True" CssClass="listmenu" runat="server" DataSourceID="SqlBranches" 
                                                                DataTextField="braName" DataValueField="braId">
                                                            </asp:DropDownList>
                                                            <asp:SqlDataSource ID="SqlBranches" runat="server" 
                                                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                                SelectCommandType="StoredProcedure"
                                                                SelectCommand="stpBranches_View">
                                                                <SelectParameters>
                                                                    <asp:ControlParameter ControlID="lstCompanies" Name="bracomId" PropertyName="SelectedValue" Type="Int32" />
                                                                </SelectParameters>
                                                            </asp:SqlDataSource>
                                                                
                                                        </td>
                                                    </tr>                                                    
                                                    <tr>
                                                        <td>
                                                            System User:</td>
                                                        <td>
                                                            <asp:DropDownList ID="lstUsers" CssClass="listmenu" runat="server" DataSourceID="SqlUsers" 
                                                                DataTextField="usrFullName" DataValueField="usrId">
                                                            </asp:DropDownList>
                                                            <asp:SqlDataSource ID="SqlUsers" runat="server" 
                                                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                                SelectCommandType="StoredProcedure"
                                                                SelectCommand="stpUsers_MinView">
                                                                <SelectParameters>
                                                                    <asp:ControlParameter ControlID="lstCompanies" Name="comId" PropertyName="SelectedValue" Type="Int32" />
                                                                </SelectParameters>
                                                            </asp:SqlDataSource>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="cmdCancel" runat="server" CausesValidation="False" Text="Cancel" CssClass="button"></asp:Button>
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="cmdUpdate" runat="server" ValidationGroup="AddItem" 
                                                                CssClass="button" Text="Add HR User"></asp:Button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton ID="cmdDelete" OnClientClick="return confirm('Are you sure you want to delete this record?');" Font-Bold="True" runat="server">Delete HR User</asp:LinkButton>
                                                        </td>
                                                        <td>
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>
                                          </td>
                                          
                                        </tr>
                                        <tr>
                                          
                                          
                                          
                                        </tr>
                                      </table>                                           
                                    </asp:Panel>    
                                </td>
                            </tr>
                        </table>                    
                    </td>
                </tr>

            </table>


</asp:Content>

