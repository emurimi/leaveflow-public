﻿<%@ Page Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.SystemAdministration_GenericAdmin" title="System Administration" Codebehind="genericadmin.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">

            <table width="100%" cellpadding="3" cellspacing="0" border="0">
                <tr>
                    <td width="100%">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr id="trCommands" runat="server">
                                <td>
                                    <table width="290" border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <td id="tdView" runat="server" width="144" height="29" align="center" valign="middle" background='/images/orangetab.png'>
                                            <asp:LinkButton ID="cmdViewGen" CssClass="whitetext" runat="server">View Menus</asp:LinkButton></td>
                                        <td width="2"></td>
                                        <td id="tdAddEdit" runat="server" width="144" align="center" valign="middle" background='/images/darkbluetab.png'>
                                        <asp:LinkButton ID="cmdAddGen" CssClass="whitetext" runat="server">Add Menu</asp:LinkButton>
                                        </td>
                                      </tr>
                                    </table>                    
                                </td>
                            </tr>
                             
                            <tr>
                                <td width="100%" valign="top">
                                    <asp:Panel ID="panList" runat="server" Width="100%">
                                        <asp:GridView ID="grvList" BorderColor="#EEEEEE" runat="server" Width="100%" AllowPaging="True"  CssClass="gridviewcontents"
                                            AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="genId" 
                                            DataSourceID="SqlList" RowStyle-Height="20">
                                            <Columns>
                                                <asp:BoundField DataField="genId" HeaderText="genId" InsertVisible="False" ReadOnly="True" SortExpression="genId" Visible="False" />
                                                <asp:BoundField DataField="genName" HeaderText="Name" SortExpression="genName" />
                                                <asp:BoundField DataField="genDescription" HeaderText="Description" SortExpression="genDescription" />
                                                <asp:CommandField ItemStyle-Font-Bold="True" SelectText="View/Edit" 
                                                    ShowSelectButton="True" >
                                                    <ItemStyle Font-Bold="True" />
                                                </asp:CommandField>
                                            </Columns>
                                            <RowStyle CssClass="greytable" />
                                            <AlternatingRowStyle CssClass="whitetable" />
                                            <PagerStyle CssClass="greytable" />
                                            <HeaderStyle HorizontalAlign="Left" Height="20" ForeColor="#FFFFFF" CssClass="tablecolors_title" />                            

                                        </asp:GridView>    
                                        <asp:SqlDataSource ID="SqlList" runat="server" 
                                            ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                            
                                            SelectCommand="SELECT [mnuId], [mnuName], [mnuLink] FROM [tblMenus] WHERE mnuRecordArchived = 0 ORDER BY [mnuName]" 
                                            DeleteCommand="DELETE FROM [tblMenus] WHERE [mnuId] = @mnuId" 
                                            InsertCommand="INSERT INTO [tblMenus] ([mnuName], [mnuLink], [mnuParent]) VALUES (@mnuName, @mnuLink, @mnuParent)" 
                                            
                                            
                                            UpdateCommand="UPDATE [tblMenus] SET [mnuName] = @mnuName, [mnuLink] = @mnuLink, [mnuParent] = @mnuParent WHERE [mnuId] = @mnuId">
                                            <DeleteParameters>
                                                <asp:Parameter Name="mnuId" Type="Int32" />
                                            </DeleteParameters>
                                            <UpdateParameters>
                                                <asp:ControlParameter ControlID="txtName" PropertyName="Text" Name="genName" Type="String" />
                                                <asp:ControlParameter ControlID="txtDescription" PropertyName="Text" Name="genDescription" Type="String" />
                                                <asp:ControlParameter ControlID="grvList" PropertyName="SelectedValue" Name="genId" Type="Int32" />
                                            </UpdateParameters>
                                            <InsertParameters>
                                                <asp:ControlParameter ControlID="txtName" PropertyName="Text" Name="genName" Type="String" />
                                                <asp:ControlParameter ControlID="txtDescription" PropertyName="Text" Name="genDescription" Type="String" />
                                            </InsertParameters>
                                        </asp:SqlDataSource>
                                    </asp:Panel>

                                        
                                   
                                    <asp:Panel ID="panAddEdit" runat="server" Width="100%" Visible="False">
                                      <table width="400" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          
                                          
                                          
                                        </tr>
                                        <tr>
                                          
                                          <td valign="top">
                                                                              
                                                <table cellpadding="3" id="tblAddEdit" runat="server" cellspacing="0" width="100%" class="tablewithborders">
                                                    <tr>
                                                        <td width="50%">
                                                            Name:</td>
                                                        <td width="50%">
                                                            <asp:TextBox ID="txtName" runat="server" CssClass="textbox"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="rvaName" runat="server" ControlToValidate="txtName" 
                                                                Display="Dynamic" ValidationGroup="AddGen" 
                                                                ErrorMessage="* please enter name"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Description:</td>
                                                        <td>
                                                            <asp:TextBox ID="txtDescription" runat="server" Rows="4" CssClass="textbox" 
                                                                Columns="30" TextMode="MultiLine"></asp:TextBox>

                                                                
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="cmdCancel" runat="server" CausesValidation="False" Text="Cancel" CssClass="button"></asp:Button>
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="cmdUpdate" runat="server" ValidationGroup="AddGen" CssClass="button" Text="Add Menu"></asp:Button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton ID="cmdDelete" OnClientClick="return confirm('Are you sure you want to delete this record?');" Font-Bold="True" runat="server">Delete Item</asp:LinkButton>
                                                        </td>
                                                        <td>
                                                          </td>
                                                    </tr>
                                                </table>
                                              </td>
                                              
                                            </tr>
                                            <tr>
                                              
                                              
                                              
                                            </tr>
                                          </table>                                                 
                                    </asp:Panel>    
                                </td>
                            </tr>
                        </table>                    
                    </td>
                </tr>

            </table>
</asp:Content>

