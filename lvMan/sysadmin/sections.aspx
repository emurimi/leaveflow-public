﻿<%@ Page Title="Manage Sections" Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.sysadmin_sections" Codebehind="sections.aspx.vb" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">

            <table width="100%" cellpadding="3" cellspacing="0" border="0">
                <tr>
                    <td width="100%">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td style="padding-bottom: 4px;">
                                
                                    <asp:DropDownList ID="lstSelectCountries" runat="server" CssClass="listmenu" 
                                        DataSourceID="SqlCompanies" DataTextField="comName" DataValueField="comId" 
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlCompanies" runat="server" 
                                        ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                        SelectCommandType="StoredProcedure"
                                        SelectCommand="stpCompanyDetails">
                                    </asp:SqlDataSource>
                                
                                    <asp:DropDownList ID="lstSelectDepartments" runat="server" AutoPostBack="True" 
                                        CssClass="listmenu" DataSourceID="SqlDepartments" DataTextField="dptName" 
                                        DataValueField="dptId">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlDepartments" runat="server" 
                                        ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                        SelectCommandType="StoredProcedure"
                                        SelectCommand="stpDepartments_View">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="lstSelectCountries" Name="dptcomId" PropertyName="SelectedValue" Type="Int32" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                
                                </td>
                            </tr>
                            <tr id="trCommands" runat="server">
                                <td>
                                    <table width="290" border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <td id="tdView" runat="server" width="144" height="29" align="center" valign="middle" background='/images/orangetab.png'>
                                            <asp:LinkButton CssClass="whitetext" ID="cmdViewConfigs" runat="server">View Sections</asp:LinkButton></td>
                                        <td width="2"></td>
                                        <td id="tdAddEdit" runat="server" width="144" align="center" valign="middle" background='/images/darkbluetab.png'>
                                            <asp:LinkButton CssClass="whitetext" ID="cmdAddConfig" runat="server">Add Section</asp:LinkButton>
                                        </td>
                                      </tr>
                                    </table>                    
                                </td>
                            </tr>
                            
                            <tr>
                                <td width="100%" valign="top">
                                    <asp:Panel ID="panList" runat="server" Width="100%">
                                        <asp:GridView ID="grvList" BorderColor="#EEEEEE" runat="server" Width="100%" AllowPaging="True"  CssClass="gridviewcontents"
                                            AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="secId" 
                                            DataSourceID="SqlList" RowStyle-Height="20">
                                            <RowStyle Height="20px" />
                                            <Columns>
                                                <asp:BoundField DataField="secId" HeaderText="secId" InsertVisible="False" Visible="False" ReadOnly="True" SortExpression="secId" />
                                                <asp:BoundField DataField="secName" HeaderText="Name" SortExpression="secName" />
                                                <asp:BoundField DataField="dptName" HeaderText="Department" SortExpression="dptName" />
                                                <asp:CommandField ShowSelectButton="True" SelectText="View/Edit" 
                                                    ItemStyle-Font-Bold="True" >
                                                    <ItemStyle Font-Bold="True" />
                                                </asp:CommandField>
                                            </Columns>
                                            <RowStyle CssClass="greytable" />
                                            <AlternatingRowStyle CssClass="whitetable" />
                                            <PagerStyle CssClass="greytable" />
                                            <HeaderStyle HorizontalAlign="Left" ForeColor="#FFFFFF" CssClass="tablecolors_title" />                            

                                        </asp:GridView>    
                                        <asp:SqlDataSource ID="SqlList" runat="server" 
                                            ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                            SelectCommandType="StoredProcedure"
                                            DeleteCommandType="StoredProcedure"
                                            InsertCommandType="StoredProcedure"
                                            UpdateCommandType="StoredProcedure"                                            
                                            SelectCommand="stpSections_View" 
                                            DeleteCommand="stpSections_Delete" 
                                            InsertCommand="stpSections_Add" 
                                            UpdateCommand="stpSections_Update">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="lstSelectDepartments" Name="secdptId" PropertyName="SelectedValue" Type="Int32" />
                                            </SelectParameters>
                                            <DeleteParameters>
                                                <asp:Parameter Name="secId" Type="Int32" />
                                            </DeleteParameters>
                                            <UpdateParameters>
                                                <asp:ControlParameter ControlID="txtName" Name="secName" PropertyName="Text" Type="String" />
                                                <asp:ControlParameter ControlID="txtDescription" Name="secDesription" PropertyName="Text" Type="String" />
                                                <asp:ControlParameter ControlID="lstDepartment" Name="secdptId" PropertyName="SelectedValue" Type="Int32" />
                                                <asp:ControlParameter ControlID="grvList" Name="secId" PropertyName="SelectedValue" Type="Int32" />
                                            </UpdateParameters>
                                            <InsertParameters>
                                                <asp:ControlParameter ControlID="txtName" Name="secName" PropertyName="Text" Type="String" />
                                                <asp:ControlParameter ControlID="txtDescription" Name="secDesription" PropertyName="Text" Type="String" />
                                                <asp:ControlParameter ControlID="lstDepartment" Name="secdptId" PropertyName="SelectedValue" Type="Int32" />
                                            </InsertParameters>
                                        </asp:SqlDataSource>
                                    </asp:Panel>

                                        
                                   
                                    <asp:Panel ID="panAddEdit" runat="server" Width="100%" Visible="False">
                                      <table width="400" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          
                                          
                                          
                                        </tr>
                                        <tr>
                                          
                                          <td valign="top">                                    
                                            <table cellpadding="3" id="tblAddEdit" runat="server" cellspacing="0" width="100%" class="tablewithborders">
                                                <tr>
                                                    <td width="50%">
                                                        Name:</td>
                                                    <td width="50%">
                                                        <asp:TextBox ID="txtName" runat="server" CssClass="textbox"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rvaName" runat="server" ControlToValidate="txtName" 
                                                            Display="Dynamic" ValidationGroup="AddItem" 
                                                            ErrorMessage="* please enter name"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Department:</td>
                                                    <td>
                                                        <asp:DropDownList ID="lstDepartment" runat="server" DataSourceID="SqlDepartments" 
                                                            DataTextField="dptName" DataValueField="dptId" CssClass="listmenu">
                                                        </asp:DropDownList>
                                                            
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Description:</td>
                                                    <td>
                                                        <asp:TextBox ID="txtDescription" runat="server" Rows="4" CssClass="textbox" 
                                                            Columns="30" TextMode="MultiLine"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Button ID="cmdCancel" runat="server" CausesValidation="False" Text="Cancel" CssClass="button"></asp:Button>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="cmdUpdate" runat="server" ValidationGroup="AddItem" 
                                                            CssClass="button" Text="Add Section"></asp:Button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:LinkButton ID="cmdDelete" OnClientClick="return confirm('Are you sure you want to delete this record?');" Font-Bold="True" runat="server">Delete Section</asp:LinkButton>
                                                    </td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                            </table>
                                          </td>
                                          
                                        </tr>
                                        <tr>
                                          
                                          
                                          
                                        </tr>
                                      </table>                                             
                                    </asp:Panel>    
                                </td>
                            </tr>
                        </table>                    
                    </td>
                </tr>

            </table>
</asp:Content>

