﻿<%@ Page Title="Manage Calendar" Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.hradmin_calendar" Codebehind="calendar.aspx.vb" %>
<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2010" Namespace="RJS.Web.WebControl" TagPrefix="rjs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">

        <table cellpadding="3" cellspacing="0" width="100%">
            <tr id="trCommands" runat="server">
                <td>
                    <table width="290" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td id="tdView" runat="server" width="144" height="29" align="center" valign="middle" background='/images/orangetab.png'>
                            <asp:LinkButton ID="cmdViewConfigs" CssClass="whitetext" runat="server">View Events</asp:LinkButton></td>
                        <td width="2"></td>
                        <td id="tdAddEdit" runat="server" width="144" align="center" valign="middle" background='/images/darkbluetab.png'>
                        <asp:LinkButton ID="cmdAddConfig" CssClass="whitetext" runat="server">Add Event</asp:LinkButton>
                        </td>
                      </tr>
                    </table>                    
                </td>
            </tr>        
            <tr>
                <td>
                    <asp:Panel ID="panList" runat="server" Width="100%">
                        <asp:GridView ID="grvList" BorderColor="#EEEEEE" Width="100%" runat="server" 
                            AutoGenerateColumns="False" DataKeyNames="calId" DataSourceID="SqlList" 
                            AllowPaging="True" AllowSorting="True" CellPadding="4">
                            <Columns>
                                <asp:BoundField DataField="calId" HeaderText="calId" Visible="False" InsertVisible="False" ReadOnly="True" SortExpression="calId" />
                                <asp:BoundField DataField="calTitle" HeaderText="Event" SortExpression="calTitle" />
                                <asp:BoundField DataField="calStartDate" HeaderText="Start Date" DataFormatString="{0:d}" SortExpression="calStartDate" />
                                <asp:BoundField DataField="calStartTime" HeaderText="Start Time" SortExpression="calStartTime" />
                                <asp:BoundField DataField="calEndDate" HeaderText="End Date" DataFormatString="{0:d}" SortExpression="calEndDate" />
                                <asp:BoundField DataField="calEndTime" HeaderText="End Time" SortExpression="calEndTime" />
                                <asp:CommandField ItemStyle-Font-Bold="True" SelectText="View/Edit" ShowSelectButton="True" />
                            </Columns>
                            <RowStyle CssClass="greytable" />
                            <AlternatingRowStyle CssClass="whitetable" />
                            <PagerStyle CssClass="greytable" />
                            <HeaderStyle HorizontalAlign="Left" ForeColor="#FFFFFF" CssClass="tablecolors_title" />                            
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlList" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                            DeleteCommandType="StoredProcedure"
                            DeleteCommand="stpCalendar_Delete" 
                            InsertCommandType="StoredProcedure"
                            InsertCommand="stpCalendar_Add" 
                            SelectCommandType="StoredProcedure"
                            SelectCommand="stpCalendar_View" 
                            UpdateCommandType="StoredProcedure"
                            UpdateCommand="stpCalendar_Update">
                            
                            <DeleteParameters>
                                <asp:Parameter Name="calId" Type="Int32" />
                            </DeleteParameters>
                            <UpdateParameters>
                                <asp:ControlParameter ControlID="txtName" PropertyName="Text" Name="calTitle" Type="String" />
                                <asp:ControlParameter ControlID="txtDescription" PropertyName="Text" Name="calText" Type="String" />
                                <asp:ControlParameter ControlID="txtStartDate" PropertyName="Text" Name="calStartDate" Type="DateTime" />
                                <asp:ControlParameter ControlID="lstStartTime" PropertyName="SelectedValue" Name="calStartTime" Type="String" />
                                <asp:ControlParameter ControlID="txtEndDate" PropertyName="Text" Name="calEndDate" Type="DateTime" />
                                <asp:ControlParameter ControlID="lstEndTime" PropertyName="SelectedValue" Name="calEndTime" Type="String" />
                                <asp:ControlParameter ControlID="grvList" PropertyName="SelectedValue" Name="calId" Type="Int32" />
                            </UpdateParameters>
                            <InsertParameters>
                                <asp:ControlParameter ControlID="txtName" PropertyName="Text" Name="calTitle" Type="String" />
                                <asp:ControlParameter ControlID="txtDescription" PropertyName="Text" Name="calText" Type="String" />
                                <asp:ControlParameter ControlID="txtStartDate" PropertyName="Text" Name="calStartDate" Type="DateTime" />
                                <asp:ControlParameter ControlID="lstStartTime" PropertyName="SelectedValue" Name="calStartTime" Type="String" />
                                <asp:ControlParameter ControlID="txtEndDate" PropertyName="Text" Name="calEndDate" Type="DateTime" />
                                <asp:ControlParameter ControlID="lstEndTime" PropertyName="SelectedValue" Name="calEndTime" Type="String" />
                            </InsertParameters>
                        </asp:SqlDataSource>
                    </asp:Panel>
                    
                    <asp:Panel ID="panAddEdit" runat="server" Width="100%" Visible="False">
                      <asp:Label ID="lblMessage" Font-Bold="True" runat="server"></asp:Label>
                      <table width="400" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          
                          
                          
                        </tr>
                        <tr>
                          
                          <td valign="top">                    
                                <table cellpadding="3" id="tblAddEdit" runat="server" cellspacing="0" width="100%" class="tablewithborders">
                                    <tr>
                                        <td width="50%">
                                            Event Name:</td>
                                        <td width="50%">
                                            <asp:TextBox ID="txtName" runat="server" CssClass="textbox"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rvaName" runat="server" 
                                                ControlToValidate="txtName" Display="Dynamic" 
                                                ErrorMessage="* please enter name" ValidationGroup="AddItem"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Description:</td>
                                        <td>
                                            <asp:TextBox ID="txtDescription" runat="server" Rows="4" CssClass="textbox" 
                                                Columns="30" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Start Date:</td>
                                        <td>
                                          <asp:TextBox ID="txtStartDate" ReadOnly="True" runat="server" CssClass="textbox"></asp:TextBox>
                                          <rjs:PopCalendar ID="ppcStartDate" runat="server" Control="txtStartDate" 
                                              AutoPostBack="False" HolidayMessage="This date is not available" 
                                              Separator="/" SelectHoliday="False" />
                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                              ControlToValidate="txtStartDate" ErrorMessage="**"></asp:RequiredFieldValidator>
                                          <rjs:PopCalendarMessageContainer ID="pmcSTartDate" runat="server" Calendar="ppcStartDate" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Start Time:</td>
                                        <td>
                                            <asp:DropDownList ID="lstStartTime" CssClass="listmenu" runat="server"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            End Date:</td>
                                        <td>
                                          <asp:TextBox ID="txtEndDate" ReadOnly="True" runat="server" CssClass="textbox"></asp:TextBox>
                                          <rjs:PopCalendar ID="ppcEndDate" runat="server" Control="txtEndDate" 
                                              AutoPostBack="False" HolidayMessage="This date is not available" 
                                              Separator="/" ShowWeekend="True" />
                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                              ControlToValidate="txtEndDate" ErrorMessage="**"></asp:RequiredFieldValidator>
                                          <rjs:PopCalendarMessageContainer ID="pmcEndDate" runat="server" Calendar="ppcEndDate" /> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            End Time:</td>
                                        <td>
                                            <asp:DropDownList ID="lstEndTime" CssClass="listmenu" runat="server"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="trEntities0" runat="server">
                                        <td bgcolor="#EEEEEE" style="text-align: center">
                                            Add meeting participants:</td>
                                        <td bgcolor="#EEEEEE" style="text-align: center">
                                            <asp:DropDownList ID="lstParticipants" AutoPostBack="True" runat="server" 
                                                CssClass="listmenu">
                                                <asp:ListItem Text="Division/Company" Value="C"></asp:ListItem>
                                                <asp:ListItem Text="Department" Value="D"></asp:ListItem>
                                                <asp:ListItem Text="Branch" Value="B"></asp:ListItem>
                                                <asp:ListItem Text="Section" Value="S"></asp:ListItem>
                                                <asp:ListItem Text="Staff" Value="U"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>     
                                    <tr id="trEntities" runat="server">
                                        <td colspan="2" bgcolor="#EEEEEE">
                                            <asp:DropDownList ID="lstCompanies" runat="server" CssClass="listmenu" 
                                                DataSourceID="SqlCompanies" DataTextField="comName" DataValueField="comId" 
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="SqlCompanies" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                SelectCommand="SELECT [comId], [comName] FROM [tblCompanies] WHERE ([comRecordArchived] = @comRecordArchived) ORDER BY [comName]">
                                                <SelectParameters>
                                                    <asp:Parameter DefaultValue="False" Name="comRecordArchived" Type="Boolean" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                            <asp:DropDownList ID="lstBranches" runat="server" AutoPostBack="True" 
                                                CssClass="listmenu" DataSourceID="SqlBranches" DataTextField="braName" 
                                                DataValueField="braId">
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="SqlBranches" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                SelectCommandType="StoredProcedure"
                                                SelectCommand="stpBranches_View">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="lstCompanies" Name="bracomId" PropertyName="SelectedValue" Type="Int32" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>                
                                            <asp:DropDownList ID="lstDepartments" runat="server" CssClass="listmenu" 
                                                DataSourceID="SqlDepartments" DataTextField="dptName" 
                                                DataValueField="dptId" AutoPostBack="True">
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="SqlDepartments" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                SelectCommandType="StoredProcedure"
                                                SelectCommand="stpDepartments_View">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="lstCompanies" Name="dptcomId" PropertyName="SelectedValue" Type="Int32" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                            <asp:DropDownList ID="lstSections" runat="server" CssClass="listmenu" 
                                                DataSourceID="SqlSections" DataTextField="secName" DataValueField="secId" 
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="SqlSections" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                SelectCommandType="StoredProcedure"
                                                SelectCommand="stpSections_View">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="lstDepartments" Name="secdptId" PropertyName="SelectedValue" Type="Int32" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                        
                                        </td>
                                    </tr>                               
                                    <tr id="trEntities1" runat="server">
                                        <td bgcolor="#EEEEEE" style="text-align: center">
                                            Available:</td>
                                        <td bgcolor="#EEEEEE" style="text-align: center">
                                            Participating:</td>
                                    </tr>
                                    <tr id="trEntities2" runat="server">
                                        <td bgcolor="#EEEEEE" style="text-align: center">
                                            <asp:ListBox ID="lstAllEntities" SelectionMode="Multiple" runat="server" CssClass="listmenu">
                                            </asp:ListBox>
                                        </td>
                                        <td bgcolor="#EEEEEE" style="text-align: center">
                                            <asp:ListBox SelectionMode="Multiple" ID="lstApplicableEntities" runat="server" CssClass="listmenu">
                                            </asp:ListBox>
                                        </td>
                                    </tr>
                                    <tr id="trEntities3" runat="server">
                                        <td bgcolor="#EEEEEE" style="text-align: center">
                                            <asp:LinkButton ID="cmdAddEntity" runat="server" Font-Bold="True">ADD &gt;&gt;&gt;</asp:LinkButton>
                                        </td>
                                        <td bgcolor="#EEEEEE" style="text-align: center">
                                            <asp:LinkButton ID="cmdRemoveEntity" runat="server" Font-Bold="True">&lt;&lt;&lt; REMOVE</asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="cmdCancel" runat="server" CausesValidation="False" 
                                                CssClass="button" Text="Cancel" />
                                        </td>
                                        <td>
                                            <asp:Button ID="cmdUpdate" runat="server" CssClass="button" 
                                                Text="Add Event" ValidationGroup="AddItem" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="cmdDelete" OnClientClick="return confirm('Are you sure you want to delete this record?');" Font-Bold="True" runat="server">Delete Event</asp:LinkButton>
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                </table>
                              </td>
                              
                            </tr>
                            <tr>
                              
                              
                              
                            </tr>
                          </table>                                      
                    </asp:Panel>    
                    
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
</asp:Content>

