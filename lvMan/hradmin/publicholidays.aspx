﻿<%@ Page Title="Setup Public Holidays" Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.hradmin_publicholidays" Codebehind="publicholidays.aspx.vb" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">

        <table cellpadding="3" cellspacing="0" width="100%">
            <tr id="trCommands" runat="server">
                <td>
                    <table width="290" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td id="tdView" runat="server" width="144" height="29" align="center" valign="middle" background='/images/orangetab.png'>
                            <asp:LinkButton ID="cmdViewConfigs" CssClass="whitetext" runat="server">View Holidays</asp:LinkButton></td>
                        <td width="2"></td>
                        <td id="tdAddEdit" runat="server" width="144" align="center" valign="middle" background='/images/darkbluetab.png'>
                        <asp:LinkButton ID="cmdAddConfig" CssClass="whitetext" runat="server">Add Holiday</asp:LinkButton>
                        </td>
                      </tr>
                    </table>                    
                </td>
            </tr>        
            <tr>
                <td>
                    <asp:Panel ID="panList" runat="server" Width="100%">
                        <asp:GridView ID="grvList" BorderColor="#EEEEEE" Width="100%" runat="server" 
                            AutoGenerateColumns="False" DataKeyNames="pbhId" DataSourceID="SqlList" 
                            AllowPaging="True" AllowSorting="True" CellPadding="4">
                            <Columns>
                                <asp:BoundField DataField="pbhId" HeaderText="pbhId" Visible="False" InsertVisible="False" ReadOnly="True" SortExpression="pbhId" />
                                <asp:BoundField DataField="pbhDay" HeaderText="Day" SortExpression="pbhDay" />
                                <asp:BoundField DataField="pbhMonth" HeaderText="Month" SortExpression="pbhMonth" />
                                <asp:BoundField DataField="pbhName" HeaderText="Name" SortExpression="pbhName" />
                                <asp:CheckBoxField DataField="pbhRecurring" HeaderText="Recurring" SortExpression="pbhRecurring" />
                                 <asp:CommandField ShowSelectButton="True" SelectText="View/Edit" 
                                    ItemStyle-Font-Bold="True" >
                                     <ItemStyle Font-Bold="True" />
                                </asp:CommandField>
                            </Columns>
                            <RowStyle CssClass="greytable" />
                            <AlternatingRowStyle CssClass="whitetable" />
                            <PagerStyle CssClass="greytable" />
                            <HeaderStyle HorizontalAlign="Left" ForeColor="#FFFFFF" CssClass="tablecolors_title" />                            
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlList" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                            DeleteCommandType="StoredProcedure"
                            DeleteCommand="stpPublicHolidays_Delete" 
                            InsertCommandType="StoredProcedure"
                            InsertCommand="stpPublicHolidays_Add" 
                            SelectCommand="stpPublicHolidays_View" 
                            SelectCommandType="StoredProcedure"
                            UpdateCommandType="StoredProcedure"
                            UpdateCommand="stpPublicHolidays_Update">
                            
                            <DeleteParameters>
                                <asp:Parameter Name="pbhId" Type="Int32" />
                            </DeleteParameters>
                            <UpdateParameters>
                                <asp:ControlParameter ControlID="lstDay" PropertyName="SelectedValue" Name="pbhDay" Type="Int32" />
                                <asp:ControlParameter ControlID="lstMonth" PropertyName="SelectedValue" Name="pbhMonth" Type="Int32" />
                                <asp:ControlParameter ControlID="lstYear" PropertyName="SelectedValue" Name="pbhYear" Type="Int32" />
                                <asp:ControlParameter ControlID="txtName" PropertyName="Text" Name="pbhName" Type="String" />
                                <asp:ControlParameter ControlID="txtDescription" PropertyName="Text" Name="pbhDescription" Type="String" />
                                <asp:ControlParameter ControlID="chkRecurring" PropertyName="Checked" Name="pbhRecurring" Type="Boolean" />
                                <asp:ControlParameter ControlID="grvList" PropertyName="SelectedValue" Name="pbhId" Type="Int32" />
                            </UpdateParameters>
                            <InsertParameters>
                                <asp:ControlParameter ControlID="lstDay" PropertyName="SelectedValue" Name="pbhDay" Type="Int32" />
                                <asp:ControlParameter ControlID="lstMonth" PropertyName="SelectedValue" Name="pbhMonth" Type="Int32" />
                                <asp:ControlParameter ControlID="lstYear" PropertyName="SelectedValue" Name="pbhYear" Type="Int32" />
                                <asp:ControlParameter ControlID="txtName" PropertyName="Text" Name="pbhName" Type="String" />
                                <asp:ControlParameter ControlID="txtDescription" PropertyName="Text" Name="pbhDescription" Type="String" />
                                <asp:ControlParameter ControlID="chkRecurring" PropertyName="Checked" Name="pbhRecurring" Type="Boolean" />
                            </InsertParameters>
                        </asp:SqlDataSource>
                    </asp:Panel>
                    
                    <asp:Panel ID="panAddEdit" runat="server" Width="100%" Visible="False">
                      <table width="400" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          
                          
                          
                        </tr>
                        <tr>
                          
                          <td valign="top">                    
                                <table cellpadding="3" id="tblAddEdit" runat="server" cellspacing="0" width="100%" class="tablewithborders">
                                    <tr>
                                        <td width="50%">
                                            Name:</td>
                                        <td width="50%">
                                            <asp:TextBox ID="txtName" runat="server" CssClass="textbox"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rvaName" runat="server" 
                                                ControlToValidate="txtName" Display="Dynamic" 
                                                ErrorMessage="* please enter name" ValidationGroup="AddItem"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Description:</td>
                                        <td>
                                            <asp:TextBox ID="txtDescription" runat="server" Rows="4" CssClass="textbox" 
                                                Columns="30" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Date:</td>
                                        <td>
                                            <asp:DropDownList ID="lstDay" runat="server" CssClass="listmenu">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="lstMonth" runat="server" CssClass="listmenu">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="lstYear" runat="server" CssClass="listmenu">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Recurring:</td>
                                        <td>
                                            <asp:CheckBox ID="chkRecurring" runat="server" />
                                        </td>
                                    </tr>
                                    <tr id="trCountries1" runat="server">
                                        <td bgcolor="#EEEEEE" style="text-align: center">
                                            Available countries:</td>
                                        <td bgcolor="#EEEEEE" style="text-align: center">
                                            Applicable in countries:</td>
                                    </tr>
                                    <tr id="trCountries2" runat="server">
                                        <td bgcolor="#EEEEEE" style="text-align: center">
                                            <asp:ListBox ID="lstCountries" runat="server" CssClass="listmenu"  SelectionMode="Multiple"
                                                DataSourceID="SqlCountries" DataTextField="ctyName" DataValueField="ctyId">
                                            </asp:ListBox>
                                            <asp:SqlDataSource ID="SqlCountries" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                SelectCommandType="StoredProcedure"
                                                SelectCommand="stpPublicHolidayCountries">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="grvList" PropertyName="SelectedValue" Name="pbhId" Type="Int32"  />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                        </td>
                                        <td bgcolor="#EEEEEE" style="text-align: center">
                                            <asp:ListBox ID="lstApplicableCountries" runat="server" CssClass="listmenu" 
                                                DataSourceID="SqlHolidayCountries" SelectionMode="Multiple" DataTextField="ctyName" 
                                                DataValueField="cphId"></asp:ListBox>
                                            <asp:SqlDataSource ID="SqlHolidayCountries" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                SelectCommandType="StoredProcedure"
                                                SelectCommand="stpCountryPublicHolidaysList">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="grvList" Name="cphpbhId" PropertyName="SelectedValue" Type="Int32" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                        </td>
                                    </tr>
                                    <tr id="trCountries3" runat="server">
                                        <td bgcolor="#EEEEEE" style="text-align: center">
                                            <asp:LinkButton ID="cmdAddCountry" runat="server" Font-Bold="True">ADD &gt;&gt;&gt;</asp:LinkButton>
                                        </td>
                                        <td bgcolor="#EEEEEE" style="text-align: center">
                                            <asp:LinkButton ID="cmdRemoveCountry" runat="server" Font-Bold="True">&lt;&lt;&lt; REMOVE</asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="cmdCancel" runat="server" CausesValidation="False" 
                                                CssClass="button" Text="Cancel" />
                                        </td>
                                        <td>
                                            <asp:Button ID="cmdUpdate" runat="server" CssClass="button" 
                                                Text="Add Holiday" ValidationGroup="AddItem" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="cmdDelete" OnClientClick="return confirm('Are you sure you want to delete this record?');" Font-Bold="True" runat="server">Delete Holiday</asp:LinkButton>
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                </table>
                              </td>
                              
                            </tr>
                            <tr>
                              
                              
                              
                            </tr>
                          </table>                                      
                    </asp:Panel>    
                    
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
</asp:Content>

