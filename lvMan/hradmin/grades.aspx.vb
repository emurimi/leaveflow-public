﻿Imports System.Data.SqlClient
Imports lvManLib.Security
Imports lvManLib.DataFunctions
Imports lvManLib.Config

Partial Class sysadmin_grades
    Inherits System.Web.UI.Page

    Public strUserPermissions As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        strUserPermissions = SecureCurrentPage()
        If InStr(strUserPermissions, "A") = 0 Then cmdAddConfig.Enabled = False

        If Not Page.IsPostBack Then
            Dim strOrganizationType As String = GetConfig("OrganizationType") & "" '1 = Organization with many companies, 2 = Single company

            If strOrganizationType = "2" Then
                trCompany.Visible = False
                lstSelectCompanies.Visible = False
                grvList.Columns(2).Visible = False
            End If

            lstCompanies.DataBind()
            lstCompanies.Items.Insert(0, New ListItem("Spans all companies", "0"))
            lstCompanies.SelectedIndex = 0

            lstSelectCompanies.DataBind()
            lstSelectCompanies.Items.Insert(0, New ListItem("Spans all companies", "0"))
            lstSelectCompanies.SelectedIndex = 0
        End If
    End Sub

    Protected Sub cmdUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
        If Page.IsValid Then

            If cmdUpdate.Text = "Add Grade" Then
                SqlList.Insert()
            Else
                SqlList.Update()
            End If

            EditMode(False)
        End If
    End Sub


    Sub ClearFields()
        txtName.Text = ""
        txtDescription.Text = ""
        lstCarryOverExpiry.SelectedIndex = 0
        txtMaxCarryOver.Text = "0"
    End Sub


    Sub GetUpdateForm(ByVal intgraId As Integer)
        Dim objDR As SqlDataReader
        Dim parameters As New List(Of SqlParameter)()
        Dim graId As New SqlParameter("@graId", intgraId)
        parameters.Add(graId)

        objDR = GetSpDataReader("stpGrades_View", parameters.ToArray())

        If objDR.Read Then
            txtName.Text = objDR("graName") & ""
            txtDescription.Text = objDR("graDescription") & ""
            lstCompanies.SelectedIndex = lstCompanies.Items.IndexOf(lstCompanies.Items.FindByValue(objDR("gracomId")))
            lstCarryOverExpiry.SelectedIndex = lstCarryOverExpiry.Items.IndexOf(lstCarryOverExpiry.Items.FindByValue(objDR("graCarryOverExpiryMonth")))
            txtMaxCarryOver.Text = objDR("graMaxYearCarryOver") & ""
        End If

        objDR.Close()
        objDR = Nothing

        cmdUpdate.Text = "Update Grade"
        cmdAddConfig.Text = cmdUpdate.Text
        EditMode(True, False)
    End Sub

    Protected Sub grvList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvList.SelectedIndexChanged
        GetUpdateForm(grvList.SelectedValue)
    End Sub

    Protected Sub cmdAddMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddConfig.Click
        cmdUpdate.Text = "Add Grade"
        EditMode(True)
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        EditMode(False)
    End Sub

    Protected Sub cmdViewMenus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdViewConfigs.Click
        EditMode(False)
    End Sub

    Sub EditMode(ByVal bolEdit As Boolean, Optional ByVal bolClearFields As Boolean = True)
        EnableDisableFields(True)

        If bolEdit Then
            If bolClearFields Then ClearFields()
            panList.Visible = False
            panAddEdit.Visible = True
            tdAddEdit.Style.Item("background-image") = "url(/images/orangetab.png);"
            tdView.Style.Item("background-image") = "url(/images/darkbluetab.png);"
            cmdDelete.Visible = False

            If cmdUpdate.Text = "Update Grade" Then
                If InStr(strUserPermissions, "E") = 0 Then EnableDisableFields(False)
                If InStr(strUserPermissions, "D") > 0 Then cmdDelete.Visible = True
            End If
        Else
            cmdAddConfig.Text = "Add Grade"
            panList.Visible = True
            panAddEdit.Visible = False
            tdAddEdit.Style.Item("background-image") = "url(/images/darkbluetab.png);"
            tdView.Style.Item("background-image") = "url(/images/orangetab.png);"
            grvList.DataBind()
        End If
    End Sub

    Sub EnableDisableFields(ByVal bolEnabled As Boolean)
        lstCompanies.Enabled = bolEnabled
        txtName.Enabled = bolEnabled
        txtDescription.Enabled = bolEnabled
        cmdUpdate.Enabled = bolEnabled
    End Sub

    Protected Sub cmdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        Dim parameters As New List(Of SqlParameter)()
        Dim graId As New SqlParameter("@graId", grvList.SelectedValue)
        parameters.Add(graId)
        ExecuteStoredProc("stpGrades_Delete", parameters.ToArray())
        EditMode(False)
    End Sub
End Class
