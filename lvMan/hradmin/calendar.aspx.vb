﻿Imports System.Data.SqlClient
Imports lvManLib.Security
Imports lvManLib.DataFunctions
Imports lvManLib.General
Imports lvManLib.Config

Partial Class hradmin_calendar
    Inherits System.Web.UI.Page

    Public strUserPermissions As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        strUserPermissions = SecureCurrentPage()
        If InStr(strUserPermissions, "A") = 0 Then cmdAddConfig.Enabled = False

        If Not Page.IsPostBack Then
            LoadTime(lstStartTime, Session("comId"))
            LoadTime(lstEndTime, Session("comId"))
            lstCompanies.DataBind()

        End If
    End Sub

    Protected Sub cmdUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
        If Page.IsValid Then

            If cmdUpdate.Text = "Add Event" Then
                SqlList.Insert()
            Else
                SqlList.Update()
            End If

            EditMode(False)
        End If
    End Sub


    Sub ClearFields()
        txtName.Text = ""
        txtDescription.Text = ""
        txtStartDate.Text = ""
        txtEndDate.Text = ""
        trEntities.Visible = False
        trEntities0.Visible = False
        lstParticipants.SelectedIndex = 0
        trEntities1.Visible = False
        trEntities2.Visible = False
        trEntities3.Visible = False
    End Sub


    Sub GetUpdateForm(ByVal intcalId As Integer)
        Dim objDR As SqlDataReader
        Dim parameters As New List(Of SqlParameter)()
        Dim calId As New SqlParameter("@calId", intcalId)
        parameters.Add(calId)

        objDR = GetSpDataReader("stpCalendar_View", parameters.ToArray())

        If objDR.Read Then
            txtName.Text = objDR("calTitle") & ""
            txtDescription.Text = objDR("calText") & ""
            txtStartDate.Text = objDR("calStartDate")
            txtEndDate.Text = objDR("calEndDate")
            lstStartTime.SelectedIndex = lstStartTime.Items.IndexOf(lstStartTime.Items.FindByValue(objDR("calStartTime")))
            lstEndTime.SelectedIndex = lstEndTime.Items.IndexOf(lstEndTime.Items.FindByValue(objDR("calEndTime")))
        End If

        objDR.Close()
        objDR = Nothing

        cmdUpdate.Text = "Update Event"
        cmdAddConfig.Text = cmdUpdate.Text

        EditMode(True, False)
        lstParticipants.SelectedIndex = 0
        trEntities.Visible = True
        trEntities0.Visible = True
        trEntities1.Visible = True
        trEntities2.Visible = True
        trEntities3.Visible = True
        LoadEntities()
    End Sub

    Sub LoadEntities()
        Dim strSql As String = ""
        Dim strSql2 As String = ""
        Dim objDR As SqlDataReader
        Dim strCalID As String
        Dim parameters As New List(Of SqlParameter)()
        Dim parameters2 As New List(Of SqlParameter)()
        Dim bolParameters As Boolean = False

        Try
            strCalID = grvList.SelectedValue
        Catch ex As Exception
            Exit Sub
        End Try

        lstCompanies.Visible = False
        lstDepartments.Visible = False
        lstBranches.Visible = False
        lstSections.Visible = False

        'Get Calendar participants
        Dim cpcEntity As New SqlParameter("@cpcEntity", lstParticipants.SelectedValue)
        parameters2.Add(cpcEntity)
        Dim calId As New SqlParameter("@calId", strCalID)
        parameters2.Add(calId)
        strSql2 = "stpCalendarParticipants_View"

        Select Case lstParticipants.SelectedValue
            Case "C" 'Division/Company
                trEntities.Visible = False
                strSql = "stpCompanyDetails_AsEntity"

            Case "D" 'Department
                trEntities.Visible = True
                lstCompanies.Visible = True
                Dim dptcomId As New SqlParameter("@dptcomId", lstCompanies.SelectedValue)
                parameters.Add(dptcomId)
                bolParameters = True
                strSql = "stpDepartments_View_AsEntity"

            Case "B" 'Branch
                trEntities.Visible = True
                lstCompanies.Visible = True
                Dim bracomId As New SqlParameter("@bracomId", lstCompanies.SelectedValue)
                parameters.Add(bracomId)
                bolParameters = True
                strSql = "stpBranches_View_AsEntity"

            Case "S" 'Section
                trEntities.Visible = True
                lstCompanies.Visible = True
                lstDepartments.Visible = True
                If lstDepartments.SelectedIndex = -1 Then lstDepartments.DataBind()
                Dim secdptId As New SqlParameter("@secdptId", lstDepartments.SelectedValue)
                parameters.Add(secdptId)
                bolParameters = True
                strSql = "stpSections_View_AsEntity"

            Case "U" 'Staff
                trEntities.Visible = True
                lstCompanies.Visible = True
                lstDepartments.Visible = True
                lstBranches.Visible = True
                lstSections.Visible = True

                If lstDepartments.SelectedIndex = -1 Then lstDepartments.DataBind()
                If lstSections.SelectedIndex = -1 Then lstSections.DataBind()
                If lstBranches.SelectedIndex = -1 Then lstBranches.DataBind()

                Dim usrdptId As New SqlParameter("@usrdptId", lstDepartments.SelectedValue)
                parameters.Add(usrdptId)
                Dim usrsecId As New SqlParameter("@usrsecId", lstSections.SelectedValue)
                parameters.Add(usrsecId)
                Dim usrbraId As New SqlParameter("@usrbraId", lstBranches.SelectedValue)
                parameters.Add(usrbraId)
                bolParameters = True
                strSql = "stpUsers_MinView_AsEntity"

        End Select

        lstAllEntities.Items.Clear()
        lstApplicableEntities.Items.Clear()

        If bolParameters Then
            objDR = GetSpDataReader(strSql, parameters.ToArray())
        Else
            objDR = GetSpDataReader(strSql)
        End If

        While objDR.Read()
            lstAllEntities.Items.Add(objDR("entName"))
            lstAllEntities.Items(lstAllEntities.Items.Count - 1).Value = objDR("entId")
        End While

        objDR.Close()

        objDR = GetSpDataReader(strSql2, parameters2.ToArray())
        While objDR.Read()
            lstApplicableEntities.Items.Add(objDR("cpcName"))
            lstApplicableEntities.Items(lstApplicableEntities.Items.Count - 1).Value = objDR("cpcId")
        End While

        objDR.Close()
        objDR = Nothing

    End Sub

    Protected Sub grvList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvList.SelectedIndexChanged
        GetUpdateForm(grvList.SelectedValue)
    End Sub

    Protected Sub cmdAddMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddConfig.Click
        cmdUpdate.Text = "Add Event"
        EditMode(True)
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        EditMode(False)
    End Sub

    Protected Sub cmdViewMenus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdViewConfigs.Click
        EditMode(False)
    End Sub

    Sub EditMode(ByVal bolEdit As Boolean, Optional ByVal bolClearFields As Boolean = True)
        EnableDisableFields(True)

        If bolEdit Then
            If bolClearFields Then ClearFields()
            panList.Visible = False
            panAddEdit.Visible = True
            tdAddEdit.Style.Item("background-image") = "url(/images/orangetab.png);"
            tdView.Style.Item("background-image") = "url(/images/darkbluetab.png);"
            cmdDelete.Visible = False

            If cmdUpdate.Text = "Update Event" Then
                If InStr(strUserPermissions, "E") = 0 Then EnableDisableFields(False)
                If InStr(strUserPermissions, "D") > 0 Then cmdDelete.Visible = True
            End If
        Else
            cmdAddConfig.Text = "Add Event"
            panList.Visible = True
            panAddEdit.Visible = False
            tdAddEdit.Style.Item("background-image") = "url(/images/darkbluetab.png);"
            tdView.Style.Item("background-image") = "url(/images/orangetab.png);"
            grvList.DataBind()
        End If
    End Sub

    Sub EnableDisableFields(ByVal bolEnabled As Boolean)
        cmdAddEntity.Enabled = bolEnabled
        cmdRemoveEntity.Enabled = bolEnabled
        txtStartDate.Enabled = bolEnabled
        txtEndDate.Enabled = bolEnabled
        txtName.Enabled = bolEnabled
        txtDescription.Enabled = bolEnabled
        cmdUpdate.Enabled = bolEnabled
    End Sub

    Protected Sub cmdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        Dim parameters As New List(Of SqlParameter)()
        Dim calId As New SqlParameter("@calId", grvList.SelectedValue)
        parameters.Add(calId)
        ExecuteStoredProc("stpCalendar_Delete")
        EditMode(False)
    End Sub

    Protected Sub cmdAddEntity_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddEntity.Click
        Dim intCount As Integer
        Dim strSql As String

        'stpCalendarParticipants_Add
        For intCount = 0 To lstAllEntities.Items.Count - 1
            If lstAllEntities.Items(intCount).Selected Then
                strSql = "INSERT INTO tblCalendarParticipants (cpccalId, cpcEntity, cpcEntityID) VALUES (" & grvList.SelectedValue & ", '" & lstParticipants.SelectedValue & "', " & lstAllEntities.Items(intCount).Value & ")"
                ExecuteQuery(strSql)
            End If
        Next

        LoadEntities()
    End Sub

    Protected Sub cmdRemoveEntity_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRemoveEntity.Click
        Dim intCount As Integer
        Dim strSql As String

        'stpCalendarParticipants_Delete
        For intCount = 0 To lstApplicableEntities.Items.Count - 1
            If lstApplicableEntities.Items(intCount).Selected Then
                strSql = "DELETE FROM tblCalendarParticipants WHERE cpcId = " & lstApplicableEntities.Items(intCount).Value
                ExecuteQuery(strSql)
            End If
        Next

        LoadEntities()
    End Sub

    Protected Sub lstCompanies_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstCompanies.SelectedIndexChanged
        LoadEntities()
    End Sub

    Protected Sub lstBranches_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstBranches.SelectedIndexChanged
        LoadEntities()
    End Sub

    Protected Sub lstDepartments_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstDepartments.SelectedIndexChanged
        LoadEntities()
    End Sub

    Protected Sub lstSections_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstSections.SelectedIndexChanged
        LoadEntities()
    End Sub

    Protected Sub lstParticipants_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstParticipants.SelectedIndexChanged
        LoadEntities()
    End Sub


End Class
