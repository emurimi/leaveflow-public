﻿Imports System.Data.SqlClient
Imports lvManLib.Security
Imports lvManLib.DataFunctions


Partial Class hradmin_publicholidays
    Inherits System.Web.UI.Page

    Public strUserPermissions As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        strUserPermissions = SecureCurrentPage()
        If InStr(strUserPermissions, "A") = 0 Then cmdAddConfig.Enabled = False

        If Not Page.IsPostBack Then
            Dim intCount As Integer

            For intCount = 1 To 31
                lstDay.Items.Add(intCount)
                lstDay.Items(intCount - 1).Value = intCount
            Next

            For intCount = 1 To 12
                lstMonth.Items.Add(MonthName(intCount, True))
                lstMonth.Items(intCount - 1).Value = intCount
            Next

            For intCount = 2007 To Year(Now()) + 1
                lstYear.Items.Add(intCount)
                lstYear.Items(lstYear.Items.Count - 1).Value = intCount
            Next
        End If
    End Sub

    Protected Sub cmdUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
        If Page.IsValid Then

            If cmdUpdate.Text = "Add Holiday" Then
                SqlList.Insert()
            Else
                SqlList.Update()
            End If

            EditMode(False)
        End If
    End Sub


    Sub ClearFields()
        txtName.Text = ""
        txtDescription.Text = ""
        lstApplicableCountries.DataBind()
        lstDay.SelectedIndex = 0
        lstMonth.SelectedIndex = 0
        lstYear.SelectedIndex = lstYear.Items.IndexOf(lstYear.Items.FindByValue(Year(Now())))
        chkRecurring.Checked = False
        trCountries1.Visible = False
        trCountries2.Visible = False
        trCountries3.Visible = False
    End Sub


    Sub GetUpdateForm(ByVal intpbhId As Integer)
        Dim objDR As SqlDataReader
        Dim parameters As New List(Of SqlParameter)()
        Dim pbhId As New SqlParameter("@pbhId", intpbhId)
        parameters.Add(pbhId)

        ClearFields()
        objDR = GetSpDataReader("stpPublicHolidays_View", parameters.ToArray())

        If objDR.Read Then
            txtName.Text = objDR("pbhName") & ""
            txtDescription.Text = objDR("pbhDescription") & ""
            chkRecurring.Checked = objDR("pbhRecurring")
            lstDay.SelectedIndex = lstDay.Items.IndexOf(lstDay.Items.FindByValue(objDR("pbhDay")))
            lstMonth.SelectedIndex = lstMonth.Items.IndexOf(lstMonth.Items.FindByValue(objDR("pbhMonth")))
            lstYear.SelectedIndex = lstYear.Items.IndexOf(lstYear.Items.FindByValue(objDR("pbhYear")))
        End If

        objDR.Close()
        objDR = Nothing

        cmdUpdate.Text = "Update Holiday"
        cmdAddConfig.Text = cmdUpdate.Text
        EditMode(True, False)
        trCountries1.Visible = True
        trCountries2.Visible = True
        trCountries3.Visible = True
        lstCountries.DataBind()
        lstApplicableCountries.DataBind()

    End Sub

    Protected Sub grvList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvList.SelectedIndexChanged
        GetUpdateForm(grvList.SelectedValue)
    End Sub

    Protected Sub cmdAddMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddConfig.Click
        cmdUpdate.Text = "Add Holiday"
        EditMode(True)
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        EditMode(False)
    End Sub

    Protected Sub cmdViewMenus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdViewConfigs.Click
        EditMode(False)
    End Sub

    Sub EditMode(ByVal bolEdit As Boolean, Optional ByVal bolClearFields As Boolean = True)
        EnableDisableFields(True)

        If bolEdit Then
            If bolClearFields Then ClearFields()
            panList.Visible = False
            panAddEdit.Visible = True
            tdAddEdit.Style.Item("background-image") = "url(/images/orangetab.png);"
            tdView.Style.Item("background-image") = "url(/images/darkbluetab.png);"
            cmdDelete.Visible = False

            If cmdUpdate.Text = "Update Holiday" Then
                If InStr(strUserPermissions, "E") = 0 Then EnableDisableFields(False)
                If InStr(strUserPermissions, "D") > 0 Then cmdDelete.Visible = True
            End If
        Else
            cmdAddConfig.Text = "Add Holiday"
            panList.Visible = True
            panAddEdit.Visible = False
            tdAddEdit.Style.Item("background-image") = "url(/images/darkbluetab.png);"
            tdView.Style.Item("background-image") = "url(/images/orangetab.png);"
            grvList.DataBind()
        End If
    End Sub

    Sub EnableDisableFields(ByVal bolEnabled As Boolean)
        lstDay.Enabled = bolEnabled
        lstMonth.Enabled = bolEnabled
        lstYear.Enabled = bolEnabled
        cmdAddCountry.Enabled = bolEnabled
        cmdRemoveCountry.Enabled = bolEnabled
        chkRecurring.Enabled = bolEnabled
        txtName.Enabled = bolEnabled
        txtDescription.Enabled = bolEnabled
        cmdUpdate.Enabled = bolEnabled
    End Sub

    Protected Sub cmdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        Dim parameters As New List(Of SqlParameter)()
        Dim pbhId As New SqlParameter("@pbhId", grvList.SelectedValue)
        parameters.Add(pbhId)
        ExecuteStoredProc("stpPublicHolidays_Delete", parameters.ToArray())
        EditMode(False)
    End Sub

    Protected Sub cmdAddCountry_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddCountry.Click
        Dim intCount As Integer
        Dim strSql As String

        For intCount = 0 To lstCountries.Items.Count - 1
            If lstCountries.Items(intCount).Selected Then
                strSql = "INSERT INTO tblCountryPublicHolidays (cphctyId, cphpbhId) VALUES (" & lstCountries.Items(intCount).Value & ", " & grvList.SelectedValue & ")"
                ExecuteQuery(strSql)
            End If
        Next

        lstCountries.DataBind()
        lstApplicableCountries.DataBind()
    End Sub

    Protected Sub cmdRemoveCountry_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRemoveCountry.Click
        Dim intCount As Integer
        Dim strSql As String

        For intCount = 0 To lstApplicableCountries.Items.Count - 1
            If lstApplicableCountries.Items(intCount).Selected Then
                strSql = "DELETE FROM tblCountryPublicHolidays WHERE cphId = " & lstApplicableCountries.Items(intCount).Value
                ExecuteQuery(strSql)
            End If
        Next

        lstCountries.DataBind()
        lstApplicableCountries.DataBind()
    End Sub
End Class
