﻿Imports lvManLib.Config
Imports lvManLib.Security
Imports lvManLib.DataFunctions
Imports System.Data.SqlClient

Partial Class hradmin_approvalsetup
    Inherits System.Web.UI.Page
    Public strUserPermissions As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        strUserPermissions = SecureCurrentPage()
        If Not Page.IsPostBack Then
            LoadLists()
            ShowDefaultScreen()

            If InStr(strUserPermissions, "A") = 0 Then
                cmdAddApprovalRule.Visible = False
            End If
            If InStr(strUserPermissions, "D") = 0 Then
                cmdDelete.Visible = False
            End If
            
        End If
    End Sub

    Protected Sub cmdCompany_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCompany.Click
        ShowCurrentSetup("C")
    End Sub

    Protected Sub cmdBranch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBranch.Click
        ShowCurrentSetup("B")
    End Sub

    Protected Sub cmdDepartment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDepartment.Click
        ShowCurrentSetup("D")
    End Sub

    Protected Sub cmdSection_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSection.Click
        ShowCurrentSetup("S")
    End Sub

    Protected Sub cmdStaff_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdStaff.Click
        ShowCurrentSetup("U")
    End Sub

    Sub ShowCurrentSetup(ByVal strCategory As String)
        Dim strEnableBranches As String = GetConfig("EnableBranches")
        Dim strEnableSections As String = GetConfig("EnableSections")

        trSelectApprovalLevel.Visible = False
        trSelectApprovalSpecifics.Visible = True
        trAddRules.Visible = False
        trViewRules.Visible = False

        Select Case strCategory
            Case "U" 'Users
                lblSelectText.Text = "SELECT STAFF TO SETUP"
                lblCurrentSetupCategory.Text = "U"
                trCompany.Visible = True
                trBranch.Visible = True
                trDepartment.Visible = True
                trSection.Visible = True
                trPosition.Visible = True
                trStaff.Visible = True
                lstBranches.Items(0).Text = "All branches"
                trGlobal.Visible = False
                chkPropagate.Checked = False
                'Standardizing the system and removing the confusing global option
                trGlobal.Visible = False
                chkPropagate.Checked = False

            Case "S" 'Section
                lblSelectText.Text = "SELECT SECTION TO SETUP"
                lblCurrentSetupCategory.Text = "S"
                trCompany.Visible = True
                trBranch.Visible = True
                trDepartment.Visible = True
                trSection.Visible = True
                trPosition.Visible = True
                trStaff.Visible = False
                lstBranches.Items(0).Text = "All branches"
                trGlobal.Visible = False
                chkPropagate.Checked = True
                'Standardizing the system and removing the confusing global option
                trGlobal.Visible = False
                chkPropagate.Checked = False

            Case "D" 'Dept
                lblSelectText.Text = "SELECT DEPARTMENT TO SETUP"
                lblCurrentSetupCategory.Text = "D"
                trCompany.Visible = True
                trBranch.Visible = True
                trDepartment.Visible = True
                trSection.Visible = False
                trPosition.Visible = True
                trStaff.Visible = False
                lstBranches.Items(0).Text = "All branches"
                trGlobal.Visible = True
                chkPropagate.Checked = False
                'Standardizing the system and removing the confusing global option
                trGlobal.Visible = False
                chkPropagate.Checked = False

            Case "B" 'Branch
                lblSelectText.Text = "SELECT BRANCH TO SETUP"
                lblCurrentSetupCategory.Text = "B"
                trCompany.Visible = True
                trBranch.Visible = True
                trDepartment.Visible = False
                trSection.Visible = False
                trPosition.Visible = True
                trStaff.Visible = False
                trGlobal.Visible = True
                chkPropagate.Checked = False
                'Standardizing the system and removing the confusing global option
                trGlobal.Visible = False
                chkPropagate.Checked = False

            Case "C" 'Company
                lblSelectText.Text = "SELECT COMPANY TO SETUP"
                lblCurrentSetupCategory.Text = "C"
                trCompany.Visible = True
                trBranch.Visible = False
                trDepartment.Visible = False
                trSection.Visible = False
                trPosition.Visible = True
                trStaff.Visible = False
                trGlobal.Visible = False
                chkPropagate.Checked = True
                'Standardizing the system and removing the confusing global option
                trGlobal.Visible = False
                chkPropagate.Checked = False

        End Select


        If strEnableBranches <> "Y" Then
            trBranch.Visible = False
        End If

        If strEnableSections <> "Y" Then
            trSection.Visible = False
        End If

    End Sub

    Sub ShowDefaultScreen()
        Dim strEnableBranches As String = GetConfig("EnableBranches")
        Dim strEnableSections As String = GetConfig("EnableSections")

        If strEnableBranches <> "Y" Then
            cmdBranch.Visible = False
            trBranch.Visible = False
        End If

        If strEnableSections <> "Y" Then
            cmdSection.Visible = False
            trSection.Visible = False
        End If


        trSelectApprovalLevel.Visible = True
        trAddRules.Visible = False
        trViewRules.Visible = False
        trSelectApprovalSpecifics.Visible = False
    End Sub

    Protected Sub cmdSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSelect.Click
        If ValidateSelection() Then
            trSelectApprovalLevel.Visible = False
            trAddRules.Visible = False
            trViewRules.Visible = True
            trSelectApprovalSpecifics.Visible = False
            grvList.DataBind()

            Select Case lblCurrentSetupCategory.Text
                Case "U" 'Staff
                    lblCurrentSetup.Text = "<b>Staff:</b> " & lstUsers.SelectedItem.Text & " (" & lstCompanies.SelectedItem.Text & ")"
                Case "S" 'Section
                    lblCurrentSetup.Text = "<b>Section:</b> " & lstSections.SelectedItem.Text & " (" & lstCompanies.SelectedItem.Text & ", " & lstBranches.SelectedItem.Text & ", " & lstDepartments.SelectedItem.Text & ")"
                Case "D" 'Department
                    lblCurrentSetup.Text = "<b>Dept:</b> " & lstDepartments.SelectedItem.Text & " (" & lstCompanies.SelectedItem.Text & ", " & lstBranches.SelectedItem.Text & ")"
                Case "B" 'Branch
                    lblCurrentSetup.Text = "<b>Branch:</b> " & lstBranches.SelectedItem.Text & " (" & lstCompanies.SelectedItem.Text & ")"
                Case "C" 'Company
                    lblCurrentSetup.Text = "<b>Company:</b> " & lstCompanies.SelectedItem.Text
            End Select
        End If
    End Sub

    Function ValidateSelection() As Boolean
        lblSelectMessage.Text = ""
        Select Case lblCurrentSetupCategory.Text
            Case "U" 'Staff
                If lstUsers.Items.Count > 0 Then
                    If lstUsers.SelectedValue = "0" Then lblSelectMessage.Text = "Please select the staff member to setup"
                Else
                    lblSelectMessage.Text = "No staff available for selection. Please correct this and retry."
                End If

            Case "S" 'Section
                If lstSections.Items.Count > 0 Then
                    If lstSections.SelectedValue = "0" Then lblSelectMessage.Text = "Please select a section to setup"
                Else
                    lblSelectMessage.Text = "No sections available for selection. Please correct this and retry."
                End If

            Case "D" 'Department
                If lstDepartments.Items.Count > 0 Then
                    If lstDepartments.SelectedValue = "0" Then lblSelectMessage.Text = "Please select a department to setup"
                Else
                    lblSelectMessage.Text = "No departments available for selection. Please correct this and retry."
                End If

            Case "B" 'Branch
                If lstBranches.Items.Count > 0 Then
                    If lstBranches.SelectedValue = "0" Then lblSelectMessage.Text = "Please select a branch to setup"
                Else
                    lblSelectMessage.Text = "No branches available for selection. Please correct this and retry."
                End If

            Case "C" 'Company
                If lstCompanies.Items.Count > 0 Then
                    If lstCompanies.SelectedValue = "0" Then lblSelectMessage.Text = "Please select a company to setup"
                Else
                    lblSelectMessage.Text = "No companies available for selection. Please correct this and retry."
                End If
        End Select

        If Len(lblSelectMessage.Text) > 0 Then Return False
        Return True
    End Function

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        ShowDefaultScreen()
    End Sub

    Protected Sub cmdBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBack.Click
        ShowCurrentSetup(lblCurrentSetupCategory.Text)
    End Sub

    Sub LoadLists()
        Dim intCount As Integer
        Dim strTemp As String
        Dim intMaxLevels As Integer

        strTemp = GetConfig("MaxApprovalLevels")
        If Len(strTemp) = 0 Then strTemp = "5"
        intMaxLevels = CType(strTemp, Integer)

        For intCount = 1 To intMaxLevels
            lstApprovalOrder.Items.Add(intCount)
        Next

        lstCompanies.DataBind()
        lstCompanies.Items.Insert(0, New ListItem("Select company", "0"))
        lstCompanies.SelectedIndex = 0

        lstBranches.DataBind()
        lstBranches.Items.Insert(0, New ListItem("Select branch", "0"))
        lstBranches.SelectedIndex = 0

        lstDepartments.DataBind()
        lstDepartments.Items.Insert(0, New ListItem("Select department", "0"))
        lstDepartments.SelectedIndex = 0

        lstSections.DataBind()
        lstSections.Items.Insert(0, New ListItem("Select section", "0"))
        lstSections.SelectedIndex = 0

        lstPositions.DataBind()
        lstPositions.Items.Insert(0, New ListItem("All positions", "0"))
        lstPositions.SelectedIndex = 0

        lstUsers.DataBind()
        lstUsers.Items.Insert(0, New ListItem("Select staff", "0"))
        lstUsers.SelectedIndex = 0
    End Sub

    Protected Sub cmdAddApprovalRule_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddApprovalRule.Click
        trViewRules.Visible = False
        trAddRules.Visible = True
        ClearForm()
    End Sub

    Protected Sub cmdAddApprover_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddApprover.Click
        If Page.IsValid Then
            If cmdAddApprover.Text = "Add Rule" Then
                SqlApprovalSetup.Insert()
            Else
                SqlApprovalSetup.Update()
            End If

            trViewRules.Visible = True
            trAddRules.Visible = False
        End If
    End Sub

    Sub ClearForm()
        cmdAddApprover.Text = "Add Rule"
        chkFinalApprover.Checked = False
        chkPropagate.Checked = False
        If lstApprover.Items.Count > 0 Then lstApprover.SelectedIndex = 0
        lstApprovalOrder.SelectedIndex = 0
        cmdDelete.Visible = False
        cmdAddApprover.Enabled = True
        'lstApprover.SelectedIndex = 0
    End Sub


    Protected Sub grvList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvList.SelectedIndexChanged
        Dim objDR As SqlDataReader
        Dim parameters As New List(Of SqlParameter)()
        Dim apsId As New SqlParameter("@apsId", grvList.SelectedValue)
        parameters.Add(apsId)

        objDR = GetSpDataReader("stpApprovalSetup_SimpleView", parameters.ToArray())

        objDR.Read()
        lstApprovalOrder.SelectedIndex = lstApprovalOrder.Items.IndexOf(lstApprovalOrder.Items.FindByValue(objDR("apsApprovalOrder")))
        lstApprover.DataBind()
        lstApprover.SelectedIndex = lstApprover.Items.IndexOf(lstApprover.Items.FindByValue(objDR("apsApproverUsrId")))
        chkFinalApprover.Checked = objDR("apsFinalApprover")
        chkPropagate.Checked = objDR("apsPropagate")

        objDR.Close()
        objDR = Nothing
        cmdAddApprover.Text = "Update Rule"
        cmdDelete.Visible = True
        If InStr(strUserPermissions, "D") = 0 Then cmdDelete.Visible = False
        If InStr(strUserPermissions, "E") = 0 Then cmdAddApprover.Enabled = False

        trViewRules.Visible = False
        trAddRules.Visible = True
    End Sub

    Protected Sub lstCompanies_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstCompanies.SelectedIndexChanged
        lstBranches.DataBind()
        If lblCurrentSetupCategory.Text = "B" Then
            lstBranches.Items.Insert(0, New ListItem("Select branch", "0"))
        Else
            lstBranches.Items.Insert(0, New ListItem("All branches", "0"))
        End If
        lstBranches.SelectedIndex = 0

        lstDepartments.DataBind()
        lstDepartments.Items.Insert(0, New ListItem("Select department", "0"))
        lstDepartments.SelectedIndex = 0

        lstSections.DataBind()
        lstSections.Items.Insert(0, New ListItem("Select section", "0"))
        lstSections.SelectedIndex = 0

        lstPositions.DataBind()
        lstPositions.Items.Insert(0, New ListItem("All positions", "0"))
        lstPositions.SelectedIndex = 0

        lstUsers.DataBind()
        lstUsers.Items.Insert(0, New ListItem("Select staff", "0"))
        lstUsers.SelectedIndex = 0
    End Sub

    Protected Sub lstDepartments_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstDepartments.SelectedIndexChanged
        lstSections.DataBind()
        lstSections.Items.Insert(0, New ListItem("Select section", "0"))
        lstSections.SelectedIndex = 0

        lstPositions.DataBind()
        lstPositions.Items.Insert(0, New ListItem("All positions", "0"))
        lstPositions.SelectedIndex = 0

        lstUsers.DataBind()
        lstUsers.Items.Insert(0, New ListItem("Select staff", "0"))
        lstUsers.SelectedIndex = 0
    End Sub

    Protected Sub lstSections_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstSections.SelectedIndexChanged
        lstPositions.DataBind()
        lstPositions.Items.Insert(0, New ListItem("All positions", "0"))
        lstPositions.SelectedIndex = 0

        lstUsers.DataBind()
        lstUsers.Items.Insert(0, New ListItem("Select staff", "0"))
        lstUsers.SelectedIndex = 0

    End Sub

    Protected Sub lstPositions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstPositions.SelectedIndexChanged
        lstUsers.DataBind()
        lstUsers.Items.Insert(0, New ListItem("Select staff", "0"))
        lstUsers.SelectedIndex = 0
    End Sub

    Protected Sub lstUsers_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstUsers.SelectedIndexChanged

    End Sub

    Protected Sub lstBranches_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstBranches.SelectedIndexChanged
        lstUsers.DataBind()
        lstUsers.Items.Insert(0, New ListItem("Select staff", "0"))
        lstUsers.SelectedIndex = 0
    End Sub

    Protected Sub cmdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        Dim parameters As New List(Of SqlParameter)()
        Dim apsId As New SqlParameter("@apsId", grvList.SelectedValue)
        parameters.Add(apsId)
        ExecuteStoredProc("stpApprovalSetup_Delete", parameters.ToArray())
        trViewRules.Visible = True
        trAddRules.Visible = False
        grvList.DataBind()
    End Sub

    Protected Sub cmdCancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel2.Click
        trViewRules.Visible = True
        trAddRules.Visible = False
    End Sub
End Class
