﻿<%@ Page Title="LeaveFlow | manual adjustments" Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.hradmin_manualadjustments" Codebehind="manualadjustments.aspx.vb" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">

        <table cellpadding="3" cellspacing="0" width="100%">
            <tr>
                <td>
                    <asp:DropDownList ID="lstSelectCompanies" AutoPostBack="True" runat="server" CssClass="listmenu" 
                        DataSourceID="SqlCompanies" DataTextField="comName" DataValueField="comId">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlCompanies" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                        SelectCommandType="StoredProcedure"
                        SelectCommand="stpCompanyDetails">
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr id="trCommands" runat="server">
                <td>
                    <table width="290" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td id="tdView" runat="server" width="144" height="29" align="center" valign="middle" background='/images/orangetab.png'>
                            <asp:LinkButton ID="cmdViewConfigs" CssClass="whitetext" runat="server">View Adjustments</asp:LinkButton></td>
                        <td width="2"></td>
                        <td id="tdAddEdit" runat="server" width="144" align="center" valign="middle" background='/images/darkbluetab.png'>
                        <asp:LinkButton ID="cmdAddConfig" CssClass="whitetext" runat="server">Add Adjustments</asp:LinkButton>
                        </td>
                      </tr>
                    </table>                    
                </td>
            </tr>        
            <tr>
                <td>
                    <asp:Panel ID="panList" runat="server" Width="100%">
                        <asp:GridView ID="grvList" BorderColor="#EEEEEE" Width="100%" runat="server" 
                            AutoGenerateColumns="False" DataKeyNames="ldaId" DataSourceID="SqlList" 
                            AllowPaging="True" AllowSorting="True" CellPadding="4">
                            <Columns>
                                <asp:BoundField Visible="False" DataField="ldaId" HeaderText="ldaId" InsertVisible="False"  ReadOnly="True" SortExpression="ldaId" />
                                <asp:BoundField DataField="usrFullName" HeaderText="Staff" SortExpression="usrFullName" />
                                <asp:BoundField DataField="ldaDate" HeaderText="Date" DataFormatString="{0:d}" SortExpression="ldaDate" />
                                <asp:BoundField DataField="areName" HeaderText="Reason" SortExpression="areName" />
                                <asp:BoundField DataField="ldaUnits" HeaderText="Units" SortExpression="ldaUnits" />
                                <asp:BoundField DataField="usrEnteredBy" HeaderText="Entered By" SortExpression="usrEnteredBy" />
                                <asp:BoundField DataField="ldaDescription" HeaderText="Description" SortExpression="ldaDescription" />
                            </Columns>
                            <RowStyle CssClass="greytable" />
                            <AlternatingRowStyle CssClass="whitetable" />
                            <PagerStyle CssClass="greytable" />
                            <HeaderStyle HorizontalAlign="Left" ForeColor="#FFFFFF" CssClass="tablecolors_title" />                            
                            <EmptyDataTemplate>
                                There are currently no records to display
                            </EmptyDataTemplate>

                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlList" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                            DeleteCommandType="StoredProcedure"
                            DeleteCommand="stpLeaveDaysAdjustments_Delete" 
                            InsertCommandType="StoredProcedure"
                            InsertCommand="stpLeaveDaysAdjustments_Add" 
                            SelectCommandType="StoredProcedure"
                            SelectCommand="stpLeaveDaysAdjustments_View" 
                            UpdateCommandType="StoredProcedure"
                            UpdateCommand="stpLeaveDaysAdjustments_Update">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="lstSelectCompanies" Name="comId" PropertyName="SelectedValue" Type="Int32" />
                            </SelectParameters>
                            <DeleteParameters>
                                <asp:Parameter Name="ldaId" Type="Int32" />
                            </DeleteParameters>
                            <UpdateParameters>
                                <asp:ControlParameter ControlID="lstUsers" Name="ldausrId" PropertyName="SelectedValue" Type="Int32" />
                                <asp:ControlParameter ControlID="lstReasons" Name="ldaareId" PropertyName="SelectedValue" Type="Int32" />
                                <asp:ControlParameter ControlID="txtDays" Name="ldaUnits" PropertyName="Text" Type="Double" />
                                <asp:ControlParameter ControlID="txtDescription" Name="ldaDescription" PropertyName="Text" Type="String" />
                                <asp:SessionParameter Name="ldaEnteredBy" SessionField="usrId" Type="Int32" />
                                <asp:ControlParameter ControlID="grvList" Name="ldaId" PropertyName="SelectedValue" Type="Int32" />
                            </UpdateParameters>
                            <InsertParameters>
                                <asp:ControlParameter ControlID="lstUsers" Name="ldausrId" PropertyName="SelectedValue" Type="Int32" />
                                <asp:ControlParameter ControlID="lstReasons" Name="ldaareId" PropertyName="SelectedValue" Type="Int32" />
                                <asp:ControlParameter ControlID="txtDays" Name="ldaUnits" PropertyName="Text" Type="Double" />
                                <asp:ControlParameter ControlID="txtDescription" Name="ldaDescription" PropertyName="Text" Type="String" />
                                <asp:SessionParameter Name="ldaEnteredBy" SessionField="usrId" Type="Int32" />
                            </InsertParameters>
                        </asp:SqlDataSource>
                    </asp:Panel>
                    
                    <asp:Panel ID="panAddEdit" runat="server" Width="100%" Visible="False">
                      <table width="400" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          
                          
                          
                        </tr>
                        <tr>
                          
                          <td valign="top">                    
                                <table cellpadding="3" id="tblAddEdit" runat="server" cellspacing="0" width="100%" class="tablewithborders">
                                    <tr>
                                        <td width="50%">
                                            Company:</td>
                                        <td width="50%">
                                            <asp:DropDownList ID="lstCompanies" runat="server" CssClass="listmenu" 
                                                DataSourceID="SqlCompanies" DataTextField="comName" DataValueField="comId" 
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="50%">
                                            Department:</td>
                                        <td width="50%">
                                            <asp:DropDownList ID="lstDepartments" runat="server" CssClass="listmenu" 
                                                DataSourceID="SqlDepartments" DataTextField="dptName" 
                                                DataValueField="dptId" AutoPostBack="True">
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="SqlDepartments" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                SelectCommandType="StoredProcedure"
                                                SelectCommand="stpDepartments_View">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="lstCompanies" Name="dptcomId" PropertyName="SelectedValue" Type="Int32" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="50%">
                                            Staff:</td>
                                        <td width="50%">
                                            <asp:DropDownList ID="lstUsers" runat="server"  
                                                CssClass="listmenu" DataSourceID="SqlUsers" DataTextField="usrFullName" 
                                                DataValueField="usrId">
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="SqlUsers" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                SelectCommandType="StoredProcedure"
                                                SelectCommand="stpUsers_MinView">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="lstDepartments" Name="usrdptId" PropertyName="SelectedValue" Type="Int32" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="50%">
                                            Reason:</td>
                                        <td width="50%">
                                            <asp:DropDownList ID="lstReasons" runat="server" 
                                                CssClass="listmenu" DataSourceID="SqlReason" DataTextField="areName" 
                                                DataValueField="areId">
                                            </asp:DropDownList>
                                            <asp:SqlDataSource ID="SqlReason" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                                SelectCommandType="StoredProcedure"
                                                SelectCommand="stpAdjustmentReasons_View">
                                            </asp:SqlDataSource>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="50%">
                                            Days:</td>
                                        <td width="50%">
                                            <asp:TextBox ID="txtDays" runat="server" CssClass="textbox"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rvaName" runat="server" 
                                                ControlToValidate="txtDays" Display="Dynamic" ErrorMessage="***" 
                                                ValidationGroup="AddItem"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="50%">
                                            Description:</td>
                                        <td width="50%">
                                            <asp:TextBox ID="txtDescription" Columns="30" Rows="5" TextMode="MultiLine" runat="server" CssClass="textbox"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                ControlToValidate="txtDescription" Display="Dynamic" ErrorMessage="***" 
                                                ValidationGroup="AddItem"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>                            
                                    <tr>
                                        <td>
                                            <asp:Button ID="cmdCancel" runat="server" CausesValidation="False" 
                                                CssClass="button" Text="Cancel" />
                                        </td>
                                        <td>
                                            <asp:Button ID="cmdUpdate" runat="server" CssClass="button" 
                                                Text="Add Adjustment" ValidationGroup="AddItem" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                </table>
                              </td>
                              
                            </tr>
                            <tr>
                              
                              
                              
                            </tr>
                          </table>                                 
                    </asp:Panel>    
                    
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
</asp:Content>

