﻿<%@ Page Title="Preloaded Leave" Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.hradmin_preloadedleave" Codebehind="preloadedleave.aspx.vb" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2010" Namespace="RJS.Web.WebControl" TagPrefix="rjs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">


        <table cellpadding="3" cellspacing="0" width="100%">
            <tr id="trCommands" runat="server">
                <td>
                    <table width="290" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td id="tdView" runat="server" width="144" height="29" align="center" valign="middle" background='/images/orangetab.png'>
                            <asp:LinkButton ID="cmdViewPL" CausesValidation="False" CssClass="whitetext" runat="server">View Preloaded</asp:LinkButton></td>
                        <td width="2"></td>
                        <td id="tdAddEdit" runat="server" width="144" align="center" valign="middle" background='/images/darkbluetab.png'>
                        <asp:LinkButton ID="cmdAddPL" CausesValidation="False" CssClass="whitetext" runat="server">Add Preloaded</asp:LinkButton>
                        </td>
                      </tr>
                    </table>                    
                </td>
            </tr>        
            <tr>
                <td>
                    <asp:Panel ID="panList" runat="server" Width="100%">
                        <asp:GridView ID="grvList" BorderColor="#EEEEEE" Width="100%" runat="server" 
                            AutoGenerateColumns="False" DataKeyNames="prlId" DataSourceID="SqlList" 
                            AllowPaging="True" AllowSorting="True" CellPadding="4">
                            <Columns>
                                <asp:BoundField DataField="prlId" HeaderText="prlId" Visible="False" InsertVisible="False" ReadOnly="True" SortExpression="prlId" />
                                <asp:BoundField DataField="prlName" HeaderText="Name" SortExpression="prlName" />
                                <asp:BoundField DataField="prlStartDate" HeaderText="Start Date" DataFormatString="{0:dd MMM yy}" SortExpression="prlStartDate" />
                                <asp:BoundField DataField="prlEndDate" HeaderText="End Date" DataFormatString="{0:dd MMM yy}" SortExpression="prlEndDate" />
                                <asp:CheckBoxField DataField="prlForce" HeaderText="Force" SortExpression="prlForce" />
                                <asp:CheckBoxField DataField="prlExecuted" HeaderText="Executed" SortExpression="prlExecuted" />
                                <asp:CommandField ShowSelectButton="True" SelectText="View/Edit" ItemStyle-Font-Bold="True" />
                            </Columns>
                            <EmptyDataTemplate>
                                There is currently no preloaded leave listed
                            </EmptyDataTemplate>
                            <RowStyle CssClass="greytable" />
                            <AlternatingRowStyle CssClass="whitetable" />
                            <PagerStyle CssClass="greytable" />
                            <HeaderStyle HorizontalAlign="Left" ForeColor="#FFFFFF" CssClass="tablecolors_title" />                            
                        </asp:GridView>
                        <asp:SqlDataSource ID="SqlList" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>"
                            DeleteCommandType="StoredProcedure" 
                            DeleteCommand="stpPreloadedLeave_Delete" 
                            InsertCommandType="StoredProcedure"
                            InsertCommand="stpPreloadedLeave_Add" 
                            SelectCommandType="StoredProcedure"
                            SelectCommand="stpPreloadedLeave_View" 
                            UpdateCommandType="StoredProcedure"
                            UpdateCommand="stpPreloadedLeave_Update">
                            
                            <DeleteParameters>
                                <asp:Parameter Name="prlId" Type="Int32" />
                            </DeleteParameters>
                            <UpdateParameters>
                                <asp:ControlParameter ControlID="lblDaysNum" PropertyName="Text" Name="prlDays" Type="Double" />
                                <asp:ControlParameter ControlID="txtName" PropertyName="Text" Name="prlName" Type="String" />
                                <asp:ControlParameter ControlID="txtDescription" PropertyName="Text" Name="prlDescription" Type="String" />
                                <asp:ControlParameter ControlID="txtStartDate" PropertyName="Text" Name="prlStartDate" Type="DateTime" />
                                <asp:ControlParameter ControlID="txtEndDate" PropertyName="Text" Name="prlEndDate" Type="DateTime" />
                                <asp:ControlParameter ControlID="chkForce" PropertyName="Checked" Name="prlForce" Type="Boolean" />
                                <asp:ControlParameter ControlID="grvList" PropertyName="SelectedValue" Name="prlId" Type="Int32" />
                            </UpdateParameters>
                            <InsertParameters>
                                <asp:ControlParameter ControlID="lblDaysNum" PropertyName="Text" Name="prlDays" Type="Double" />
                                <asp:ControlParameter ControlID="txtName" PropertyName="Text" Name="prlName" Type="String" />
                                <asp:ControlParameter ControlID="txtDescription" PropertyName="Text" Name="prlDescription" Type="String" />
                                <asp:ControlParameter ControlID="txtStartDate" PropertyName="Text" Name="prlStartDate" Type="DateTime" />
                                <asp:ControlParameter ControlID="txtEndDate" PropertyName="Text" Name="prlEndDate" Type="DateTime" />
                                <asp:ControlParameter ControlID="chkForce" PropertyName="Checked" Name="prlForce" Type="Boolean" />
                            </InsertParameters>
                        </asp:SqlDataSource>
                    </asp:Panel>
                    
                    <asp:Panel ID="panAddEdit" runat="server" Width="100%" Visible="False">
                      
                      <table width="400" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          
                          
                          
                        </tr>
                        <tr>
                          
                          <td valign="top">                    
                                NOTE: Newly created preloaded leave will be executed the next day
                                <table cellpadding="3" id="tblAddEdit" runat="server" cellspacing="0" width="100%" class="tablewithborders">
                                    <tr>
                                        <td width="50%">
                                            Name:</td>
                                        <td width="50%">
                                            <asp:TextBox ID="txtName" runat="server" CssClass="textbox"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rvaName" runat="server" 
                                                ControlToValidate="txtName" Display="Dynamic" 
                                                ErrorMessage="* please enter name" ValidationGroup="AddItem"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Description:</td>
                                        <td>
                                            <asp:TextBox ID="txtDescription" runat="server" Rows="4" CssClass="textbox" 
                                                Columns="30" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Start Date:</td>
                                        <td>
                                              <asp:TextBox ID="txtStartDate" ReadOnly="True" runat="server" CssClass="textbox"></asp:TextBox>
                                              <rjs:PopCalendar ID="ppcStartDate" runat="server" Control="txtStartDate" 
                                                  AutoPostBack="False" HolidayMessage="This date is not available" 
                                                  Separator="/" SelectHoliday="False" />
                                              
                                              <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                  ControlToValidate="txtStartDate" ErrorMessage="**"></asp:RequiredFieldValidator>
                                              <rjs:PopCalendarMessageContainer ID="pmcSTartDate" runat="server" Calendar="ppcStartDate" />

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            End Date:</td>
                                        <td>
                                              <asp:TextBox ID="txtEndDate" ReadOnly="True" runat="server" CssClass="textbox"></asp:TextBox>
                                              <rjs:PopCalendar ID="ppcEndDate" runat="server" Control="txtEndDate" 
                                                  AutoPostBack="False" HolidayMessage="This date is not available" 
                                                  Separator="/" ShowWeekend="True" />
                                              
                                              <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                  ControlToValidate="txtEndDate" ErrorMessage="**"></asp:RequiredFieldValidator>
                                              <rjs:PopCalendarMessageContainer ID="pmcEndDate" runat="server" Calendar="ppcEndDate" /> 
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td>Days:</td>
                                        <td><asp:Label ID="lblDays" runat="server"></asp:Label> 
                                        <asp:Label ID="lblDaysNum" Visible="False" runat="server"></asp:Label> 
                                            <asp:LinkButton ID="cmdCalculateDays" runat="server" CausesValidation="False">Calculate Days</asp:LinkButton>
                                        </td>
                                    </tr>                                   
                                    <tr>
                                        <td>
                                            Force (apply even without available leave days):</td>
                                        <td>
                                            <asp:CheckBox ID="chkForce" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Executed:</td>
                                        <td>
                                            <asp:CheckBox ID="chkExecuted" Enabled="False" runat="server" />
                                        </td>
                                    </tr>                                    
                                    <tr id="trApplicableTo1" runat="server">
                                        <td bgcolor="#EEEEEE" style="text-align: center">
                                            Available 
                                            <asp:DropDownList ID="lstApplicableTo" runat="server" AutoPostBack="True" 
                                                CssClass="listmenu">
                                                <asp:ListItem Value="C" Text="Company" />
                                                <asp:ListItem Value="D" Text="Department" />
                                                <asp:ListItem Value="S" Text="Section" />
                                            </asp:DropDownList></td>
                                        <td bgcolor="#EEEEEE" style="text-align: center">
                                            Applicable in:</td>
                                    </tr>
                                    <tr id="trApplicableTo2" runat="server">
                                        <td bgcolor="#EEEEEE" style="text-align: center">
                                            <asp:ListBox ID="lstAvailable" runat="server" CssClass="listmenu">
                                            </asp:ListBox>
                                        </td>
                                        <td bgcolor="#EEEEEE" style="text-align: center">
                                            <asp:ListBox ID="lstApplicable" runat="server" CssClass="listmenu">
                                            </asp:ListBox>
                                        </td>
                                    </tr>
                                    <tr id="trApplicableTo3" runat="server">
                                        <td bgcolor="#EEEEEE" style="text-align: center">
                                            <asp:LinkButton ID="cmdAddApplicable" runat="server" Font-Bold="True">ADD &gt;&gt;&gt;</asp:LinkButton>
                                        </td>
                                        <td bgcolor="#EEEEEE" style="text-align: center">
                                            <asp:LinkButton ID="cmdRemoveApplicable" runat="server" Font-Bold="True">&lt;&lt;&lt; REMOVE</asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="cmdCancel" runat="server" CausesValidation="False" 
                                                CssClass="button" Text="Cancel" />
                                        </td>
                                        <td>
                                            <asp:Button ID="cmdUpdate" runat="server" CssClass="button" 
                                                Text="Add Preloaded Leave" ValidationGroup="AddItem" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="cmdDelete" OnClientClick="return confirm('Are you sure you want to delete this record?');" Font-Bold="True" runat="server">Delete Preloaded Leave</asp:LinkButton>
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                </table>
                              </td>
                              
                            </tr>
                            <tr>
                              
                              
                              
                            </tr>
                          </table>                                      
                    </asp:Panel>    
                    
                </td>
            </tr>
        </table>
 
</asp:Content>

