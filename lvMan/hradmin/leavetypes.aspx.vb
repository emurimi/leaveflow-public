﻿Imports System.Data.SqlClient
Imports lvManLib.Security
Imports lvManLib.DataFunctions
Imports lvManLib.Config

Partial Class hradmin_leavetypes
    Inherits System.Web.UI.Page

    Public strUserPermissions As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        strUserPermissions = SecureCurrentPage()
        If InStr(strUserPermissions, "A") = 0 Then cmdAddConfig.Enabled = False

        If Not Page.IsPostBack Then
            Dim strOrganizationType As String = GetConfig("OrganizationType") & "" '1 = Organization with many companies, 2 = Single company

            If strOrganizationType = "2" Then
                trCompany.Visible = False
                lstSelectCompanies.Visible = False
                ''grvList.Columns(2).Visible = False
            End If

            lstCompanies.DataBind()
            lstCompanies.Items.Insert(0, New ListItem("Spans all companies", "0"))
            lstCompanies.SelectedIndex = 0

            lstSelectCompanies.DataBind()
            lstSelectCompanies.Items.Insert(0, New ListItem("Spans all companies", "0"))
            lstSelectCompanies.SelectedIndex = 0
        End If
    End Sub

    Protected Sub cmdUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
        If Page.IsValid Then
            If cmdUpdate.Text = "Add Leave Type" Then
                SqlList.Insert()
            Else
                SqlList.Update()
            End If
            EditMode(False)
        End If
    End Sub

    Sub ClearFields()
        txtName.Text = ""
        txtDescription.Text = ""
        txtMax.Text = ""
        lstGender.SelectedIndex = 0
        chkCount.Checked = False
        chkCountAfterMax.Checked = False
        chkPaid.Checked = False
    End Sub


    Sub GetUpdateForm(ByVal intltyId As Integer)
        Dim objDR As SqlDataReader
        Dim parameters As New List(Of SqlParameter)()
        Dim ltyId As New SqlParameter("@ltyId", intltyId)
        parameters.Add(ltyId)

        objDR = GetSpDataReader("stpLeaveTypes_View", parameters.ToArray())

        If objDR.Read Then
            txtName.Text = objDR("ltyName") & ""
            txtDescription.Text = objDR("ltyDescription") & ""
            txtMax.Text = objDR("ltyMax") & ""
            lstCompanies.SelectedIndex = lstCompanies.Items.IndexOf(lstCompanies.Items.FindByValue(objDR("ltycomId") & ""))
            lstGender.SelectedIndex = lstGender.Items.IndexOf(lstGender.Items.FindByValue(objDR("ltySex") & ""))
            chkCount.Checked = objDR("ltyCount")
            chkCountAfterMax.Checked = objDR("ltyCountAfterMax")
            chkPaid.Checked = objDR("ltyPaid")
        End If

        objDR.Close()
        objDR = Nothing

        cmdUpdate.Text = "Update Leave Type"
        cmdAddConfig.Text = "Edit Leave Type"
        EditMode(True, False)

    End Sub

    Protected Sub grvList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvList.SelectedIndexChanged
        GetUpdateForm(grvList.SelectedValue)
    End Sub

    Protected Sub cmdAddMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddConfig.Click
        cmdUpdate.Text = "Add Leave Type"
        EditMode(True)
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        EditMode(False)
    End Sub

    Protected Sub cmdViewMenus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdViewConfigs.Click
        EditMode(False)
    End Sub

    Sub EditMode(ByVal bolEdit As Boolean, Optional ByVal bolClearFields As Boolean = True)
        EnableDisableFields(True)

        If bolEdit Then
            If bolClearFields Then ClearFields()
            panList.Visible = False
            panAddEdit.Visible = True
            tdAddEdit.Style.Item("background-image") = "url(/images/orangetab.png);"
            tdView.Style.Item("background-image") = "url(/images/darkbluetab.png);"
            cmdDelete.Visible = False

            If cmdUpdate.Text = "Update Leave Type" Then
                If InStr(strUserPermissions, "E") = 0 Then EnableDisableFields(False)
                If InStr(strUserPermissions, "D") > 0 Then cmdDelete.Visible = True
            End If
        Else
            cmdAddConfig.Text = "Add Leave Type"
            panList.Visible = True
            panAddEdit.Visible = False
            tdAddEdit.Style.Item("background-image") = "url(/images/darkbluetab.png);"
            tdView.Style.Item("background-image") = "url(/images/orangetab.png);"
            grvList.DataBind()
        End If
    End Sub

    Sub EnableDisableFields(ByVal bolEnabled As Boolean)
        txtMax.Enabled = bolEnabled
        lstCompanies.Enabled = bolEnabled
        lstGender.Enabled = bolEnabled
        chkCount.Enabled = bolEnabled
        chkCountAfterMax.Enabled = bolEnabled
        chkPaid.Enabled = bolEnabled
        txtName.Enabled = bolEnabled
        txtDescription.Enabled = bolEnabled
        cmdUpdate.Enabled = bolEnabled
    End Sub

    Protected Sub cmdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        Dim parameters As New List(Of SqlParameter)()
        Dim ltyId As New SqlParameter("@ltyId", grvList.SelectedValue)
        parameters.Add(ltyId)
        ExecuteStoredProc("stpLeaveTypes_Delete", parameters.ToArray())
        EditMode(False)
    End Sub

End Class
