﻿<%@ Page Title="LeaveFlow | approval setup" Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.hradmin_approvalsetup" Codebehind="approvalsetup.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">
    <table cellpadding="4" cellspacing="0" width="100%">
        <tr>
            <td align="center">
            <b>SETUP APPROVAL PROCESS FOR:</b>
            </td>
        </tr>
        <tr id="trSelectApprovalLevel" runat="server">
            <td style="text-align: center">
                <table cellpadding="3" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <asp:Button Width="120" ID="cmdCompany" runat="server" CssClass="button2" Text="COMPANY" />
                            &nbsp;<asp:Button Width="120" ID="cmdBranch" runat="server" CssClass="button2" Text="BRANCH" />
                            &nbsp;<asp:Button Width="120" ID="cmdDepartment" runat="server" CssClass="button2" Text="DEPT" />
                            &nbsp;<asp:Button Width="120" ID="cmdSection" runat="server" CssClass="button2" Text="SECTION" />
                            &nbsp;<asp:Button Width="120" ID="cmdStaff" runat="server" CssClass="button2" Text="SPECIFIC STAFF" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="trSelectApprovalSpecifics" runat="server">
            <td>
            <table width="500" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  
                  
                  
                </tr>
                <tr>
                  
                  <td valign="top">            
                        <table cellpadding="3" cellspacing="0" class="style1">
                            <tr>
                                <td colspan="2" style="text-align: center">
                                    <asp:Label ID="lblSelectText" runat="server" Font-Bold="True" style="text-align: center"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: center">
                                    <asp:Label ID="lblSelectMessage" ForeColor="Red" runat="server" Font-Bold="True" style="text-align: center"></asp:Label>
                                </td>
                            </tr>
                            <tr id="trCompany" runat="server">
                                <td width="50%">
                                    Company:</td>
                                <td width="50%">
                                    <asp:DropDownList ID="lstCompanies" runat="server" CssClass="listmenu" 
                                        DataSourceID="SqlCompanies" DataTextField="comName" DataValueField="comId" 
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlCompanies" runat="server" 
                                        ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                        SelectCommandType="StoredProcedure"
                                        SelectCommand="stpCompanyDetails">

                                    </asp:SqlDataSource>                                
                                </td>
                            </tr>
                            <tr id="trBranch" runat="server">
                                <td>
                                    Branch:</td>
                                <td>
                                    <asp:DropDownList ID="lstBranches" runat="server" AutoPostBack="True" 
                                        CssClass="listmenu" DataSourceID="SqlBranches" DataTextField="braName" 
                                        DataValueField="braId">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlBranches" runat="server" 
                                        ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                        SelectCommandType="StoredProcedure"
                                        SelectCommand="stpBranches_View">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="lstCompanies" Name="bracomId" PropertyName="SelectedValue" Type="Int32" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>                                 
                                </td>
                            </tr>
                            <tr id="trDepartment" runat="server">
                                <td>
                                    Department:</td>
                                <td>
                                    <asp:DropDownList ID="lstDepartments" runat="server" CssClass="listmenu" 
                                        DataSourceID="SqlDepartments" DataTextField="dptName" 
                                        DataValueField="dptId" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlDepartments" runat="server" 
                                        ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                        SelectCommandType="StoredProcedure"
                                        SelectCommand="stpDepartments_View">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="lstCompanies" Name="dptcomId" PropertyName="SelectedValue" Type="Int32" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>                                
                                </td>
                            </tr>
                            <tr id="trSection" runat="server">
                                <td>
                                    Section:</td>
                                <td>
                                    <asp:DropDownList ID="lstSections" runat="server" CssClass="listmenu" 
                                        DataSourceID="SqlSections" DataTextField="secName" DataValueField="secId" 
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlSections" runat="server" 
                                        ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                        SelectCommandType="StoredProcedure"
                                        SelectCommand="stpSections_View">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="lstDepartments" Name="secdptId" PropertyName="SelectedValue" Type="Int32" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>                                
                                </td>
                            </tr>
                            <tr id="trPosition" runat="server">
                                <td>
                                    Position:</td>
                                <td>
                                    <asp:DropDownList ID="lstPositions" runat="server" CssClass="listmenu" 
                                        DataSourceID="SqlPositions" DataTextField="posName" DataValueField="posId" 
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlPositions" runat="server" 
                                        ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                        SelectCommandType="StoredProcedure"
                                        SelectCommand="stpPositions_View">
                                    </asp:SqlDataSource>                                
                                </td>
                            </tr>
                            <tr id="trStaff" runat="server">
                                <td>
                                    Staff:</td>
                                <td>
                                    <asp:DropDownList ID="lstUsers" runat="server" CssClass="listmenu" 
                                        DataSourceID="SqlUsers" DataTextField="usrFullName" DataValueField="usrId" 
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlUsers" runat="server" 
                                        ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                        SelectCommandType="StoredProcedure"
                                        SelectCommand="stpUsers_MinView">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="lstBranches" Name="usrbraId" PropertyName="SelectedValue" Type="Int32" />
                                            <asp:ControlParameter ControlID="lstDepartments" Name="usrdptId" PropertyName="SelectedValue" Type="Int32" />
                                            <asp:ControlParameter ControlID="lstSections" Name="usrsecId" PropertyName="SelectedValue" Type="Int32" />
                                            <asp:ControlParameter ControlID="lstPositions" Name="usrposId" PropertyName="SelectedValue" Type="Int32" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>                                
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Button ID="cmdSelect" runat="server" CssClass="button" Text="SELECT" />&nbsp;
                                    <asp:Button ID="cmdCancel" runat="server" CssClass="button" Text="CANCEL" />
                                </td>
                            </tr>

                        </table>
                      </td>
                      
                    </tr>
                    <tr>
                      
                      
                      
                    </tr>
                  </table>            
            </td>
        </tr>
        <tr id="trViewRules" runat="server">
            <td align="center">
            <b>CURRENT SETUP FOR:</b><br /><br />
            <asp:Label ID="lblCurrentSetup" ForeColor="Red" runat="server"></asp:Label>
            <asp:Label ID="lblCurrentSetupCategory" Visible="False" runat="server"></asp:Label>
                <br /><br />
                <asp:LinkButton ID="cmdBack" runat="server" Font-Bold="True">&lt;&lt; BACK.....</asp:LinkButton>
&nbsp;|
                <asp:LinkButton ID="cmdAddApprovalRule" runat="server" Font-Bold="True">Add Approval Rule</asp:LinkButton>
            <br /><br />
                <asp:GridView ID="grvList" BorderColor="#EEEEEE" Width="600px" runat="server" AllowPaging="True" 
                    AllowSorting="False" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="apsId" 
                    DataSourceID="SqlApprovalSetup">
                    <Columns>
                        <asp:BoundField DataField="apsId" HeaderText="apsId" InsertVisible="False" ReadOnly="True" SortExpression="apsId" Visible="False" />
                        <asp:BoundField DataField="usrFullName" HeaderText="Approver" SortExpression="usrFullName" />
                        <asp:BoundField DataField="apsApprovalOrder" HeaderText="Approval Order" SortExpression="apsApprovalOrder" />
                        <asp:CheckBoxField DataField="apsFinalApprover" HeaderText="Is Final Approver" SortExpression="apsFinalApprover" />
                        <asp:CommandField ShowSelectButton="True" UpdateText="Remove">
                        <ItemStyle Font-Bold="True" />
                        </asp:CommandField>
                    </Columns>
                    <RowStyle CssClass="greytable" />
                    <AlternatingRowStyle CssClass="whitetable" />
                    <PagerStyle CssClass="greytable" />
                    <HeaderStyle HorizontalAlign="Left" CssClass="tablecolors_title" />
                    <EmptyDataTemplate>
                    There are currently no approvers specified for the specified selection(s)
                    </EmptyDataTemplate>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlApprovalSetup" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                    DeleteCommand="stpApprovalSetup_Delete" 
                    DeleteCommandType="StoredProcedure"
                    InsertCommandType="StoredProcedure"
                    InsertCommand="stpApprovalSetup_Add" 
                    SelectCommandType="StoredProcedure"
                    SelectCommand="stpApprovalSetup_View" 
                    UpdateCommandType="StoredProcedure"
                    UpdateCommand="stpApprovalSetup_Update">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="lstCompanies" Name="apscomId" PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="lstPositions" Name="apsposId" PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="lstSections" Name="apssecId" PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="lstDepartments" Name="apsdptId" PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="lstBranches" Name="apsbraId" PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="lstUsers" Name="apsusrId" PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                    <DeleteParameters>
                        <asp:Parameter Name="apsId" Type="Int32" />
                    </DeleteParameters>
                    <UpdateParameters>
                        <asp:ControlParameter ControlID="lstApprover" PropertyName="SelectedValue" Name="apsApproverUsrId" Type="Int32" />
                        <asp:ControlParameter ControlID="lstApprovalOrder" PropertyName="SelectedValue" Name="apsApprovalOrder" Type="Int32" />
                        <asp:ControlParameter ControlID="chkFinalApprover" PropertyName="Checked" Name="apsFinalApprover" Type="Boolean" />
                        <asp:ControlParameter ControlID="grvList" PropertyName="SelectedValue" Name="apsId" Type="Int32" />
                        <asp:ControlParameter ControlID="chkPropagate" PropertyName="Checked" Name="apsPropagate" Type="Boolean" />
                    </UpdateParameters>
                    <InsertParameters>
                        <asp:ControlParameter ControlID="lstCompanies" Name="apscomId" PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="lstBranches" Name="apsbraId" PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="lstApprover" PropertyName="SelectedValue" Name="apsApproverUsrId" Type="Int32" />
                        <asp:ControlParameter ControlID="lstApprovalOrder" PropertyName="SelectedValue" Name="apsApprovalOrder" Type="Int32" />
                        <asp:ControlParameter ControlID="chkFinalApprover" PropertyName="Checked" Name="apsFinalApprover" Type="Boolean" />
                        <asp:ControlParameter ControlID="chkPropagate" PropertyName="Checked" Name="apsPropagate" Type="Boolean" />
                        <asp:ControlParameter ControlID="lstUsers" PropertyName="SelectedValue" Name="apsusrId" Type="Int32" />
                        <asp:ControlParameter ControlID="lstSections" PropertyName="SelectedValue" Name="apssecId" Type="Int32" />
                        <asp:ControlParameter ControlID="lstDepartments" PropertyName="SelectedValue" Name="apsdptId" Type="Int32" />
                        <asp:ControlParameter ControlID="lstPositions" PropertyName="SelectedValue" Name="apsposId" Type="Int32" />
                    </InsertParameters>
                </asp:SqlDataSource>
            
            </td>
        </tr>
        <tr id="trAddRules" runat="server">
            <td>
                <table width="500" border="0" cellspacing="0" cellpadding="0" align="center"> 
                 <tr>
                  
                  
                  
                </tr>
                <tr>
                  
                  <td valign="top">            
                        <table cellpadding="3" cellspacing="0" width="100%">
                            <tr>
                                <td colspan="2">
                                    <b>ADD APPROVAL RULE</b></td>
                            </tr>
                            <tr>
                                <td>
                                    Approver:</td>
                                <td>
                                    <asp:DropDownList ID="lstApprover" runat="server" CssClass="listmenu" 
                                        DataSourceID="SqlApprovers" DataTextField="usrFullName" DataValueField="usrId">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="SqlApprovers" runat="server" 
                                        ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                                        SelectCommandType="StoredProcedure"
                                        SelectCommand="stpUsers_MinView">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="lstCompanies" Name="comId" PropertyName="SelectedValue" Type="Int32" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Approval Order:</td>
                                <td>
                                    <asp:DropDownList ID="lstApprovalOrder" runat="server" CssClass="listmenu">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td width="50%">
                                    Is Final Approver:</td>
                                <td>
                                    <asp:CheckBox ID="chkFinalApprover" runat="server" />
                                </td>
                            </tr>
                            <tr id="trGlobal" runat="server">
                                <td width="50%">
                                    Applies to all:<br />(All staff under this selection will use this approver, regardless of section/ branch)</td>
                                <td>
                                    <asp:CheckBox ID="chkPropagate" runat="server" />
                                </td>
                            </tr>                            
                            <tr>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    <asp:Button ID="cmdAddApprover" runat="server" CssClass="button" Text="add approver" />
                                    &nbsp;<asp:Button ID="cmdCancel2" runat="server" CausesValidation="False" CssClass="button" Text="cancel" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:LinkButton ID="cmdDelete" OnClientClick="return confirm('Are you sure you want to delete this record?');" Font-Bold="True" runat="server">Delete Approver</asp:LinkButton>
                                </td>
                                <td>
                                    &nbsp;</td>
                            </tr>                            
                        </table>
                      </td>
                      
                    </tr>
                    <tr>
                      
                      
                      
                    </tr>
                  </table>                          

            
            </td>
        </tr>        
    </table>

</asp:Content>

