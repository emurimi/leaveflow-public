﻿Imports System.Data.SqlClient
Imports lvManLib.Security
Imports lvManLib.DataFunctions
Imports lvManLib.Leave
Imports lvManLib.Config

Partial Class hradmin_preloadedleave
    Inherits System.Web.UI.Page

    Public strUserPermissions As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        strUserPermissions = SecureCurrentPage()
        If InStr(strUserPermissions, "A") = 0 Then cmdAddPL.Enabled = False

        If Not Page.IsPostBack Then
            'do nothing
            Dim strDateFormat As String = GetConfig("DateFormat") & ""

            Select Case strDateFormat
                Case "UK"
                    ppcStartDate.Format = "dd/mm/yyyy"
                    ppcEndDate.Format = "dd/mm/yyyy"
                Case "UN"
                    ppcStartDate.Format = "yyyy/mm/dd"
                    ppcEndDate.Format = "yyyy/mm/dd"
                Case "US"
                    ppcStartDate.Format = "mm/dd/yyyy"
                    ppcEndDate.Format = "mm/dd/yyyy"
            End Select
        End If
    End Sub

    Protected Sub cmdUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
        If Page.IsValid Then

            lblDaysNum.Text = CountLeaveDays(txtStartDate.Text, txtEndDate.Text, Session("comLeaveUnit") & "", Session("usrId"), "", "", 0)

            If cmdUpdate.Text = "Add Preloaded Leave" Then
                SqlList.Insert()
            Else
                SqlList.Update()
            End If

            EditMode(False)
        End If
    End Sub


    Sub ClearFields()
        txtName.Text = ""
        txtDescription.Text = ""
        txtStartDate.Text = ""
        txtEndDate.Text = ""
        chkForce.Checked = False
        trApplicableTo1.Visible = False
        trApplicableTo2.Visible = False
        trApplicableTo3.Visible = False
        cmdUpdate.Visible = True
    End Sub


    Sub GetUpdateForm(ByVal intprlId As Integer)
        Dim objDR As SqlDataReader
        Dim parameters As New List(Of SqlParameter)()
        Dim prlId As New SqlParameter("@prlId", intprlId)
        parameters.Add(prlId)

        objDR = GetSpDataReader("stpPreloadedLeave_View", parameters.ToArray())

        If objDR.Read Then
            txtName.Text = objDR("prlName") & ""
            txtDescription.Text = objDR("prlDescription") & ""
            chkForce.Checked = objDR("prlForce")
            txtStartDate.Text = objDR("prlStartDate")
            txtEndDate.Text = objDR("prlEndDate")
            lblDays.Text = objDR("prlDays")
            chkExecuted.Checked = objDR("prlExecuted")


            If UCase(Session("comLeaveUnit")) = "H" Then
                lblDays.Text = GetHoursToDays(lblDays.Text, Session("comId"))
                lblDays.Text = Replace(lblDays.Text, "(", "")
                lblDays.Text = Replace(lblDays.Text, ")", "")
            End If
        End If

        objDR.Close()
        objDR = Nothing

        cmdUpdate.Visible = True
        cmdDelete.Visible = True
        cmdUpdate.Text = "Update Preloaded Leave"
        cmdAddPL.Text = "Edit Preloaded"
        EditMode(True, False)
        trApplicableTo1.Visible = True
        trApplicableTo2.Visible = True
        trApplicableTo3.Visible = True
        LoadApplicableTo()

        If chkExecuted.Checked Then
            trApplicableTo1.Visible = False
            trApplicableTo2.Visible = False
            trApplicableTo3.Visible = False
            cmdDelete.Visible = False
            cmdUpdate.Visible = False
        End If

    End Sub

    Protected Sub grvList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvList.SelectedIndexChanged
        GetUpdateForm(grvList.SelectedValue)
    End Sub

    Protected Sub cmdAddMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddPL.Click
        cmdUpdate.Text = "Add Preloaded Leave"
        EditMode(True)
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        EditMode(False)
    End Sub

    Protected Sub cmdViewMenus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdViewPL.Click
        EditMode(False)
    End Sub

    Sub EditMode(ByVal bolEdit As Boolean, Optional ByVal bolClearFields As Boolean = True)
        EnableDisableFields(True)

        If bolEdit Then
            If bolClearFields Then ClearFields()
            panList.Visible = False
            panAddEdit.Visible = True
            tdAddEdit.Style.Item("background-image") = "url(/images/orangetab.png);"
            tdView.Style.Item("background-image") = "url(/images/darkbluetab.png);"
            cmdDelete.Visible = False

            If cmdUpdate.Text = "Update Preloaded Leave" Then
                If InStr(strUserPermissions, "E") = 0 Then EnableDisableFields(False)
                If InStr(strUserPermissions, "D") > 0 Then cmdDelete.Visible = True
            End If
        Else
            cmdAddPL.Text = "Add Preloaded"
            panList.Visible = True
            panAddEdit.Visible = False
            tdAddEdit.Style.Item("background-image") = "url(/images/darkbluetab.png);"
            tdView.Style.Item("background-image") = "url(/images/orangetab.png);"
            grvList.DataBind()
        End If
    End Sub

    Sub EnableDisableFields(ByVal bolEnabled As Boolean)
        txtStartDate.Enabled = bolEnabled
        txtEndDate.Enabled = bolEnabled
        cmdAddApplicable.Enabled = bolEnabled
        cmdRemoveApplicable.Enabled = bolEnabled
        chkForce.Enabled = bolEnabled
        txtName.Enabled = bolEnabled
        txtDescription.Enabled = bolEnabled
        cmdUpdate.Enabled = bolEnabled
    End Sub

    Protected Sub cmdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        Dim parameters As New List(Of SqlParameter)()
        Dim prlId As New SqlParameter("@prlId", grvList.SelectedValue)
        parameters.Add(prlId)
        ExecuteStoredProc("stpPreloadedLeave_Delete", parameters.ToArray())
        EditMode(False)
    End Sub


    Protected Sub cmdCalculateDays_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCalculateDays.Click
        If Len(txtStartDate.Text) > 0 And Len(txtEndDate.Text) > 0 Then
            lblDays.Text = CountLeaveDays(txtStartDate.Text, txtEndDate.Text, Session("comLeaveUnit") & "", Session("usrId"), "", "", 0)
            If UCase(Session("comLeaveUnit")) = "H" Then
                lblDays.Text = GetHoursToDays(lblDays.Text, Session("comId"))
                lblDays.Text = Replace(lblDays.Text, "(", "")
                lblDays.Text = Replace(lblDays.Text, ")", "")
            End If
        Else
            lblDays.Text = "Please specify start and end date"
        End If

    End Sub

    Protected Sub cmdAddApplicable_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddApplicable.Click
        Dim strSql As String
        Dim intCount As Integer = 0

        For intCount = 0 To lstAvailable.Items.Count - 1
            If lstAvailable.Items(intCount).Selected Then
                strSql = "INSERT INTO tblPreloadedLeaveApplicableto (plaprlId, plaEntity, plaEntityID) VALUES (" & grvList.SelectedValue & ", '" & lstApplicableTo.SelectedValue & "', " & lstAvailable.Items(intCount).Value & ")"
                ExecuteQuery(strSql)
            End If
        Next
        LoadApplicableTo()
    End Sub

    Protected Sub cmdRemoveApplicable_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRemoveApplicable.Click
        Dim strSql As String
        Dim intCount As Integer = 0

        For intCount = 0 To lstApplicable.Items.Count - 1
            If lstApplicable.Items(intCount).Selected Then
                strSql = "DELETE FROM tblPreloadedLeaveApplicableto WHERE plaId = " & lstApplicable.Items(intCount).Value
                ExecuteQuery(strSql)
            End If
        Next
        LoadApplicableTo()
    End Sub

    Protected Sub lstApplicableTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstApplicableTo.SelectedIndexChanged
        LoadApplicableTo()
    End Sub

    Sub LoadApplicableTo()
        Dim objDR As SqlDataReader
        Dim intCount As Integer = 0
        Dim parameters As New List(Of SqlParameter)()

        Dim plaprlId As New SqlParameter("@plaprlId", grvList.SelectedValue)
        parameters.Add(plaprlId)
        Dim plaEntity As New SqlParameter("@plaEntity", lstApplicableTo.SelectedValue)
        parameters.Add(plaEntity)
        Dim Category As New SqlParameter("@Category", "AVAIL")
        parameters.Add(Category)

        objDR = GetSpDataReader("stpPreloadedLeave_ApplyLists", parameters.ToArray())

        lstAvailable.Items.Clear()

        While objDR.Read()
            lstAvailable.Items.Add(objDR("appName"))
            lstAvailable.Items(intCount).Value = objDR("appId")
            intCount += 1
        End While

        objDR.Close()

        parameters.Clear()
        Dim plaprlId1 As New SqlParameter("@plaprlId", grvList.SelectedValue)
        parameters.Add(plaprlId1)
        Dim plaEntity1 As New SqlParameter("@plaEntity", lstApplicableTo.SelectedValue)
        parameters.Add(plaEntity1)
        Dim Category1 As New SqlParameter("@Category", "APPLI")
        parameters.Add(Category1)


        objDR = GetSpDataReader("stpPreloadedLeave_ApplyLists", parameters.ToArray())
        intCount = 0

        lstApplicable.Items.Clear()
        While objDR.Read()
            lstApplicable.Items.Add(objDR("appName"))
            lstApplicable.Items(intCount).Value = objDR("plaId")
            intCount += 1
        End While

        objDR = Nothing
    End Sub
End Class
