﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="lvMan.uploadrelateddocs" Codebehind="uploadrelateddocs.aspx.vb" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Upload Related Document(s)</title>
    <link href="/css/ndio.css" rel="stylesheet" type="text/css">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
    <script type = "text/javascript">
    function uploadComplete(sender) {
        $get("<%= lblMessage.ClientID %>").innerHTML = "File Uploaded Successfully";
        window.location.href = "uploadrelateddocs.aspx?ent=<%= lblEntity.Text %>&trn=<%= lblTransactionID.Text %>";
    }
 
    function uploadError(sender) {
        $get("<%= lblMessage.ClientID %>").innerHTML = "File upload failed.";
    }
</script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table cellpadding="5" cellspacing="0" class="style1">
        <tr>
            <td colspan="2">
                <asp:Label ID="lblMessage" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                
                <asp:Label ID="lblTitle" Visible="False" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                <asp:Label ID="lblFileName" Visible="False" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                <asp:Label ID="lblTransactionID" Visible="False" runat="server"></asp:Label>
                <asp:Label ID="lblEntityID" Visible="False" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                <asp:Label ID="lblEntity" Visible="False" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <cc1:AsyncFileUpload OnClientUploadError="uploadError"
                        OnClientUploadComplete="uploadComplete" runat="server"
                        ID="AsyncFileUpload1" Width="300px" UploaderStyle="Modern"
                        CompleteBackColor = "White"
                        UploadingBackColor="#CCFFFF"  ThrobberID="imgLoader"
                        OnUploadedComplete = "FileUploadComplete"
                      />         
                <asp:Image ID="imgLoader" runat="server"
                        ImageUrl = "~/images/progress-wheel.gif" />                      
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="grvList" GridLines="None" Width="100%" runat="server" AutoGenerateColumns="False" 
                    DataKeyNames="ldcId" DataSourceID="SqlList">
                    <Columns>
                        <asp:BoundField HeaderStyle-HorizontalAlign="Left" DataField="ldcId" HeaderText="ldcId" InsertVisible="False" ReadOnly="True" SortExpression="ldcId" Visible="False" />
                        <asp:BoundField HeaderStyle-HorizontalAlign="Left" DataField="ldcTitle" HeaderText="Title" SortExpression="ldcTitle" />
                        <asp:CommandField EditText="Edit Title" ShowDeleteButton="True" ShowEditButton="True">
                            <ItemStyle Font-Bold="True" />
                        </asp:CommandField>
                    </Columns>
                    <EmptyDataTemplate>
                    Please upload the files to be attached to this request
                    </EmptyDataTemplate>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlList" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                    DeleteCommandType="StoredProcedure"
                    SelectCommandType="StoredProcedure"
                    UpdateCommandType="StoredProcedure"
                    DeleteCommand="stpLeaveDocuments_Delete" 
                    SelectCommand="stpLeaveDocuments_View" 
                    UpdateCommand="stpLeaveDocuments_Update"
                    InsertCommand="stpLeaveDocuments_Add">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="lblTransactionID" Name="ldcTransactionID" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="lblEntity" Name="ldcEntity" PropertyName="Text" Type="String" />
                    </SelectParameters>
                    <DeleteParameters>
                        <asp:Parameter Name="ldcId" Type="Int32" />
                    </DeleteParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="ldcTitle" Type="String" />
                        <asp:Parameter Name="ldcId" Type="Int32" />
                    </UpdateParameters>
                    <InsertParameters>
                        <asp:ControlParameter ControlID="lblTransactionID" PropertyName="Text" Name="ldcTransactionID" Type="String" />
                        <asp:ControlParameter ControlID="lblEntity" PropertyName="Text" Name="ldcEntity" Type="String" />
                        <asp:ControlParameter ControlID="lblEntityID" PropertyName="Text" Name="ldcEntityID" Type="Int32" />
                        <asp:ControlParameter ControlID="lblTitle" PropertyName="Text" Name="ldcTitle" Type="String" />
                        <asp:ControlParameter ControlID="lblFileName" PropertyName="Text" Name="ldcPath" Type="String" />
                    </InsertParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <a href="javascript:window.close()">CLOSE</a>
            </td>
        </tr>
    </table>

    </form>
</body>
</html>
