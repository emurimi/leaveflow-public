﻿<%@ Page Title="LeaveFlow | leave plans" Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.reports_leaveplans" Codebehind="leaveplans.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">
    <table cellpadding="3" cellspacing="0" width="100%">
        <tr>
            <td class="bigtext">
            <asp:Label ID="lblTitle" Text="Leave Plans" runat="server"></asp:Label>
            </td>
        </tr>  
        <tr>
            <td>
                <asp:GridView ID="grvList" Width="100%" CellPadding="4" BorderColor="#EEEEEE" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="SqlList">
                    <Columns>
                        <asp:BoundField DataField="comName" HeaderText="Company" SortExpression="comName" />
                        <asp:BoundField DataField="dptName" HeaderText="Department" SortExpression="dptName" />
                        <asp:BoundField DataField="secName" HeaderText="Section" SortExpression="secName" />
                        <asp:BoundField DataField="usrFullName" HeaderText="Name" SortExpression="usrFullName" />
                        <asp:BoundField DataField="usrStaffID" HeaderText="Staff ID" SortExpression="usrStaffID" />
                        <asp:BoundField DataField="lepStartDate" DataFormatString="{0:d}" HeaderText="Start Date" SortExpression="lepStartDate" />
                        <asp:BoundField DataField="lepStartTime" HeaderText="Time" SortExpression="lepStartTime" />
                        <asp:BoundField DataField="lepEndDate" DataFormatString="{0:d}" HeaderText="End Date" SortExpression="lepEndDate" />
                        <asp:BoundField DataField="lepEndTime" HeaderText="Time" SortExpression="lepEndTime" />
                        <asp:BoundField DataField="ltyName" HeaderText="Leave Type" SortExpression="ltyName" />
                    </Columns>
                    <RowStyle CssClass="greytable" />
                    <AlternatingRowStyle CssClass="whitetable" />
                    <PagerStyle CssClass="greytable" />
                    <HeaderStyle HorizontalAlign="Left" ForeColor="#FFFFFF" CssClass="tablecolors_title" />                            
                    <EmptyDataTemplate>
                    There are no records to show in this report
                    </EmptyDataTemplate>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlList" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                    SelectCommandType="StoredProcedure"
                    SelectCommand="stpLeavePlans_Report">
                    <SelectParameters>
                        <asp:QueryStringParameter QueryStringField="comId" Name="comId" DefaultValue="0" Type="Int32" />
                        <asp:QueryStringParameter QueryStringField="dptId" Name="dptId" DefaultValue="0" Type="Int32" />
                        <asp:QueryStringParameter QueryStringField="secId" Name="secId" DefaultValue="0" Type="Int32" />
                        <asp:QueryStringParameter QueryStringField="usrId" Name="usrId" DefaultValue="0" Type="Int32" />
                        <asp:QueryStringParameter QueryStringField="ltyId" Name="ltyId" DefaultValue="0" Type="Int32" />
                        <asp:QueryStringParameter QueryStringField="sdt" Name="lepStartDate1" Type="DateTime" />
                        <asp:QueryStringParameter QueryStringField="edt" Name="lepStartDate2" Type="DateTime" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td> 
        </tr> 
     </table> 

</asp:Content>

