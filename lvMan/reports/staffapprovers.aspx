﻿<%@ Page Title="staff with approvers | LeaveFlow" Language="VB" MasterPageFile="~/InnerMasterPage.master" AutoEventWireup="false" Inherits="lvMan.reports_staffapprovers" Codebehind="staffapprovers.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphInnerHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphInnerContent" Runat="Server">
    <table cellpadding="3" cellspacing="0" width="100%">
        <tr>
            <td class="bigtext">
            <asp:Label ID="lblTitle" Text="Staff approvers" runat="server"></asp:Label>
            </td>
        </tr>  
        <tr>
            <td>
                <asp:GridView ID="grvList" Width="100%" CellPadding="4" BorderColor="#EEEEEE" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="SqlList">
                    <Columns>
                        <asp:TemplateField HeaderText="Approver">
                            <ItemTemplate>
                                <asp:Label ID="lblApprover" runat="server" Text='<%# GetUser(DataBinder.Eval(Container.DataItem, "apsApproverUsrId")) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="apsApprovalOrder" HeaderText="Approval Order" SortExpression="apsApprovalOrder" />
                        <asp:BoundField DataField="apsFinalApprover" HeaderText="Is Final Approver" SortExpression="apsFinalApprover" />
                    </Columns>
                    <RowStyle CssClass="greytable" />
                    <AlternatingRowStyle CssClass="whitetable" />
                    <PagerStyle CssClass="greytable" />
                    <HeaderStyle HorizontalAlign="Left" ForeColor="#FFFFFF" CssClass="tablecolors_title" />                            
                    <EmptyDataTemplate>
                        There are no records to show in this report
                    </EmptyDataTemplate>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlList" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:smartlvConnectionString %>" 
                    SelectCommand="SELECT [apsApprovalOrder], [apsFinalApprover], [apsApprovalOrder] FROM [tblApprovalSetup] WHERE 1=0">
                </asp:SqlDataSource>
            </td> 
        </tr> 
     </table> 

</asp:Content>

